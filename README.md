# Setup

You need the following software:

- mongodb
- redis-server
- postgresql

I would recommend using homebrew on mac to install all of them. I use RVM to manage my own ruby versions but you can use whatever you want.

Create a `.env` file in the shogun-web directory and add this:

```
PORT=3000
RACK_ENV=development
PUSHER_APP_ID=118212
PUSHER_KEY=2ece0e997983a18ad35d
PUSHER_SECRET=4d49186c3756c2a85164
SHOGUN_EDITOR_URL=https://localhost.ssl:3000
SHOGUN_PREVIEW_URL=https://localhost.ssl:3000
UPLOADCARE_PUBLIC_KEY=19c5458fd6d891ef489e
DEFAULT_HOST=localhost.ssl:3000
SHOGUN_URL=https://localhost.ssl:3000
SHOPIFY_API_KEY=babf582bd982bf0564cfea7f3daee982
SHOPIFY_SECRET=a618d9e4e6739c033504eb6c5d37299d
ASSET_HOST=localhost.ssl:3000
PASSENGER_MAX_POOL_SIZE=1
PASSENGER_MIN_INSTANCES=1
```

Follow this guide to setup a local ssl certificate: https://gist.github.com/trcarden/3295935

Make sure the ssl `server.crt` and `server.key` are at `~/.ssl/server.crt` and `~/.ssl/server.key`

Create a postgres database called `shogun_web_development`.

```
bundle install
bundle exec rake db:reset
foreman run ssl
```

You should now have Shogun running at `https://localhost.ssl:3000` and you can access your local Shogun by going through Shopify admin at `https://shogun-test-store.myshopify.com/admin`.

When you first try and connect to `localhost.ssl` via https, you may need to download the certificate and add it to your system's trusted certificates. You can do this by clicking the lock icon and then saving the certificate to your computer and double clicking it.
