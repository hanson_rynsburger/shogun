class RemoveSiteIdFromComponents < ActiveRecord::Migration
  def change
    remove_column :components, :site_id
  end
end
