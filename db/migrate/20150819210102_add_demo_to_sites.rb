class AddDemoToSites < ActiveRecord::Migration
  def change
    add_column :sites, :demo, :boolean
  end
end
