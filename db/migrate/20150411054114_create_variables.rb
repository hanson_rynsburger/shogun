class CreateVariables < ActiveRecord::Migration
  def change
    create_table :variables, id: :uuid do |t|
      t.string :name
      t.uuid :template_id
      t.string :type
      t.timestamps
    end
    add_index :variables, :template_id
  end
end
