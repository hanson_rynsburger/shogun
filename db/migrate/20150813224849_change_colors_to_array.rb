class ChangeColorsToArray < ActiveRecord::Migration
  def change
    remove_column :sites, :colors
    add_column :sites, :colors, :text, array: true, default: []
  end

  def down
    raise ActiveRecord::IrreversibleMigration.new
  end
end
