class CreateInvites < ActiveRecord::Migration
  def change
    create_table :invites, id: :uuid do |t|
      t.string :email
      t.uuid :user_id
      t.uuid :site_id
      t.datetime :accepted_at
      t.timestamps
    end

    add_index :invites, :site_id
  end
end
