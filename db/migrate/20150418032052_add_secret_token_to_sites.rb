class AddSecretTokenToSites < ActiveRecord::Migration
  def change
    add_column :sites, :secret_token, :string
  end
end
