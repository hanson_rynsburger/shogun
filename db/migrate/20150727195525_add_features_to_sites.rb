class AddFeaturesToSites < ActiveRecord::Migration
  def change
    add_column :sites, :features, :text
  end
end
