class AddJavascriptToShopifyShops < ActiveRecord::Migration
  def change
    add_column :shopify_shops, :javascript, :boolean, default: false
  end
end
