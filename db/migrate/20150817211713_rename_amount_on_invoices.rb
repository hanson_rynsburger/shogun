class RenameAmountOnInvoices < ActiveRecord::Migration
  def change
    rename_column :invoices, :amount, :_amount
  end
end
