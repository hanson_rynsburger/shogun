class AddDeadToShopifyShops < ActiveRecord::Migration
  def change
    add_column :shopify_shops, :dead, :boolean, default: false
  end
end
