class AddRootIdToPages < ActiveRecord::Migration
  def change
    add_column :pages, :root_id, :uuid
  end
end
