class AddTypeToSites < ActiveRecord::Migration
  def up
    add_column :sites, :type, :string
    Site.where("id IN (SELECT site_id FROM shopify_shops)").update_all(type: "ShopifySite")
    Site.where(type: nil).update_all(type: "ShogunSite")
    add_index :sites, :type
  end

  def down
    remove_index :sites, :type
    remove_column :sites, :type
  end
end
