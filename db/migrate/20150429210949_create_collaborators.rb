class CreateCollaborators < ActiveRecord::Migration
  def up
    create_table :collaborators, id: :uuid do |t|
      t.uuid :site_id
      t.uuid :user_id
      t.boolean :admin
      t.timestamps
    end

    add_index :collaborators, [:site_id, :user_id], unique: true

    Site.all.each do |site|
      site.users.each do |u|
        site.collaborators.create user_id: u.id
      end
    end

    drop_table :sites_users
  end

  def down
    create_table :sites_users, id: :uuid do |t|
      t.uuid :site_id
      t.uuid :user_id
      t.timestamps
    end
    add_index :sites_users, [:site_id, :user_id], unique: true

    Site.all.each do |s|
      s.collaborators.each do |c|
        s.users << c.user
      end
    end

    drop_table :collaborators
  end
end
