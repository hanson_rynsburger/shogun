class AddScrapeToShopifyShops < ActiveRecord::Migration
  def change
    add_column :shopify_shops, :scrape, :boolean, default: true
  end
end
