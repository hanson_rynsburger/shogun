class AddOutdatedAtToPages < ActiveRecord::Migration
  def change
    add_column :pages, :outdated_at, :datetime
  end
end
