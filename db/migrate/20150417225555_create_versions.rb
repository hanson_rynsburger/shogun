class CreateVersions < ActiveRecord::Migration
  def change
    create_table :versions, id: :uuid do |t|
      t.text :html
      t.text :css
      t.text :js
      t.uuid :page_id
      t.timestamps
    end

    add_index :versions, :page_id
  end
end
