class AddUpdatingToPages < ActiveRecord::Migration
  def change
    add_column :pages, :updating, :boolean
  end
end
