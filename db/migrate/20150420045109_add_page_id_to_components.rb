class AddPageIdToComponents < ActiveRecord::Migration
  def change
    add_column :components, :page_id, :uuid
    add_index :components, :page_id
  end
end
