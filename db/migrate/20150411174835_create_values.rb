class CreateValues < ActiveRecord::Migration
  def change
    create_table :values, id: :uuid do |t|
      t.text :value, array: true, default: []
      t.boolean :multi, default: false
      t.uuid :variable_id
      t.uuid :component_id
      t.timestamps
    end

    add_index :values, [:component_id, :variable_id], unique: true
  end
end
