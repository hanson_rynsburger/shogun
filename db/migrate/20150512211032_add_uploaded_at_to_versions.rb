class AddUploadedAtToVersions < ActiveRecord::Migration
  def change
    add_column :versions, :uploaded_at, :datetime
  end
end
