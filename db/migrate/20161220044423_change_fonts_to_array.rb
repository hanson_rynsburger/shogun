class ChangeFontsToArray < ActiveRecord::Migration
  def change
    remove_column :sites, :fonts, :text
    add_column :sites, :fonts, :text, array: true, default: []
  end
end
