class CreatePlans < ActiveRecord::Migration
  def up
    create_table :plans, id: :uuid do |t|
      t.uuid :site_id
      t.text :internal_notes
      t.integer :price
      t.integer :max_users
      t.integer :price_per_user
      t.integer :max_pages
      t.integer :price_per_page
      t.datetime :activated_at
      t.datetime :deactivated_at
      t.timestamps
    end
    add_index :plans, :site_id
    rename_column :sites, :plan, :old_plan
  end

  def down
    rename_column :sites, :old_plan, :plan
    drop_table :plans
  end
end
