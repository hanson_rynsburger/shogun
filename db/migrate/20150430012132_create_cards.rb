class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards, id: :uuid do |t|
      t.uuid :site_id
      t.string :stripe_customer_id
      t.timestamps
    end

    add_index :cards, :site_id
  end
end
