class MakeVariablePropertiesSerializable < ActiveRecord::Migration
  def change
    remove_column :variables, :properties
    add_column :variables, :properties, :text
  end
end
