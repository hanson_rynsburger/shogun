class CreateReferences < ActiveRecord::Migration
  def change
    create_table :references, id: :uuid do |t|
      t.integer :position
      t.uuid :value_id
      t.uuid :component_id
      t.timestamps
    end

    add_index :references, :value_id
  end
end
