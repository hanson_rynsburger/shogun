class AddDeletedAtToTemplates < ActiveRecord::Migration
  def change
    add_column :templates, :deleted_at, :datetime
  end
end
