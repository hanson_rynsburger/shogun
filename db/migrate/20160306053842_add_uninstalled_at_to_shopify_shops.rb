class AddUninstalledAtToShopifyShops < ActiveRecord::Migration
  def change
    add_column :shopify_shops, :uninstalled_at, :datetime
  end
end
