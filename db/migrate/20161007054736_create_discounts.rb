class CreateDiscounts < ActiveRecord::Migration
  def change
    create_table :discounts do |t|
      t.integer :amount
      t.uuid :site_id, null: false
      t.string :plan_name
      t.datetime :used_at
      t.datetime :expires_at
      t.timestamps
    end
    add_index :discounts, :site_id
  end
end
