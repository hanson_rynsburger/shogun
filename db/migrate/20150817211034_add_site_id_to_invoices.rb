class AddSiteIdToInvoices < ActiveRecord::Migration
  def up
    add_column :invoices, :site_id, :uuid
    add_index :invoices, :site_id
    Invoice.includes(:plan).all.each do |i|
      i.update_column :site_id, i.plan.site_id
    end
  end

  def down
    remove_column :invoices, :site_id
  end
end
