class AddPaidAtToInvoices < ActiveRecord::Migration
  def up
    add_column :invoices, :paid_at, :datetime
    Invoice.all.each do |i|
      i.update_column :paid_at, i.created_at
    end
  end

  def down
    remove_column :invoices, :paid_at
  end
end
