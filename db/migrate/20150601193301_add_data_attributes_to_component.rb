class AddDataAttributesToComponent < ActiveRecord::Migration
  def change
    add_column :components, :data_attributes, :hstore, default: {}
  end
end
