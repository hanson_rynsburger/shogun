class AddDigestToSites < ActiveRecord::Migration
  def change
    add_column :sites, :digest, :string
  end
end
