class AddCardIdToInvoices < ActiveRecord::Migration
  def up
    add_column :invoices, :card_id, :uuid
    add_index :invoices, :card_id
    Invoice.includes(plan: :site).all.each do |i|
      i.update_column :card_id, i.plan.site.default_card_id
    end
  end

  def down
    remove_column :invoices, :card_id
  end
end
