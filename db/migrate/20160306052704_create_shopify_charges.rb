class CreateShopifyCharges < ActiveRecord::Migration
  def change
    create_table :shopify_charges, id: :uuid do |t|
      t.uuid :shopify_shop_id, null: false
      t.datetime :activated_at
      t.datetime :cancelled_at
      t.datetime :trial_ends_at
      t.datetime :billing_at
      t.integer :shopify_charge_id, null: false
      t.string :name
      t.integer :price
      t.string :status
      t.timestamps
    end

    add_index :shopify_charges, :shopify_charge_id, unique: true
    add_index :shopify_charges, :shopify_shop_id
  end
end
