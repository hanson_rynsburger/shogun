class AddIndexToType < ActiveRecord::Migration
  def change
    add_index :templates, :type
  end
end
