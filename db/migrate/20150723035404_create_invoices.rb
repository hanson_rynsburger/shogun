class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices, id: :uuid do |t|
      t.uuid :plan_id
      t.string :stripe_charge_id
      t.integer :amount
      t.timestamps
    end

    add_index :invoices, :plan_id
  end
end
