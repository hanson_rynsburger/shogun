class ChangeValueToSerialized < ActiveRecord::Migration
  def change
    rename_column :values, :value, :old_value
    add_column :values, :value, :text
    # Value.all.each do |value|
    #   value.value = value.old_value
    #   value.save!
    # end
    remove_column :values, :old_value
  end
end
