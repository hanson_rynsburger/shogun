class AddDefaultCardIdToSite < ActiveRecord::Migration
  def change
    add_column :sites, :default_card_id, :uuid
  end
end
