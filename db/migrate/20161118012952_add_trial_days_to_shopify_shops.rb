class AddTrialDaysToShopifyShops < ActiveRecord::Migration
  def change
    add_column :shopify_shops, :trial_days, :integer, default: 7
  end
end
