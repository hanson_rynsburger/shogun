class AddTrialStartedAtToShopifyShops < ActiveRecord::Migration
  def change
    add_column :shopify_shops, :trialled_at, :datetime
  end
end
