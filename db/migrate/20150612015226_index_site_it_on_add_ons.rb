class IndexSiteItOnAddOns < ActiveRecord::Migration
  def change
    add_index :add_ons, :site_id
  end
end
