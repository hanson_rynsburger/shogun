class CreateShopifyShops < ActiveRecord::Migration
  def change
    create_table :shopify_shops, id: :uuid do |t|
      t.string :domain
      t.string :token
      t.uuid :site_id
      t.timestamps
    end

    add_index :shopify_shops, :domain, unique: true
    add_index :shopify_shops, :site_id, unique: true
  end
end
