class AddHerokuToUser < ActiveRecord::Migration
  def change
    add_column :users, :heroku, :boolean, default: false
  end
end
