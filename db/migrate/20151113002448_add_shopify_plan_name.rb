class AddShopifyPlanName < ActiveRecord::Migration
  def change
    add_column :shopify_shops, :shopify_plan_name, :string
  end
end
