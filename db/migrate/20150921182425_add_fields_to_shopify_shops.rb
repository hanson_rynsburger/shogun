class AddFieldsToShopifyShops < ActiveRecord::Migration
  def change
    add_column :shopify_shops, :email, :string
    add_column :shopify_shops, :shop_owner, :string
  end
end
