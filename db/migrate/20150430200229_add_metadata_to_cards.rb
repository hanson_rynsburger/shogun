class AddMetadataToCards < ActiveRecord::Migration
  def change
    add_column :cards, :brand, :string
    add_column :cards, :last4, :string
    add_column :cards, :exp_month, :integer
    add_column :cards, :exp_year, :integer
  end
end
