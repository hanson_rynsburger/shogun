class AddPropertiesToVariables < ActiveRecord::Migration
  def change
    add_column :variables, :properties, :hstore
    remove_column :values, :multi
  end
end
