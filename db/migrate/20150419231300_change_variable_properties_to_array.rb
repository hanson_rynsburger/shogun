class ChangeVariablePropertiesToArray < ActiveRecord::Migration
  def change
    remove_column :variables, :properties
    add_column :variables, :properties, :text, array: true, default: []
  end
end
