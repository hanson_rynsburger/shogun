class AddPusherChannelToUsers < ActiveRecord::Migration
  def change
    add_column :users, :pusher_channel, :string
    User.all.each do |user|
      user.update_column :pusher_channel, SecureRandom.uuid
    end
  end
end
