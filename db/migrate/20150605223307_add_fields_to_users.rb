class AddFieldsToUsers < ActiveRecord::Migration
  def up
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :company_name, :string
    User.all.each do |user|
      parts = user.name.split(" ")
      if parts.length > 1
        if parts.length > 2
          user.update_attributes first_name: parts[0..-2].join(" "), last_name: parts.last, company_name: "."
        else
          user.update_attributes first_name: parts.first, last_name: parts.last, company_name: "."
        end
      else
        user.update_attributes first_name: user.name, last_name: ".", company_name: "."
      end
    end
  end

  def down
    User.all.each do |user|
      user.update_column :name, "#{user.first_name} #{user.last_name}"
    end
    remove_column :users, :first_name
    remove_column :users, :last_name
    remove_column :users, :company_name
  end
end
