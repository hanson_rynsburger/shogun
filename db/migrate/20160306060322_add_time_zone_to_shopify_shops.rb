class AddTimeZoneToShopifyShops < ActiveRecord::Migration
  def change
    add_column :shopify_shops, :time_zone, :string
  end
end
