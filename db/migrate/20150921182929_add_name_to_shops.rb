class AddNameToShops < ActiveRecord::Migration
  def change
    add_column :shopify_shops, :name, :string
  end
end
