class AddStylesToComponents < ActiveRecord::Migration
  def change
    add_column :components, :styles, :text
  end
end
