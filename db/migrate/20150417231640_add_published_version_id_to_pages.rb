class AddPublishedVersionIdToPages < ActiveRecord::Migration
  def change
    add_column :pages, :published_version_id, :uuid
  end
end
