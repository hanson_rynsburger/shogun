class AddEditorUrlToSites < ActiveRecord::Migration
  def change
    add_column :sites, :editor_url, :string
  end
end
