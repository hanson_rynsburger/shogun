class CreateAddOns < ActiveRecord::Migration
  def change
    create_table :add_ons do |t|
      t.string :site_id
      t.string :name
      t.hstore :properties
      t.boolean :active

      t.timestamps null: false
    end
  end
end
