class AddShopifyToUsers < ActiveRecord::Migration
  def change
    add_column :users, :shopify, :boolean
  end
end
