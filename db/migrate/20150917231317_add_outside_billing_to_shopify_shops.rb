class AddOutsideBillingToShopifyShops < ActiveRecord::Migration
  def change
    add_column :shopify_shops, :outside_billing, :boolean, default: false
  end
end
