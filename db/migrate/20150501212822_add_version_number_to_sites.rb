class AddVersionNumberToSites < ActiveRecord::Migration
  def change
    add_column :sites, :version_number, :integer, default: 0
  end
end
