class AddVersionNumberToPages < ActiveRecord::Migration
  def change
    add_column :pages, :version_number, :integer, default: 0
  end
end
