class AddUuidToComponents < ActiveRecord::Migration
  def up
    add_column :components, :uuid, :uuid
    # Component.all.each do |c|
    #   c.update! uuid: SecureRandom.uuid
    # end
  end

  def down
    remove_column :components, :uuid
  end
end
