class AddActiveToShopifyShops < ActiveRecord::Migration
  def change
    add_column :shopify_shops, :active, :boolean
  end
end
