class AddColorsToSites < ActiveRecord::Migration
  def change
    add_column :sites, :colors, :text
  end
end
