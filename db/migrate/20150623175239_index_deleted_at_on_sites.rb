class IndexDeletedAtOnSites < ActiveRecord::Migration
  def change
    add_index :sites, :deleted_at
  end
end
