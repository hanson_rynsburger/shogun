class SetDefaultValuesForShopifyBooleans < ActiveRecord::Migration
  def up
    change_column :shopify_shops, :dead, :boolean, default: false, null: false
    change_column :shopify_shops, :active, :boolean, default: false, null: false
  end

  def down
    change_column :shopify_shops, :dead, :boolean, default: nil, null: true
    change_column :shopify_shops, :active, :boolean, default: nil, null: true
  end
end
