class AddFontsToSites < ActiveRecord::Migration
  def change
    add_column :sites, :fonts, :text
  end
end
