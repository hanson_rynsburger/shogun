class AddVersionNumberToTemplates < ActiveRecord::Migration
  def change
    add_column :templates, :version_number, :integer, default: 0
  end
end
