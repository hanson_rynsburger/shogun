# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161220044423) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"
  enable_extension "hstore"

  create_table "add_ons", force: :cascade do |t|
    t.string   "site_id"
    t.string   "name"
    t.hstore   "properties"
    t.boolean  "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "add_ons", ["site_id"], name: "index_add_ons_on_site_id", using: :btree

  create_table "cards", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "site_id"
    t.string   "stripe_customer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "brand"
    t.string   "last4"
    t.integer  "exp_month"
    t.integer  "exp_year"
  end

  add_index "cards", ["site_id"], name: "index_cards_on_site_id", using: :btree

  create_table "collaborators", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "site_id"
    t.uuid     "user_id"
    t.boolean  "admin"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "collaborators", ["site_id", "user_id"], name: "index_collaborators_on_site_id_and_user_id", unique: true, using: :btree

  create_table "components", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "template_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.uuid     "page_id"
    t.text     "styles"
    t.uuid     "uuid"
    t.hstore   "data_attributes", default: {}
  end

  add_index "components", ["page_id"], name: "index_components_on_page_id", using: :btree
  add_index "components", ["template_id"], name: "index_components_on_template_id", using: :btree

  create_table "discounts", force: :cascade do |t|
    t.integer  "amount"
    t.uuid     "site_id",    null: false
    t.string   "plan_name"
    t.datetime "used_at"
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "discounts", ["site_id"], name: "index_discounts_on_site_id", using: :btree

  create_table "invites", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "email"
    t.uuid     "user_id"
    t.uuid     "site_id"
    t.datetime "accepted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invites", ["site_id"], name: "index_invites_on_site_id", using: :btree

  create_table "invoices", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "plan_id"
    t.string   "stripe_charge_id"
    t.integer  "_amount"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "paid_at"
    t.uuid     "site_id"
    t.uuid     "card_id"
    t.text     "description"
  end

  add_index "invoices", ["card_id"], name: "index_invoices_on_card_id", using: :btree
  add_index "invoices", ["plan_id"], name: "index_invoices_on_plan_id", using: :btree
  add_index "invoices", ["site_id"], name: "index_invoices_on_site_id", using: :btree

  create_table "pages", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "site_id"
    t.string   "path",                             null: false
    t.hstore   "properties"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.uuid     "root_id"
    t.uuid     "published_version_id"
    t.boolean  "updating"
    t.datetime "outdated_at"
    t.integer  "version_number",       default: 0
    t.datetime "deleted_at"
  end

  add_index "pages", ["path"], name: "index_pages_on_path", using: :btree
  add_index "pages", ["site_id"], name: "index_pages_on_site_id", using: :btree

  create_table "plans", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "site_id"
    t.text     "internal_notes"
    t.integer  "price"
    t.integer  "max_users"
    t.integer  "price_per_user"
    t.integer  "max_pages"
    t.integer  "price_per_page"
    t.datetime "activated_at"
    t.datetime "deactivated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  add_index "plans", ["site_id"], name: "index_plans_on_site_id", using: :btree

  create_table "references", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.integer  "position"
    t.uuid     "value_id"
    t.uuid     "component_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "references", ["value_id"], name: "index_references_on_value_id", using: :btree

  create_table "shopify_charges", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "shopify_shop_id",   null: false
    t.datetime "activated_at"
    t.datetime "cancelled_at"
    t.datetime "trial_ends_at"
    t.datetime "billing_at"
    t.integer  "shopify_charge_id", null: false
    t.string   "name"
    t.integer  "price"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "shopify_charges", ["shopify_charge_id"], name: "index_shopify_charges_on_shopify_charge_id", unique: true, using: :btree
  add_index "shopify_charges", ["shopify_shop_id"], name: "index_shopify_charges_on_shopify_shop_id", using: :btree

  create_table "shopify_shops", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "domain"
    t.string   "token"
    t.uuid     "site_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "outside_billing",   default: false
    t.string   "email"
    t.string   "shop_owner"
    t.string   "name"
    t.boolean  "active",            default: false, null: false
    t.datetime "trialled_at"
    t.string   "shopify_plan_name"
    t.boolean  "dead",              default: false, null: false
    t.datetime "uninstalled_at"
    t.string   "time_zone"
    t.boolean  "javascript",        default: false
    t.boolean  "scrape",            default: true
    t.integer  "trial_days",        default: 7
  end

  add_index "shopify_shops", ["domain"], name: "index_shopify_shops_on_domain", unique: true, using: :btree
  add_index "shopify_shops", ["site_id"], name: "index_shopify_shops_on_site_id", unique: true, using: :btree

  create_table "sites", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "secret_token"
    t.string   "old_plan"
    t.uuid     "default_card_id"
    t.integer  "version_number",  default: 0
    t.datetime "deleted_at"
    t.string   "editor_url"
    t.string   "preview_url"
    t.text     "features"
    t.text     "colors",          default: [], array: true
    t.string   "digest"
    t.boolean  "demo"
    t.string   "type"
    t.text     "fonts",           default: [], array: true
  end

  add_index "sites", ["deleted_at"], name: "index_sites_on_deleted_at", using: :btree
  add_index "sites", ["type"], name: "index_sites_on_type", using: :btree

  create_table "templates", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "site_id"
    t.text     "liquid"
    t.text     "css"
    t.text     "js"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "type",           default: "user"
    t.integer  "version_number", default: 0
    t.datetime "deleted_at"
  end

  add_index "templates", ["type"], name: "index_templates_on_type", using: :btree

  create_table "users", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "token"
    t.string   "name"
    t.string   "provider"
    t.string   "uid"
    t.string   "pusher_channel"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "company_name"
    t.boolean  "heroku",          default: false
    t.boolean  "admin",           default: false
    t.boolean  "shopify"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["provider", "uid"], name: "index_users_on_provider_and_uid", unique: true, using: :btree

  create_table "values", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "variable_id"
    t.uuid     "component_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "value"
  end

  add_index "values", ["component_id", "variable_id"], name: "index_values_on_component_id_and_variable_id", unique: true, using: :btree

  create_table "variables", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.uuid     "template_id"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "input"
    t.text     "properties"
  end

  add_index "variables", ["template_id"], name: "index_variables_on_template_id", using: :btree

  create_table "versions", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.text     "html"
    t.text     "css"
    t.text     "js"
    t.uuid     "page_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "uploaded_at"
  end

  add_index "versions", ["page_id"], name: "index_versions_on_page_id", using: :btree

end
