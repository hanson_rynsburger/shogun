source 'https://rubygems.org'

ruby '2.3.0'

gem 'active_model_serializers'
gem 'analytics-ruby', '~> 2.0.0', require: 'segment/analytics'
gem 'annotate'
gem 'aws-sdk'
gem 'bcrypt'
gem 'bootstrap-sass'
gem 'coffee-rails'
gem 'concurrent-ruby'
gem 'curb'
gem 'email_validator'
gem 'fastly-rails'
gem 'haml'
gem 'hashie'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'liquid'
gem 'lograge'
gem 'mailchimp-api', require: 'mailchimp'
gem 'mongoid'
gem 'multi_json'
gem 'newrelic_rpm'
gem 'non-stupid-digest-assets'
gem 'nokogiri'
gem 'omniauth'
gem 'omniauth-google-oauth2'
gem 'omniauth-shopify-oauth2'
gem 'pg'
gem 'premailer-rails', github: 'dkan/premailer-rails'
gem 'pusher'
gem 'rails'
gem 'react-rails', github: 'reactjs/react-rails'
gem 'redcarpet'
gem 'render_anywhere', require: false
gem 'rollbar'
gem 'sass-rails'
gem 'settingslogic'
gem 'shogun', github: 'getshogun/shogun_rails'
gem 'sidekiq', '> 4'
gem 'simple_form'
gem 'sinatra', require: nil
gem 'stripe'
gem 'timers'
gem 'therubyracer', platforms: :ruby
gem 'uglifier'
gem 'warden'
gem 'warden-basic_auth', '~> 0.1.0'
gem 'yajl-ruby', require: 'yajl'

source 'https://rails-assets.org' do
  gem 'rails-assets-bowser'
  gem 'rails-assets-react', '0.13.0'
  gem 'rails-assets-react-router', '0.13.4'
end

group :development do
  gem 'capistrano'
  gem 'capistrano-bundler'
  gem 'capistrano-passenger'
  gem 'capistrano-rails'
  gem 'capistrano-rvm'
  gem 'capistrano-sidekiq'
  gem 'derailed'
  gem 'thin'
end

group :development, :test do
  gem 'awesome_print'
  gem 'byebug'
  gem 'guard-rspec'
  gem 'letter_opener'
  gem 'pry'
  gem 'pry-byebug'
  gem 'pry-rails'
  gem 'quiet_assets'
  gem 'spring'
  gem 'spring-commands-rspec'
end

group :test do
  gem 'capybara'
  gem 'capybara-webkit'
  gem 'database_cleaner'
  gem 'factory_girl_rails'
  gem 'fakeredis', require: 'fakeredis/rspec'
  gem 'ffaker'
  gem 'rspec-rails'
  gem 'simplecov', require: false
end
