class FounderMailer < ApplicationMailer
  default to: "founders@getshogun.com"

  def shopify_installed(shop)
    @shop = shop
    mail(subject: "Shopify Install: #{@shop.name}")
  end

  def shopify_charge_accepted(shop)
    @shop = shop
    plan = shop.site.plan.name
    mail(subject: "Shopify Charge Accepted (#{plan}): #{@shop.name}")
  end

  def shopify_uninstalled(shop)
    @shop = shop
    mail(subject: "Shopify UNINSTALL: #{@shop.name}")
  end
end
