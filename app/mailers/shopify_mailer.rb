class ShopifyMailer < ApplicationMailer
  default from: "Nick Raushenbush <nick@getshogun.com>"

  def shopify_installed(shop)
    @shop = shop
    mail(subject: "Welcome to Shogun!", to: @shop.email)
  end

  def shopify_uninstalled(shop)
    @shop = shop
    mail(subject: "Sorry to see you go.", to: @shop.email)
  end
end
