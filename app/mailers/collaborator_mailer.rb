class CollaboratorMailer < ApplicationMailer
  def collaborator_email(current_user, user, site)
    @current_user = current_user
    @site = site
    mail(to: user.email, subject: "#{@current_user.email} has invited you to collaborate on their project on Shogun")
  end
end
