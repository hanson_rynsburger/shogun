class InviteMailer < ApplicationMailer
  def invite_email(current_user, email, site)
    @current_user = current_user
    @site = site
    mail(to: email, subject: "#{current_user.email} has invited you to join their project on Shogun")
  end
end
