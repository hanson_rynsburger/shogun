class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@getshogun.com"
  layout 'mailer'
end
