class EditorController < ApplicationController
  before_action :authenticate!
  after_action :allow_iframe, only: [:outer]

  def outer
    render "shogun/outer/show", layout: "blank"
  end

  def inner
    render "shogun/inner/show", layout: "editor"
  end
end
