class Api::ShopifyTemplatesController < ApplicationController
  before_action :authenticate!
  before_action :find_site

  def index
    ss = @site.shopify_shop
    service = ss.service
    templates = service.get_templates
    render json: { shopify_templates: templates }
  end

  private

  def find_site
    @site = current_user.sites.find params[:site_id]
  end
end
