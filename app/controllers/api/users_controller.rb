class Api::UsersController < ApplicationController
  before_action :authenticate!

  def show
    render json: CurrentUserSerializer.new(current_user, root: "user").as_json
  end

  def update
    if current_user.update user_params
      render json: CurrentUserSerializer.new(current_user, root: "user").as_json
    else
      render json: {errors: current_user.errors.full_messages}, status: :bad_request
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :first_name, :last_name, :company_name, :password, :password_confirmation, :provider, :uid)
  end
end
