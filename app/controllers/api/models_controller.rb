class Api::ModelsController < ApplicationController
  before_action :authenticate!
  before_action :find_site
  before_action :find_model, only: [:update]

  def index
    render json: {models: ActiveModel::ArraySerializer.new(@site.models.includes(:entries), each_serializer: ::ModelSerializer)}
  end

  def create
    model = @site.models.create model_params

    if model.persisted?
      render json: ModelSerializer.new(model)
    else
      render json: {errors: model.errors.full_messages}, status: :bad_request
    end
  end

  def update
    if params[:model][:entries].present?
      params[:model][:entries].each_with_index do |e, i|
        entry = @model.entries.find(e[:id])
        entry.update position: i
      end
    end

    if @model.update model_params
      render json: ModelSerializer.new(@model)
    else
      render json: {errors: @model.errors.full_messages}, status: :bad_request
    end
  end

  private

  def find_model
    @model = @site.models.find params[:id]
  end

  def find_site
    @site = current_user.sites.find params[:site_id]
  end

  def model_params
    params.require(:model)
      .permit(:name)
      .merge({properties: params[:model][:properties]})
  end
end
