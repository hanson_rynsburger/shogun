class Api::ShopifyPagesController < ApplicationController
  before_action :authenticate!
  before_action :find_site

  def index
    ss = @site.shopify_shop
    if ss.nil?
      render json: {pages: []}
    else
      service = ss.service
      pages = service.pages rescue []
      imported = Set.new(@site.pages.pluck(:external_page_id))
      external = pages.reject { |p| imported.member?(p["id"].to_s) }
      output = external.map do |p|
        { name: p["title"], path: p["handle"], id: p["id"] }
      end
      render json: {pages: output}
    end
  end

  def import
    external_id = params[:id]
    service = @site.shopify_shop.service
    page = service.get_page external_id

    html = page["body_html"]
    path = page["handle"]
    name = page["title"]
    metafields = service.get_page_metafields external_id
    meta_tags = []
    metafields.each do |mf|
      if mf["key"] == "title_tag"
        meta_tags << {"property" => "title", "value" => mf["value"]}
      elsif mf["key"] == "description_tag"
        meta_tags << {"property" => "description", "value" => mf["value"]}
      end
    end
    mp = @site.pages.build external_page_id: external_id, path: path, name: name, meta_tags: meta_tags
    mpv = mp.versions.build uploaded_at: Time.now
    mpv.root = ComponentService.build values_attributes: [
      {
        references_attributes: [
          {
            component_attributes: {
              template_id: "text", template_name: "Text", values_attributes: [
                {
                  variable_id: "text_variable_id", value: [html]
                }
              ]
            }
          }
        ]
      }
    ]
    if mpv.save
      mp.current_version_id = mpv._id
      mp.published_version_id = mpv._id
      if mp.save
        render json: PageSerializer.new(mp)
      else
        render json: {errors: mp.errors.full_messages}
      end
    else
      render json: {errors: mpv.errors.full_messages}
    end
  end

private

  def find_site
    @site = current_user.sites.find params[:site_id]
  end
end
