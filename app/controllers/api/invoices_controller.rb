class Api::InvoicesController < ApplicationController
  before_action :authenticate!
  before_action :find_site
  before_action :find_invoice, only: [:pay]

  def index
    invoices = @site.invoices.includes(:card).order("created_at DESC").all
    render json: {invoices: ActiveModel::ArraySerializer.new(invoices, each_serializer: InvoiceSerializer)}
  end

  def pay
    if InvoiceService.pay! site: @site, invoice: @invoice, stripe_token: params[:stripe_token]
      render json: InvoiceSerializer.new(@invoice)
    else
      render json: {errors: []}, status: :bad_request
    end
  end

  private

  def find_site
    @site = current_user.sites.find params[:site_id]
  end

  def find_invoice
    @invoice = @site.invoices.find params[:id]
  end
end
