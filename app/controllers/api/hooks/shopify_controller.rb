class Api::Hooks::ShopifyController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def receive
    domain = request.headers["X-Shopify-Shop-Domain"]
    hmac = request.headers["X-Shopify-Hmac-Sha256"]
    topic = request.headers["X-Shopify-Topic"]
    json = request.raw_post

    digest = OpenSSL::Digest.new("sha256")
    calculated_hmac = Base64.encode64(OpenSSL::HMAC.digest(digest, ENV["SHOPIFY_SECRET"], json)).strip
    if calculated_hmac == hmac
      shop = ShopifyShop.where(domain: domain).take!
      model = MongoModel.where(site_id: shop.site_id, external: true).first
      parsed = JSON.parse json
      ShopifyRouter.route shop: shop, model: model, topic: topic, payload: parsed
      head 200
    else
      head 400
    end
  end
end
