class Api::CollaboratorsController < ApplicationController
  before_action :authenticate!
  before_action :find_site
  before_action :find_collaborator, only: [:update, :destroy]
  before_action :verify_admin, only: [:create, :update, :destroy]

  def index
    render json: {collaborators: ActiveModel::ArraySerializer.new(@site.collaborators.includes(:user), each_serializer: CollaboratorSerializer)}
  end

  def update
    if @collaborator.update collaborator_params
      render json: CollaboratorSerializer.new(@collaborator)
    else
      render json: {errors: collaborator.errors.full_messages}, status: :bad_request
    end
  end

  def destroy
    @collaborator.destroy!
    render json: {}
  end

  private

  def collaborator_params
    params.require(:collaborator).permit(:admin)
  end

  def find_site
    @site = current_user.sites.find params[:site_id]
  end

  def find_collaborator
    @collaborator = @site.collaborators.find params[:id]
  end

  def verify_admin
    col = current_user.collaborators.where(site_id: @site.id).take!
    return head :unauthorized unless col.admin?
  end
end
