class Api::EntriesController < ApplicationController
  before_action :authenticate!
  before_action :find_site
  before_action :find_model
  before_action :find_entry, only: [:update, :destroy]

  def create
    entry = @model.entries.create entry_params
    if entry.persisted?
      @model.flush_changes_to_pages!
      render json: EntrySerializer.new(entry)
    else
      render json: {errors: entry.errors.full_messages}, status: :bad_request
    end
  end

  def update
    if @entry.update entry_params
      @model.flush_changes_to_pages!
      render json: EntrySerializer.new(@entry)
    else
      render json: {errors: @entry.errors.full_messages}, status: :bad_request
    end
  end

  def destroy
    @entry.destroy!
    @model.flush_changes_to_pages!
    render json: {}
  end

  def index
    render json: {entries: ActiveModel::ArraySerializer.new(@model.entries, each_serializer: ::EntrySerializer)}
  end

  private

  def entry_params
    ep = {}
    @model.properties.keys.each do |k|
      ep[k] = params[:entry][k]
    end
    ep
  end

  def find_entry
    @entry = @model.entries.find params[:id]
  end

  def find_model
    @model = @site.models.find params[:model_id]
  end

  def find_site
    @site = current_user.sites.find params[:site_id]
  end
end
