class Api::SnippetsController < ApplicationController
  before_action :authenticate!
  before_action :find_site

  def create
    snippet = @site.snippets.build name: params[:snippet][:name]
    snippet.root = ComponentService.build params[:snippet][:root_attributes]
    if snippet.save
      render json: SnippetSerializer.new(snippet)
    else
      render json: {errors: snippet.errors.full_messages}, status: :bad_request
    end
  end

  def index
    render json: {snippets: ActiveModel::ArraySerializer.new(@site.snippets, each_serializer: SnippetSerializer)}
  end

  def destroy
    snippet = @site.snippets.find params[:id]
    snippet.destroy
    render json: {}
  end

  private

  def find_site
    @site = current_user.sites.find params[:site_id]
  end
end
