class Api::PagesController < ApplicationController
  before_action :authenticate!
  before_action :find_site
  before_action :find_page, only: [:show, :update, :destroy, :publish, :unpublish, :switch_version, :version_history]

  def show
    render json: FullPageSerializer.new(@page)
  end

  def index
    render json: {pages: ActiveModel::ArraySerializer.new(@site.pages, each_serializer: PageSerializer)}
  end

  def create
    page = PageService.create(@site, params, current_user)
    if page.persisted?
      Analytics.track(user_id: current_user.id, event: "Created Page", properties: page.tracking_fields)
      render json: FullPageSerializer.new(page)
    else
      render json: {errors: page.errors.full_messages}, status: :bad_request
    end
  end

  def switch_version
    if PageService.switch_version(@page, params[:version_id])
      Analytics.track(user_id: current_user.id, event: "Switched Page Version", properties: @page.tracking_fields)
      render json: FullPageSerializer.new(@page)
    else
      render json: {errors: @page.errors.full_messages}, status: :bad_request
    end
  end

  def update
    if PageService.update(@page, params, current_user)
      Analytics.track(user_id: current_user.id, event: "Updated Page", properties: @page.tracking_fields)
      render json: FullPageSerializer.new(@page)
    else
      render json: {errors: @page.errors.full_messages}, status: :bad_request
    end
  end

  def destroy
    if @page.update(deleted_at: Time.now)
      Analytics.track(user_id: current_user.id, event: "Deleted Page", properties: @page.tracking_fields)
      PageDeletedWorker.perform_async @page._id.to_s
      render json: FullPageSerializer.new(@page)
    else
      render json: {errors: @page.errors.full_messages}, status: :bad_request
    end
  end

  def publish
    PageService.publish! @page
    Analytics.track(user_id: current_user.id, event: "Published Page", properties: @page.tracking_fields)
    render json: FullPageSerializer.new(@page)
  end

  def unpublish
    PageService.unpublish! @page
    Analytics.track(user_id: current_user.id, event: "Unpublished Page", properties: @page.tracking_fields)
    render json: FullPageSerializer.new(@page)
  end

  def version_history
    render json: {version_history: @page.version_history}
  end

  private

  def find_page
    @page = @site.pages.find params[:id]
  end

  def find_site
    @site = current_user.sites.find params[:site_id]
  end
end
