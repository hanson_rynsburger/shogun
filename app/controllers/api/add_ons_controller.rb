class Api::AddOnsController < ApplicationController
  before_action :authenticate!
  before_action :find_site
  before_action :find_add_on, only: [:update]

  # Get all AddOns for site
  def index
    render json: {add_ons: ActiveModel::ArraySerializer.new(@site.add_ons, each_serializer: ::AddOnSerializer)}
  end

  def create
    add_on = @site.add_ons.create add_on_params
    if add_on.persisted?
      render json: AddOnSerializer.new(add_on)
    else
      render json: {errors: add_on.errors.full_messages}, status: :bad_request
    end
  end

  def update
    if @add_on.update add_on_params
      render json: AddOnSerializer.new(@add_on)
    else
      render json: {errors: @add_on.errors.full_messages}, status: :bad_request
    end
  end

  private

  def add_on_params
    keys = params[:add_on][:properties].try(:keys)
    params.require(:add_on).permit(
      :site_id,
      :name,
      :active,
      { properties: keys })
  end

  def find_site
    @site = current_user.sites.find params[:site_id]
  end

  def find_add_on
    @add_on = @site.add_ons.find params[:id]
  end
end
