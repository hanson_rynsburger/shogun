class Api::InvitesController < ApplicationController
  before_action :authenticate!
  before_action :find_site
  before_action :find_invite, only: [:destroy]

  def index
    render json: {invites: ActiveModel::ArraySerializer.new(@site.invites.pending, each_serializer: InviteSerializer)}
  end

  def destroy
    @invite.destroy!
    render json: {}
  end

  private

  def find_site
    @site = current_user.sites.find params[:site_id]
  end

  def find_invite
    @invite = @site.invites.find params[:id]
  end
end
