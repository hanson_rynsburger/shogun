class Api::PlansController < ApplicationController
  before_action :authenticate!
  before_action :find_site
  before_action :find_plan

  def activate
    if PlanService.activate! @site, @plan, params[:stripe_token]
      render json: SiteSerializer.new(@site)
    else
      render json: {}, status: :bad_request
    end
  end

  private

  def find_site
    @site = current_user.sites.find params[:site_id]
  end

  def find_plan
    @plan = @site.plan
    raise ActiveRecord::RecordNotFound.new unless @plan.present? && @plan.id == params[:id]
  end
end
