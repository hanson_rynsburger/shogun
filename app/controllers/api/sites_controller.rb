class Api::SitesController < ApplicationController
  before_action :authenticate!
  before_action :find_site, only: [:update, :show, :collaborators, :users]

  def users
    unless @site.within_user_limit?
      return render json: {errors: ["Upgrade your plan to add extra users."]}, status: :bad_request
    end

    email = params[:email].to_s.strip.downcase

    unless EmailValidator.valid?(email)
      return render json: {errors: ["Invalid email"]}, status: :bad_request
    end

    result = SiteService.collaborate_or_invite current_user, @site, email

    if result.is_a?(Invite)
      render json: InviteSerializer.new(result)
    elsif result.is_a?(Collaborator)
      render json: CollaboratorSerializer.new(result)
    else
      render json: {}
    end
  end

  def create
    site = current_user.sites.create site_params.merge(type: 'ShogunSite')
    site.collaborators.update_all admin: true
    if site.persisted?
      Analytics.track(user_id: current_user.id, event: "Created Site", properties: site.tracking_fields)
      render json: SiteSerializer.new(site, scope: current_user)
    else
      render json: {errors: site.errors.full_messages}, status: :bad_request
    end
  end

  def show
    render json: SiteSerializer.new(@site, scope: current_user)
  end

  def index
    if current_user.admin?
      render json: {sites: ActiveModel::ArraySerializer.new(current_user.sites.where(deleted_at: nil).includes(:plan, :invites, :shopify_shop, :default_card, :collaborators), each_serializer: SiteSerializer, scope: current_user)}
    else
      render json: {sites: ActiveModel::ArraySerializer.new(current_user.sites.shogun.where(deleted_at: nil).includes(:plan, :invites, :shopify_shop, :default_card, :collaborators), each_serializer: SiteSerializer, scope: current_user)}
    end
  end

  def update
    if @site.update site_params
      Analytics.track(user_id: current_user.id, event: "Updated Site", properties: @site.tracking_fields)
      render json: SiteSerializer.new(@site, scope: current_user)
    else
      render json: {errors: @site.errors.full_messages}, status: :bad_request
    end
  end

  private

  def site_params
    params.require(:site).permit(:name, :url, :plan, fonts: [])
  end

  def find_site
    @site = current_user.sites.find params[:id]
  end
end
