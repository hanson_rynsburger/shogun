class Api::ShopifyArticlesController < ApplicationController
  before_action :authenticate!
  before_action :find_site

  def index
    ss = @site.shopify_shop
    service = ss.service
    blogs = service.blogs rescue []
    imported = Set.new(@site.pages.pluck(:external_page_id))

    output = []
    blogs.each do |blog|
      articles = service.articles(blog["id"]) rescue []
      external = articles.reject { |p| imported.member?(p["id"].to_s) }

      external.each do |pa|
        output << { name: pa["title"], path: pa["handle"], blog: blog["title"], id: pa["id"], blog_id: blog["id"] }
      end
    end

    render json: output
  end

  def import
    external_id = params[:id]
    blog_id = params[:blog_id]

    service = @site.shopify_shop.service
    article = service.get_article blog_id, external_id

    html = article["body_html"]
    path = article["handle"]
    name = article["title"]

    metafields = service.get_article_metafields blog_id, external_id
    meta_tags = []

    metafields.each do |mf|
      if mf["key"] == "title_tag"
        meta_tags << {"property" => "title", "value" => mf["value"]}
      elsif mf["key"] == "description_tag"
        meta_tags << {"property" => "description", "value" => mf["value"]}
      end
    end

    mp = @site.pages.build external_page_id: external_id, path: path, name: name, meta_tags: meta_tags, external_blog_id: blog_id
    mpv = mp.versions.build uploaded_at: Time.now
    mpv.root = ComponentService.build values_attributes: [
      {
        references_attributes: [
          {
            component_attributes: {
              template_id: "text", template_name: "Text", values_attributes: [
                {
                  variable_id: "text_variable_id", value: [html]
                }
              ]
            }
          }
        ]
      }
    ]
    if mpv.save
      mp.current_version_id = mpv._id
      mp.published_version_id = mpv._id
      if mp.save
        render json: PageSerializer.new(mp)
      else
        render json: {errors: mp.errors.full_messages}
      end
    else
      render json: {errors: mpv.errors.full_messages}
    end
  end

  def blogs
    ss = @site.shopify_shop
    service = ss.service
    blogs = service.blogs rescue []

    output = blogs.map do |blog|
      [ blog["title"], blog["id"] ]
    end

    render json: output
  end

private

  def find_site
    @site = current_user.sites.find params[:site_id]
  end
end
