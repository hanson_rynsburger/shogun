class Api::TemplatesController < ApplicationController
  before_action :authenticate!
  before_action :find_site
  before_action :find_template, only: [:show, :update, :destroy, :html, :css, :js]

  def show
    render json: TemplateSerializer.new(@template)
  end

  def index
    templates = @site.templates.where(deleted_at: nil).includes(:current_version).select { |t| t.current_version.present? }
    render json: {templates: ActiveModel::ArraySerializer.new(templates, each_serializer: ::TemplateSerializer)}
  end

  def create
    template = TemplateService.create @site, template_params
    if template.persisted?
      Analytics.track(user_id: current_user.id, event: "Created Template", properties: template.tracking_fields)
      render json: TemplateSerializer.new(template)
    else
      render json: {errors: template.errors.full_messages}, status: :bad_request
    end
  end

  def update
    if TemplateService.update @template, template_params, current_user
      Analytics.track(user_id: current_user.id, event: "Updated Template", properties: @template.tracking_fields)
      render json: TemplateSerializer.new(@template)
    else
      render json: {errors: @template.errors.full_messages}, status: :bad_request
    end
  end

  def destroy
    @template.update deleted_at: Time.now
    Analytics.track(user_id: current_user.id, event: "Deleted Template", properties: @template.tracking_fields)
    render json: {}
  end

  def parse
    begin
      parsed = TemplateService.parse_variables(params[:liquid])
      render json: {variables: parsed}
    rescue Liquid::SyntaxError => e
      render json: {errors: [e.message]}, status: :bad_request
    end
  end

  private

  def find_template
    @template = @site.templates.find params[:id]
  end

  def find_site
    @site = current_user.sites.find params[:site_id]
  end

  def template_params
    params.require(:template).permit(
      :name,
      current_version_attributes: [
        :liquid,
        :css,
        :js,
        variables_attributes: [
          :name,
          :type,
          :input,
          :data_source,
          :model_id,
          properties: [:name, :type]
        ]
      ]
    )
  end
end
