class UsersController < ApplicationController
  def create
    @user = User.create user_params
    if @user.persisted?
      sign_in! @user

      redirect_to "/"
    else
      render action: :sign_up
    end
  end

  def sign_up
    @user = User.new
    render layout: 'shogun'
  end

  def sign_in
    render layout: 'shogun'
  end

  private

  def user_params
    params.require(:user).permit(
      :email,
      :first_name,
      :last_name,
      :company_name,
      :password,
      :password_confirmation)
  end
end
