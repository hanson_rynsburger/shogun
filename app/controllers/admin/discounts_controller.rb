class Admin::DiscountsController < ApplicationController
  def create
    discount = Discount.create! discount_params
    discount.save!
    redirect_to admin_shop_path(discount.site.shopify_shop), notice: "Discount created."
  end

  def update
    discount = Discount.find params[:id]
    discount.update! discount_params
    redirect_to admin_shop_path(discount.site.shopify_shop), notice: "Discount updated."
  end

  def discount_params
    params.require(:discount).permit(:amount, :plan_name, :site_id).merge(used_at: nil, expires_at: nil)
  end
end
