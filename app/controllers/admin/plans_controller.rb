class Admin::PlansController < ApplicationController
  before_action :authenticate!
  before_action :find_site

  def create
    plan = @site.build_plan plan_params

    if plan.save
      redirect_to :back, notice: "Plan created."
    else
      redirect_to :back, notice: "There was a problem creating the plan."
    end
  end

  private

  def plan_params
    params.require(:plan).permit(:price, :internal_notes, :max_users, :max_pages, :price_per_page, :price_per_user)
  end

  def find_site
    @site = Site.find params[:site_id]
  end
end
