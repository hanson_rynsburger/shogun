class Admin::InvoicesController < ApplicationController
  layout "admin"

  before_action :find_site
  before_action :find_invoice, only: [:show, :update, :destroy, :edit]

  def new
    @invoice = Invoice.new
  end

  def edit
  end

  def show
  end

  def index
    @invoices = @site.invoices.order("created_at DESC")
  end

  def update
    if @invoice.update invoice_params
      redirect_to admin_site_path(@site), notice: "Invoice updated"
    else
      render action: :edit
    end
  end

  def create
    @invoice = @site.invoices.create invoice_params

    if @invoice.persisted?
      redirect_to admin_site_path(@site), notice: "Invoice created"
    else
      render action: :new
    end
  end

  private

  def invoice_params
    params.require(:invoice).permit(:description, :_amount)
  end

  def find_site
    @site = Site.find params[:site_id]
  end

  def find_invoice
    @invoice = @site.invoices.find params[:id]
  end
end
