class Admin::SitesController < ApplicationController
  layout "admin"

  before_action :authenticate!
  before_action :find_site, except: [:index]

  def index
  end

  def show
  end

  def duplicate
    s = SiteService.copy(@site)
    s.name = site_params[:name]
    s.demo = true
    s.build_plan({max_users: 12345, max_pages: 12345, price_per_page: 12345, price_per_user: 12345, price: 12345})
    s.save!
    s.users << current_user
    s.collaborators.update_all(admin: true)
    redirect_to app_path(s), notice: "Site duplicated!"
  end

  private

  def site_params
    params.require(:site).permit(:name)
  end

  def find_site
    @site = Site.find params[:id]
  end
end
