require 'csv'

class Admin::ShopsController < ApplicationController
  layout "admin"

  def become
    shop = ShopifyShop.find params[:id]
    user = shop.site.users.shopify.take!
    sign_out!
    sign_in! user
    redirect_to '/'
  end

  def show
    @shop = ShopifyShop.find params[:id]
    @service = @shop.service
    @theme = @service.get_main_theme
    @page_asset = @service.get_theme_asset @theme["id"], "templates/page.liquid"
    @page_liquid = @page_asset["value"]
  end

  def retrial
    shop = ShopifyShop.find params[:id]
    shop.update_column :trialled_at, nil
    flash[:notice] = "Re-trial enabled."
    redirect_to admin_shops_path
  end

  def index
  end

  def export
    string = CSV.generate do |csv|
      csv << ['url', 'name', 'email', 'status', 'plan_name', 'shop_owner', 'signed_up']
      ShopifyShop.order(name: :asc).includes(:active_shopify_charge, site: :plan).each do |shop|
        status = shop.active? ? 'active' : shop.dead? ? 'dead' : 'uninstalled'
        csv << [
          "https://#{shop.domain}",
          shop.name,
          shop.email,
          status,
          shop.site.plan.try(:name),
          shop.shop_owner,
          shop.created_at.in_time_zone(shop.time_zone).to_date
        ]
      end
    end
    render text: string
  end
end
