class Shopify::PlansController < ApplicationController
  before_action :authenticate!
  before_action :find_shop

  def index
    Analytics.track(user_id: @shop.id, event: "Visited Shopify Plans Page")
  end

private

  def find_shop
    @shop = current_user.shopify_shops.take!
  end
end
