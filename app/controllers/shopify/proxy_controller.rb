class Shopify::ProxyController < ApplicationController
  before_action :check_signature
  before_action :set_content_type

  def previews
    page = @site.pages.find params[:id]
    html = page.current_version.raw_html

    if page.article?
      render text: render_article_liquid({title: page.name, blog_id: page.external_blog_id, content: html, template: page.shopify_template_key})
    else
      render text: render_page_liquid({title: page.name, content: html, template: page.shopify_template_key})
    end
  end

  def probe
    html = '<div id="shogun-probe"></div>'
    blog_id = params[:blog_id]

    if blog_id.present?
      render text: render_article_liquid({title: "Shogun Blog Editor", blog_id: blog_id, content: html})
    else
      render text: render_page_liquid({title: "Shogun Editor", content: html})
    end
  end

  private

  def check_signature
    query_string = request.query_string
    query_hash = Rack::Utils.parse_query(query_string)
    # => {
    #   "extra" => ["1", "2"],
    #   "shop" => "shop-name.myshopify.com",
    #   "path_prefix" => "/apps/awesome_reviews",
    #   "timestamp" => "1317327555",
    #   "signature" => "a9718877bea71c2484f91608a7eaea1532bdf71f5c56825065fa4ccabe549ef3",
    # }

    @shop = ShopifyShop.where(domain: query_hash["shop"]).take!

    # Remove and save the "signature" entry
    signature = query_hash.delete("signature")

    sorted_params = query_hash.collect{ |k, v| "#{k}=#{Array(v).join(',')}" }.sort.join
    # => "extra=1,2path_prefix=/apps/awesome_reviewsshop=shop-name.myshopify.comtimestamp=1317327555"

    calculated_signature = OpenSSL::HMAC.hexdigest(OpenSSL::Digest::Digest.new('sha256'), ENV["SHOPIFY_SECRET"], sorted_params)
    return head :unauthorized unless signature == calculated_signature

    @site = @shop.site
  end

  def set_content_type
    response.headers["Content-Type"] = "application/liquid"
  end

  def render_article_liquid(args)
    service = @shop.service
    liquid = service.get_article_template_liquid(args[:template])
    handle = "shoguntemp#{SecureRandom.hex(4)}"
    blog_id = args[:blog_id]
    blog = service.get_blog blog_id
    blog_handle = blog["handle"]

    article = @shop.service.create_article(blog_id, {body_html: args[:content], title: args[:title], handle: handle, published_at: 1.year.ago})
    article_id = article["id"]

    Shopify::DeleteTemporaryArticleWorker.perform_in 3.seconds, @shop.id, blog_id, article_id
    output = "{% assign blog = blogs.#{blog_handle} %}{% for a in blog.articles %}{% if a.id == #{article_id} %}{% assign article = a %}{% endif %}{% endfor %}#{liquid}"
    output
  end

  def render_page_liquid(args)
    service = @shop.service
    liquid = service.get_page_template_liquid(args[:template])
    handle = "shoguntemp#{SecureRandom.hex(4)}"
    page = @shop.service.create_page({body_html: args[:content], title: args[:title], handle: handle, published: true})
    Shopify::DeleteTemporaryPageWorker.perform_in 3.seconds, @shop.id, page["id"]
    output = "{% assign page = pages.#{handle} %}#{liquid}"
    output
  end
end
