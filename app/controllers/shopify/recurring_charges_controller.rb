class Shopify::RecurringChargesController < ApplicationController
  before_action :authenticate!
  before_action :find_shop
  skip_before_action :verify_authenticity_token

  def create
    service = @shop.service
    plan = params[:plan]
    data = Settings.shopify_plans[plan] || Settings.new_shopify_plans[plan]
    if data.present?
      price = data["price"].to_i

      price = (price * @price_modifier).floor

      discount = @shop.site.discount
      if discount.present? && discount.usable? && discount.plan_name == plan
        price -= discount.amount
      end
      p = {name: plan, price: price, return_url: activate_shopify_recurring_charges_url, test: !ENV["SHOPIFY_LIVE_CHARGES"]}
      unless @shop.trialled_at.present?
        p[:trial_days] = @shop.trial_days
      end
      chg = service.create_recurring_charge p
      @shop.import_shopify_charge! chg
      conf_url = chg["confirmation_url"]
      Analytics.track(user_id: @shop.id, event: "Chose Shopify Plan", properties: {plan: plan})
      redirect_to conf_url
    else
      redirect_to app_path
    end
  end

  def activate
    charge_id = params[:charge_id]
    service = @shop.service
    @site = @shop.site
    chg = service.get_recurring_charge(charge_id)
    plan = chg["name"]
    if chg["status"] == "accepted"
      data = Settings.shopify_plans[plan] || Settings.new_shopify_plans[plan]
      features = {}
      if data["level"] >= 2
        features[:integrations] = true
        features[:templates] = true
        features[:blogs] = true
      end
      if data["level"] >= 3
        features[:collections] = true
      end
      ActiveRecord::Base.transaction do
        unless @shop.trialled_at.present?
          @shop.trialled_at = Time.now
          @shop.save!
        end
        @site.features = features
        discount = @site.discount
        price = chg["price"].to_i
        if discount.present? && discount.usable? && discount.plan_name == plan
          discount.touch(:used_at)
        end
        if @site.plan.nil?
          @site.build_plan site_id: @site.id, price_per_page: 1, price_per_user: 1, max_users: 1
        end
        @site.plan.max_pages = data["pages"].to_i
        @site.plan.name = plan
        @site.plan.activated_at = Time.now
        @site.plan.price = price * 100
        @site.plan.save!
        @site.save!
      end
      chg = service.activate_recurring_charge(charge_id)
      Analytics.track(user_id: @shop.id, event: "Accepted Shopify Charge", properties: {plan: plan})
      Shopify::ChargeAcceptedWorker.perform_async @shop.id
    elsif chg["status"] == "declined"
      Analytics.track(user_id: @shop.id, event: "Declined Shopify Charge", properties: {plan: plan})
    end
    @shop.import_shopify_charge! chg
    redirect_to app_path
  end

private

  def find_shop
    @shop = current_user.shopify_shops.take!
  end
end
