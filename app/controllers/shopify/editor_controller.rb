class Shopify::EditorController < ApplicationController
  before_action :authenticate!

  def outer
    # renders view
    render layout: "editor"
  end

  def inner
    # renders scrape
    shop = current_user.shopify_shops.take!

    if shop.scrape?
      html = shop.service.scrape(params[:blog_id]) rescue nil
    else
      html = nil
    end

    if html.nil?
      scrape = MongoScrape.where(default: true).first
      html = scrape.html
    end

    render text: html, layout: nil
  end
end
