class InvoicesController < ApplicationController
  before_action :find_invoice

  def show
  end

  def pay
    if InvoiceService.pay! invoice: @invoice, stripe_token: params[:stripeToken]
      redirect_to invoice_path(@invoice), notice: "Thanks! This invoice has been paid."
    else
      redirect_to invoice_path(@invoice), alert: "There was a problem trying to pay this invoice. Please try again and if the problem persists contact support."
    end
  end

  private

  def find_invoice
    @invoice = Invoice.find params[:id]
  end
end
