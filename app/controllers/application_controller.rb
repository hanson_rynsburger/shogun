class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  before_action :set_price_modifier

  def set_price_modifier
    @price_modifier = cookies[:price_modifier].to_f
    @price_modifier = 1 unless @price_modifier > 0
  end

  def signed_in?
    warden.authenticated?
  end

  def warden
    request.env['warden']
  end

  def current_user
    warden.user
  end

  def sign_in!(user, identify=true)
    Analytics.identify(user_id: user.id, traits: user.tracking_fields) if identify
    cookies.permanent.signed[:token] = user.token
  end

  def sign_out!
    warden.logout
    cookies.delete :token
  end

  def authenticate!
    warden.authenticate!
  end

  def allow_iframe
    response.headers.except! 'X-Frame-Options'
  end

  helper_method :current_user, :signed_in?
end
