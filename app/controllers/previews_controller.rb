class PreviewsController < ApplicationController
  def show
    st = request.headers["X-Secret-Token"]
    return head 400 unless st.present?
    site = Site.where(secret_token: st).take
    return head 400 unless site.present?
    if site.demo?
      page = MongoPage.find params[:id]
      return head 400 unless page.site.demo?
    else
      page = site.pages.find params[:id]
    end
    render text: page.current_version.raw_html
  end
end
