class HerokuController < ApplicationController
  skip_before_action :verify_authenticity_token

  def sso
    sign_out!

    pre_token = params[:id] + ':' + ENV['SSO_SALT'] + ':' + params[:timestamp]
    token = Digest::SHA1.hexdigest(pre_token).to_s

    if token != params[:token] || params[:timestamp].to_i < (Time.now - 2*60).to_i
      render json: {}, :status => :forbidden
      return
    end

    site = Site.find(params[:id])
    user = site.users.where(heroku: true).take
    sign_in!(user)

    session[:user] = user.id
    session[:heroku_sso] = true
    session[:app] = params['app']

    cookies['heroku-nav-data'] = params['nav-data']

    redirect_to '/'
  end

  def provision
    authorize!

    user = User.find_by_email(params['heroku_id'])
    user ||= User.create(
      email: params['heroku_id'],
      first_name: 'heroku',
      last_name: 'user',
      company_name: params['heroku_id'],
      password: SecureRandom.hex(12),
      heroku: true
      )

    site = Site.find_by_id(params['uuid'])
    site ||= user.sites.create(
      id: params['uuid'],
      name: params['heroku_id'],
      url: 'https://example.com/',
      type: 'ShogunSite'
      )

    site.collaborators.update_all admin: true

    HerokuAddOnWorker.perform_async(site.id, user.id) if Rails.env.production?

    result = { id: site.id, config: {
      "SHOGUN_SITE_ID" => site.id,
      "SHOGUN_SECRET_TOKEN" => site.secret_token
      }
    }

    render json: result
  end

  def deprovision
    authorize!
    site = Site.find(params[:id])
    site.update(deleted_at: Time.now)
    render text: "ok"
  end

  def change_plan
    authorize!
    Site.find(params[:id]).update(plan: params[:plan])
    result = {}
    render json: result.to_json
  end

  private

  def authorize!
    warden.authenticate! :basic_auth
  end
end
