class AppController < ApplicationController
  before_action :authenticate!

  def app
    if session[:heroku_sso]
      @app = session[:app]
      render layout: 'heroku'
    end
    if current_user.shopify?
      @shop = current_user.shopify_shops.take!
      recurring_charges = @shop.service.recurring_charges rescue []
      @active_charge = recurring_charges.detect { |rc| rc["status"] == "active" }
      if @active_charge.nil? && !@shop.outside_billing? && @shop.billable?
        return redirect_to shopify_plans_path
      else
        Analytics.track(user_id: @shop.id, event: "Visited Shopify App")
      end
      render action: :shopify_app
    else
      # standard app renders
    end
  end
end
