class SessionsController < ApplicationController
  before_action :authenticate!, only: [:logout, :destroy]
  skip_before_action :verify_authenticity_token, only: :create

  def create
    email = params[:email].to_s.downcase.strip
    if user = User.find_by(email: email).try(:authenticate, params[:password])
      sign_in! user
      Analytics.track(user_id: user.id, event: "Signed in", properties: {method: "regular"})
      redirect_user user
    else
      redirect_to :back, notice: "Invalid username/password."
    end
  end

  def omniauth
    info = auth_hash["info"]
    user = {
      email: info["email"],
      first_name: info["first_name"],
      last_name: info["last_name"],
      company_name: (auth_hash["extra"]["raw_info"]["hd"] || "Example"),
      provider: auth_hash["provider"],
      uid: auth_hash["uid"]
    }
    user = UserService.find_or_create user

    if user.persisted?
      sign_out!
      sign_in! user
      redirect_user user
    else
      redirect_to sign_in_path, notice: "There was a problem signing in."
    end
  end

  def shopify
    auth_hash = request.env["omniauth.auth"]
    # find or create a shop
    shop = ShopifyShop.where(domain: auth_hash["uid"]).take
    if shop.blank?
      # create the shop
      domain = auth_hash["uid"]
      name = domain.split(".").first.titleize
      shop = ShopifyShop.new domain: domain, token: auth_hash["credentials"]["token"], active: true
      shop.sync_shopify_data!
      ActiveRecord::Base.transaction do
        site = ShopifySite.create! url: "https://#{domain}", name: name
        shop.site_id = site.id
        shop.save!
        user = site.users.create! email: "email@#{domain}", first_name: "shopify", last_name: "user", company_name: domain, password: SecureRandom.uuid, shopify: true
        site.collaborators.update_all(admin: true)
      end
      identify_shop shop
      Analytics.track(user_id: shop.id, event: "Installed Shopify App")
    else
      if !shop.active?
        Analytics.track(user_id: shop.id, event: "Installed Shopify App", properties: {reinstall: true})
        Shopify::FetchProductsWorker.perform_async shop.id, shop.site.models.where(external: true).first._id.to_s
        # Shopify::ModifyThemesWorker.perform_async shop.id
        Shopify::InstalledWorker.perform_async(shop.id)
      end
      shop.update! token: auth_hash["credentials"]["token"], active: true
      Analytics.track(user_id: shop.id, event: "Signed in via Shopify App")
    end
    sign_out!
    sign_in! shop.site.users.where(shopify: true).take
    redirect_to app_path
  end

  def destroy
    sign_out!
    redirect_to "/", notice: "Logged out"
  end

  def failure
    # auth failed
  end

private

  def identify_shop(shop)
    Analytics.identify(user_id: shop.id, traits: {name: shop.shop_owner, email: shop.email, company_name: shop.name, shopify_plan_name: shop.shopify_plan_name})
  end


  def redirect_user(user)
    site = user.sites.take
    if site
      redirect_to "/sites/#{site.id}/pages"
    else
      redirect_to "/"
    end
  end

  def auth_hash
    request.env["omniauth.auth"]
  end
end
