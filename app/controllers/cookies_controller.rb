class CookiesController < ApplicationController
  def spendless
    cookies.permanent[:price_modifier] = 0.8
    redirect_to "https://apps.shopify.com/shogun?utm_source=spendless"
  end

  def sloyalty20
    cookies.permanent[:price_modifier] = 0.8
    redirect_to "https://apps.shopify.com/shogun?utm_source=sloyalty20"
  end

  def guru20
    cookies.permanent[:price_modifier] = 0.8
    redirect_to "https://apps.shopify.com/shogun?utm_source=guru20"
  end
end
