var editorDomNode = document.getElementById("shogun-editor");

var promises = {};

function pm(args) {
  args.source = "outer";
  window.parent.postMessage(args, window.SHOGUN_EDITOR_URL);
}

function save(args) {
  pm({save: args});
}

function saveSnippet(args) {
  pm({saveSnippet: args});
}

function destroySnippet(sid) {
  pm({destroySnippet: sid});
}

function exit() {
  pm({exit: true});
}

function versions() {
  pm({versions: true});
}

function publish() {
  pm({publish: true});
}

function unpublish() {
  pm({unpublish: true});
}

function duplicate() {
  pm({duplicate: true});
}

function shogun() {
  pm({shogun: true});
}

function changed() {
  pm({changed: true});
}

function unchanged() {
  pm({unchanged: true});
}

function changePage(pageId, blogId) {
  pm({changePage: { pid: pageId, bid: blogId }});
}

function createPage() {
  pm({createPage: true});
}

var editorComponent;

function receiver(e) {
  if (e.origin == window.SHOGUN_EDITOR_URL) {
    if (!editorComponent && e.data.sync) {
      var editor = (
        <Editor
          {...e.data.sync}
          onSave={save}
          onChanged={changed}
          onChangePage={changePage}
          onCreatePage={createPage}
          onUnchanged={unchanged}
          onVersions={versions}
          onSaveSnippet={saveSnippet}
          onDestroySnippet={destroySnippet}
          onPublish={publish}
          onUnpublish={unpublish}
          onExit={exit}
          onDuplicate={duplicate}
          onShogun={shogun} />
      );
      editorComponent = React.render(editor, editorDomNode);
    } else if (editorComponent && e.data.sync) {
      editorComponent.setProps(e.data.sync);
    }
  }
};

window.addEventListener("message", receiver);
pm({ready: true});

var ctrlKeys = [17, 91, 92]; // Ctrl for Win/Mac, L Command, R Command for Mac
var ctrlDown = false; // jquery has ctrlDown but for only Windows
var shiftDown = false; // 16: Shift

$(document).on("keydown", function(e) {
  ctrlDown = ( ctrlKeys.indexOf( e.keyCode ) > -1 ) ? true : ctrlDown;
  shiftDown = ( e.keyCode == 16 ) ? true : shiftDown;
});
$(document).on("keyup", function(e) {
  ctrlDown = ( ctrlKeys.indexOf( e.keyCode ) > -1 ) ? false : ctrlDown;
  shiftDown = ( e.keyCode == 16 ) ? false : shiftDown;
  if ( ctrlDown && e.keyCode == 90 ) {
    target = $(e.target);
    if ( target.is('input') || target.is('textarea') || target.is('select') ) return;
    if ( shiftDown ) { 
      args = { redo: true, source: "inner" };
      window.postMessage(args, window.SHOGUN_EDITOR_URL);
    }
    else { 
      args = { undo: true, source: "inner" };
      window.postMessage(args, window.SHOGUN_EDITOR_URL);
    }
  }
});