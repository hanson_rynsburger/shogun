var Template = React.createClass({
  mixins: [Form],
  contextTypes: {
    router: React.PropTypes.func
  },
  getInitialState: function() {
    var t = this.props.template;
    var ts = {
      name: t.name,
      liquid: t.liquid,
      css: t.css,
      js: t.js,
      id: t.id
    };
    return {
      template: Immutable.fromJS(ts),
      variables: Immutable.fromJS(t.variables)
    };
  },
  getArgs: function() {
    var s = this.state;
    var t = s.template.toJS();
    var v = s.variables.toJS();
    return {
      id: t.id,
      site_id: this.props.site.id,
      name: t.name,
      current_version_attributes: {
        liquid: t.liquid,
        css: t.css,
        js: t.js,
        variables_attributes: v
      }
    };
  },
  save: function() {
    if (this.state.saving || this.state.parsing || this.state.needsParse || this.state.parseError) {
      return;
    }

    this.setState({saving: true}, function() {
      var args = this.getArgs();
      if (args.id) {
        TemplatesStore.update(args).then(function(template) {
          AlertsStore.success("Template updated.");
          this.setState({saving: false, changed: false});
        }.bind(this)).fail(this.handleErrors);
      } else {
        TemplatesStore.create(args).then(function(template) {
          AlertsStore.success("Template created.");
          var t = this.state.template.set("id", template.id);
          this.context.router.replaceWith("edit_template", {site_id: this.props.site.id, template_id: template.id});
        }.bind(this)).fail(this.handleErrors);
      }
    });
  },
  updateVariable: function(variable, args) {
    var variables = this.state.variables.map(function(v) {
      if (v.get("name") == variable.name) {
        return v.merge(args);
      } else {
        return v;
      }
    });
    this.setState({variables: variables, changed: true});
  },
  parseTemplate: _.debounce(function() {
    this.setState({parsing: true, parseError: false}, function() {
      TemplatesStore.parse({site_id: this.props.site.id}, this.state.template.get("liquid")).then(function(data) {
        var parsedByName = _.indexBy(data.variables, function(v) {
          return v.name;
        });
        var existing = this.state.variables.filter(function(v) {
          return !!parsedByName[v.get("name")];
        });
        var existingByName = _.indexBy(existing.toJS(), function(v) {
          return v.name;
        });
        var variables = _.map(data.variables, function(v) {
          var e = existingByName[v.name];
          if (v.type == "complex") {
            v.data_source = "user";
          }
          if (v.type == "complex" && e) {
            if (e.type == "complex") {
              var existingPropertiesByName = _(e.properties).indexBy(function(p) {
                return p.name;
              });

              e.properties = _(v.properties).map(function(p) {
                if (existingPropertiesByName[p.name]) {
                  return existingPropertiesByName[p.name];
                } else {
                  return p;
                }
              });

              return e;
            } else {
              return v;
            }
          } else {
            if (e && e.type == "complex" && v.type != "complex") {
              return v;
            }
            return e || v;
          }
        });
        this.setState({parsing: false, needsParse: false, variables: Immutable.fromJS(variables)});
      }.bind(this)).fail(function(errors) {
        this.setState({parsing: false, parseError: errors[0]});
      }.bind(this));
    }.bind(this));
  }, 500),
  handleChange: function(attr, value) {
    var t = this.state.template;
    var l = attr == "liquid";
    if (l) {
      this.parseTemplate();
    }
    var np = this.state.needsParse || l;
    this.setState({template: t.set(attr, value), changed: true, needsParse: np});
  },
  renderBreadcrumb: function() {
    var site_id = this.props.site.id;
    return (
      <ol className="breadcrumb">
        <NavItemLink to="pages" params={{site_id: site_id}}>{this.props.site.name}</NavItemLink>
        <NavItemLink to="templates" params={{site_id: site_id}}>Templates</NavItemLink>
        <li className="active">{this.props.template.name || "New"}</li>
      </ol>
    );
  },
  renderForm: function() {
    var disabled = this.state.saving || this.state.parsing || this.state.needsParse || this.state.parseError || !this.state.changed;
    return (
      <div className="input-group">
        <TextInput placeholder="Name" onChange={this.handleChange.bind(this, "name")} value={this.state.template.get("name")} />
        <div className="input-group-btn">
          <Button text="Save" onClick={this.save} disabled={disabled} />
        </div>
      </div>
    );
  },
  renderLiquid: function() {
    var readOnly = this.state.saving;
    var t = this.state.template;
    var loading = this.state.parsing ? <Loading /> : null;

    var data;
    if (this.state.parsing) {
      data = <Loading />;
    } else if (this.state.parseError) {
      data = <div className="alert alert-danger">{this.state.parseError}</div>;
    } else if (this.state.needsParse) {
      data = <div className="alert alert-warning">Pending...</div>;
    } else {
      data = (
        <Variables
          tabName="Data"
          variables={this.state.variables.toJS()}
          onUpdate={this.updateVariable} />
      );
    }

    return (
      <div className="row" key="liquid" tabName="Liquid">
        <div className="col-xs-8 col-lg-9">
          <div>
            <Ace
              mode="liquid"
              value={t.get("liquid")}
              onChange={this.handleChange.bind(this, "liquid")}
              readOnly={readOnly} />
          </div>
        </div>
        <div className="col-xs-4 col-lg-3">
          {data}
        </div>
      </div>
    );
  },
  renderCSS: function() {
    var readOnly = this.state.saving;
    var t = this.state.template;

    return (
      <Ace
        key="css"
        mode="css"
        value={t.get("css")}
        onChange={this.handleChange.bind(this, "css")}
        tabName="CSS"
        readOnly={readOnly} />
    );
  },
  renderJavascript: function() {
    var readOnly = this.state.saving;
    var t = this.state.template;

    return (
      <Ace
        key="js"
        mode="javascript"
        value={t.get("js")}
        onChange={this.handleChange.bind(this, "js")}
        tabName="Javascript"
        readOnly={readOnly} />
    );
  },
  renderTabs: function() {
    return (
      <Tabs defaultActive="liquid">
        {this.renderLiquid()}
        {this.renderCSS()}
        {this.renderJavascript()}
      </Tabs>
    );
  },
  render: function() {
    return (
      <div className="template container">
        <div className="row">
          <div className="col-xs-12 col-sm-6">
            {this.renderBreadcrumb()}
          </div>
          <div className="col-xs-12 col-sm-6">
            {this.renderForm()}
          </div>
        </div>
        {this.renderTabs()}
      </div>
    );
  }
});
