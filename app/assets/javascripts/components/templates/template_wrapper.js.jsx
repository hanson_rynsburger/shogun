var TemplateWrapper = React.createClass({
  getInitialState: function() {
    return TemplatesStore.getState(this.props.params.template_id);
  },
  handleChange: function() {
    this.setState(TemplatesStore.getState(this.props.params.template_id));
  },
  componentDidMount: function() {
    TemplatesStore.on("update", this.handleChange);
  },
  componentWillUnmount: function() {
    TemplatesStore.off("update", this.handleChange);
  },
  render: function() {
    if (!this.state.ready) {
      return <Loading />;
    }

    return <RouteHandler template={this.state.template} {...this.props} />;
  }
});
