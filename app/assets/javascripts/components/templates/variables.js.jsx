var Variables = React.createClass({
  handleChangeType: function(variable, type) {
    if (window.TYPE_INPUTS[type].indexOf(variable.input) < 0) {
      this.props.onUpdate(variable, {type: type, input: window.TYPE_INPUTS[type][0]})
    } else {
      this.props.onUpdate(variable, {type: type});
    }
  },
  handleChangeInput: function(variable, input) {
    this.props.onUpdate(variable, {input: input});
  },
  handleChangeProperties: function(variable, properties) {
    this.props.onUpdate(variable, {properties: properties});
  },
  handleChangeModel: function(variable, model_id) {
    this.props.onUpdate(variable, {model_id: model_id});
  },
  handleChangeDataSource: function(variable, data_source) {
    if (data_source == "model") {
      var input = variable.input == "multi" ? "multi" : "hidden";
      this.props.onUpdate(variable, {data_source: data_source, input: input});
    } else {
      this.props.onUpdate(variable, {data_source: data_source});
    }
  },
  renderVariables: function(variables) {
    return variables.map(function(v) {
      if (v.data_source == "model") {
        return (
          <ModelVariable
            key={v.name}
            name={v.name}
            input={v.input}
            data_source={v.data_source}
            model_id={v.model_id}
            onChangeInput={this.handleChangeInput.bind(this, v)}
            onChangeModel={this.handleChangeModel.bind(this, v)}
            onChangeDataSource={this.handleChangeDataSource.bind(this, v)} />
        );
      } else {
        if (v.type == "complex") {
          return (
            <ComplexVariable
              key={v.name}
              name={v.name}
              data_source={v.data_source}
              type={v.type}
              properties={v.properties}
              onChangeProperties={this.handleChangeProperties.bind(this, v)}
              onChangeDataSource={this.handleChangeDataSource.bind(this, v)} />
          );
        } else {
          return (
            <Variable
              key={v.name}
              name={v.name}
              data_source={v.data_source}
              type={v.type}
              input={v.input}
              onChangeType={this.handleChangeType.bind(this, v)}
              onChangeInput={this.handleChangeInput.bind(this, v)}
              onChangeDataSource={this.handleChangeDataSource.bind(this, v)} />
          );
        }
      }
    }.bind(this));
  },
  render: function() {
    return (
      <div className="variables">
        {this.renderVariables(this.props.variables)}
      </div>
    );
  }
});

var DataSourceSelector = React.createClass({
  render: function() {
    return (
      <div>
        <label className="control-label">Data Source</label><br/>
        <RadioButtonGroup value={this.props.value} onSelect={this.props.onChange} options={[{value: "user", label: "User Input"}, {value: "model", label: "Collection"}]} />
      </div>
    );
  }
});

var ModelVariable = React.createClass({
  componentDidMount: function() {
    ModelsStore.on("update", this.forceUpdate);
  },
  componentWillUnmount: function() {
    ModelsStore.off("update", this.forceUpdate);
  },
  getModel: function() {
    if (this.props.model_id) {
      var s = ModelsStore.getState(this.props.model_id);
      if (s.ready) {
        return s.model;
      }
    }
  },
  renderModelProperties: function() {
    var model = this.getModel();
    if (model) {
      var keys = _.keys(model.properties);
      if (keys.length > 0) {
        var props = _.map(keys, function(key) {
          return <p key={key}>{key}: {model.properties[key]}</p>;
        });
        return (
          <div>
            <p><strong>Model properties</strong></p>
            {props}
          </div>
        );
      }
    }
  },
  render: function() {
    var s = ModelsStore.getState();
    var models = s.models;
    var opts = _.map(models, function(m) {
      return [m.name, m.id];
    });
    opts.unshift(["", null]);
    return (
      <div className="well well-sm">
        <h4>{this.props.name}</h4>
        <DataSourceSelector value={this.props.data_source} onChange={this.props.onChangeDataSource} />
        <SelectInput options={opts} label="Which collection?" onChange={this.props.onChangeModel} value={this.props.model_id} />
        <label className="control-label">Input Type</label><br/>
        <RadioButtonGroup value={this.props.input} onSelect={this.props.onChangeInput} options={[{label: "Multi", value: "multi"}, {label: "Single", value: "hidden"}]} />
        {this.renderModelProperties()}
      </div>
    );
  }
});

var ComplexVariable = React.createClass({
  handlePropertyChange: function(name, type) {
    var np = this.props.properties.map(function(p) {
      if (p.name == name) {
        return {name: name, type: type};
      } else {
        return p;
      }
    });
    this.props.onChangeProperties(np);
  },
  renderProperties: function() {
    return this.props.properties.map(function(p) {
      return (
        <VariableTypeSelectInput
          key={p.name}
          value={p.type}
          onChange={this.handlePropertyChange.bind(this, p.name)}
          label={p.name} />
      );
    }.bind(this));
  },
  render: function() {
    return (
      <div className="well well-sm">
        <h4>{this.props.name}</h4>
        <DataSourceSelector value={this.props.data_source} onChange={this.props.onChangeDataSource} />
        {this.renderProperties()}
      </div>
    );
  }
});

var Variable = React.createClass({
  render: function() {
    var input_options = window.TYPE_INPUTS[this.props.type].map(function(input) {
      return {value: input, label: window.INPUT_NAMES[input]};
    });
    return (
      <div className="well well-sm">
        <h4>{this.props.name}</h4>
        <DataSourceSelector value={this.props.data_source || "user"} onChange={this.props.onChangeDataSource} />
        <VariableTypeSelectInput dropzone value={this.props.type} onChange={this.props.onChangeType} label="Value Type" />
        <label className="control-label">Input Type</label><br/>
        <RadioButtonGroup value={this.props.input} onSelect={this.props.onChangeInput} options={input_options} />
      </div>
    );
  }
});
