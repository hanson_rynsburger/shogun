var NewTemplate = React.createClass({
  getProps: function() {
    return {
      name: "",
      liquid: "",
      css: "",
      js: "",
      variables: [],
      id: null
    }
  },
  render: function() {
    return <Template {...this.props} template={this.getProps()} />;
  }
});
