var ShopifySiteWrapper = React.createClass({
  componentDidMount: function() {
    this.setup(window.SHOPIFY_SITE.id);
  },
  componentWillUnmount: function() {
    this.reset();
  },
  setup: function(site_id) {
    PagesStore.setSiteId(site_id);
    TemplatesStore.setSiteId(site_id);
    SnippetsStore.setSiteId(site_id);
    AddOnsStore.setSiteId(site_id);
    ModelsStore.setSiteId(site_id);
    ShopifyPagesStore.setSiteId(site_id);
    ShopifyArticlesStore.setSiteId(site_id);
    ShopifyBlogsStore.setSiteId(site_id);
    ShopifyTemplatesStore.setSiteId(site_id);
  },
  reset: function() {
    PagesStore.setSiteId(null);
    TemplatesStore.setSiteId(null);
    SnippetsStore.setSiteId(null);
    AddOnsStore.setSiteId(null);
    ModelsStore.setSiteId(null);
    ShopifyPagesStore.setSiteId(null);
    ShopifyArticlesStore.setSiteId(null);
    ShopifyBlogsStore.setSiteId(null);
    ShopifyTemplatesStore.setSiteId(null);
  },
  render: function() {
    return <RouteHandler site={window.SHOPIFY_SITE} {...this.props} />;
  }
});
