var Link = ReactRouter.Link;

var ShopifySite = React.createClass({
  render: function() {
    var trial;
    if (window.SHOPIFY_TRIAL_REMAINING) {
      trial = <div className="label label-success pull-right">trial active</div>;
    }
    return (
      <div className="container">
        <div className="page-header">
          <h2>{this.props.site.name} <small>{this.props.site.url}</small></h2>
        </div>
        {trial}
        <ul className="nav nav-pills">
          <NavItemLink to="pages"><i className="fa fa-laptop"></i> Pages</NavItemLink>
          <NavItemLink to="blogs"><i className="fa fa-newspaper-o"></i> Blogs</NavItemLink>
          <NavItemLink to="templates"><i className="fa fa-code"></i> Templates</NavItemLink>
          <NavItemLink to="models"><i className="fa fa-database"></i> Collections</NavItemLink>
          <NavItemLink to="edit_site"><i className="fa fa-cog"></i> Settings</NavItemLink>
        </ul>
        <hr />
        <RouteHandler {...this.props} />
      </div>
    );
  }
});
