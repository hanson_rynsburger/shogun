var Link = ReactRouter.Link;

var Templates = React.createClass({
  getInitialState: function() {
    return TemplatesStore.getState();
  },
  handleChange: function() {
    this.setState(TemplatesStore.getState());
  },
  componentDidMount: function() {
    TemplatesStore.on("update", this.handleChange);
  },
  componentWillUnmount: function() {
    TemplatesStore.off("update", this.handleChange);
  },
  renderAddTemplateButton: function() {
    if (!this.props.site.shopify || this.props.site.features.templates) {
      return <Link className="btn btn-primary" to="new_template" params={{site_id: this.props.site.id}}><i className="fa fa-plus-circle"></i> New Template</Link>
    } else {
      return <UpgradeButton text="Upgrade to enable custom templating" />
    }
  },
  render: function() {
    if (!this.state.ready) {
      return <Loading />;
    }

    return (
      <div className="templates">
        <TemplatesTable templates={this.state.templates} site_id={this.props.site.id} />
        {this.renderAddTemplateButton()}
      </div>
    );
  }
});
