var Pages = React.createClass({
  getInitialState: function() {
    return PagesStore.getState();
  },
  handleChange: function() {
    this.setState(PagesStore.getState());
  },
  componentDidMount: function() {
    PagesStore.on("update", this.handleChange);
  },
  componentWillUnmount: function() {
    PagesStore.off("update", this.handleChange);
  },
  renderAddPageButton: function() {
    var site = this.props.site;
    if (!site.shopify || site.max_pages > this.state.pages.length) {
      return <Link className="btn btn-primary" to="layouts" params={{site_id: this.props.site.id}}><i className="fa fa-plus-circle"></i> New Page</Link>;
    } else {
      return <UpgradeButton text="Upgrade to add more pages" />;
    }
  },
  renderShopifyPagesTable: function() {
    if (this.props.site.shopify) {
      return <ShopifyPagesTable site_id={this.props.site.id} />;
    }
  },
  renderFirefoxWarning: function() {
    if (bowser.firefox) {
      return <FirefoxWarning />;
    }
  },
  render: function() {
    if (!this.state.ready) {
      return <Loading />;
    }

    var pages = _(this.state.pages).filter(function(obj) {
      return obj.external_blog_id == null;
    });

    return (
      <div className="pages">
        {this.renderFirefoxWarning()}
        <PagesTable pages={pages} site_id={this.props.site.id} />
        {this.renderAddPageButton()}
        {this.renderShopifyPagesTable()}
      </div>
    );
  }
});
