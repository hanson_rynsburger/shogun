var Models = React.createClass({
  getInitialState: function() {
    return ModelsStore.getState();
  },
  handleChange: function() {
    this.setState(ModelsStore.getState());
  },
  componentDidMount: function() {
    ModelsStore.on("update", this.handleChange);
  },
  componentWillUnmount: function() {
    ModelsStore.off("update", this.handleChange);
  },
  renderAddModelButton: function() {
    if (!this.props.site.shopify || this.props.site.features.collections) {
      return <Link className="btn btn-primary" to="new_model" params={{site_id: this.props.site.id}}><i className="fa fa-plus-circle"></i> New Collection</Link>;
    } else {
      return <UpgradeButton text="Upgrade to enable collections for custom data" />;
    }
  },
  render: function() {
    if (!this.state.ready) {
      return <Loading />;
    }

    return (
      <div className="models">
        <ModelsTable models={this.state.models} site_id={this.props.site.id} />
        {this.renderAddModelButton()}
      </div>
    );
  }
});
