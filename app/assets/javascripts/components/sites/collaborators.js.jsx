var Collaborators = React.createClass({
  getInitialState: function() {
    return CollaboratorsStore.getState();
  },
  isAdmin: function() {
    return this.state.collaborators.some(function(collaborator) {
      return collaborator.user.id == this.props.user.id && collaborator.admin;
    }.bind(this));
  },
  handleChange: function() {
    this.setState(CollaboratorsStore.getState());
  },
  componentDidMount: function() {
    CollaboratorsStore.on("update", this.handleChange);
  },
  componentWillUnmount: function() {
    CollaboratorsStore.off("update", this.handleChange);
  },
  handleDestroy: function(collaborator) {
    CollaboratorsStore.destroy({id: collaborator.id, site_id: this.props.site.id});
  },
  handleMakeAdmin: function(collaborator) {
    CollaboratorsStore.update({id: collaborator.id, site_id: this.props.site.id, admin: true});
  },
  renderActions: function(collaborator, admin) {
    var user = collaborator.user;
    var me = this.props.user.id == user.id;

    if (me || !admin) {
      return "-";
    } else {
      if (!collaborator.admin) {
        admin = <button className="btn btn-sm btn-success" onClick={this.handleMakeAdmin.bind(this, collaborator)}>Make Admin</button>
      } else {
        admin = <button className="btn btn-sm btn-success" disabled="disabled">Make Admin</button>
      }
      return (
        <span className="actions">
          <button className="btn btn-sm btn-danger" onClick={this.handleDestroy.bind(this, collaborator)}>&times;</button>
          {admin}
        </span>
      );
    }
  },
  renderCollaborators: function() {
    var admin = this.isAdmin();
    return this.state.collaborators.map(function(collaborator) {
      var user = collaborator.user;
      return (
        <tr key={collaborator.id}>
          <td>{user.first_name}</td>
          <td>{user.last_name}</td>
          <td>{user.email}</td>
          <td>{this.renderActions(collaborator, admin)}</td>
        </tr>
      )
    }.bind(this));
  },
  render: function() {
    if (!this.state.ready) {
      return <Loading />;
    }
    return (
      <div className="collaborators">
        <table className="table table-striped table-bordered">
          <thead>
            <tr>
              <th className="col-xs-2">First Name</th>
              <th className="col-xs-2">Last Name</th>
              <th className="col-xs-4">Email</th>
              <th className="col-xs-4">Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.renderCollaborators()}
          </tbody>
        </table>
      </div>
    );
  }
});
