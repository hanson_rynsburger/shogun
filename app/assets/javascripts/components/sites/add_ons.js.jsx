var AddOns = React.createClass({
  getInitialState: function() {
    var as = AddOnsStore.getState();

    return {
      addOns: as.addOns,
      ready: as.ready,
      addOnProperties: {},
      editing: null,
      options: window.ADD_ONS,
      values: {}
    };
  },
  handleChange: function(ref, e) {
    var values = this.state.values;
    values[ref] = e.target.value;
    this.setState({values: values});
  },
  handleClose: function() {
    this.setEditing(null);
  },
  handleConfirm: function() {
    var t = this;
    var addOn = t.getAddOn(this.editing);
    var properties = {};
    var option = t.getOption();
    $.each(option.settings_fields, function(i, v){
      var ref = option.name + '-' + v;
      properties[v] = t.refs[ref].getDOMNode().value.trim()
    });

    // if there is no addOn then create new
    // else update
    if (typeof addOn === 'undefined') {
      t.activate(option, properties);
    } else {
      var args = {id: addOn.id, site_id: t.props.params.site_id, properties: properties};
      AddOnsStore.update(args);
    }

    this.handleClose();
  },
  handleUpdate: function() {
    this.setState(AddOnsStore.getState());
  },
  componentDidMount: function() {
    AddOnsStore.on("update", this.handleUpdate);
  },
  componentWillUnmount: function() {
    AddOnsStore.off("update", this.handleUpdate);
  },
  activate: function(option, properties) {
    var addOn = this.getAddOn(option.name);

    if (addOn) {
      var args = {id: addOn.id, site_id: this.props.site.id, active: true};
      AddOnsStore.update(args);
    } else if (properties) {
      var args = {site_id: this.props.site.id, name: option.name, active: true, properties: properties};
      AddOnsStore.create(args);
    } else {
      this.setEditing(option.name);
    }
  },
  deactivate: function(addOn) {
    var id = this.getAddOn(addOn.name).id
    var args = {id: id, site_id: this.props.site.id, active: false};
    AddOnsStore.update(args);
  },
  getOption: function(name) {
    name = name || this.state.editing
    return (
      $.grep(this.state.options, function(e){
        return e.name == name;
      })[0]
    );
  },
  getAddOn: function(name) {
    name = name || this.state.editing
    return (
      $.grep(this.state.addOns, function(e){
        return e.name == name;
      })[0]
    );
  },
  setEditing: function(name) {
    this.setState({editing: name});
  },
  isEnabled: function() {
    var site = this.props.site;
    return !site.shopify || site.features.integrations;
  },
  renderAddOns: function() {
    var t = this;
    var sorted = _(this.state.options).sortBy(function(option) {
      return option.name;
    });
    var enabled = this.isEnabled();
    return _(sorted).map(function(option) {
      return (
        <AddOn
          enabled={enabled}
          key={option.name}
          addOn={t.getAddOn(option.name)}
          option={option}
          activate={t.activate}
          deactivate={t.deactivate}
          setEditing={t.setEditing}/>
      );
    });
  },
  renderUpgradeButton: function() {
    if (!this.isEnabled()) {
      return (
        <div>
          <UpgradeButton text="Upgrade to enable add-on integrations" />
          <hr />
        </div>
      );
    }
  },
  renderModal: function() {
    var t = this;
    if (this.state.editing) {
      var option = t.getOption();

      var values;
      if (typeof t.getAddOn() !== 'undefined') {
        values = t.getAddOn().properties;
      } else {
        values = {};
      }

      return (
        <Modal large onClose={this.handleClose} show={true}>
          <ModalHeader title={"Configure " + this.state.editing} onClose={this.handleClose}/>
          <ModalBody>
            {
              option.settings_fields.map(function(field){
                var ref = option.name + '-' + field;
                var value = t.state.values[ref] || values[field];
                return (
                  <div key={ref} className="add-on-input-field">
                    <label className="control-label">{field}</label>
                    <input className="form-control" type="text" ref={ref} value={value} onChange={t.handleChange.bind(t, ref)}/>
                  </div>
                );
              })
            }
          </ModalBody>
          <ModalFooter>
            <a className="btn btn-success" onClick={this.handleConfirm}>Save</a>
            <br />
          </ModalFooter>
        </Modal>
      );
    }
  },
  render: function() {
    if (!this.state.ready) {
      return <Loading />;
    }

    return (
      <div className="add-ons">
        {this.renderUpgradeButton()}
        {this.renderAddOns()}
        {this.renderModal()}
      </div>
    );
  }
});

var AddOn = React.createClass({
  activate: function(e) {
    this.props.activate(this.props.option)
  },
  deactivate: function(e) {
    this.props.deactivate(this.props.option)
  },
  handleConfigure: function(e) {
    this.props.setEditing(this.props.option.name);
  },
  renderAddOnToggle: function(active) {
    if (this.props.enabled) {
      var toggle;

      if (active) {
        toggle = <button className="btn btn-success btn-sm" onClick={this.deactivate}>ON</button>
      } else {
        toggle = <button className="btn btn-default btn-sm" onClick={this.activate}>OFF</button>
      }

      return toggle;
    }
  },
  renderConfigure: function() {
    if (typeof this.props.addOn !== 'undefined') {
      return (
        <a className="btn btn-primary btn-sm" onClick={this.handleConfigure}>
          <i className="fa fa-cog"></i>
        </a>
      );
    }
  },
  render: function() {
    var active;
    if (this.props.addOn) {
      active = this.props.addOn.active;
    }

    var cn = "add-on-item"

    if (active) {
      cn += " add-on-active"
    }

    return (
      <div className={cn}>
        <strong>{this.props.option.human_name}</strong>
        <span className="pull-right">
          {this.renderConfigure()} {this.renderAddOnToggle(active)}
        </span>
      </div>
    );
  }
});
