var Link = ReactRouter.Link;

var Site = React.createClass({
  render: function() {
    var site_id = this.props.site.id;
    return (
      <div className="container">
        <div className="page-header">
          <h2>{this.props.site.name} <small>{this.props.site.url}</small></h2>
        </div>
        <ul className="nav nav-pills">
          <NavItemLink to="pages" params={{site_id: site_id}}><i className="fa fa-laptop"></i> Pages</NavItemLink>
          <NavItemLink to="templates" params={{site_id: site_id}}><i className="fa fa-code"></i> Templates</NavItemLink>
          <NavItemLink to="models" params={{site_id: site_id}}><i className="fa fa-database"></i> Collections</NavItemLink>
          <NavItemLink to="edit_site" params={{site_id: site_id}}><i className="fa fa-cog"></i> Settings</NavItemLink>
        </ul>
        <hr />
        <RouteHandler {...this.props} />
      </div>
    );
  }
});
