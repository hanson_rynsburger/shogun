var Router = ReactRouter;
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

var SiteSettingsWrapper = React.createClass({
  renderMenu: function() {
    var sid = this.props.site.id;
    var params = {site_id: sid};
    return (
      <ul className="nav nav-pills nav-stacked">
        <NavItemLink to="edit_site" params={params}><i className="fa fa-cog"></i> General</NavItemLink>
        <NavItemLink to="users" params={params}><i className="fa fa-users"></i> Users</NavItemLink>
        <NavItemLink to="integrate" params={params}><i className="fa fa-plug"></i> Integrate</NavItemLink>
        <NavItemLink to="add_ons" params={params}><i className="fa fa-plus-square"></i> Add Ons</NavItemLink>
        <NavItemLink to="billing" params={params}><i className="fa fa-credit-card"></i> Billing</NavItemLink>
      </ul>
    )
  },
  render: function() {
    return (
      <div className="site-settings">
        <div className="row">
          <div className="col-xs-3">
            {this.renderMenu()}
          </div>
          <div className="col-xs-9">
            <RouteHandler {...this.props} />
          </div>
        </div>
      </div>
    );
  }
});
