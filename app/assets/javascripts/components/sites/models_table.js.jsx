var Router = ReactRouter;
var Link = Router.Link;

var ModelsTable = React.createClass({
  renderModels: function() {
    var sorted = _(this.props.models).sortBy(function(model) {
      return model.name.toLowerCase();
    });
    return _(sorted).map(function(model) {
      return (
        <tr key={model.id}>
          <td><Link to="edit_model" params={{site_id: this.props.site_id, model_id: model.id}}>{model.name}</Link></td>
        </tr>
      );
    }.bind(this));
  },
  render: function() {
    return (
      <table className="table table-striped table-bordered">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {this.renderModels()}
        </tbody>
      </table>
    );
  }
});
