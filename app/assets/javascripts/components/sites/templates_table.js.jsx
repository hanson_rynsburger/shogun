var Router = ReactRouter;
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

var TemplatesTable = React.createClass({
  handleDestroy: function(template) {
    var c = confirm("Are you sure you want to delete '"+template.name+"' template? This action cannot be reversed.");
    if (c == true) {
      TemplatesStore.destroy({id: template.id, site_id: this.props.site_id});
    }
  },
  renderActions: function(template) {
    return (
      <button className="btn btn-xs btn-danger" onClick={this.handleDestroy.bind(this, template)}><i className="fa fa-minus-circle"></i></button>
    );
  },
  renderTemplates: function() {
    var sorted = _(this.props.templates).sortBy(function(template) {
      return template.name.toLowerCase();
    });
    return _(sorted).map(function(template) {
      if (template.deleted_at === null) {
        return (
          <tr key={template.id}>
            <td><Link to="edit_template" params={{site_id: this.props.site_id, template_id: template.id}}>{template.name}</Link></td>
            <td>{this.renderActions(template)}</td>
          </tr>
        )
      }
    }.bind(this));
  },
  render: function() {
    return (
      <table className="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th className="col-xs-11">Name</th>
            <th className="col-xs-1">Actions</th>
          </tr>
        </thead>
        <tbody>
          {this.renderTemplates()}
        </tbody>
      </table>
    );
  }
});
