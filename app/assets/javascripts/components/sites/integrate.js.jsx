var Integrate = React.createClass({
  render: function() {
    return (
      <Tabs defaultActive="rails">
        <IntegrateRails key="rails" site={this.props.site} tabName="Rails" />
        <IntegratePHP key="php" site={this.props.site} tabName="PHP" />
        <IntegratePython key="python" site={this.props.site} tabName="Python" />
        <IntegrateNode key="node" site={this.props.site} tabName="Node.js" />
      </Tabs>
    );
  }
});

var IntegrateRails = React.createClass({
  render: function() {
    return (
      <div>
        <p>Add the Shogun gem to your Gemfile:</p>
        <pre>
          gem 'shogun', github: 'getshogun/shogun_rails'
        </pre>
        <p>Run <code>bundle install</code></p>
        <p>Set these environment variables:</p>
        <pre>
          SHOGUN_SITE_ID={this.props.site.id}
          <br />
          SHOGUN_SECRET_TOKEN={this.props.site.secret_token}
        </pre>
        <p>See <a href="https://github.com/getshogun/shogun_rails" target="_blank">https://github.com/getshogun/shogun_rails</a> for advanced configuration options.</p>
      </div>
    );
  }
});

var IntegratePHP = React.createClass({
  render: function() {
    return (
      <div>
        Our PHP integration will be ready soon.
      </div>
    );
  }
});

var IntegratePython = React.createClass({
  render: function() {
    return (
      <div>
        Our Python integration will be ready soon.
      </div>
    );
  }
});

var IntegrateNode = React.createClass({
  render: function() {
    return (
      <div>
        Our Node.js integration will be ready soon.
      </div>
    );
  }
});
