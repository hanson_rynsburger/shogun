var Billing = React.createClass({
  mixins: [Form],

  getInitialState: function() {
    var is = InvoicesStore.getState();
    return _.extend(is, {
      handler: StripeCheckout.configure({
        email: this.props.user.email,
        allowRememberMe: false,
        zipCode: true,
        key: window.STRIPE_PUBLISHABLE_KEY,
        token: this.handleToken
      })
    });
  },
  handleChange: function() {
    this.setState(InvoicesStore.getState());
  },
  componentDidMount: function() {
    InvoicesStore.on("update", this.handleChange);
  },
  componentWillUnmount: function() {
    InvoicesStore.off("update", this.handleChange);
  },
  handleToken: function(obj) {
    if (this.state.saving) {
      return;
    }
    var args = {site_id: this.props.site.id, id: this.state.invoiceId, stripe_token: obj.id};
    this.setState({saving: true}, function() {
      InvoicesStore.pay(args).then(function(invoice) {
        AlertsStore.success("Thanks! This invoice has been paid.");
        this.setState({invoiceId: null, saving: false});
      }.bind(this)).fail(this.handleErrors);
    }.bind(this));
  },
  pay: function(invoice) {
    this.setState({invoiceId: invoice.id}, function() {
      this.state.handler.open({
        name: "Shogun",
        description: invoice.description,
        amount: invoice._amount
      });
    }.bind(this));
  },
  renderInvoices: function() {
    var mapped = this.state.invoices.map(function(invoice) {
      var p, c;
      if (invoice.paid_at) {
        p = invoice.paid_at;
        c = invoice.card.brand + " " + invoice.card.last4;
      } else if(!this.state.saving) {
        p = <Button block success onClick={this.pay.bind(this, invoice)} text="Pay" />;
      } else {
        p = <Loading />;
      }
      return (
        <tr key={invoice.id}>
          <td>{invoice.id}</td>
          <td>${numberToCurrency(invoice._amount / 100.0)}</td>
          <td>{invoice.description}</td>
          <td>{invoice.created_at}</td>
          <td>{p}</td>
          <td>{c}</td>
        </tr>
      );
    }.bind(this));

    return (
      <table className="table table-bordered table-striped">
        <thead>
          <tr>
            <th className="col-xs-2">#</th>
            <th className="col-xs-1">Amount</th>
            <th className="col-xs-3">Description</th>
            <th className="col-xs-2">Created at</th>
            <th className="col-xs-2">Paid at</th>
            <th className="col-xs-2">Card</th>
          </tr>
        </thead>
        <tbody>
          {mapped}
        </tbody>
      </table>
    );
  },
  renderPlan: function() {
    var plan = this.props.site.plan;
    if (!plan) {
      return <div className="alert alert-warning">Your account is currently in Trial Mode. Please contact us to enable billing.</div>;
    } else {
      return (
        <div className="panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title text-center">Your plan</h3>
          </div>
          <ul className="list-group">
            <li className="list-group-item text-center">{plan.max_users} users</li>
            <li className="list-group-item text-center">{plan.max_pages} pages</li>
          </ul>
        </div>
      );
    }
  },
  componentWillUnmount: function() {
    this.state.handler.close();
  },
  render: function() {
    if (!this.state.ready) {
      return <Loading />;
    }
    return (
      <div className="billing">
        <div className="row">
          <div className="col-xs-6">
            {this.renderPlan()}
          </div>
        </div>
        <h4>Invoices</h4>
        {this.renderInvoices()}
        <h4>Usage</h4>
        <div className="row">
          <div className="col-xs-6">
            <Usage site={this.props.site} />
          </div>
        </div>
      </div>
    );
  }
});
