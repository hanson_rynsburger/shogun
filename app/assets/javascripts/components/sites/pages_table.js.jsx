var Router = ReactRouter;
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

var PagesTable = React.createClass({
  mixins: [PageStatus],

  handleDestroy: function(page) {
    var c = confirm("Are you sure you want to delete '"+page.name+"' page? This action cannot be reversed.");
    if (c == true) {
      PagesStore.destroy({id: page.id, site_id: this.props.site_id});
    }
  },
  renderActions: function(page) {
    return (
      <button className="btn btn-xs btn-danger" onClick={this.handleDestroy.bind(this, page)}><i className="fa fa-minus-circle"></i></button>
    );
  },
  renderPages: function() {
    var sorted = _(this.props.pages).sortBy(function(page) {
      return page.name.toLowerCase();
    });
    return _(sorted).map(function(page) {
      return (
        <tr key={page.id}>
          <td><Link to="edit_page" params={{site_id: this.props.site_id, page_id: page.id}}>{page.name}</Link></td>
          <td>{page.path}</td>
          <td>{this.renderStatus(page)}</td>
          <td>{this.renderActions(page)}</td>
        </tr>
      )
    }.bind(this));
  },
  render: function() {
    return (
      <table className="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th className="col-xs-5">Name</th>
            <th className="col-xs-5">Path</th>
            <th className="col-xs-1">Status</th>
            <th className="col-xs-1">Actions</th>
          </tr>
        </thead>
        <tbody>
          {this.renderPages()}
        </tbody>
      </table>
    );
  }
});
