var Blogs = React.createClass({
  getInitialState: function() {
    return {
      pages: PagesStore.getState(),
      blogs: ShopifyBlogsStore.getState()
    }
  },
  handleChange: function() {
    this.setState({
      pages: PagesStore.getState(),
      blogs: ShopifyBlogsStore.getState()
    });
  },
  handleBlogSelect: function(val) {
    ShopifyBlogsStore.setBlogSelected(val);
  },
  componentDidMount: function() {
    PagesStore.on("update", this.handleChange);
    ShopifyBlogsStore.on("update", this.handleChange);
  },
  componentWillUnmount: function() {
    PagesStore.off("update", this.handleChange);
    ShopifyBlogsStore.off("update", this.handleChange);
  },
  renderBlogs: function() {
    return (
      <SelectInput label="Blog" options={this.state.blogs.blogs} onChange={this.handleBlogSelect} />
    );
  },
  renderAddArticleButton: function() {
    if (this.props.site.features.blogs) {
      return (
        <div className="select-blog">
          {this.renderBlogs()}
          <Link className="btn btn-primary btn-disabled" to="blogs_layouts" params={{site_id: this.props.site.id, blog_id: this.state.blogs.selected }}><i className="fa fa-plus-circle"></i> New Article</Link>
        </div>
      );
    } else {
      return <UpgradeButton text="Upgrade to enable blogs" />;
    }
  },
  renderShopifyArticlesTable: function() {
    return <ShopifyArticlesTable site={this.props.site} />;
  },
  renderFirefoxWarning: function() {
    if (bowser.firefox) {
      return <FirefoxWarning />;
    }
  },
  render: function() {
    if (!this.state.pages.ready) {
      return <Loading />;
    }

    var articles = _(this.state.pages.pages).filter(function(obj) {
      return obj.external_blog_id != null;
    });

    return (
      <div className="blogs">
        {this.renderFirefoxWarning()}
        <BlogsTable articles={articles} site={this.props.site} />
        {this.renderAddArticleButton()}
        {this.renderShopifyArticlesTable()}
      </div>
    );
  }
});
