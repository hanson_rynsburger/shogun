var EditSite = React.createClass({
  mixins: [Form],

  getInitialState: function() {
    return {site: Immutable.fromJS(this.props.site)};
  },
  handleChange: function(attr, value) {
    this.setState({site: this.state.site.set(attr, value), changed: true});
  },
  save: function(e) {
    this.handleSubmit(e, function() {
      SitesStore.update(this.state.site.toJS()).then(function(site) {
        AlertsStore.success("Site updated.");
        this.setState({site: Immutable.fromJS(site), changed: false, saving: false});
      }.bind(this)).fail(this.handleErrors);
    }.bind(this));
  },
  renderURL: function() {
    if (this.props.site.shopify) {
      return (
        <div className="form-group">
          <label className="control-label">Shopify store name</label>
          <div className="input-group">
            <input readOnly disabled className="form-control disabled" type="text" value={this.props.site.shopify_shop.name} />
            <div className="input-group-addon">.myshopify.com</div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="form-group">
          <label className="control-label">URL</label>
          <TextInput value={this.state.site.get("url")} onChange={this.handleChange.bind(this, "url")} />
          <p className="help-block">Must be a fully qualified URL, e.g., https://getshogun.com</p>
        </div>
      );
    }
  },
  renderForm: function() {
    var cn = "btn btn-primary";
    if (this.state.saving || !this.state.changed) {
      cn += " disabled";
    }

    return (
      <form onSubmit={this.save}>
        <div className="form-group">
          <label className="control-label">Name</label>
          <TextInput value={this.state.site.get("name")} onChange={this.handleChange.bind(this, "name")} />
        </div>
        {this.renderURL()}
        <button type="submit" className={cn}>Save</button>
      </form>
    )
  },
  render: function() {
    return (
      <div className="edit_site">
        {this.renderForm()}
      </div>
    );
  }
});
