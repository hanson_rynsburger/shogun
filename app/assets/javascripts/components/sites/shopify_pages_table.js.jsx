var ShopifyPagesTable = React.createClass({
  getInitialState: function() {
    return {loading: false, pages: ShopifyPagesStore.getState().pages};
  },
  handleUpdate: function() {
    this.setState({pages: ShopifyPagesStore.getState().pages});
  },
  componentDidMount: function() {
    ShopifyPagesStore.on("update", this.handleUpdate);
  },
  componentWillUnmount: function() {
    ShopifyPagesStore.off("update", this.handleUpdate);
  },
  handleImport: function(page) {
    if (!this.state.loading) {
      this.setState({loading: true}, function() {
        ShopifyPagesStore.import({id: page.id, site_id: this.props.site_id}).then(function(p) {
          AlertsStore.success("Your Shopify page was imported.");
          this.setState({loading: false});
        }.bind(this));
      }.bind(this));
    }
  },
  renderActions: function(page) {
    var cn = "btn btn-xs btn-primary";
    if (this.state.loading) {
      cn += " disabled";
    }
    return (
      <button className={cn} onClick={this.handleImport.bind(this, page)}><i className="fa fa-cloud-download"></i></button>
    );
  },
  renderPages: function() {
    var sorted = _(this.state.pages).sortBy(function(page) {
      return page.name.toLowerCase();
    });
    return _(sorted).map(function(page) {
      return (
        <tr key={page.id}>
          <td>{page.name}</td>
          <td>{page.path}</td>
          <td>{this.renderActions(page)}</td>
        </tr>
      )
    }.bind(this));
  },
  render: function() {
    if (this.state.pages.length > 0) {
      return (
        <div className="shopify-pages-table">
          <hr />
          <div className="alert alert-info">These pages already exist in your Shopify account. You can import them to Shogun below.</div>
          <table className="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th className="col-xs-5">Name</th>
                <th className="col-xs-5">Path</th>
                <th className="col-xs-2">Import?</th>
              </tr>
            </thead>
            <tbody>
              {this.renderPages()}
            </tbody>
          </table>
        </div>
      );
    } else {
      return <div className="shopify-pages-table"></div>;
    }
  }
});
