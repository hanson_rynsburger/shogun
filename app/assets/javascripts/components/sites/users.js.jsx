var Link = ReactRouter.Link;

var Users = React.createClass({
  mixins: [Form],

  getInitialState: function() {
    return {email: "", max_users: this.props.site.max_users};
  },
  refresh: function() {
    this.forceUpdate();
  },
  componentDidMount: function() {
    UsersStore.on("update", this.refresh);
  },
  componentWillUnmount: function() {
    UsersStore.off("update", this.refresh);
  },
  componentWillReceiveProps: function(nextProps) {
    var site = nextProps.site;
    if (site.max_users != this.state.max_users) {
      this.setState({max_users: site.max_users});
    }
  },
  save: function(e) {
    this.handleSubmit(e, function() {
      SitesStore.addUser({email: this.state.email, id: this.props.site.id}).then(function() {
        this.setState({changed: false, email: "", saving: false});
      }.bind(this)).fail(this.handleErrors);
    }.bind(this));
  },
  handleChange: function(value) {
    this.setState({email: value, changed: true});
  },
  isOverLimit: function() {
    return UsersStore.getState().users >= this.state.max_users;
  },
  renderUpgradeNotice: function() {
    return (
      <div className="alert alert-success">
        <Link className="alert-link" to="billing" params={{site_id: this.props.site.id}}>Upgrade your plan</Link> to add more users.
      </div>
    );
  },
  renderForm: function() {
    var cn = "btn btn-primary";
    if (!this.state.changed) {
      cn += " disabled";
    }
    return (
      <form onSubmit={this.save}>
        <div className="input-group">
          <TextInput placeholder="Email" onChange={this.handleChange} value={this.state.email} />
          <span className="input-group-btn">
            <button type="submit" className={cn}><i className="fa fa-user-plus"></i> Invite</button>
          </span>
        </div>
      </form>
    );
  },
  renderActions: function() {
    if (this.isOverLimit()) {
      return this.renderUpgradeNotice();
    } else if (UsersStore.getState().admin) {
      return this.renderForm();
    }
  },
  render: function() {
    return (
      <div className="collaborators">
        {this.renderActions()}
        <h4>Team members</h4>
        <Collaborators site={this.props.site} user={this.props.user}/>
        <h4>Pending invites</h4>
        <Invites site={this.props.site} />
      </div>
    );
  }
});
