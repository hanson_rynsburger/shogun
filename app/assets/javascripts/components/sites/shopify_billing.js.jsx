var ShopifyBilling = React.createClass({
  render: function() {
    var plans = [];
    var currentPlan = this.props.site.plan;
    var names = _.keys(window.SHOPIFY_PLANS);
    var discount = this.props.site.discount;
    _.each(names, function(name) {
      var data = window.SHOPIFY_PLANS[name];
      var active = currentPlan && currentPlan.active && currentPlan.name == name;
      var price = parseInt(data["price"]);
      price = Math.floor(price * window.PRICE_MODIFIER);
      if (currentPlan && currentPlan.name == name) {
        price = currentPlan.price / 100;
      } else if (discount && discount.usable && discount.plan_name == name) {
        price -= discount.amount;
      }
      var props = {name: name, price: price, features: data["features"], active: active};
      plans.push(<ShopifyPlan key={name} plan={name} {...props} />);
    }.bind(this));

    return (
      <Columns>
        {plans}
      </Columns>
    );
  }
});

var ShopifyPlan = React.createClass({
  renderFeatures: function() {
    var items = this.props.features.map(function(f) {
      return <li key={uuid.v4()} className="list-group-item">{f}</li>;
    });
    return (
      <ul className="list-group">
        {items}
      </ul>
    );
  },
  renderButton: function() {
    if (this.props.active) {
      return (
        <div className="shopify-billing-form">
          <a className="btn btn-success btn-block disabled">ACTIVE</a>
        </div>
      );
    } else {
      return (
        <form className="shopify-billing-form" action={"/shopify/recurring_charges?plan=" + this.props.plan} method="POST">
          <input className="btn btn-success btn-block" type="submit" value="CHOOSE" />
        </form>
      );
    }
  },
  render: function() {
    var cn = "panel panel-";
    if (this.props.active) {
      cn += "success";
    } else {
      cn += "default";
    }
    return (
      <div className={cn}>
        <div className="panel-heading">
          <strong>{this.props.name}</strong> <span className="pull-right">${this.props.price}</span>
        </div>
        {this.renderFeatures()}
        {this.renderButton()}
      </div>
    );
  }
});
