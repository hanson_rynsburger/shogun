var ShopifyArticlesTable = React.createClass({
  getInitialState: function() {
    return {loading: false, articles: ShopifyArticlesStore.getState().articles};
  },
  handleUpdate: function() {
    this.setState({articles: ShopifyArticlesStore.getState().articles});
  },
  componentDidMount: function() {
    ShopifyArticlesStore.on("update", this.handleUpdate);
  },
  componentWillUnmount: function() {
    ShopifyArticlesStore.off("update", this.handleUpdate);
  },
  handleImport: function(article) {
    if (!this.state.loading) {
      this.setState({loading: true}, function() {
        ShopifyArticlesStore.import({id: article.id, blog_id: article.blog_id, site_id: this.props.site.id}).then(function(p) {
          AlertsStore.success("Your Shopify article was imported.");
          this.setState({loading: false});
        }.bind(this));
      }.bind(this));
    }
  },
  renderActions: function(article) {
    if (!this.props.site.features.blogs) {
      return null;
    }
    var cn = "btn btn-xs btn-primary";
    if (this.state.loading) {
      cn += " disabled";
    }
    return (
      <button className={cn} onClick={this.handleImport.bind(this, article)}><i className="fa fa-cloud-download"></i></button>
    );
  },
  renderArticles: function() {
    var sorted = _(this.state.articles).sortBy(function(article) {
      return article.name.toLowerCase();
    });
    return _(sorted).map(function(article) {
      return (
        <tr key={article.id}>
          <td>{article.name}</td>
          <td>{article.path}</td>
          <td>{article.blog}</td>
          <td>{this.renderActions(article)}</td>
        </tr>
      )
    }.bind(this));
  },
  render: function() {
    if (this.state.articles.length > 0) {
      return (
        <div className="shopify-articles-table">
          <hr />
          <div className="alert alert-info">These articles already exist in your Shopify account. You can import them to Shogun below.</div>
          <table className="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th className="col-xs-4">Name</th>
                <th className="col-xs-4">Path</th>
                <th className="col-xs-2">Blog</th>
                <th className="col-xs-2">Import?</th>
              </tr>
            </thead>
            <tbody>
              {this.renderArticles()}
            </tbody>
          </table>
        </div>
      );
    } else {
      return <div className="shopify-articles-table"></div>;
    }
  }
});
