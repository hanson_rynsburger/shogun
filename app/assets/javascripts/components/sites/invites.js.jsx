var Invites = React.createClass({
  getInitialState: function() {
    return InvitesStore.getState();
  },
  handleChange: function() {
    this.setState(InvitesStore.getState());
  },
  handleDestroy: function(invite) {
    InvitesStore.destroy({site_id: this.props.site.id, id: invite.id});
  },
  componentDidMount: function() {
    InvitesStore.on("update", this.handleChange);
  },
  componentWillUnmount: function() {
    InvitesStore.off("update", this.handleChange);
  },
  renderInvites: function() {
    return this.state.invites.map(function(invite) {
      return (
        <tr key={invite.id}>
          <td>{invite.email}</td>
          <td><button className="btn btn-danger btn-sm" onClick={this.handleDestroy.bind(this, invite)}>&times;</button></td>
        </tr>
      );
    }.bind(this));
  },
  render: function() {
    if (!this.state.ready) {
      return <Loading />;
    }
    return (
      <table className="table table-bordered table-striped">
        <thead>
          <tr>
            <th className="col-xs-10">Email</th>
            <th className="col-xs-2">Actions</th>
          </tr>
        </thead>
        <tbody>
          {this.renderInvites()}
        </tbody>
      </table>
    )
  }
});
