var Router = ReactRouter;
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

var ShopifySiteSettingsWrapper = React.createClass({
  renderMenu: function() {
    return (
      <ul className="nav nav-pills nav-stacked">
        <NavItemLink to="edit_site"><Icon name="cog" /> General</NavItemLink>
        <NavItemLink to="billing"><Icon name="credit-card" /> Billing</NavItemLink>
        <NavItemLink to="add_ons"><Icon name="plus-square" /> Add Ons</NavItemLink>
        <NavItemLink to="fonts"><Icon name="font" /> Fonts</NavItemLink>
      </ul>
    )
  },
  render: function() {
    return (
      <div className="shopify-site-settings">
        <div className="row">
          <div className="col-xs-3">
            {this.renderMenu()}
          </div>
          <div className="col-xs-9">
            <RouteHandler {...this.props} />
          </div>
        </div>
      </div>
    );
  }
});
