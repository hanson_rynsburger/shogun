var NewSite = React.createClass({
  render: function() {
    var title = this.props.first ? "Create your first site" : "New Site";
    return (
      <div className="new-site container">
        <div className="page-header">
          <h2>{title}</h2>
        </div>
        <div className="row">
          <div className="col-sm-6">
            <StandardSiteForm />
          </div>
        </div>
      </div>
    );
  }
});

var StandardSiteForm = React.createClass({
  contextTypes: {
    router: React.PropTypes.func
  },
  getInitialState: function() {
    return {data: Immutable.fromJS({name: "", url: ""}), saving: false, changed: false};
  },
  save: function(e) {
    e.preventDefault();
    if (this.state.saving) {
      return;
    }
    this.setState({saving: true}, function() {
      SitesStore.create(this.state.data.toJS()).then(function(site) {
        AlertsStore.success("Site created.");
        this.context.router.transitionTo("site", {site_id: site.id});
      }.bind(this));
    }.bind(this));
  },
  handleChange: function(attr, value) {
    this.setState({data: this.state.data.set(attr, value), changed: true});
  },
  render: function() {
    var cname = "btn btn-primary";
    if (this.state.saving || !this.state.changed) {
      cname += " disabled";
    }
    return (
      <form onSubmit={this.save}>
        <div className="form-group">
          <label className="control-label">Name</label>
          <TextInput onChange={this.handleChange.bind(this, "name")} value={this.state.data.get("name")} />
        </div>
        <div className="form-group">
          <label className="control-label">URL</label>
          <TextInput onChange={this.handleChange.bind(this, "url")} value={this.state.data.get("url")} />
          <p className="help-block">Must be a fully qualified URL, e.g., https://getshogun.com</p>
        </div>
        <button type="submit" className={cname}>Create</button>
      </form>
    );
  }
});
