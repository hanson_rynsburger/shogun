var Fonts = React.createClass({
  getInitialState: function() {
    var fonts = this.props.site.fonts;
    return {fonts: fonts, changed: false};
  },
  handleChange: function(fonts) {
    this.setState({fonts: fonts, changed: true});
  },
  save: function() {
    var args = {id: this.props.site.id, fonts: this.state.fonts};
    SitesStore.update(args).then(function() {
      AlertsStore.success("Fonts updated.");
      this.setState({changed: false});
    }.bind(this));
  },
  renderSaveButton: function() {
    if (this.state.changed) {
      return <Button primary onClick={this.save} text="Save" />;
    }
  },
  render: function() {
    return (
      <div>
        <Alert info>Add your existing font names here. Font names are case sensitive.</Alert>
        <MultiTextInput
          label="Font names"
          value={{value: this.state.fonts}}
          onChange={this.handleChange} />
        {this.renderSaveButton()}
      </div>
    );
  }
});
