var Router = ReactRouter;
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

var BlogsTable = React.createClass({
  mixins: [PageStatus],

  getInitialState: function() {
    return ShopifyBlogsStore.getState();
  },
  handleChange: function() {
    this.setState(ShopifyBlogsStore.getState());
  },
  componentDidMount: function() {
    ShopifyBlogsStore.on("update", this.handleChange);
  },
  componentWillUnmount: function() {
    ShopifyBlogsStore.off("update", this.handleChange);
  },

  handleDestroy: function(article) {
    var c = confirm("Are you sure you want to delete '" + article.name + "' article? This action cannot be reversed.");
    if (c == true) {
      PagesStore.destroy({id: article.id, site_id: this.props.site.id});
    }
  },
  renderActions: function(article) {
    if (!this.props.site.features.blogs) {
      return null;
    }
    return (
      <button className="btn btn-xs btn-danger" onClick={this.handleDestroy.bind(this, article)}><i className="fa fa-minus-circle"></i></button>
    );
  },
  renderArticles: function() {
    var sorted = _(this.props.articles).sortBy(function(article) {
      return article.name.toLowerCase();
    });

    var kvp = _(this.state.blogs).indexBy(function(obj) { return obj[1]; });

    return _(sorted).map(function(article) {
      var blogname = kvp[article.external_blog_id] ? kvp[article.external_blog_id][0] : article.external_blog_id;
      var name = this.props.site.features.blogs ? <Link to="edit_article" params={{site_id: this.props.site.id, blog_id: article.external_blog_id, page_id: article.id}}>{article.name}</Link> : article.name;
      return (
        <tr key={article.id}>
          <td>{name}</td>
          <td>{blogname}</td>
          <td>{this.renderStatus(article)}</td>
          <td>{this.renderActions(article)}</td>
        </tr>
      )
    }.bind(this));
  },
  render: function() {
    return (
      <table className="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th className="col-xs-8">Name</th>
            <th className="col-xs-2">Blog</th>
            <th className="col-xs-1">Status</th>
            <th className="col-xs-1">Actions</th>
          </tr>
        </thead>
        <tbody>
          {this.renderArticles()}
        </tbody>
      </table>
    );
  }
});
