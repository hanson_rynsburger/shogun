var SiteWrapper = React.createClass({
  getInitialState: function() {
    return SitesStore.getState(this.props.params.site_id);
  },
  handleChange: function() {
    this.setState(SitesStore.getState(this.props.params.site_id));
  },
  componentDidMount: function() {
    SitesStore.on("update", this.handleChange);
    this.setup(this.props.params.site_id);
  },
  componentWillUnmount: function() {
    SitesStore.off("update", this.handleChange);
    this.reset();
  },
  componentWillReceiveProps: function(nextProps) {
    if (nextProps.params.site_id != this.props.params.site_id) {
      this.setup(nextProps.params.site_id);
      this.setState(SitesStore.getState(nextProps.params.site_id));
    }
  },
  setup: function(site_id) {
    PagesStore.setSiteId(site_id);
    TemplatesStore.setSiteId(site_id);
    SnippetsStore.setSiteId(site_id);
    CollaboratorsStore.setSiteId(site_id);
    InvitesStore.setSiteId(site_id);
    AddOnsStore.setSiteId(site_id);
    ModelsStore.setSiteId(site_id);
    InvoicesStore.setSiteId(site_id);
  },
  reset: function() {
    PagesStore.setSiteId(null);
    TemplatesStore.setSiteId(null);
    SnippetsStore.setSiteId(null);
    CollaboratorsStore.setSiteId(null);
    InvitesStore.setSiteId(null);
    AddOnsStore.setSiteId(null);
    ModelsStore.setSiteId(null);
    InvoicesStore.setSiteId(null);
  },
  render: function() {
    if (!this.state.ready) {
      return <Loading />;
    }

    return <RouteHandler site={this.state.site} {...this.props} />;
  }
});
