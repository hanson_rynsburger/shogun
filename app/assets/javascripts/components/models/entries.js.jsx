var Router = ReactRouter;
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

var Entries = React.createClass({
  getInitialState: function() {
    return {rand: uuid.v4()};
  },
  componentDidMount: function() {
    if (this.props.model.mutable) {
      var domNode = React.findDOMNode(this.refs.sortable);
      var c = this;
      var func = function(e) {
        c.handleMove.call(c, e);
      };
      this.sortable = Sortable.create(domNode, {
        animation: 200,
        handle: '.handle',
        draggable: 'tr',
        onEnd: func
      });
    }
  },
  componentWillUnmount: function() {
    if (this.props.model.mutable) {
      this.sortable.destroy();
      this.sortable = null;
    }
  },
  handleMove: function(e) {
    var i1 = e.oldIndex;
    var i2 = e.newIndex;
    if (i1 == i2) {
      return;
    }

    var entries = JSON.parse(JSON.stringify(this.props.model.entries));
    entries.splice(i2, 0, entries.splice(i1, 1)[0]);

    var clone = _.extend({}, this.props.model, {entries: entries});

    ModelsStore.update(clone).then(function() {
      this.setState(this.getInitialState());
    }.bind(this));
  },
  destroyEntry: function(id) {
    ModelsStore.destroyEntry({site_id: this.props.model.site_id, model_id: this.props.model.id, id: id}).then(function() {
      AlertsStore.success(this.props.model.singularized_name + " deleted.");
    }.bind(this));
  },
  renderTable: function() {
    var keys = _.keys(this.props.model.properties);
    var heading;

    if (this.props.model.mutable) {
      heading = _.map(keys, function(k) {
        return <th key={k}>{k}</th>;
      });
    } else {
      heading = [];
      heading.push(<th key="title">Title</th>);
      heading.push(<th key="handle">Handle</th>);
      heading.push(<th key="variants">Variants</th>);
    }

    var sorted = _.sortBy(this.props.model.entries, "position");
    var entries = sorted.map(function(e) {
      if (this.props.model.mutable) {
        var cols = _.map(keys, function(k) {
          v = e[k];
          if (typeof(v) == "string" && v.length > 100) {
            v = v.substring(0,100) + "...";
          }
          return <td key={k}>{v}</td>;
        });
        return (
          <tr key={this.state.rand + e.id}>
            <td><i className="fa fa-bars handle"></i></td>
            {cols}
            <td className="collections-actions">
              <Link to="edit_entry" className="btn btn-default btn-xs" params={{site_id: this.props.model.site_id, model_id: this.props.model.id, entry_id: e.id}}><i className="fa fa-edit"></i></Link>
              <a className="btn btn-danger btn-xs" onClick={this.destroyEntry.bind(this, e.id)}><i className="fa fa-minus-circle"></i></a>
            </td>
          </tr>
        );
      } else {
        var cols = [];
        cols.push(<td key="title">{e.title}</td>);
        cols.push(<td key="handle">{e.handle}</td>);
        var variants = _.map(e.variants, function(v) {
          return (
            <tr key={v.id}>
              <td>{v.id}</td><td>{v.title}</td><td>{v.price}</td><td>{v.compare_at_price}</td>
            </tr>
          );
        });
        var table = (
          <table className="table table-bordered">
            <tr><th className="col-xs-3">id</th><th className="col-xs-3">title</th><th className="col-xs-3">price</th><th className="col-xs-3">compare at price</th></tr>
            {variants}
          </table>
        );
        cols.push(<td key="variants">{table}</td>);
        return (
          <tr key={e.id}>
            {cols}
          </tr>
        );
      }
    }.bind(this));
    return (
      <table className="table table-bordered table-striped table-hover">
        <thead>
          <tr>
            {this.props.model.mutable ? <th>Order</th> : null}
            {heading}
            {this.props.model.mutable ? <th className="col-xs-1">Actions</th> : null}
          </tr>
        </thead>
        <tbody ref="sortable">
          {entries}
        </tbody>
      </table>
    )
  },
  render: function() {
    var add = this.props.model.mutable ? <Link to="new_entry" className="btn btn-xs btn-success" params={{site_id: this.props.model.site_id, model_id: this.props.model.id}}><i className="fa fa-plus-circle"></i></Link> : null;
    return (
      <div className="entries">
        <h4>{this.props.model.pluralized_name} {add}</h4>
        {this.renderTable()}
      </div>
    );
  }
});
