var Model = React.createClass({
  mixins: [Form],
  contextTypes: {
    router: React.PropTypes.func
  },

  getInitialState: function() {
    return {model: Immutable.fromJS(this.props.model)};
  },
  handleChange: function(attr, value) {
    this.setState({model: this.state.model.set(attr, value), changed: true});
  },
  getArgs: function() {
    var a = this.state.model.toJS();
    a.site_id = this.props.site.id;
    return a;
  },
  onAddProperty: function(prop) {
    this.setState({addProperty: false, changed: true, model: this.state.model.setIn(["properties", prop.name], prop.type)});
  },
  addProperty: function() {
    this.setState({addProperty: true});
  },
  removeProperty: function(name) {
    this.setState({model: this.state.model.deleteIn(["properties", name]), changed: true});
  },
  closeAddProperty: function() {
    this.setState({addProperty: false});
  },
  getPropertyNames: function() {
    return _.keys(this.state.model.get("properties").toJS());
  },
  save: function(e) {
    this.handleSubmit(e, function() {
      if (this.state.model.get("id")) {
        ModelsStore.update(this.getArgs()).then(function(model) {
          AlertsStore.success("Model updated.");
          this.setState({model: Immutable.fromJS(model), saving: false, changed: false});
        }.bind(this)).fail(this.handleErrors);
      } else {
        ModelsStore.create(this.getArgs()).then(function(model) {
          AlertsStore.success("Model created.");
          this.context.router.replaceWith("edit_model", {site_id: this.props.site.id, model_id: model.id});
        }.bind(this)).fail(this.handleErrors);
      }
    });
  },
  renderBreadcrumb: function() {
    var site_id = this.props.site.id;
    return (
      <ol className="breadcrumb">
        <NavItemLink to="pages" params={{site_id: site_id}}>{this.props.site.name}</NavItemLink>
        <NavItemLink to="models" params={{site_id: site_id}}>Collections</NavItemLink>
        <li className="active">{this.props.model.name || "New"}</li>
      </ol>
    );
  },
  renderEntries: function() {
    if (this.props.model.id && !this.state.changed) {
      return <Entries model={this.props.model} />;
    }
  },
  renderAddPropertyModal: function() {
    if (this.state.addProperty) {
      return <AddPropertyModal existing={this.getPropertyNames()} onClose={this.closeAddProperty} onAdd={this.onAddProperty} />;
    }
  },
  renderProperties: function() {
    var properties = this.state.model.get("properties").toJS();
    properties = _.map(_.keys(properties), function(key) {
      return (
        <tr key={key}>
          <td>{key}</td>
          <td>{properties[key]}</td>
          <td>
            <a className="btn btn-danger btn-xs" onClick={this.removeProperty.bind(this, key)}><i className="fa fa-minus-circle"></i></a>
          </td>
        </tr>
      );
    }.bind(this));
    return (
      <div className="properties">
        <table className="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th>Name</th>
              <th>Type</th>
              <th className="col-xs-1">Actions</th>
            </tr>
          </thead>
          <tbody>
            {properties}
          </tbody>
        </table>
      </div>
    );
  },
  renderForm: function() {
    if (!this.props.model.mutable) {
      return;
    }
    var cname = "btn btn-primary";
    if (this.state.saving || !this.state.changed) {
      cname += " disabled";
    }
    return (
      <div className="row">
        <div className="col-md-6 col-xs-12">
          <form onSubmit={this.save}>
            <div className="input-group">
              <TextInput placeholder="Name" value={this.state.model.get("name")} onChange={this.handleChange.bind(this, "name")} />
              <div className="input-group-btn">
                <button type="submit" className={cname}>Save</button>
              </div>
            </div>
            <h4>Fields <a className="btn btn-success btn-xs" onClick={this.addProperty}><i className="fa fa-plus-circle"></i></a></h4>
            {this.renderProperties()}
          </form>
        </div>
      </div>
    );
  },
  render: function() {
    return (
      <div className="model container">
        <div className="row">
          <div className="col-md-6 col-xs-12">
            {this.renderBreadcrumb()}
          </div>
        </div>
        {this.renderForm()}
        {this.renderAddPropertyModal()}
        <hr />
        {this.renderEntries()}
      </div>
    );
  }
});
