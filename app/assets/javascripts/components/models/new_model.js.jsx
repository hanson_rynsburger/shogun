var NewModel = React.createClass({
  getProps: function() {
    return {
      id: null,
      mutable: true,
      external: false,
      name: "",
      properties: {}
    };
  },
  render: function() {
    return <Model {...this.props} model={this.getProps()} />;
  }
});
