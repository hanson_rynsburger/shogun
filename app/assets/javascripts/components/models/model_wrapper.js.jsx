var ModelWrapper = React.createClass({
  getInitialState: function() {
    return ModelsStore.getState(this.props.params.model_id);
  },
  handleChange: function() {
    this.setState(ModelsStore.getState(this.props.params.model_id));
  },
  componentDidMount: function() {
    ModelsStore.on("update", this.handleChange);
  },
  componentWillUnmount: function() {
    ModelsStore.off("update", this.handleChange);
  },
  render: function() {
    if (!this.state.ready) {
      return <Loading />;
    }

    return <RouteHandler model={this.state.model} {...this.props} />;
  }
});
