var AddPropertyModal = React.createClass({
  getInitialState: function() {
    return {data: Immutable.fromJS({name: "", type: "text"})};
  },
  handleChange: function(attr, value) {
    this.setState({data: this.state.data.set(attr, value)});
  },
  handleAdd: function() {
    if (!this.valid()) {
      return;
    }
    var d = this.state.data.toJS();
    d.name = $.trim(d.name).toLowerCase();
    this.props.onAdd(d);
  },
  valid: function() {
    var n = $.trim(this.state.data.get("name")).toLowerCase();
    return n && n.length > 0 && n != "slug" && n != "published" && !_.contains(this.props.existing, n) && /^[a-z0-9_]+$/.test(n);
  },
  render: function() {
    var data = this.state.data;
    var cn = "btn btn-success";
    if (!this.valid()) {
      cn += " disabled";
    }
    return (
      <Modal show={true} onClose={this.props.onClose}>
        <ModalHeader title="Add Field" onClose={this.props.onClose} />
        <ModalBody>
          <div className="form-group">
            <label className="control-label">Name</label>
            <TextInput placeholder="e.g. title" onChange={this.handleChange.bind(this, "name")} value={data.get("name")} />
            <p className="help-block">a-z, 0-9 and _ only.</p>
          </div>
          <VariableTypeSelectInput label="Type" value={data.get("type")} onChange={this.handleChange.bind(this, "type")} />
        </ModalBody>
        <ModalFooter>
          <a className="btn btn-danger" onClick={this.props.onClose}>Cancel</a>
          <a className={cn} onClick={this.handleAdd}>Add</a>
        </ModalFooter>
      </Modal>
    );
  }
});
