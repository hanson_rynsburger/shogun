var NewEntry = React.createClass({
  getModel: function() {
    return ModelsStore.getState(this.props.params.model_id).model;
  },
  getEntry: function() {
    var e = {};
    var keys = _.keys(this.getModel().properties);
    _.each(keys, function(k) {
      e[k] = ""
    });
    return e;
  },
  render: function() {
    return (
      <Entry {...this.props} model={this.getModel()} entry={this.getEntry()} />
    );
  }
});
