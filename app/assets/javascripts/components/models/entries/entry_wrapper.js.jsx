var EntryWrapper = React.createClass({
  getModel: function() {
    return ModelsStore.getState(this.props.params.model_id).model;
  },
  getEntry: function() {
    return _.find(this.getModel().entries, function(e) {
      return e.id == this.props.params.entry_id;
    }.bind(this));
  },
  render: function() {
    return <Entry {...this.props} model={this.getModel()} entry={this.getEntry()} />;
  }
});
