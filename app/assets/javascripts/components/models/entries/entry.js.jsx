var Entry = React.createClass({
  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [Form],
  getInitialState: function() {
    return {data: Immutable.fromJS(this.props.entry)};
  },
  handleChange: function(hash) {
    this.setState({data: this.state.data.merge(hash), changed: true});
  },
  getArgs: function() {
    var args = this.state.data.toJS();
    args.site_id = this.props.model.site_id;
    args.model_id = this.props.model.id;
    return args;
  },
  save: function(e) {
    if (!this.valid()) {
      return;
    }
    this.handleSubmit(e, function() {
      var args = this.getArgs();
      if (args.id) {
        ModelsStore.updateEntry(args).then(function(entry) {
          AlertsStore.success(this.props.model.singularized_name + " updated.");
          this.setState({data: Immutable.fromJS(entry), saving: false, changed: false});
        }.bind(this)).fail(this.handleErrors);
      } else {
        ModelsStore.createEntry(args).then(function(entry) {
          AlertsStore.success(this.props.model.singularized_name + " created");
          this.context.router.replaceWith("edit_entry", {site_id: this.props.site.id, model_id: this.props.params.model_id, entry_id: entry.id});
        }.bind(this)).fail(this.handleErrors);
      }
    }.bind(this))
  },
  getKeys: function() {
    return _.keys(this.props.model.properties);
  },
  valid: function() {
    var keys = this.getKeys();
    return _.every(keys, function(k) {
      if (k == "slug") {
        var s = $.trim(this.state.data.get(k));
        var r = /^[a-z0-9_-]+$/i
        // TODO validate not existing slug
        return s && s.length > 0 && r.test(s);
      } else {
        return !!this.state.data.get(k);
      }
    }.bind(this));
  },
  renderForm: function() {
    var keys = this.getKeys();
    var properties = [];
    var value = {};
    _.each(keys, function(key) {
      var type = this.props.model.properties[key];
      properties.push({name: key, type: type});
      var d = this.state.data.get(key);
      if (!d) {
        if (type == "image") {
          d = {};
        } else {
          d = "";
        }
      } else if (typeof d.toJS == 'function') {
        d = d.toJS();
      }
      value[key] = d;
    }.bind(this));
    // this is kind of a hack
    // ideally change complex value properties to be a hash of name -> type
    // or change model properties to be an array of hashes with keys {name, type}
    var cn = "btn btn-success";
    if (!this.valid() || !this.state.changed) {
      cn += " disabled";
    }
    return (
      <div>
        <ComplexValue
          value={value}
          properties={properties}
          onChange={this.handleChange} />
        <a className={cn} onClick={this.save}>Save</a>
      </div>
    );
  },
  renderBreadcrumb: function() {
    var site_id = this.props.model.site_id;
    return (
      <ol className="breadcrumb">
        <NavItemLink to="pages" params={{site_id: site_id}}>{this.props.site.name}</NavItemLink>
        <NavItemLink to="models" params={{site_id: site_id}}>Collections</NavItemLink>
        <NavItemLink to="edit_model" params={{site_id: site_id, model_id: this.props.model.id}}>{this.props.model.name}</NavItemLink>
        <li className="active">{this.props.entry.id || "New"}</li>
      </ol>
    );
  },
  render: function() {
    return (
      <div className="container entry">
        <div className="row">
          <div className="col-md-6 col-xs-12">
            {this.renderBreadcrumb()}
          </div>
        </div>
        {this.renderForm()}
      </div>
    );
  },
});
