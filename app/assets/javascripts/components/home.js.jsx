var Router = ReactRouter;
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

var Home = React.createClass({
  getInitialState: function() {
    return SitesStore.getState();
  },
  handleChange: function() {
    this.setState(SitesStore.getState());
  },
  componentDidMount: function() {
    SitesStore.on("update", this.handleChange);
  },
  componentWillUnmount: function() {
    SitesStore.off("update", this.handleChange);
  },
  renderSites: function() {
    var sorted = _.sortBy(this.state.sites, function(site) {
      return site.name.toLowerCase();
    });
    var sites = _.map(sorted, function(site) {
      return (
        <div className="col-xs-4" key={site.id}>
          <div className="well text-center">
            <h3><Link to="pages" params={{site_id: site.id}}>{site.name}</Link></h3>
            <p className="text-muted">{site.url}</p>
          </div>
        </div>
      );
    });
    return (
      <div className="row">
        <div className="col-xs-4">
          <div className="well text-center">
            <h3><Link to="new_site">Add site</Link></h3>
            <p className="text-muted"> - - - - -</p>
          </div>
        </div>
        {sites}
      </div>
    );
  },
  render: function() {
    if (!this.state.ready) {
      return <Loading />;
    }
    var sites = this.state.sites;
    if (sites.length == 0) {
      return <NewSite first={true} />;
    } else {
      return (
        <div className="home container">
          <div className="page-header">
            <h2>Sites</h2>
          </div>
          {this.renderSites()}
        </div>
      );
    }
  }
});
