var FAQ = React.createClass({
  render: function() {
    return (
      <dl>
        <dt>Why don't I see my Shopify theme when using Shogun?</dt>
        <dd>If your Shopify storefront is password protected, we cannot display your theme in our editor. Once you have made your storefront public, Shogun will display your theme after a delay of up to 15 minutes.</dd>
        <dt>Can I edit my Shopify header/footer or other theme elements using Shogun?</dt>
        <dd>No - you can only create/edit pages and their content using Shogun.</dd>
        <dt>My page doesn't look right on mobile.</dt>
        <dd>Make sure you are not setting widths or heights on page elements - this is usually the cause of mobile display issues.</dd>
        <dt>Can I use a different layout for Shogun pages? Can I make Shogun pages full width or remove the page title? Shogun pages look different to my other pages.</dt>
        <dd>Please follow the steps <a href="/shopify-layout-fix">in this guide</a> and contact us if you need help.</dd>
        <dt>How can I use Shogun for the home page of my Shopify store?</dt>
        <dd>Please follow the steps <a href="/shopify-home-page">in this guide</a> and contact us if you need help.</dd>
        <dt>Can I use Shogun on my tablet or mobile phone?</dt>
        <dd>No and we have no plans to support this.</dd>
        <dt>Can I remove the /pages/ part of the URL on my Shopify store?</dt>
        <dd>No - there is no way for us to do this.</dd>
        <dt>Will I lose the Shogun pages if I uninstall the app?</dt>
        <dd>No - you just can't edit them until you reinstall the app.</dd>
        <dt>Can I make [image galleries/sliders/a widget I saw on a website] using Shogun?</dt>
        <dd>Shogun supports custom templates and you can make absolutely anything by writing custom html/css/javascript. Unfortunately we don't have the capacity to add these on request. We may add more built in templates in the future that you can use without writing code.</dd>
        <dt>I'm experiencing a bug or other issue using Shogun.</dt>
        <dd>Please email us at <a href="mailto:support@getshogun.com">support@getshogun.com</a> and we'll fix it.</dd>
      </dl>
    );
  }
});
