var SelectInput = React.createClass({
  handleChange: function(event) {
    this.props.onChange(event.target.value);
  },
  render: function() {
    var options = this.props.options.map(function(o) {
      return <option key={o[1]} value={o[1]}>{o[0]}</option>;
    });
    return (
      <div>
        <label className="control-label">{this.props.label}</label>
        <select className="form-control" defaultValue={this.props.value} onChange={this.handleChange}>
          {options}
        </select>
      </div>
    );
  }
});
