var SimpleImageInput = React.createClass({
  handleAdd: function() {
    _uploadImage(function(url) {
      this.props.onChange(url);
    }.bind(this));
  },
  render: function() {
    if (this.props.value) {
      return (
        <img src={this.props.value} className="img-responsive image-input" onClick={this.handleAdd} />
      );
    } else {
      return (
        <div className="well image-input text-center">
          <a className="btn btn-primary" onClick={this.handleAdd}>Add Image</a>
        </div>
      );
    }
  }
});
