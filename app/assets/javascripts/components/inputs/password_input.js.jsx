var PasswordInput = React.createClass({
  handleChange: function(e) {
    this.props.onChange(e.target.value);
  },
  render: function() {
    return (
      <input
        className="form-control"
        type="password"
        onChange={this.handleChange}
        value={this.props.value}
        placeholder={this.props.placeholder} />
    );
  }
});
