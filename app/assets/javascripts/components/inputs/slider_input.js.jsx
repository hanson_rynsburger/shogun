var SliderInput = React.createClass({
  componentDidMount: function() {
    var value = this.props.value || 1;
    this.getSlider().slider({
      value: value,
      step: 0.01,
      min: 0,
      max: 1,
      slide: function(event, ui) {
        this.handleChange(ui.value);
      }.bind(this)
    });
  },
  componentWillReceiveProps: function(nextProps) {
    var value = nextProps.value || 1;
    this.getSlider().slider("value", value);
  },
  componentWillUnmount: function() {
    this.getSlider().slider("destroy");
  },
  handleChange: function(e) {
    this.props.onChange(e.toString());
    this.getSlider().slider("value", e);
  },
  getSlider: function() {
    return $(React.findDOMNode(this.refs.slider));
  },
  render: function() {
    return (
      <div className="form-group">
        <label className="control-label">{this.props.label}</label>
        <div ref="slider"></div>
        <TextInput ref="slideinput" small value={this.props.value} onChange={this.handleChange} />
      </div>
    );
  }
});
