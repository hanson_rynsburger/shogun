var ImageInput = React.createClass({
  handleAdd: function(e) {
    _uploadImage(function(url) {
      this.props.onChange({url: url});
    }.bind(this));
  },
  handleChange: function(value) {
    this.props.onChange(value);
  },
  renderImage: function() {
    var url, width, height;
    if (this.props.value && this.props.value.url) {
      url = this.props.value.url;
      width = this.props.value.width;
      height = this.props.value.height;
      return (
        <div>
          <img src={url} style={{width: width, height: height}} className="img-responsive image-input" onClick={this.handleAdd} />
          {this.renderImageAttributes()}
        </div>
      );
    } else {
      return (
        <div className="well well-sm text-center">
          <a className="btn btn-info" onClick={this.handleAdd}>Add Image</a>
        </div>
      )
    }
  },
  renderImageAttributes: function() {
    return (
      <ImageAttributes
        value={this.props.value}
        onChange={this.handleChange} />
    );
  },
  render: function() {
    return (
      <div>
        {this.renderImage()}
      </div>
    )
  }
});
