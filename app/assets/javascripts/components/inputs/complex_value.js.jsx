var ComplexValue = React.createClass({
  handleChange: function(propertyName, value) {
    var clone = {};
    this.props.properties.forEach(function(p) {
      if (p.name == propertyName) {
        clone[p.name] = value;
      } else {
        clone[p.name] = this.props.value[p.name];
      }
    }.bind(this));
    this.props.onChange(clone);
  },
  renderInput: function(property, value) {
    if (property.type == "text" || property.type == "integer") {
      return (
        <TextInput
          small
          key={property.name}
          value={value}
          onChange={this.handleChange.bind(this, property.name)}
          placeholder={property.name} />
      );
    } else if (property.type == "richtext") {
      return (
        <RichText
          key={property.name}
          value={value}
          onChange={this.handleChange.bind(this, property.name)} />
      );
    } else if (property.type == "image") {
      return (
        <ImageInput
          key={property.name}
          value={value}
          onChange={this.handleChange.bind(this, property.name)} />
      );
    } else if (property.type == "markdown") {
      return (
        <Markdown
          key={property.name}
          value={value}
          onChange={this.handleChange.bind(this, property.name)} />
      );
    } else if (property.type == "html") {
      return (
        <TextareaInput
          key={property.name}
          value={value}
          onChange={this.handleChange.bind(this, property.name)} />
      );
    } else {
      console.log("NOT IMPLEMENTED");
    }
  },
  renderProperties: function() {
    return this.props.properties.map(function(p) {
      var v = this.props.value[p.name];
      return (
        <div key={p.name} className="form-group">
          <label>{p.name}</label>
          {this.renderInput(p, v)}
        </div>
      );
    }.bind(this));
  },
  render: function() {
    return (
      <div className="complex-value">
        {this.renderProperties()}
      </div>
    );
  }
});
