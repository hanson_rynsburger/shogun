var OnOffSwitch = React.createClass({
  on: function() {
    this.props.onChange(true);
  },
  off: function() {
    this.props.onChange(false);
  },
  render: function() {
    var onClass = "btn";
    var offClass = "btn";
    if (this.props.value) {
      onClass += " btn-success";
      offClass += " btn-default";
    } else {
      onClass += " btn-default";
      offClass += " btn-info";
    }
    var on = <a className={onClass} onClick={this.on}>On</a>;
    var off = <a className={offClass} onClick={this.off}>Off</a>;
    return (
      <div>
        <div className="btn-group">{on}{off}</div>
      </div>
    );
  }
});
