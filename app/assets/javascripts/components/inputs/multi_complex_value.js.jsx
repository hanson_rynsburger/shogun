var MultiComplexValue = React.createClass({
  mixins: [MultiInput],

  buildComplexValue: function() {
    var v = {};
    this.props.value.properties.forEach(function(p) {
      if (p.type == "image") {
        v[p.name] = {};
      } else {
        v[p.name] = "";
      }
    });
    return v;
  },
  renderValues: function() {
    return this.getValue().map(function(obj, i) {
      if (typeof obj != "object") {
        obj = this.buildComplexValue();
      }
      return (
        <div key={this.state.rand+i} className="multi-complex-value">
          <div className="well well-sm">
            <span className="handle multi-complex-value-handle">
              <i className="fa fa-bars"></i>
            </span>
            <button className="btn btn-danger btn-xs pull-right" onClick={this.handleRemove.bind(this, i)} type="button">&times;</button>
            <ComplexValue properties={this.props.value.properties} value={obj} onChange={this.handleChange.bind(this, i)} />
          </div>
        </div>
      );
    }.bind(this));
  }
});
