var RadioButtonGroup = React.createClass({
  handleSelect: function(value) {
    this.props.onSelect(value);
  },
  renderOptions: function() {
    return this.props.options.map(function(opt) {
      var p = {};
      p.primary = this.props.primary;
      p.active = opt.value == this.props.value;
      p.small = this.props.small;
      if (opt.icon) {
        p.icon = opt.icon;
      } else {
        p.text = opt.label;
      }
      if (opt.tooltip) {
        p.tooltip = "bottom";
        p.title = opt.tooltip;
      }
      return <Button key={opt.value} {...p} onClick={this.handleSelect.bind(this, opt.value)} />;
    }.bind(this));
  },
  render: function() {
    var cn = "btn-group";
    if (this.props.justified) {
      cn += " btn-group-justified";
    }
    return (
      <div className={cn}>
        {this.renderOptions()}
      </div>
    );
  }
});
