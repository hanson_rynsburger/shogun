var UnitTextInput = React.createClass({
  getInitialState: function() {
    return ({
      value: this.getValue(this.props),
      unit: this.getUnit(this.props)
    });
  },
  componentWillReceiveProps: function(newProps) {
    var unit = this.getUnit(newProps),
    value = this.getValue(newProps);

    if (unit === "" && value !== "auto") {
      this.setState({unit: "px"}, function() {
        this.sendChange()
      });
    }

    if (value === "") {
      this.setState({
        value: value
      });
    } else {
      this.setState({
        value: value,
        unit: unit
      });
    }

    if (value == "auto") {
      this.setState({unit: ""});
    }
  },
  handleChange: function(e) {
    this.setState({
      value: e.target.value
    }, function() {
      this.sendChange()
    });
  },
  handleChangeUnit: function(u) {
    var state;

    if (u == "auto") {
      state = {
        value: "auto",
        unit: ""
      }
    } else {
      var raw = parseFloat(this.props.value),
      value = isNaN(raw) ? "" : raw,
      unit = u;

      state = {
        value: value.toString(),
        unit: unit
      }
    }

    this.setState(state, function() {
      this.sendChange()
    });
  },
  sendChange: function(){
    var value = this.state.value;
    if (!this.props.negative && parseFloat(value) < 0) {
      value = '0';
    }

    if (value === "" || value === "auto") {
      this.props.onChange(value);
    } else {
      var cleaned = this.cleanString(value);
      if (cleaned === "") {
        this.props.onChange("");
      } else {
        if (cleaned != '-') {
          this.props.onChange(cleaned + this.state.unit);
        }
      }
    }
  },
  cleanString: function(str) {
    var cleaned;
    if (this.props.negative) {
      cleaned = str.replace(/[^-\d]/g,'');
    } else {
      cleaned = str.replace(/[^\d]/g,'');
    }
    return cleaned
  },
  getValue: function(props) {
    if (typeof(props.value) === "undefined" || props.value === "") {
      return "";
    }

    if (props.value === "auto") {
      return "auto";
    }

    var parsed = parseFloat(props.value);

    if (isNaN(parsed)) {
      return "";
    }
    return parsed;
  },
  getUnit: function(props) {
    if (!props.value) {
      return "px";
    }

    if (props.value === "auto") {
      return "";
    }

    var units = ["px", "%", "em"];
    var str = props.value.toString();
    var unit = "px"
    _.each(units, function(u) {
      if (str.indexOf(u) > -1) {
        unit = u;
      }
    });
    return unit;
  },
  increaseStart: function(e) {
    this.incrementStart(this.increase);
  },
  increase: function(e) {
    var value = this.cleanString($(React.findDOMNode(this.refs.inputField)).val());
    if (typeof(value) === "undefined" || value === "") {
      value = 0;
    }
    this.setState({
      value: (parseFloat(value) + 1).toString()
    }, function() {
      this.sendChange()
    });
  },
  decreaseStart: function(e) {
    this.incrementStart(this.decrease);
  },
  decrease: function(e) {
    var value = this.cleanString($(React.findDOMNode(this.refs.inputField)).val());
    if (typeof(value) === "undefined" || value === "") {
      value = 0;
    }
    this.setState({
      value: (parseFloat(value) - 1).toString()
    }, function() {
      this.sendChange()
    });
  },
  incrementStart: function(func) {
    var interval;
    var timeout = setTimeout(function(){
      interval = setInterval(func, 100);
      this.setState({interval: interval});
    }.bind(this), 500);

    this.setState({timeout: timeout});
  },
  incrementStop: function(e) {
    clearTimeout(this.state.timeout);
    clearInterval(this.state.interval);
  },
  renderArrows: function() {
    return (
      <div className="shogun-arrow-container">
        <div className="btn-group-vertical">
          <Button tabIndex="-1" xs onClick={this.increase} onMouseDown={this.increaseStart} onMouseUp={this.incrementStop} onMouseLeave={this.incrementStop} icon="chevron-up" />
          <Button tabIndex="-1" xs onClick={this.decrease} onMouseDown={this.decreaseStart} onMouseUp={this.incrementStop} onMouseLeave={this.incrementStop} icon="chevron-down" />
        </div>
      </div>
    );
  },
  render: function() {
    var units = ["px", "em"];
    if (this.props.percentage) {
      units.push("%");
    }
    if (this.props.auto) {
      units.push("auto");
    }
    var links = _.map(units, function(u) {
      return <a key={u} onClick={this.handleChangeUnit.bind(this, u)}>{u}</a>;
    }.bind(this));

    return (
      <div className="shogun-unit-text-input-wrapper">
        <label className="control-label">{this.props.label}</label>
        <div className="shogun-unit-text-input">
          <input type="text" ref="inputField" className="form-control input-sm" value={this.state.value} onChange={this.handleChange} />
          {this.renderArrows()}
          <div className="shogun-unit-dropdown-container">
            <DropdownButton tabIndex="-1" small text={this.state.unit}>
              {links}
            </DropdownButton>
          </div>
        </div>
      </div>
    );
  }
});
