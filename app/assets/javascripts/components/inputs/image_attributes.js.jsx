var ImageAttributes = React.createClass({
  handleChange: function(attr, value) {
    var v = {width: this.props.value.width, height: this.props.value.height, url: this.props.value.url, alt_text: this.props.value.alt_text};
    v[attr] = value;
    this.props.onChange(v);
  },
  render: function() {
    return (
      <div className="shogun-image-attributes">
        <TextInput placeholder="Alt text" value={this.props.value.alt_text} onChange={this.handleChange.bind(this, "alt_text")} />
        <Columns>
          <UnitTextInput percentage key="width" value={this.props.value.width} label="width" onChange={this.handleChange.bind(this, "width")} />
          <UnitTextInput percentage key="height" value={this.props.value.height} label="height" onChange={this.handleChange.bind(this, "height")} />
        </Columns>
      </div>
    );
  }
});
