var Checkbox = React.createClass({
  handleChange: function(val) {
    this.props.onChange(val);
  },
  render: function() {
    var content = this.props.icon ? <Icon name={this.props.icon} /> : this.props.label;
    return (
      <div className="checkbox">
        <label>
          <input type="checkbox" checked={this.props.checked} onChange={this.handleChange.bind(this, !this.props.checked)} /> {content}
        </label>
      </div>
    );
  }
})
