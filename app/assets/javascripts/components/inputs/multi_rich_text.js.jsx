var MultiRichText = React.createClass({
  mixins: [MultiInput],

  renderValues: function() {
    return this.getValue().map(function(html, i) {
      return (
        <div key={this.state.rand+i} className="multi-rich-text">
          <button className="btn btn-danger btn-xs pull-right" onClick={this.handleRemove.bind(this, i)} type="button">&times;</button>
          <RichText value={html} onChange={this.handleChange.bind(this, i)} />
        </div>
      );
    }.bind(this));
  }
});
