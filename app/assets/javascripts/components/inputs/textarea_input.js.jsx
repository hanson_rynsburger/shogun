var TextareaInput = React.createClass({
  handleChange: function(e) {
    this.props.onChange(e.target.value);
  },
  render: function() {
    return (
      <textarea
        className="form-control"
        onChange={this.handleChange}
        defaultValue={this.props.value}
        placeholder={this.props.placeholder} />
    );
  }
});
