var Ace = React.createClass({
  onChange: function() {
    var value = this.editor.getValue();
    this.props.onChange(value);
  },
  resize: function() {
    var div = React.findDOMNode(this.refs.editor);
    var $div = $(div);
    var $anc = $div.parent().parent();
    $div.width($anc.width());
    $div.height(Math.min(($(document).height() - $anc.offset().top - 20), ($(window).height() - 218)));
    this.editor.resize();
  },
  componentDidMount: function() {
    var div = React.findDOMNode(this.refs.editor);
    var editor = ace.edit(div)
    this.editor = editor;
    this.resize();
    var session = editor.getSession();
    session.setMode('ace/mode/'+this.props.mode);
    session.setTabSize(2);
    editor.setTheme('ace/theme/xcode');
    editor.setValue(this.props.value);
    editor.setShowPrintMargin(false);
    editor.on('change', this.onChange);
    editor.renderer.setShowGutter(true);
    editor.setOption('readOnly', this.props.readOnly);
    editor.setOption('highlightActiveLine', true);
    editor.setOption('showInvisibles', true);
    $(window).on("resize", this.resize);
  },
  componentWillUnmount: function() {
    this.editor.destroy();
    this.editor = null;
    $(window).off("resize", this.resize);
  },
  componentWillReceiveProps: function(nextProps) {
    this.editor.setOption('readOnly', nextProps.readOnly);
  },
  render: function() {
    return <div ref="editor"></div>;
  }
});
