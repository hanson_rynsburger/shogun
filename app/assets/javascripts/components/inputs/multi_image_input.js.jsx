var MultiImageInput = React.createClass({
  mixins: [MultiInput],

  renderValues: function() {
    return this.getValue().map(function(image, i) {
      return (
        <div key={this.state.rand + i} className="multi-image-input">
          <span className="handle multi-image-input-handle">
            <i className="fa fa-bars"></i>
          </span>
          <button className="btn btn-danger btn-xs pull-right" onClick={this.handleRemove.bind(this, i)} type="button">&times;</button>
          <ImageInput value={image} onChange={this.handleChange.bind(this, i)} />
        </div>
      );
    }.bind(this));
  }
});
