var TextInput = React.createClass({
  handleChange: function(e) {
    this.props.onChange(e.target.value);
  },
  render: function() {
    var cn = "form-control";
    if (this.props.small) {
      cn += " input-sm";
    }
    return (
      <input
        className={cn}
        type="text"
        onChange={this.handleChange}
        value={this.props.value}
        placeholder={this.props.placeholder} />
    );
  }
});
