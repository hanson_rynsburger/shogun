var MultiTextInput = React.createClass({
  mixins: [MultiInput],

  renderValues: function() {
    return this.getValue().map(function(text, i) {
      return (
        <div key={this.state.rand+i} className="input-group input-group-sm multi-text-input">
          <span className="input-group-addon handle multi-text-input-handle">
            <i className="fa fa-bars"></i>
          </span>
          <TextInput value={text} small onChange={this.handleChange.bind(this, i)} />
          <span className="input-group-btn">
            <button className="btn btn-danger" onClick={this.handleRemove.bind(this, i)} type="button">&times;</button>
          </span>
        </div>
      );
    }.bind(this));
  }
});
