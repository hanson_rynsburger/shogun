var RichText = React.createClass({
  handleChange: function(e) {
    this.props.onChange($(React.findDOMNode(this.refs.redactor)).redactor('code.get'));
  },
  componentDidMount: function() {
    $(React.findDOMNode(this.refs.redactor)).redactor({
      blurCallback: this.handleChange,
      changeCallback: this.handleChange,
      keydownCallback: this.handleChange,
      plugins: ['uploadcare'],
      uploadcare: {
        publicKey: window.UPLOADCARE_PUBLIC_KEY,
        buttonLabel: "Image"
      }
    });
  },
  componentWillUnmount: function() {
    $(React.findDOMNode(this.refs.redactor)).redactor('core.destroy');
  },
  render: function() {
    return (
      <textarea ref="redactor" defaultValue={this.props.value}></textarea>
    );
  }
});
