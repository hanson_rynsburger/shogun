var Markdown = React.createClass({
  handleChange: function(e) {
    this.props.onChange(e.target.value);
  },
  render: function() {
    return (
      <div className="markdown">
        <Tabs defaultActive="edit">
          <textarea key="edit" tabName="Edit" className="form-control" onChange={this.handleChange} value={this.props.value} />
          <div key="preview" tabName="Preview" dangerouslySetInnerHTML={{__html: marked(this.props.value)}} />
        </Tabs>
      </div>
    );
  }
});
