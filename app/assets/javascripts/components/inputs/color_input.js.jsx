var ColorInput = React.createClass({
  handleColorPickerChange: function(e) {
    var input = React.findDOMNode(this.refs.input);
    this.props.onChange($(input).colorpicker('getValue', $(input).val()));
  },
  componentDidMount: function() {
    var input = React.findDOMNode(this.refs.input);
    var colors = this.props.colors;
    var h = {};
    _.each(colors, function(hex) {
      h[hex] = "#"+hex;
    })
    $(input).colorpicker({
      align: 'right',
      sliders: {
        alpha: {
            maxTop: 200
        }
      },
      colorSelectors: h
    }).on("changeColor", this.handleColorPickerChange);
  },
  componentWillUnmount: function() {
    $(React.findDOMNode(this.refs.input)).colorpicker('destroy');
  },
  componentDidUpdate: function(prevProps, prevState) {
    if (prevProps.value != this.props.value) {
      var $input = $(React.findDOMNode(this.refs.input));
      $input.colorpicker().off("changeColor", this.handleColorPickerChange);
      $input.colorpicker("setValue", this.props.value);
      $input.colorpicker().on("changeColor", this.handleColorPickerChange);
    }
  },
  handleFocus: function() {
    $(React.findDOMNode(this.refs.input)).colorpicker('show');
  },
  handleBlur: function() {
    $(React.findDOMNode(this.refs.input)).colorpicker('hide');
  },
  handleChange: function(e) {
    this.props.onChange(event.target.value);
  },
  render: function() {
    var value = this.props.value;

    return (
      <div className="form-group">
        <label className="control-label">color</label>
        <div ref="input" className="input-group">
          <input onFocus={this.handleFocus} onBlur={this.handleBlur} className="form-control input-sm" value={value} onChange={this.handleChange} />
          <span className="input-group-addon"><i></i></span>
        </div>
      </div>
    );
  }
});
