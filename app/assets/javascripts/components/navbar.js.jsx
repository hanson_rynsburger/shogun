var Router = ReactRouter;
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

var Navbar = React.createClass({
  mixins: [Router.State],

  userUpdated: function() {
    this.forceUpdate();
  },
  pagesUpdated: function() {
    this.forceUpdate();
  },
  sitesUpdated: function() {
    this.forceUpdate();
  },
  componentDidMount: function() {
    UserStore.on("updated", this.userUpdated);
    PagesStore.on("update", this.pagesUpdated);
    SitesStore.on("update", this.sitesUpdated);
  },
  componentWillUnmount: function() {
    UserStore.off("updated", this.userUpdated);
    PagesStore.off("update", this.pagesUpdated);
    SitesStore.off("update", this.sitesUpdated);
  },
  renderSiteDropdown: function() {
    var ss = SitesStore.getState();
    if (ss.ready) {
      var sites = ss.sites;
      var items = _.map(sites, function(s) {
        return <NavItemLink key={s.id} to="pages" params={{site_id: s.id}}>{s.name}</NavItemLink>;
      });
      var name;
      if (this.props.params.site_id) {
        var site = _.find(sites, function(s) {
          return s.id == this.props.params.site_id;
        }.bind(this));
        name = site.name;
      } else {
        name = "Sites";
      }
      return (
        <li className="dropdown">
          <a className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            {name}
            <span className="caret"></span>
          </a>
          <ul className="dropdown-menu sites-menu" role="menu">
            <li><Link to="new_site">+ New Site</Link></li>
            <li className="divider"></li>
            {items}
          </ul>
        </li>
      );
    }
  },
  renderPageDropdown: function() {
    var ps = PagesStore.getState();
    if (ps.ready && this.props.params.site_id) {
      var pages = ps.pages;
      var items = _.map(pages, function(p) {
        return <NavItemLink key={p.id} to="edit_page" params={{site_id: p.site_id, page_id: p.id}}>{p.name}</NavItemLink>;
      });
      var name;
      if (this.props.params.page_id && !this.isActive("duplicate_page", {site_id: this.props.params.site_id, page_id: this.props.params.page_id})) {
        var page = _.find(pages, function(p) {
          return p.id == this.props.params.page_id;
        }.bind(this));
        name = page.name;
      } else {
        name = "Pages";
      }
      return (
        <li className="dropdown">
          <a className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            {name}
            <span className="caret"></span>
          </a>
          <ul className="dropdown-menu pages-menu" role="menu">
            <li><Link to="new_page" params={{site_id: this.props.params.site_id}}>+ New Page</Link></li>
            <li className="divider"></li>
            {items}
          </ul>
        </li>
      );
    }
  },
  render: function() {
    var user = this.props.user;

    var brandLink;
    if (this.props.params.site_id && !this.isActive("pages", {site_id: this.props.params.site_id})) {
      brandLink = <Link to="pages" params={{site_id: this.props.params.site_id}} className="navbar-brand"><div className="brand" /></Link>
    } else {
      brandLink = <Link to="root" className="navbar-brand"><div className="brand" /></Link>
    }

    return (
      <nav className="navbar navbar-default navbar-fixed-top">
        <div className="container">
          <div className="navbar-header">
            {brandLink}
          </div>

          <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul className="nav navbar-nav">
              {this.renderSiteDropdown()}
              {this.renderPageDropdown()}
            </ul>

            <ul className="nav navbar-nav navbar-right">
              <li><a href="/docs" title="Read documentation">Docs</a></li>
              <li><a href="/tutorials" title="Watch tutorial videos">Tutorials</a></li>
              <NavItemLink to="support">Support</NavItemLink>
              <NavItemLink to="edit_user">{user.first_name} {user.last_name}</NavItemLink>
              <li><a data-method="delete" href="/session">Log Out</a></li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
});
