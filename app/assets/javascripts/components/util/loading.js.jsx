var Loading = React.createClass({
  render: function() {
    return (
      <div className="loader-container">
        <div className="shogun-loader"></div>
      </div>
    );
  }
});
