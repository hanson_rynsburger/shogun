var VariableTypeSelectInput = React.createClass({
  render: function() {
    if (this.props.dropzone) {
      return <SelectInput {...this.props} options={window.TYPES_FOR_SELECT} />
    } else {
      return <SelectInput {...this.props} options={window.TYPES_FOR_SELECT_EXCLUDING_DROPZONE} />
    }
  }
});
