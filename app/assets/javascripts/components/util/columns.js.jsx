var Columns = React.createClass({
  renderColumns: function() {
    var col = 12 / React.Children.count(this.props.children);
    var widths = this.props.widths;
    return React.Children.map(this.props.children, function(child, i) {
      col = widths ? widths[i] : col;
      var cn = "col-xs-" + col;
      return <div key={child.key} className={cn}>{child}</div>;
    });
  },
  render: function() {
    return (
      <div className="row">
        {this.renderColumns()}
      </div>
    );
  }
});
