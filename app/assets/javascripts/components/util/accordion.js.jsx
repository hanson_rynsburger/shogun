var Accordion = React.createClass({
  getInitialState: function() {
    return {active: this.props.active};
  },
  componentWillReceiveProps: function(nextProps) {
    if (nextProps.active != this.state.active) {
      this.setState({active: nextProps.active});
    }
  },
  componentDidMount: function() {
    $('.shogun-style-tooltip[data-toggle="popover"]').popover();
  },
  componentWillUnmount: function() {
    $('.shogun-style-tooltip[data-toggle="popover"]').popover("destroy");
  },
  selectPane: function(key) {
    this.setState({active: key}, function() {
      if (this.props.onChangeTab) {
        this.props.onChangeTab(key);
      }
    }.bind(this));
  },
  getPopoverContent: function(key) {
    var content = {
      'font': '<p>Change font size, color and weight.</p>',
      'basic': '<p>Here you can adjust the basic attributes of the component.</p>',
      'background': '<p>Add color or images to the background of this component.</p>',
      'margins': '<p>Margins control the spacing between this component and other components.</p>',
      'padding': '<p>Padding controls the spacing between this component\'s borders and its internal content.</p>',
      'borders': '<p>Change the thickness and color of the border for this component.</p>',
      'box-shadow': '<p>Change the shadow attributes of this component.</p>',
      'display': '<p>Change visibility settings for different screen sizes.</p>',
      'position': '<p>Change to absolute position and configure placement for different screen sizes.</p>'
    }
    return content[key];
  },
  renderAccordionPanes: function() {
    return React.Children.map(this.props.children, function(child) {
      var cn = "panel-collapse collapse";
      var btn = "btn btn-block";
      if (child.key == this.state.active) {
        cn += " in";
        btn += " disabled";
      }
      return (
        <div className="panel panel-default">
          <div className="panel-heading">
            <a className="shogun-style-tooltip"
            tab-index="0"
            role='button'
            data-animation='false'
            data-content={this.getPopoverContent(child.key)}
            data-html='true'
            data-placement='top'
            data-title={child.props.tabName}
            data-toggle='popover'
            data-trigger='hover'>
              <i className="fa fa-question-circle"></i>
            </a>
            <a className={btn} onClick={this.selectPane.bind(this, child.key)}>{child.props.tabName}</a>
          </div>
          <div className={cn}>
            <div className="panel-body">
              {child}
            </div>
          </div>
        </div>
      );
    }.bind(this));
  },
  render: function() {
    return (
      <div className="panel-group">
        {this.renderAccordionPanes()}
      </div>
    );
  }
});
