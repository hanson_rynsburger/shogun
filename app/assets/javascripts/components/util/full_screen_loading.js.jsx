var FullScreenLoading = React.createClass({
  render: function() {
    return (
      <div className="full-screen-loader">
        <div className="loader-container">
          <div className="shogun-loader"></div>
          <p className="loading-text">Loading...</p>
        </div>
      </div>
    );
  }
});
