var Icon = React.createClass({
  render: function() {
    var iname = "fa fa-" + this.props.name;
    if (this.props.spin) {
      iname += " fa-spin";
    }
    return <i className={iname}></i>;
  }
});
