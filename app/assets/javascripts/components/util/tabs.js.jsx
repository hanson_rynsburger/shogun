var Tabs = React.createClass({
  getInitialState: function() {
    return {active: this.props.active || this.props.defaultActive};
  },
  componentWillReceiveProps: function(nextProps) {
    if (nextProps.active && nextProps.active != this.state.active) {
      this.setState({active: nextProps.active});
    }
  },
  selectPane: function(key) {
    this.setState({active: key}, function() {
      if (this.props.onChangeTab) {
        this.props.onChangeTab(key);
      }
    }.bind(this));
  },
  renderTabs: function() {
    return React.Children.map(this.props.children, function(child) {
      if (child) {
        var tabName = child.props.tabName;
        var content = child.props.tabIcon ? <Icon name={child.props.tabIcon} /> : tabName;
        return (
          <li key={tabName} className={this.classNameForKey(child.key)}>
            <a onClick={this.selectPane.bind(this, child.key)}>{content}</a>
          </li>
        );
      }
    }.bind(this));
  },
  classNameForKey: function(key) {
    return key == this.state.active ? "active" : null;
  },
  renderTabPanes: function() {
    return React.Children.map(this.props.children, function(child) {
      if (child) {
        var cn = "tab-pane";
        var fi = this.classNameForKey(child.key);
        if (fi) {
          cn += " " + fi;
        }
        if (child) {
          return (
            <div key={child.key} className={cn}>
              {child}
            </div>
          );
        }
      }
    }.bind(this));
  },
  render: function() {
    return (
      <div>
        <ul className="nav nav-tabs">
          {this.renderTabs()}
        </ul>
        <div className="tab-content">
          {this.renderTabPanes()}
        </div>
      </div>
    );
  }
});
