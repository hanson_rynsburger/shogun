var DropdownButton = React.createClass({
  renderLinks: function() {
    return React.Children.map(this.props.children, function(child) {
      if (child.key == "divider") {
        return <li key={uuid.v4()} className="divider"></li>;
      } else {
        return <li key={child.key}>{child}</li>;
      }
    });
  },
  render: function() {
    var cn = "btn btn-default dropdown-toggle";
    if (this.props.small) {
      cn += " btn-sm";
    }
    var content = this.props.icon ? <Icon name={this.props.icon} /> : this.props.text;
    return (
      <div className="btn-group">
        <button type="button" className={cn} data-toggle="dropdown" tabIndex={this.props.tabIndex}>
          {content} <span className="caret"></span>
        </button>
        <ul className="dropdown-menu">
          {this.renderLinks()}
        </ul>
      </div>
    );
  }
});
