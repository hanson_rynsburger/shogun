var Router = ReactRouter;
var Navigation = Router.Navigation;
var State = Router.State;

var NavItemLink = React.createClass({
  mixins: [Navigation, State],
  render: function() {
    var active = this.isActive(this.props.to, this.props.params, this.props.query);
    var cn = active ? "active" : null;
    return (
      <li className={cn}><Link to={this.props.to} params={this.props.params}>{this.props.children}</Link></li>
    );
  }
});
