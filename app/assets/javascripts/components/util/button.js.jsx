var Button = React.createClass({
  getButtonElement: function() {
    return React.findDOMNode(this.refs.button);
  },
  componentDidMount: function() {
    if (this.props.tooltip) {
      $(this.getButtonElement()).tooltip({
        animation: false,
        trigger: 'hover',
        container: 'body'
      });
    }
  },
  componentWillUnmount: function() {
    if (this.props.tooltip) {
      $(this.getButtonElement()).tooltip('destroy');
    }
  },
  getContents: function() {
    if (this.props.children) {
      return this.props.children;
    } else if (this.props.icon) {
      return <Icon name={this.props.icon} />;
    } else {
      return this.props.text;
    }
  },
  render: function() {
    var cn = "btn";
    var attrs = {};

    if (this.props.href) {
      attrs.href = this.props.href;
      if (this.props.target) {
        attrs.target = this.props.target;
      }
    }
    if (this.props.small) {
      cn += " btn-sm";
    }
    if (this.props.xs) {
      cn += " btn-xs";
    }
    if (this.props.info) {
      cn += " btn-info";
    } else if (this.props.primary) {
      cn += " btn-primary";
    } else if (this.props.success) {
      cn += " btn-success";
    } else if (this.props.danger) {
      cn += " btn-danger";
    } else if (this.props.warning) {
      cn += " btn-warning";
    } else {
      cn += " btn-default";
    }

    if (this.props.block) {
      cn += " btn-block";
    }

    if (this.props.disabled) {
      cn += " disabled";
    }
    if (this.props.active) {
      cn += " active";
    }
    if (this.props.tooltip) {
      attrs["data-placement"] = this.props.tooltip;
    }
    if (this.props.title) {
      attrs.title = this.props.title;
    }
    if (this.props.className) {
      attrs.className = this.props.className + " " + cn;
    } else {
      attrs.className = cn;
    }

    if (this.props.onMouseUp) {
      attrs.onMouseUp = this.props.onMouseUp;
    }
    if (this.props.onMouseDown) {
      attrs.onMouseDown = this.props.onMouseDown;
    }
    if (this.props.onMouseLeave) {
      attrs.onMouseLeave = this.props.onMouseLeave;
    }

    return <a tabIndex={this.props.tabIndex} ref="button" {...attrs} onClick={this.props.onClick}>{this.getContents()}</a>;
  }
});
