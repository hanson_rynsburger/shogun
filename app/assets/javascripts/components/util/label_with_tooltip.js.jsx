var LabelWithTooltip = React.createClass({
  componentDidMount: function() {
    $(React.findDOMNode(this.refs.label)).tooltip();
  },
  componentWillUnmount: function() {
    $(React.findDOMNode(this.refs.label)).tooltip("destroy");
  },
  getClassName: function() {
    var cn = "label label-tooltip label-";
    if (this.props.info) {
      cn += "info";
    } else if (this.props.success) {
      cn += "success";
    } else if (this.props.warning) {
      cn += "warning";
    } else {
      cn += "default";
    }
    return cn;
  },
  render: function() {
    return (
      <span ref="label" className={this.getClassName()} title={this.props.tooltip} data-placement="top">
        {this.props.label}
      </span>
    );
  }
});
