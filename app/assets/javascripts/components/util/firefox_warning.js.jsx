var FirefoxWarning = React.createClass({
  render: function() {
    return (
      <Alert danger><strong>Warning:</strong> Firefox is not supported! Please use <a href="https://www.google.com/chrome/browser">Google Chrome</a>.</Alert>
    );
  }
});
