var Panel = React.createClass({
  render: function() {
    var cname = "panel";
    if (this.props.danger) {
      cname += " panel-danger";
    } else {
      cname += " panel-default";
    }

    return (
      <div className={cname}>
        <div className="panel-heading">
          <h3 className="panel-title">
            {this.props.title}
          </h3>
        </div>
        <div className="panel-body">
          {this.props.children}
        </div>
      </div>
    );
  }
});
