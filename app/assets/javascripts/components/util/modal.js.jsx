var Modal = React.createClass({
  getModalNode: function() {
    return $(React.findDOMNode(this.refs.modal));
  },
  show: function() {
    this.getModalNode().modal("show");
  },
  hide: function() {
    this.getModalNode().modal("hide");
  },
  componentDidMount: function() {
    if (this.props.show) {
      this.show();
    }
    this.getModalNode().on("shown.bs.modal", function(e) {
      this.props.onShow && this.props.onShow();
    }.bind(this));
    this.getModalNode().on("hidden.bs.modal", function(e) {
      if (this.props.show) {
        this.props.onClose();
      }
    }.bind(this));
  },
  componentWillUnmount: function() {
    this.hide();
    this.getModalNode().off("shown.bs.modal").off("hidden.bs.modal").data("bs.modal", null);
  },
  componentWillReceiveProps: function(nextProps) {
    if (nextProps.show) {
      this.show();
    } else {
      this.hide();
    }
  },
  render: function() {
    var backdrop, keyboard;
    if (this.props.permanent) {
      backdrop = 'static';
      keyboard = 'false';
    }
    var cn = "modal-dialog";
    if (this.props.large) {
      cn += " modal-lg";
    }
    if (this.props.small) {
      cn += " modal-sm";
    }
    return (
      <div ref="modal" className="modal fade" data-backdrop={backdrop} data-keyboard={keyboard}>
        <div className={cn}>
          <div className="modal-content">
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
});

var ModalHeader = React.createClass({
  handleClose: function() {
    this.props.onClose();
  },
  render: function() {
    var closeButton;
    if (!this.props.permanent) {
      closeButton = <button type="button" className="close" onClick={this.handleClose}>&times;</button>
    }
    return (
      <div className="modal-header">
        {closeButton}
        <h4 className="modal-title">{this.props.title}</h4>
      </div>
    );
  },
});

var ModalBody = React.createClass({
  render: function() {
    return <div className="modal-body">{this.props.children}</div>;
  }
});

var ModalFooter = React.createClass({
  render: function() {
    return <div className="modal-footer">{this.props.children}</div>;
  }
});
