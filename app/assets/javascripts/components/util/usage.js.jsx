var Usage = React.createClass({
  badgeClassName: function(num, limit) {
    var cn = "pull-right label label-";
    if (num < limit) {
      cn += "success";
    } else {
      cn += "danger";
    }
    return cn;
  },
  renderBadge: function(num, limit) {
    return (
      <span className={this.badgeClassName(num, limit)}>{num}/{limit}</span>
    );
  },
  render: function() {
    var s = this.props.site;
    return (
      <ul className="list-group">
        <li className="list-group-item">
          Users
          {this.renderBadge(s.users_count, s.max_users)}
        </li>
        <li className="list-group-item">
          Pages
          {this.renderBadge(s.published_pages_count, s.max_pages)}
        </li>
      </ul>
    );
  }
});
