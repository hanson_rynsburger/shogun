var Alert = React.createClass({
  getContents: function() {
    if (this.props.children) {
      return this.props.children;
    } else {
      return this.props.text;
    }
  },
  render: function() {
    var cn = "alert alert-";
    _.each(["danger", "info", "primary", "success", "warning"], function(type) {
      if (this.props[type]) {
        cn += type;
      }
    }.bind(this));

    return (
      <div className={cn}>{this.getContents()}</div>
    );
  }
});
