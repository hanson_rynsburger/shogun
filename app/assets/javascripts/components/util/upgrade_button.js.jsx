var UpgradeButton = React.createClass({
  contextTypes: {
    router: React.PropTypes.func
  },
  upgrade: function() {
    this.context.router.transitionTo("billing");
  },
  render: function() {
    return <Button success text={this.props.text} onClick={this.upgrade} />
  }
});
