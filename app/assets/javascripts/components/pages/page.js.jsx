var Page = React.createClass({
  mixins: [EmbedManager],
  contextTypes: {
    router: React.PropTypes.func
  },
  getInitialState: function() {
    return this.getStateFromStores();
  },

  // location change handlers

  statics: {
    willTransitionFrom: function (transition, component) {
      if (component.changed && !component.state.saving) {
        if (!confirm('You have unsaved information, are you sure you want to leave this page?')) {
          transition.abort();
        }
      }
    },
    willTransitionTo: function (transition, params, query, callback) {
      callback();
    }
  },
  confirmOnPageExit: function(e) {
    if (this.changed) {

      e = e || window.event;

      var message = "You have unsaved information, are you sure you want to leave this page?";

      // For IE6-8 and Firefox prior to version 4
      if (e) {
        e.returnValue = message;
      }

      // For Chrome, Safari, IE8+ and Opera 12+
      return message;
    }
  },

  // helpers

  handleErrors: function(errors) {
    this.setState({saving: false, publishing: false, unpublishing: false}, function() {
      errors.forEach(function(error) {
        AlertsStore.danger(error);
      });
    });
  },
  getStateFromStores: function() {
    var ts = TemplatesStore.getState();
    var ss = SnippetsStore.getState();
    var ms = ModelsStore.getState();
    var as = AddOnsStore.getState();
    var ps = PagesStore.getState();
    var sbs = ShopifyBlogsStore.getState();
    var sts = ShopifyTemplatesStore.getState();

    var filtered = _.filter(as.addOns, function(a) {
      return a.active;
    });

    var ready = (ts.ready && ss.ready && ms.ready && as.ready && ps.ready && sbs.ready && sts.ready);

    return {templates: ts.templates, pages: ps.pages, snippets: ss.snippets, models: ms.models, activeAddOns: filtered, blogs: sbs, shopifyTemplates: sts, ready: ready};
  },
  handleChange: function() {
    this.setState(this.getStateFromStores());
  },

  // modals

  openSaveSnippetModal: function(args) {
    this.setState({snippetArgs: args});
  },
  closeSaveSnippetModal: function() {
    this.setState({snippetArgs: null});
  },
  openDestroySnippetModal: function(sid) {
    this.setState({destroySnippet: sid});
  },
  closeDestroySnippetModal: function() {
    this.setState({destroySnippet: null});
  },
  openVersionsModal: function() {
    this.setState({versions: true});
  },
  closeVersionsModal: function() {
    this.setState({versions: false});
  },

  // backend actions
  save: function(args) {
    this.setState({saving: true}, function() {
      if (args.id) {
        PagesStore.update(args).then(function(page) {
          AlertsStore.success("Page updated.");
          this.setState({saving: false});
        }.bind(this)).fail(this.handleErrors);
      } else {
        PagesStore.create(args).then(function(page) {
          if (args.external_blog_id) {
            AlertsStore.success("Article created.");
            this.context.router.replaceWith("edit_article", {site_id: this.props.site.id, blog_id: args.external_blog_id, page_id: page.id});
          } else {
            AlertsStore.success("Page created.");
            this.context.router.replaceWith("edit_page", {site_id: this.props.site.id, page_id: page.id});
          }
        }.bind(this)).fail(this.handleErrors);
      }
    }.bind(this));
  },
  duplicate: function() {
    if (this.props.params.blog_id) {
      this.context.router.transitionTo("duplicate_article", {site_id: this.props.site.id, blog_id: this.props.params.blog_id, page_id: this.props.params.page_id});
    } else {
      this.context.router.transitionTo("duplicate_page", {site_id: this.props.site.id, page_id: this.props.params.page_id});
    }
  },
  publish: function() {
    this.setState({publishing: true}, function() {
      PagesStore.publish({site_id: this.props.site.id, id: this.props.page.id}).then(function(page) {
        if (this.props.page.external_blog_id) {
          AlertsStore.success("Article published.");
        } else {
          AlertsStore.success("Page published.");
        }
        this.setState({publishing: false});
      }.bind(this)).fail(this.handleErrors);
    }.bind(this));
  },
  unpublish: function() {
    this.setState({unpublishing: true}, function() {
      PagesStore.unpublish({site_id: this.props.site.id, id: this.props.page.id}).then(function(page) {
        if (this.props.page.external_blog_id) {
          AlertsStore.success("Article unpublished.");
        } else {
          AlertsStore.success("Page unpublished.");
        }
        this.setState({unpublishing: false});
      }.bind(this)).fail(this.handleErrors);
    }.bind(this));
  },
  exit: function() {
    this.context.router.transitionTo("pages", {site_id: this.props.site.id});
  },
  saveSnippet: function(args) {
    SnippetsStore.create(args).then(function(snippet) {
      this.setState({snippetArgs: null});
      AlertsStore.success("Snippet created");
    }.bind(this)).fail(this.handleErrors);
  },
  destroySnippet: function() {
    var args = {site_id: this.props.site.id, id: this.state.destroySnippet};
    SnippetsStore.destroy(args).then(function() {
      this.setState({destroySnippet: null});
      AlertsStore.success("Snippet deleted");
    }.bind(this));
  },
  switchVersion: function(versionId) {
    var args = {site_id: this.props.site.id, id: this.props.page.id, version_id: versionId};
    this.setState({saving: true}, function() {
      PagesStore.switchVersion(args).then(function(page) {
        this.setState({versions: false, saving: false});
        AlertsStore.success("Page reverted.");
      }.bind(this)).fail(this.handleErrors);
    }.bind(this));

  },

  // react lifecycle events

  componentDidMount: function() {
    ModelsStore.on("update", this.handleChange);
    TemplatesStore.on("update", this.handleChange);
    SnippetsStore.on("update", this.handleChange);
    AddOnsStore.on("update", this.handleChange);
    PagesStore.on("update", this.handleChange);
    ShopifyBlogsStore.on("update", this.handleChange);
    ShopifyTemplatesStore.on("update", this.handleChange);

    $("html").css("overflow", "hidden");
    $(window).on("beforeunload", this.confirmOnPageExit);
  },
  componentWillUnmount: function() {
    ModelsStore.off("update", this.handleChange);
    TemplatesStore.off("update", this.handleChange);
    SnippetsStore.off("update", this.handleChange);
    AddOnsStore.off("update", this.handleChange);
    PagesStore.off("update", this.handleChange);
    ShopifyBlogsStore.off("update", this.handleChange);

    ShopifyTemplatesStore.off("update", this.handleChange);

    $("html").css("overflow", "");
    $(window).off("beforeunload", this.confirmOnPageExit);
  },

  // embed

  handleMessageData: function(data) {
    if (data.save) {
      this.save(data.save);
    } else if (data.publish) {
      this.publish();
    } else if (data.unpublish) {
      this.unpublish();
    } else if (data.duplicate) {
      this.duplicate();
    } else if (data.exit) {
      this.exit();
    } else if (data.saveSnippet) {
      this.openSaveSnippetModal(data.saveSnippet);
    } else if (data.destroySnippet) {
      this.openDestroySnippetModal(data.destroySnippet);
    } else if (data.versions) {
      this.openVersionsModal();
    } else if (data.shogun) {
      this.setState({shogun: true});
    } else if (data.changed) {
      this.changed = true;
    } else if (data.unchanged) {
      this.changed = false;
    } else if (data.changePage) {
      if (this.props.params.blog_id) {
        this.context.router.transitionTo("edit_article", {site_id: this.props.site.id, blog_id: data.changePage.bid, page_id: data.changePage.pid});
      } else {
        this.context.router.transitionTo("edit_page", {site_id: this.props.site.id, page_id: data.changePage.pid});
      }
    } else if (data.createPage) {
      if (this.props.params.blog_id) {
        this.context.router.transitionTo("new_article", {site_id: this.props.site.id, blog_id: this.props.params.blog_id});
      } else {
        this.context.router.transitionTo("new_page", {site_id: this.props.site.id});
      }
    } else {
      console.log("NOT IMPLEMENTED");
    }
  },
  getSyncArgs: function() {
    return {
      pages: this.state.pages,
      page: this.props.page,
      templates: this.state.templates,
      snippets: this.state.snippets,
      models: this.state.models,
      site: this.props.site,
      saving: this.state.saving,
      activeAddOns: this.state.activeAddOns,
      params: this.props.params,
      blogs: this.state.blogs,
      publishing: this.state.publishing,
      unpublishing: this.state.unpublishing,
      shopifyTemplates: this.state.shopifyTemplates.templates,
      ready: this.state.ready
    };
  },
  getEmbedOrigin: function() {
    if (this.state.embedError) {
      return window.SHOGUN_EDITOR_URL;
    } else if (this.props.site.editor_url) {
      return this.props.site.editor_url;
    } else if (this.props.site.shopify) {
      return window.SHOGUN_EDITOR_URL;
    } else {
      return this.props.site.url;
    }
  },
  getEmbedSource: function() {
    return "outer";
  },
  getEmbedPath: function() {
    if (this.state.embedError) {
      return "/editor/outer";
    } else if (this.props.site.shopify) {
      return "/shopify/editor/outer";
    } else {
      return "/shogun/editor/outer?d=" + this.props.site.digest;
    }
  },

  // rendering

  renderSaveSnippetModal: function() {
    if (this.state.snippetArgs) {
      return (
        <SaveSnippetModal
          args={this.state.snippetArgs}
          onConfirm={this.saveSnippet}
          onClose={this.closeSaveSnippetModal} />
      );
    }
  },
  renderDestroySnippetModal: function() {
    if (this.state.destroySnippet) {
      return (
        <DestroySnippetModal
          onConfirm={this.destroySnippet}
          onClose={this.closeDestroySnippetModal} />
      );
    }
  },
  renderVersionsModal: function() {
    if (this.state.versions) {
      return (
        <VersionsModal
          page={this.props.page}
          onConfirm={this.switchVersion}
          onClose={this.closeVersionsModal}
          saving={this.state.saving} />
      );
    }
  },
  renderModals: function() {
    return (
      <div>
        {this.renderSaveSnippetModal()}
        {this.renderDestroySnippetModal()}
        {this.renderVersionsModal()}
      </div>
    );
  },
  render: function() {
    var loading = !this.state.shogun ? <FullScreenLoading /> : null;
    return (
      <div className="page">
        {loading}
        {this.getEmbed()}
        {this.renderModals()}
      </div>
    );
  }
});
