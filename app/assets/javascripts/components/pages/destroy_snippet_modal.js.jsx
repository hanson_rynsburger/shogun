var DestroySnippetModal = React.createClass({
  handleConfirm: function() {
    this.props.onConfirm();
  },
  render: function() {
    var t = "Are you sure you want to delete this snippet?";
    return (
      <Modal small show={true} onClose={this.props.onClose}>
        <ModalHeader title="Confirm" onClose={this.props.onClose} />
        <ModalBody>
          {t}
        </ModalBody>
        <ModalFooter>
          <a className="btn btn-success" onClick={this.handleConfirm}>Confirm</a>
        </ModalFooter>
      </Modal>
    );
  }
});
