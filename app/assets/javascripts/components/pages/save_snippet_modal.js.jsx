var SaveSnippetModal = React.createClass({
  getInitialState: function() {
    return {name: ""};
  },
  handleConfirm: function(e) {
    e.preventDefault();
    if (this.valid()) {
      var clone = JSON.parse(JSON.stringify(this.props.args));
      clone.name = this.state.name;
      this.props.onConfirm(clone);
    }
  },
  handleChange: function(name) {
    this.setState({name: name});
  },
  valid: function() {
    return this.state.name && this.state.name.length > 0;
  },
  focusName: function() {
    React.findDOMNode(this.refs.name).focus();
  },
  render: function() {
    var cn = "btn btn-success";
    if (!this.valid()) {
      cn += " disabled";
    }
    return (
      <Modal onShow={this.focusName} small show={true} onClose={this.props.onClose}>
        <form onSubmit={this.handleConfirm}>
          <ModalHeader title="Create a new Snippet" onClose={this.props.onClose} />
          <ModalBody>
            <div className="form-group">
              <label className="control-label">Name</label>
              <TextInput ref="name" placeholder="Name" value={this.state.value} onChange={this.handleChange} />
              <p className="help-block">Choose a name for this re-usable Snippet.</p>
            </div>
          </ModalBody>
          <ModalFooter>
            <a className={cn} onClick={this.handleConfirm}>Save</a>
          </ModalFooter>
        </form>
      </Modal>
    );
  }
});
