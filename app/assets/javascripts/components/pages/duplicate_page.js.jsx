var DuplicatePage = React.createClass({
  getInitialState: function() {
    return PagesStore.getState(this.props.params.page_id);
  },
  duplicate: function(page) {
    page.id = null;
    page.updating = false;
    page.outdated = false;
    page.draft = true;
    page.published = false;
    page.path = null;
    page.name = null;

    return page;
  },
  componentWillReceiveProps: function(nextProps) {
    if (nextProps.params.page_id != this.props.params.page_id) {
      this.setState(PagesStore.getState(nextProps.params.page_id));
    }
  },
  handleChange: function() {
    // only bind to changes when we haven't already received the page
    if (!this.state.ready) {
      this.setState(PagesStore.getState(this.props.params.page_id));
    }
  },
  componentDidMount: function() {
    PagesStore.on("update", this.handleChange);
  },
  componentWillUnmount: function() {
    PagesStore.off("update", this.handleChange);
  },
  render: function() {
    if (!this.state.ready) {
      return <Loading />;
    }

    return <RouteHandler page={this.duplicate(this.state.page)} {...this.props} />;
  }
});
