var NewPage = React.createClass({
  getDefaultValue: function(variable) {
    if (variable.type == "complex") {
      return [];
    } else if (variable.default_value) {
      return [variable.default_value];
    } else if (variable.type == "boolean") {
      return [false];
    } else {
      return [""];
    }
  },
  buildValues: function(template, values) {
    var map = _.indexBy(values, function(value) {
      return value.variable_id;
    });

    return _.map(template.variables, function(variable) {
      var v = map[variable.id];
      var r = {
        variable: variable
      };
      if (v) {
        r.value =  v.value || this.getDefaultValue(variable);
        r.references = this.buildReferences(v.references) || [];
      } else {
        r.value = this.getDefaultValue(variable);
        r.references = [];
      }
      return r;
    }.bind(this));
  },
  buildComponent: function(props) {
    var template = _.find(STANDARD_TEMPLATES, function(t) {
      return t.id == props.template_id;
    });

    var u = uuid.v4();
    var styles = _.extend({}, (template.styles || {}), (props.styles || {}));
    return {
      id: u,
      uuid: u,
      template_id: props.template_id,
      template_version_id: props.template_id,
      template_name: template.name,
      values: this.buildValues(template, props.values),
      styles: styles,
      css: template.css,
      liquid: template.liquid,
      js: template.js,
      css_classes: ""
    };
  },
  buildReferences: function(references) {
    return _.map(references, function(ref, i) {
      var comp = this.buildComponent(ref)
      return {component: comp, position: i};
    }.bind(this));
  },
  buildComponents: function(layoutId) {
    if (!layoutId) {
      return [];
    }
    var layout = _.find(LAYOUTS, function(l) {
      return l.id == layoutId;
    });
    if (!layout) {
      return [];
    }

    return this.buildReferences(layout.root);
  },
  getRootProps: function() {
    var layoutId = this.props.query.layout_id;
    return {
      id: null,
      template_id: null,
      values: [
        {
          id: null,
          variable_id: null,
          variable: null,
          value: [],
          references: this.buildComponents(layoutId)
        }
      ]
    }
  },
  getProps: function() {
    return {
      id: null,
      root_component: this.getRootProps(),
      path: "",
      name: "",
      published: false,
      draft: true,
      outdated: false
    }
  },
  render: function() {
    return <RouteHandler page={this.getProps()} {...this.props} />;
  }
});
