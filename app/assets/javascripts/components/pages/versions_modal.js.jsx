var VersionsModal = React.createClass({
  getInitialState: function() {
    return {ready: false, versions: []};
  },
  componentDidMount: function() {
    PagesStore.versionHistory({site_id: this.props.page.site_id, id: this.props.page.id}).then(function(vh) {
      this.setState({versions: vh, ready: true});
    }.bind(this));
  },
  revert: function(version, number) {
    if (!this.props.saving && confirm('Are you sure you want to revert to Version ' + number + '?')) {
      this.props.onConfirm(version.id);
    }
  },
  renderVersions: function() {
    var count = this.state.versions.length;
    var items = this.state.versions.map(function(v, i) {
      return (
        <VersionItem
          key={v.id}
          version={v}
          number={count - i}
          revert={this.revert}/>
      );
    }.bind(this));

    return (
      <ul className="versions-list">
        {items}
      </ul>
    );
  },
  render: function() {
    var content = this.state.ready ? this.renderVersions() : <Loading />;
    return (
      <Modal show={true} onClose={this.props.onClose}>
        <ModalHeader title="Versions" onClose={this.props.onClose} />
        <ModalBody>
          {content}
        </ModalBody>
        <ModalFooter>
          <a className="btn btn-success" onClick={this.props.onClose}>Close</a>
        </ModalFooter>
      </Modal>
    );
  }
});

var VersionItem = React.createClass({
  convertTime: function(datetime) {
    var time = moment(moment.utc(datetime, "YYYY-MM-DDThh:mm").toDate()).calendar();
    return time;
  },
  revert: function() {
    this.props.revert(this.props.version, this.props.number);
  },
  render: function() {
    var version = this.props.version;
    var creator = version.creator.name;

    var revertBtn;
    var versionNumber = 'Version ' + this.props.number;
    if (this.props.version.current_version) {
      versionNumber = 'Current Version';
    } else {
      revertBtn = <a onClick={this.revert}>Revert to this Version</a>
    }
    return (
      <li className="version-item">
        {versionNumber} - Created {this.convertTime(version.created_at)} by {creator}
        <br />
        {revertBtn}
      </li>
    );
  }
});
