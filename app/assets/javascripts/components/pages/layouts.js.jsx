var Layouts = React.createClass({
  // rendering
  renderLink: function(l) {
    if (this.props.params.blog_id) {
      return (
        <Link className="btn btn-primary" to="new_article" query={{layout_id: l.id}} params={{site_id: this.props.site.id, blog_id: this.props.params.blog_id}}><i className="fa fa-plus-circle"></i> New Page</Link>
      );
    } else {
      return (
        <Link className="btn btn-primary" to="new_page" query={{layout_id: l.id}} params={{site_id: this.props.site.id}}><i className="fa fa-plus-circle"></i> New Page</Link>
      );
    }
  },
  renderLayouts: function() {
    return _.map(LAYOUTS, function(l) {
      return (
        <div className="shogun-layout" key={l.id}>
          <h4>{l.name}</h4>
          <p>{l.description}</p>
          {this.renderLink(l)}
          <hr />
        </div>
      );
    }.bind(this));
  },
  render: function() {
    return (
      <div className="shogun-layouts container">
        <h1>Choose a layout</h1>
        <hr />
        {this.renderLayouts()}
      </div>
    );
  }
});
