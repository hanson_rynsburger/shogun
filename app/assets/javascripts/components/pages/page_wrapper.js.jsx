var PageWrapper = React.createClass({
  getInitialState: function() {
    return PagesStore.getState(this.props.params.page_id);
  },
  handleChange: function() {
    this.setState(PagesStore.getState(this.props.params.page_id));
  },
  componentDidMount: function() {
    PagesStore.on("update", this.handleChange);
  },
  componentWillUnmount: function() {
    PagesStore.off("update", this.handleChange);
  },
  componentWillReceiveProps: function(nextProps) {
    if (nextProps.params.page_id != this.props.params.page_id) {
      this.setState(PagesStore.getState(nextProps.params.page_id));
    }
  },
  render: function() {
    if (!this.state.ready) {
      return <FullScreenLoading />;
    }

    return <RouteHandler page={this.state.page} {...this.props} />;
  }
});
