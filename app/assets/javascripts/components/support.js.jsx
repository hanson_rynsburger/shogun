var Support = React.createClass({
  renderFAQ: function() {
    return (
      <div key="faq">
        <h2>FAQ</h2>
        <FAQ />
      </div>
    );
  },
  renderHelp: function() {
    return (
      <div key="help">
        <h2>Need help?</h2>
        <p>Contact us at <a href="mailto:support@getshogun.com">support@getshogun.com</a>.</p>
        <p><em>We typically respond within 24 hours and the team is based in California, USA.</em></p>
      </div>
    );
  },
  render: function() {
    return (
      <div className="support container">
        <Columns widths={[8, 4]}>
          {this.renderFAQ()}
          {this.renderHelp()}
        </Columns>
      </div>
    );
  }
});
