var UserWrapper = React.createClass({
  getInitialState: function() {
    return UserStore.getState();
  },
  handleChange: function() {
    this.setState(UserStore.getState());
  },
  componentDidMount: function() {
    UserStore.on("update", this.handleChange);
  },
  componentWillUnmount: function() {
    UserStore.off("update", this.handleChange);
  },
  render: function() {
    if (!this.state.ready) {
      return <Loading />;
    }

    return <RouteHandler user={this.state.user} {...this.props} />;
  }
});
