var EditUser = React.createClass({
  mixins: [Form],

  getInitialState: function() {
    return {data: Immutable.fromJS(this.props.user)};
  },
  handleChange: function(attr, value) {
    this.setState({data: this.state.data.set(attr, value), changed: true});
  },
  save: function(e) {
    this.handleSubmit(e, function() {
      UserStore.update(this.state.data.toJS()).then(function(user) {
        AlertsStore.success("Account updated.");
        this.setState({data: Immutable.fromJS(user), saving: false, changed: false});
      }.bind(this)).fail(this.handleErrors);
    });
  },
  renderPassword: function() {
    if (this.state.data.get("has_password")) {
      return (
        <div>
          <div className="form-group">
            <label className="control-label">Password</label>
            <PasswordInput value={this.state.data.get("password")} onChange={this.handleChange.bind(this, "password")} />
          </div>
          <div className="form-group">
            <label className="control-label">Password Confirmation</label>
            <PasswordInput value={this.state.data.get("password_confirmation")} onChange={this.handleChange.bind(this, "password_confirmation")} />
          </div>
        </div>
      );
    }
  },
  renderForm: function() {
    var cname = "btn btn-primary";
    if (this.state.saving || !this.state.changed) {
      cname += " disabled";
    }
    return (
      <form onSubmit={this.save}>
        <div className="form-group">
          <label className="control-label">First Name</label>
          <TextInput value={this.state.data.get("first_name")} onChange={this.handleChange.bind(this, "first_name")} />
        </div>
        <div className="form-group">
          <label className="control-label">Last Name</label>
          <TextInput value={this.state.data.get("last_name")} onChange={this.handleChange.bind(this, "last_name")} />
        </div>
        <div className="form-group">
          <label className="control-label">Email</label>
          <TextInput value={this.state.data.get("email")} onChange={this.handleChange.bind(this, "email")} />
        </div>
        <div className="form-group">
          <label className="control-label">Company Name</label>
          <TextInput value={this.state.data.get("company_name")} onChange={this.handleChange.bind(this, "company_name")} />
        </div>
        {this.renderPassword()}
        <button type="submit" className={cname}>Submit</button>
      </form>
    );
  },
  render: function() {
    return (
      <div className="settings container">
        <h2>Settings</h2>
        <div className="row">
          <div className="col-sm-6">
            {this.renderForm()}
          </div>
        </div>
      </div>
    );
  }
});
