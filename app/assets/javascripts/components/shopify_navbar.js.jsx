var Router = ReactRouter;
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

var ShopifyNavbar = React.createClass({
  mixins: [Router.State],

  pagesUpdated: function() {
    this.forceUpdate();
  },
  componentDidMount: function() {
    PagesStore.on("update", this.pagesUpdated);
  },
  componentWillUnmount: function() {
    PagesStore.off("update", this.pagesUpdated);
  },
  renderPageDropdown: function() {
    var ps = PagesStore.getState();
    if (ps.ready) {
      var pages = _(ps.pages).filter(function(obj) {
        return obj.external_blog_id == null;
      });

      var items = _.map(pages, function(p) {
        return <NavItemLink key={p.id} to="edit_page" params={{page_id: p.id}}>{p.name}</NavItemLink>;
      });
      var name;
      if (this.props.params.page_id && !this.isActive("duplicate_page", {page_id: this.props.params.page_id})) {
        var page = _.find(pages, function(p) {
          return p.id == this.props.params.page_id;
        }.bind(this));
        name = page.name;
      } else {
        name = "Pages";
      }
      return (
        <li className="dropdown">
          <a className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            {name}
            <span className="caret"></span>
          </a>
          <ul className="dropdown-menu pages-menu" role="menu">
            <li><Link to="new_page">+ New Page</Link></li>
            <li className="divider"></li>
            {items}
          </ul>
        </li>
      );
    }
  },
  renderArticleDropdown: function() {
    var ps = PagesStore.getState();

    if (ps.ready) {
      var articles = _(ps.pages).filter(function(obj) {
        return obj.external_blog_id != null;
      });

      var items = _.map(articles, function(p) {
        if (window.SHOPIFY_SITE.features.blogs) {
          return <NavItemLink key={p.id} to="edit_article" params={{page_id: p.id, blog_id: p.external_blog_id}}>{p.name}</NavItemLink>;
        } else {
          return <NavItemLink key={p.id} to="blogs">{p.name}</NavItemLink>;
        }
      });
      var name;
      if (this.props.params.page_id && !this.isActive("duplicate_article", {page_id: this.props.params.page_id, blog_id: this.props.params.blog_id})) {
        var article = _.find(articles, function(p) {
          return p.id == this.props.params.page_id;
        }.bind(this));
        name = article.name;
      } else {
        name = "Articles";
      }
      return (
        <li className="dropdown">
          <a className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            {name}
            <span className="caret"></span>
          </a>
          <ul className="dropdown-menu pages-menu" role="menu">
            <li><Link to="blogs">+ New Article</Link></li>
            <li className="divider"></li>
            {items}
          </ul>
        </li>
      );
    }
  },
  render: function() {
    return (
      <nav className="navbar navbar-default navbar-fixed-top">
        <div className="container">
          <div className="navbar-header">
            <Link to="pages" className="navbar-brand"><div className="brand" /></Link>
          </div>

          <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul className="nav navbar-nav">
              {this.renderPageDropdown()}
              {this.renderArticleDropdown()}
            </ul>

            <ul className="nav navbar-nav navbar-right">
              <li><a href="/docs" title="Read documentation">Docs</a></li>
              <li><a href="/tutorials" title="Watch tutorial videos">Tutorials</a></li>
              <NavItemLink to="support">Support</NavItemLink>
              <li><a data-method="delete" href="/session">Log Out</a></li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
});
