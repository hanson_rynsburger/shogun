Liquid.Template.registerFilter({
  float: function(i) {
    return parseFloat(i) || 0.0;
  },
  int: function(i) {
    return parseInt(i) || 0;
  },
  string: function(i) {
    if (i || i.toString) {
      return i.toString();
    }
    return "";
  },
  ceil: function(i) {
    return Math.ceil(i);
  },
  round: function(i) {
    return Math.round(i);
  },
  floor: function(i) {
    return Math.floor(i);
  }
});
