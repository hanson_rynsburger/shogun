class AddOnsStore extends EventEmitter
  constructor: ->
    @ready = false
    @addOns = Immutable.fromJS([])

  getState: (tid) ->
    if tid
      t = @_find tid
      if t?
        {addOn: t.toJS(), ready: true}
      else
        {addOn: {}, ready: false}
    else
      {addOns: @addOns.toJS(), ready: @ready}

  _find: (addOnId) ->
    @addOns.find (addOn) ->
      addOn.get("id") == addOnId

  setSiteId: (siteId) ->
    if siteId != @siteId
      @siteId = siteId
      @ready = false
      @addOns = Immutable.fromJS([])
      @emit "update"
      if @siteId?
        @list().then (addOns) =>
          @ready = true
          @addOns = Immutable.fromJS(addOns)
          @emit "update"

  list: () ->
    p = $.Deferred()

    $.getJSON "/api/sites/#{@siteId}/add_ons", (data) =>
      p.resolve data.add_ons

    p

  _create: (addOn) ->
    if addOn.site_id == @siteId
      @addOns = @addOns.push Immutable.fromJS(addOn)
      @emit "update"

  create: (args) ->
    p = $.Deferred()

    $.ajax
      type: "POST"
      contentType: "application/json"
      url: "/api/sites/#{args.site_id}/add_ons"
      data: JSON.stringify({add_on: args})
      success: (data) =>
        @_create data.add_on
        p.resolve data.add_on
      error: (data) ->
        p.reject data.responseJSON.errors

    p

  _update: (addOn) ->
    if addOn.site_id == @siteId
      @addOns = @addOns.map (p) ->
        if p.get("id") == addOn.id
          Immutable.fromJS(addOn)
        else
          p
      @emit "update"

  update: (args) ->
    p = $.Deferred()

    $.ajax
      type: "PUT"
      contentType: "application/json"
      url: "/api/sites/#{args.site_id}/add_ons/#{args.id}"
      data: JSON.stringify({add_on: args})
      success: (data) =>
        @_update(data.add_on)
        p.resolve data.add_on
      error: (data) ->
        p.reject data.responseJSON.errors

    p

window.AddOnsStore = new AddOnsStore()
