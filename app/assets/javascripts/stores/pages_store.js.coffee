class PagesStore extends EventEmitter
  constructor: ->
    @pages = Immutable.fromJS([])
    @ready = false

  getState: (pid) ->
    if pid
      page = @_find pid
      if page
        if page.get("root_component")
          {page: page.toJS(), ready: true}
        else
          @_reload pid
          {page: {}, ready: false}
      else
        {page: {}, ready: false}
    else
      {pages: @pages.toJS(), ready: @ready}

  _find: (pageId) ->
    @pages.find (page) ->
      page.get("id") == pageId

  setSiteId: (siteId) ->
    if siteId != @siteId
      @siteId = siteId
      @ready = false
      @pages = Immutable.fromJS([])
      @emit "update"
      if @siteId?
        @list().then (pages) =>
          @ready = true
          @pages = Immutable.fromJS(pages)
          @emit "update"


  list: () ->
    p = $.Deferred()

    $.getJSON "/api/sites/#{@siteId}/pages", (data) ->
      p.resolve data.pages

    p

  versionHistory: (args) ->
    p = $.Deferred()

    $.getJSON "/api/sites/#{args.site_id}/pages/#{args.id}/version_history", (data) ->
      p.resolve data.version_history

    p

  _create: (page) ->
    if page.site_id == @siteId
      @pages = @pages.push Immutable.fromJS(page)
      @emit "update"

  create: (args) ->
    p = $.Deferred()

    $.ajax
      type: "POST"
      contentType: "application/json"
      url: "/api/sites/#{args.site_id}/pages"
      data: JSON.stringify({page: args})
      success: (data) =>
        @_create data.page
        p.resolve data.page
      error: (data) ->
        p.reject data.responseJSON.errors

    p

  _update: (page) ->
    if page.site_id == @siteId
      @pages = @pages.map (p) ->
        if p.get("id") == page.id && page.version_number >= p.get("version_number")
          Immutable.fromJS(page)
        else
          p
      @emit "update"

  _reload: (id) ->
    $.getJSON("/api/sites/#{@siteId}/pages/#{id}").then (data) =>
      @_update data.page

  updated: (page) ->
    @_update page

  update: (args) ->
    p = $.Deferred()
    $.ajax
      type: "PUT"
      contentType: "application/json"
      url: "/api/sites/#{args.site_id}/pages/#{args.id}"
      data: JSON.stringify({page: args})
      success: (data) =>
        @_update(data.page)
        p.resolve data.page
      error: (data) ->
        p.reject data.responseJSON.errors

    p

  switchVersion: (args) ->
    p = $.Deferred()

    $.ajax
      type: "POST"
      url: "/api/sites/#{args.site_id}/pages/#{args.id}/switch_version?version_id=#{args.version_id}"
      success: (data) =>
        @_update(data.page)
        p.resolve data.page
      error: (data) ->
        p.reject data.responseJSON.errors

    p

  _destroy: (site_id, id) ->
    if site_id == @siteId
      @pages = @pages.filterNot (c) ->
        c.get("id") == id
      @emit "update"

  destroy: (args) ->
    p = $.Deferred()

    $.ajax
      type: "DELETE"
      url: "/api/sites/#{args.site_id}/pages/#{args.id}"
      success: (data) =>
        @_destroy args.site_id, args.id
        p.resolve()

    p

  publish: (args) ->
    p = $.Deferred()

    $.ajax
      type: "POST"
      url: "/api/sites/#{args.site_id}/pages/#{args.id}/publish"
      success: (data) =>
        @_update(data.page)
        p.resolve()
      error: (data) ->
        p.reject()

    p

  unpublish: (args) ->
    p = $.Deferred()

    $.ajax
      type: "POST"
      url: "/api/sites/#{args.site_id}/pages/#{args.id}/unpublish"
      success: (data) =>
        @_update(data.page)
        p.resolve()
      error: (data) ->
        p.reject()

    p

window.PagesStore = new PagesStore()
