class SnippetsStore extends EventEmitter
  constructor: ->
    @ready = false
    @snippets = Immutable.fromJS([])

  getState: (sid) ->
    if sid
      s = @_find sid
      if s?
        {snippet: s.toJS(), ready: true}
      else
        {snippet: {}, ready: false}
    else
      {snippets: @snippets.toJS(), ready: @ready}

  _find: (snippetId) ->
    @snippets.find (snippet) ->
      snippet.get("id") == snippetId

  setSiteId: (siteId) ->
    if siteId != @siteId
      @siteId = siteId
      @ready = false
      @snippets = Immutable.fromJS([])
      @emit "update"
      if @siteId?
        @list().then (snippets) =>
          @ready = true
          @snippets = Immutable.fromJS(snippets)
          @emit "update"

  list: () ->
    p = $.Deferred()

    $.getJSON "/api/sites/#{@siteId}/snippets", (data) =>
      p.resolve data.snippets

    p

  _create: (snippet) ->
    if snippet.site_id == @siteId
      @snippets = @snippets.push Immutable.fromJS(snippet)
      @emit "update"

  create: (args) ->
    p = $.Deferred()

    $.ajax
      type: "POST"
      contentType: "application/json"
      url: "/api/sites/#{args.site_id}/snippets"
      data: JSON.stringify({snippet: args})
      success: (data) =>
        @_create data.snippet
        p.resolve data.snippet
      error: (data) ->
        p.reject data.responseJSON.errors

    p

  _destroy: (site_id, id) ->
    if site_id == @siteId
      @snippets = @snippets.filterNot (c) ->
        c.get("id") == id
      @emit "update"

  destroy: (args) ->
    p = $.Deferred()

    $.ajax
      type: "DELETE"
      url: "/api/sites/#{args.site_id}/snippets/#{args.id}"
      success: (data) =>
        @_destroy(args.site_id, args.id)
        p.resolve()

    p

window.SnippetsStore = new SnippetsStore()
