class ShopifyArticlesStore extends EventEmitter
  constructor: ->
    @articles = Immutable.fromJS([])
    @ready = false

  getState: () ->
    {articles: @articles.toJS(), ready: @ready}

  setSiteId: (siteId) ->
    if siteId != @siteId
      @siteId = siteId
      @ready = false
      @articles = Immutable.fromJS([])
      @emit "update"
      if @siteId?
        @list().then (articles) =>
          @ready = true
          @articles = Immutable.fromJS(articles)
          @emit "update"

  list: () ->
    p = $.Deferred()

    $.getJSON "/api/sites/#{@siteId}/shopify_articles", (data) ->
      p.resolve data.shopify_articles

    p

  _remove: (id) ->
    @articles = @articles.filter (p) ->
      p.get("id") != id
    @emit "update"

  import: (args) ->
    p = $.Deferred()

    $.ajax
      type: "POST"
      contentType: "application/json"
      data: JSON.stringify({ blog_id: args.blog_id })
      url: "/api/sites/#{args.site_id}/shopify_articles/#{args.id}/import"
      success: (data) =>
        @_remove args.id

        if PagesStore.getState(data.page.id).ready
          PagesStore._update data.page
        else
          PagesStore._create data.page

        p.resolve data.page
      error: (data) ->
        p.reject data.responseJSON.errors

    p

window.ShopifyArticlesStore = new ShopifyArticlesStore()
