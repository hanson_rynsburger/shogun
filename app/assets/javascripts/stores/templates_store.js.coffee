class TemplatesStore extends EventEmitter
  constructor: ->
    @ready = false
    @templates = Immutable.fromJS([])

  getState: (tid) ->
    if tid
      t = @_find tid
      if t?
        {template: t.toJS(), ready: true}
      else
        {template: {}, ready: false}
    else
      {templates: @templates.toJS(), ready: @ready}

  _find: (templateId) ->
    @templates.find (template) ->
      template.get("id") == templateId

  setSiteId: (siteId) ->
    if siteId != @siteId
      @siteId = siteId
      @ready = false
      @templates = Immutable.fromJS([])
      @emit "update"
      if @siteId?
        @list().then (templates) =>
          @ready = true
          @templates = Immutable.fromJS(templates)
          @emit "update"

  list: () ->
    p = $.Deferred()

    $.getJSON "/api/sites/#{@siteId}/templates", (data) =>
      p.resolve data.templates

    p

  _create: (template) ->
    if template.site_id == @siteId
      @templates = @templates.push Immutable.fromJS(template)
      @emit "update"

  create: (args) ->
    p = $.Deferred()

    $.ajax
      type: "POST"
      contentType: "application/json"
      url: "/api/sites/#{args.site_id}/templates"
      data: JSON.stringify({template: args})
      success: (data) =>
        @_create data.template
        p.resolve data.template
      error: (data) ->
        p.reject data.responseJSON.errors

    p

  _update: (template) ->
    if template.site_id == @siteId
      @templates = @templates.map (p) ->
        if p.get("id") == template.id && template.version_number > p.get("version_number")
          Immutable.fromJS(template)
        else
          p
      @emit "update"

  _destroy: (site_id, id) ->
    if site_id == @siteId
      @templates = @templates.filterNot (c) ->
        c.get("id") == id
      @emit "update"

  update: (args) ->
    p = $.Deferred()

    $.ajax
      type: "PUT"
      contentType: "application/json"
      url: "/api/sites/#{args.site_id}/templates/#{args.id}"
      data: JSON.stringify({template: args})
      success: (data) =>
        @_update(data.template)
        p.resolve data.template
      error: (data) ->
        p.reject data.responseJSON.errors

    p

  destroy: (args) ->
    p = $.Deferred()

    $.ajax
      type: "DELETE"
      url: "/api/sites/#{args.site_id}/templates/#{args.id}"
      success: (data) =>
        @_destroy(args.site_id, args.id)
        p.resolve()

    p

  parse: (args, liquid) ->
    p = $.Deferred()

    $.ajax
      type: "POST"
      contentType: "application/json"
      url: "/api/sites/#{args.site_id}/templates/parse"
      data: JSON.stringify({liquid: liquid})
      success: (data) ->
        p.resolve data
      error: (data) ->
        p.reject data.responseJSON.errors

    p

window.TemplatesStore = new TemplatesStore()
