class CollaboratorsStore extends EventEmitter
  constructor: ->
    @ready = false
    @collaborators = Immutable.fromJS([])

  getState: ->
    {collaborators: @collaborators.toJS(), ready: @ready}

  setSiteId: (siteId) ->
    if siteId != @siteId
      @siteId = siteId
      @ready = false
      @collaborators = Immutable.fromJS([])
      @emit "update"
      if @siteId?
        @list().then (collaborators) =>
          @ready = true
          @collaborators = Immutable.fromJS(collaborators)
          @emit "update"

  list: () ->
    p = $.Deferred()

    $.getJSON "/api/sites/#{@siteId}/collaborators", (data) =>
      p.resolve data.collaborators

    p

  _update: (collaborator) ->
    if collaborator.site_id == @siteId
      @collaborators = @collaborators.map (p) ->
        if p.get("id") == collaborator.id
          Immutable.fromJS(collaborator)
        else
          p
      @emit "update"

  update: (args) ->
    p = $.Deferred()

    $.ajax
      url: "/api/sites/#{args.site_id}/collaborators/#{args.id}"
      type: "PUT"
      data: JSON.stringify({collaborator: args})
      contentType: "application/json"
      success: (data) =>
        @_update data.collaborator
        p.resolve data.collaborator
      error: (data) ->
        p.reject data.responseJSON.errors

    p

  created: (collaborator) ->
    if collaborator.site_id == @siteId
      @collaborators = @collaborators.push Immutable.fromJS(collaborator)
      @emit "update"

  _destroy: (site_id, id) ->
    if site_id == @siteId
      @collaborators = @collaborators.filterNot (c) ->
        c.get("id") == id
      @emit "update"

  destroy: (args) ->
    p = $.Deferred()

    $.ajax
      type: "DELETE"
      url: "/api/sites/#{args.site_id}/collaborators/#{args.id}"
      success: (data) =>
        @_destroy args.site_id, args.id
        p.resolve()

    p

window.CollaboratorsStore = new CollaboratorsStore()
