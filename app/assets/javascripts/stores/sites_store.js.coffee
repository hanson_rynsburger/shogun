class SitesStore extends EventEmitter
  constructor: ->
    @ready = false
    @sites = Immutable.fromJS([])

    if !window.SHOPIFY_SITE
      @list().then (sites) =>
        @ready = true
        @sites = Immutable.fromJS(sites)
        @emit "update"

  getState: (sid) ->
    if sid
      site = @_find sid
      if site
        {ready: true, site: site.toJS()}
      else
        {ready: false, site: {}}
    else
      {ready: @ready, sites: @sites.toJS()}

  _find: (siteId) ->
    @sites.find (site) ->
      site.get("id") == siteId

  list: ->
    p = $.Deferred()

    $.getJSON "/api/sites", (data) =>
      p.resolve data.sites

    p

  _create: (site) ->
    @sites = @sites.push Immutable.fromJS(site)
    @emit "update"

  create: (args) ->
    p = $.Deferred()

    $.ajax
      type: "POST"
      contentType: "application/json"
      url: "/api/sites"
      data: JSON.stringify({site: args})
      success: (data) =>
        @_create data.site
        p.resolve data.site
      error: (data) ->
        p.reject data.responseJSON.errors

    p

  _update: (site) ->
    @sites = @sites.map (p) ->
      if p.get("id") == site.id && site.version_number > p.get("version_number")
        Immutable.fromJS(site)
      else
        p
    @emit "update"

  updated: (site) ->
    @_update site

  update: (args) ->
    p = $.Deferred()

    $.ajax
      url: "/api/sites/#{args.id}"
      type: "PUT"
      data: JSON.stringify({site: args})
      contentType: "application/json"
      success: (data) =>
        @_update data.site
        p.resolve data.site
      error: (data) ->
        p.reject data.responseJSON.errors

    p

  addUser: (args) ->
    p = $.Deferred()

    $.ajax
      type: "POST"
      contentType: "application/json"
      url: "/api/sites/#{args.id}/users"
      data: JSON.stringify(args)
      success: (data) =>
        if data.collaborator
          CollaboratorsStore.created data.collaborator
        else if data.invite
          InvitesStore.created data.invite
        p.resolve()

      error: (data) ->
        p.reject data.responseJSON.errors

    p


window.SitesStore = new SitesStore()
