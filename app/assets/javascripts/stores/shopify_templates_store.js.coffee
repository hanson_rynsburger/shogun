class ShopifyTemplatesStore extends EventEmitter
  constructor: ->
    @temps = Immutable.fromJS({page: [], article: []})
    @ready = false

  getState: () ->
    {templates: @temps.toJS(), ready: @ready}

  setSiteId: (siteId) ->
    if siteId != @siteId
      @siteId = siteId
      @ready = false
      @temps = Immutable.fromJS({page: [], article: []})
      @emit "update"
      if @siteId?
        @list().then (templates) =>
          @ready = true
          @temps = Immutable.fromJS(templates)
          @emit "update"

  list: () ->
    p = $.Deferred()

    $.getJSON "/api/sites/#{@siteId}/shopify_templates", (data) ->
      p.resolve data.shopify_templates

    p

window.ShopifyTemplatesStore = new ShopifyTemplatesStore()
