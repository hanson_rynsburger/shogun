class ModelsStore extends EventEmitter
  constructor: ->
    @models = Immutable.fromJS([])
    @ready = false

  getState: (mid) ->
    if mid
      model = @_find mid
      if model
        {model: model.toJS(), ready: true}
      else
        {model: {}, ready: false}
    else
      {models: @models.toJS(), ready: @ready}

  _find: (modelId) ->
    @models.find (model) ->
      model.get("id") == modelId

  setSiteId: (siteId) ->
    if siteId != @siteId
      @siteId = siteId
      @ready = false
      @models = Immutable.fromJS([])
      @emit "update"
      if @siteId?
        @list().then (models) =>
          @ready = true
          @models = Immutable.fromJS(models)
          @emit "update"

  list: () ->
    p = $.Deferred()

    $.getJSON "/api/sites/#{@siteId}/models", (data) =>
      p.resolve data.models

    p

  _create: (model) ->
    if model.site_id == @siteId
      @models = @models.push Immutable.fromJS(model)
      @emit "update"

  _entryCreated: (model_id, entry) ->
    m = @_find(model_id)
    if m
      up = m.toJS()
      up.entries.push(entry)
      @_update up

  _entryUpdated: (model_id, entry) ->
    m = @_find(model_id)
    if m
      up = m.toJS()
      up.entries = _.map up.entries, (e) ->
        if e.id == entry.id
          entry
        else
          e
      @_update up

  _entryDestroyed: (model_id, entry) ->
    m = @_find(model_id)
    if m
      up = m.toJS()
      up.entries = _.filter up.entries, (e) ->
        e.id != entry.id
      @_update up

  createEntry: (args) ->
    p = $.Deferred()

    $.ajax
      type: "POST"
      contentType: "application/json"
      url: "/api/sites/#{args.site_id}/models/#{args.model_id}/entries"
      data: JSON.stringify({entry: args})
      success: (data) =>
        @_entryCreated args.model_id, data.entry
        p.resolve data.entry
      error: (data) ->
        p.reject data.responseJSON.errors

    p

  updateEntry: (args) ->
    p = $.Deferred()

    $.ajax
      type: "PUT"
      contentType: "application/json"
      url: "/api/sites/#{args.site_id}/models/#{args.model_id}/entries/#{args.id}"
      data: JSON.stringify({entry: args})
      success: (data) =>
        @_entryUpdated args.model_id, data.entry
        p.resolve data.entry
      error: (data) ->
        p.reject data.responseJSON.errors

    p


  destroyEntry: (args) ->
    p = $.Deferred()

    $.ajax
      type: "DELETE"
      url: "/api/sites/#{args.site_id}/models/#{args.model_id}/entries/#{args.id}"
      success: (data) =>
        @_entryDestroyed(args.model_id, args)
        p.resolve()
      error: (data) ->
        p.reject()

    p

  create: (args) ->
    p = $.Deferred()

    $.ajax
      type: "POST"
      contentType: "application/json"
      url: "/api/sites/#{args.site_id}/models"
      data: JSON.stringify({model: args})
      success: (data) =>
        @_create data.model
        p.resolve data.model
      error: (data) ->
        p.reject data.responseJSON.errors

    p

  _update: (model) ->
    if model.site_id == @siteId
      @models = @models.map (p) ->
        if p.get("id") == model.id && model.version_number >= p.get("version_number")
          Immutable.fromJS(model)
        else
          p
      @emit "update"

  update: (args) ->
    p = $.Deferred()
    $.ajax
      type: "PUT"
      contentType: "application/json"
      url: "/api/sites/#{args.site_id}/models/#{args.id}"
      data: JSON.stringify({model: args})
      success: (data) =>
        @_update(data.model)
        p.resolve data.model
      error: (data) ->
        p.reject data.responseJSON.errors

    p

window.ModelsStore = new ModelsStore()
