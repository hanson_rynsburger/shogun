class InvitesStore extends EventEmitter
  constructor: ->
    @invites = Immutable.fromJS([])
    @ready = false

  getState: ->
    {invites: @invites.toJS(), ready: @ready}

  setSiteId: (siteId) ->
    if siteId != @siteId
      @siteId = siteId
      @ready = false
      @invites = Immutable.fromJS([])
      @emit "update"
      if @siteId?
        @list().then (invites) =>
          @ready = true
          @invites = Immutable.fromJS(invites)
          @emit "update"

  list: () ->
    p = $.Deferred()

    $.getJSON "/api/sites/#{@siteId}/invites", (data) =>
      p.resolve data.invites

    p

  created: (invite) ->
    if invite.site_id == @siteId
      @invites = @invites.push Immutable.fromJS(invite)
      @emit "update"

  _destroy: (site_id, id) ->
    if site_id == @siteId
      @invites = @invites.filterNot (c) ->
        c.get("id") == id
      @emit "update"

  destroy: (args) ->
    p = $.Deferred()

    $.ajax
      type: "DELETE"
      url: "/api/sites/#{args.site_id}/invites/#{args.id}"
      success: (data) =>
        @_destroy args.site_id, args.id
        p.resolve()

    p

window.InvitesStore = new InvitesStore()
