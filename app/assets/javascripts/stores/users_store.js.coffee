class UsersStore extends EventEmitter
  constructor: ->
    @state = Immutable.fromJS({admin: false, users: 0})

    CollaboratorsStore.on "update", =>
      @_update()

    InvitesStore.on "update", =>
      @_update()

    UserStore.on "update", =>
      @_update()

  getState: ->
    @state.toJS()

  _update: ->
    users = CollaboratorsStore.getState().collaborators.length + InvitesStore.getState().invites.length

    user = UserStore.getState().user

    cs = CollaboratorsStore.getState()
    admin = cs.collaborators.some (collaborator) ->
      collaborator.user.id == user.id && collaborator.admin

    @state = Immutable.fromJS({users: users, admin: admin})
    @emit "update"

window.UsersStore = new UsersStore()
