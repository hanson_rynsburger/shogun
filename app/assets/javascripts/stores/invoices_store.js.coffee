class InvoicesStore extends EventEmitter
  constructor: ->
    @invoices = Immutable.fromJS([])
    @ready = false

  getState: ->
    {invoices: @invoices.toJS(), ready: @ready}

  setSiteId: (siteId) ->
    if siteId != @siteId
      @siteId = siteId
      @ready = false
      @invoices = Immutable.fromJS([])
      @emit "update"
      if @siteId?
        @list().then (invoices) =>
          @ready = true
          @invoices = Immutable.fromJS(invoices)
          @emit "update"

  list: () ->
    p = $.Deferred()

    $.getJSON "/api/sites/#{@siteId}/invoices", (data) =>
      p.resolve data.invoices

    p

  _update: (invoice) ->
    @invoices = @invoices.map (i) ->
      if i.get("id") == invoice.id
        Immutable.fromJS(invoice)
      else
        i
    @emit "update"

  pay: (args) ->
    p = $.Deferred()

    $.ajax
      type: "POST"
      contentType: "application/json"
      url: "/api/sites/#{@siteId}/invoices/#{args.id}/pay"
      data: JSON.stringify({stripe_token: args.stripe_token})
      success: (data) =>
        @_update data.invoice
        p.resolve data.invoice
      error: (data) ->
        p.reject data.responseJSON.errors

    p

window.InvoicesStore = new InvoicesStore()
