class AlertsStore
  handleAlert: (type, message, suc) ->
    if $('.page iframe').length > 0
      el = '.page'
    else
      el = 'body'

    $.bootstrapGrowl(message, {
      ele: el,
      type: type,
      allow_dismiss: true,
      width: 'auto',
      delay: 2000
    });

  success: (message) ->
    @handleAlert "success", message, true

  info: (message) ->
    @handleAlert "info", message, true

  warning: (message) ->
    @handleAlert "warning", message

  danger: (message) ->
    @handleAlert "danger", message

window.AlertsStore = new AlertsStore()
