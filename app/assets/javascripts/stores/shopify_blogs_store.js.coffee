class ShopifyBlogsStore extends EventEmitter
  constructor: ->
    @blogs = Immutable.fromJS([])
    @ready = false
    @selected = ''

  getState: () ->
    {blogs: @blogs.toJS(), selected: @selected, ready: @ready}

  setSiteId: (siteId) ->
    if siteId != @siteId
      @siteId = siteId
      @ready = false
      @blogs = Immutable.fromJS([])
      @emit "update"
      if @siteId?
        @list().then (blogs) =>
          @ready = true
          @blogs = Immutable.fromJS(blogs)
          @selected = blogs[0][1] if blogs.length > 0
          @emit "update"

  setBlogSelected: (val) ->
    @selected = val
    @emit "update"

  list: () ->
    p = $.Deferred()

    $.getJSON "/api/sites/#{@siteId}/shopify_articles/blogs", (data) ->
      p.resolve data.shopify_articles

    p

window.ShopifyBlogsStore = new ShopifyBlogsStore()
