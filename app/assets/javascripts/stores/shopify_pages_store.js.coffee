class ShopifyPagesStore extends EventEmitter
  constructor: ->
    @pages = Immutable.fromJS([])
    @ready = false

  getState: () ->
    {pages: @pages.toJS(), ready: @ready}

  setSiteId: (siteId) ->
    if siteId != @siteId
      @siteId = siteId
      @ready = false
      @pages = Immutable.fromJS([])
      @emit "update"
      if @siteId?
        @list().then (pages) =>
          @ready = true
          @pages = Immutable.fromJS(pages)
          @emit "update"

  list: () ->
    p = $.Deferred()

    $.getJSON "/api/sites/#{@siteId}/shopify_pages", (data) ->
      p.resolve data.pages

    p

  _remove: (id) ->
    @pages = @pages.filter (p) ->
      p.get("id") != id
    @emit "update"

  import: (args) ->
    p = $.Deferred()

    $.ajax
      type: "POST"
      contentType: "application/json"
      url: "/api/sites/#{args.site_id}/shopify_pages/#{args.id}/import"
      success: (data) =>
        @_remove args.id

        if PagesStore.getState(data.page.id).ready
          PagesStore._update data.page
        else
          PagesStore._create data.page

        p.resolve data.page
      error: (data) ->
        p.reject data.responseJSON.errors

    p

window.ShopifyPagesStore = new ShopifyPagesStore()
