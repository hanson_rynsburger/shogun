class UserStore extends EventEmitter
  constructor: ->
    @user = Immutable.fromJS(window.CURRENT_USER)

  getState: ->
    {user: @user.toJS(), ready: true}

  update: (args) ->
    p = $.Deferred()

    $.ajax
      url: "/api/user"
      type: "PUT"
      data: JSON.stringify({user: args})
      contentType: "application/json"
      success: (data) =>
        @user = Immutable.fromJS(data.user)
        @emit "update"
        p.resolve data.user
      error: (data) ->
        p.reject data.responseJSON.errors

    p

window.UserStore = new UserStore()
