var MultiInput = {
  getInitialState: function() {
    return {rand: uuid.v4()};
  },
  componentDidMount: function() {
    var domNode = React.findDOMNode(this.refs.sortable);
    var c = this;
    var func = function(e) {
      c.handleMove.call(c, e);
    };
    this.sortable = Sortable.create(domNode, {
      handle: ".handle",
      animation: 200,
      onEnd: func
    });
  },
  componentWillUnmount: function() {
    this.sortable.destroy();
    this.sortable = null;
  },
  cloneValue: function() {
    return this.props.value.value.slice(0);
  },
  getValue: function() {
    return this.props.value.value;
  },
  changeRand: function() {
    this.setState({rand: uuid.v4()});
  },
  handleChange: function(index, text) {
    var clone = this.cloneValue();
    clone[index] = text;
    this.props.onChange(clone, 'change', index);
  },
  handleMove: function(e) {
    var i1 = e.oldIndex;
    var i2 = e.newIndex;
    if (i1 == i2) {
      return;
    }
    var clone = this.cloneValue();
    var v = clone.splice(i1, 1)[0];
    clone.splice(i2, 0, v);
    this.props.onChange(clone, 'move', [i1, i2]);
    this.changeRand();
  },
  handleRemove: function(index) {
    var clone = this.cloneValue();
    clone.splice(index, 1);
    this.props.onChange(clone, 'remove', index);
    this.changeRand();
  },
  handleAdd: function() {
    var clone = this.cloneValue();
    clone.push("");
    this.props.onChange(clone, 'add', clone.length);
  },
  render: function() {
    return (
      <Panel title={this.props.label}>
        <div ref="sortable">
          {this.renderValues()}
        </div>
        <button className="btn btn-sm btn-block btn-success" onClick={this.handleAdd} type="button">+</button>
      </Panel>
    );
  }
};
