var Form = {
  getInitialState: function() {
    return {changed: false, saving: false};
  },
  handleErrors: function(errors) {
    this.setState({saving: false}, function() {
      errors.forEach(function(error) {
        AlertsStore.danger(error);
      });
    });
  },
  handleSubmit: function(e, callback) {
    e.preventDefault();

    if (this.state.saving) {
      return;
    }

    this.setState({saving: true}, callback);
  }
}
