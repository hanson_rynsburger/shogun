var Clickable = {
  onMouseDown: function() {
    this.setState({active: true});
  },
  onMouseUp: function() {
    this.setState({active: false});
  },
  getClickEvents: function() {
    return {onMouseUp: this.onMouseUp, onMouseDown: this.onMouseDown};
  }
}
