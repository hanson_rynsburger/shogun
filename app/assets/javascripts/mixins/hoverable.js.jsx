var Hoverable = {
  onMouseOver: function() {
    this.setState({hover: true});
  },
  onMouseOut: function() {
    this.setState({hover: false});
  },
  getHoverEvents: function() {
    return {onMouseOut: this.onMouseOut, onMouseOver: this.onMouseOver};
  }
}
