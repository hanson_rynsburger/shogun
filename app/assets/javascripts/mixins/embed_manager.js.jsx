var EmbedManager = {
  /*
  must implement:
  - getSyncArgs
  - getEmbedOrigin
  - getEmbedPath
  - handleMessageData(data)
  */

  getInitialState: function() {
    return {embedLoaded: false, embedError: false, embedReady: false};
  },
  postMessage: function(msg) {
    var iframe = React.findDOMNode(this.refs.embed);
    try {
      iframe.contentWindow.postMessage(msg, this.getEmbedOrigin());
      return true;
    } catch(e) {
      this.handleEmbedError();
      return false;
    }
  },
  componentDidMount: function() {
    $(window).on("message", this.handleEmbedMessage);
  },
  componentWillUnmount: function() {
    $(window).off("message", this.handleEmbedMessage);
  },
  handleEmbedError: function(e) {
    this.setState({embedError: true});
  },
  handleEmbedLoad: function() {
    this.setState({embedLoaded: true}, function() {
      this.postMessage({sync: this.getSyncArgs()});
      setTimeout(function() {
        if (!this.state.embedReady) {
          this.handleEmbedError();
        }
      }.bind(this), 2000);
    }.bind(this));
  },
  handleEmbedReady: function() {
    this.setState({embedReady: true});
  },
  handleEmbedMessage: function(jqe) {
    var e = jqe.originalEvent;
    if (e.origin == this.getEmbedOrigin() && this.getEmbedSource() == e.data.source) {
      if (e.data.ready) {
        this.handleEmbedReady();
      } else if (this.state.embedReady) {
        this.handleMessageData(e.data);
      }
    }
  },
  componentDidUpdate: function(prevProps, prevState) {
    this.syncEmbed();
  },
  syncEmbed: function() {
    this.postMessage({sync: this.getSyncArgs()});
  },
  getEmbed: function(scrolling) {
    var cn;
    if (this.getEmbedClassName) {
      cn = this.getEmbedClassName();
    }
    cn = "embed " + cn;
    if (this.state.embedReady) {
      cn += " embed-ready";
    }
    var src = this.getEmbedOrigin() + this.getEmbedPath();
    return (
      <iframe
        className={cn}
        ref="embed"
        src={src}
        frameBorder="0"
        onLoad={this.handleEmbedLoad}
        onError={this.handleEmbedError}
        scrolling={!!scrolling ? "yes" : "no"} />
    );
  }
}
