var Clearable = {
  /*
  * - clearStyles
  */
  renderClear: function() {
    return (
      <div className="shogun-clear-button-wrapper">
        <Button xs icon="trash-o" onClick={this.clearStyles} title="Clear" tooltip="bottom" />
      </div>
    );
  }
};
