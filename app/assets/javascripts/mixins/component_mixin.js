var ComponentMixin = {
  getInitialState: function() {
    return {dragging: false};
  },

  // helpers

  isSelected: function() {
    return this.props.uuid && this.props.uuid == this.props.selected_uuid;
  },
  handleUpdate: function(variable_id, value) {
    this.props.onUpdate(this.props.uuid, variable_id, value);
  },
  findValue: function(vname) {
    return SHOGUN_UNDERSCORE(this.props.values).find(function(v) {
      return v.name == vname;
    });
  },

  // react lifecycle

  componentDidMount: function() {
    if (this.isSelected()) {
      this.props.onRedrawOutlines();
    }
  },

  // handlers

  handleDragStart: function(e) {
    if (!e.dataTransfer.getData("component_uuid")) {
      e.dataTransfer.setData("component_uuid", this.props.uuid);
      this.setState({dragging: true});
    }
  },
  handleDragEnd: function(e) {
    if (!e.dataTransfer.getData("component_uuid")) {
      e.dataTransfer.setData("component_uuid", this.props.uuid);
      this.setState({dragging: false});
    }
  },
  handleEditHTML: function(variable_id) {
    this.props.onEditHTML(this.props.uuid, variable_id);
  },
  handleEditMarkdown: function(variable_id) {
    this.props.onEditMarkdown(this.props.uuid, variable_id);
  },
  handleUploadImage: function(variable_id) {
    this.props.onUploadImage(this.props.uuid, variable_id);
  },

  // styling

  getStyle: function() {
    var s = {};
    var styles = this.props.styles;
    if (this.state.hover && styles.hover) {
      styles = SHOGUN_UNDERSCORE.extend({}, styles, styles.hover);
    }
    if (this.state.active && styles.active) {
      styles = SHOGUN_UNDERSCORE.extend({}, styles, styles.active);
    }
    var keys = SHOGUN_UNDERSCORE.keys(styles);
    SHOGUN_UNDERSCORE.each(keys, function(k) {
      var sp = humps.camelize(k);
      if (k == "background_image") {
        s.backgroundImage = "url("+styles[k]+")";
        s.backgroundRepeat = "no-repeat";
      } else {
        if (!SHOGUN_UNDERSCORE.isObject(styles[k])) {
          s[sp] = styles[k];
        }
      }
    }.bind(this));
    if (styles["border_color"] || styles["border_top_width"] || styles["border_bottom_width"] || styles["border_left_width"] || styles["border_right_width"]) {
      s.borderStyle = "solid";
    }
    if (styles["box_shadow_color"] || styles["box_shadow_horizontal"] || styles["box_shadow_vertical"] || styles["box_shadow_blur"] || styles["box_shadow_spread"]) {
      var bs = styles.box_shadow_horizontal;
      bs += " " + styles.box_shadow_vertical;
      bs += " " + styles.box_shadow_blur;
      bs += " " + styles.box_shadow_spread;
      bs += " " + styles.box_shadow_color;
      s.boxShadow = bs;
    }
    var postfix = styles["position_" + this.props.currentScreenSize] ? "_" + this.props.currentScreenSize : "";
    var t = "top" + postfix;
    var l = "left" + postfix;
    var trans = "translate(" + (parseFloat(styles[l]) || 0) + "px, " + (parseFloat(styles[t]) || 0) + "px)";
    s.WebkitTransform = trans;
    s.transform = trans;
    var display = styles["display_" + this.props.currentScreenSize];
    if (display) {
      s.display = display;
    }
    return s;
  },

  // rendering

  getElementProperties: function() {
    var cname = "shogun-component";

    if (this.state.dragging) {
      cname += " shogun-component-dragging";
    }

    if (this.props.css_classes) {
      cname += " " + this.props.css_classes;
    }

    return {
      id: this.props.uuid,
      ref: "component",
      style: this.getStyle(),
      className: cname,
      onDragStart: this.handleDragStart,
      onDragEnd: this.handleDragEnd,
      draggable: this.isSelected()
    };
  }
}
