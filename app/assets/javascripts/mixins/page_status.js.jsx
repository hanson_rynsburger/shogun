var PageStatus = {
  renderStatus: function(page) {
    if (page.updating) {
      return (
        <LabelWithTooltip info tooltip="This page is currently being refreshed on your site. Hold tight!" label="updating" />
      );
    } else if (page.draft) {
      return (
        <LabelWithTooltip default tooltip="This page is not live on your site." label="draft" />
      );
    } else if (page.outdated) {
      return (
        <LabelWithTooltip warning tooltip="This page has been changed since it was last published." label="outdated" />
      );
    } else {
      return (
        <LabelWithTooltip success tooltip="This page is up to date and live on your site." label="published" />
      );
    }
  }
};
