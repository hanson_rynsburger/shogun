var Dropzone = React.createClass({
  render: function() {
    var cn = "shogun-dropzone";
    if (this.props.empty) {
      cn += " shogun-dropzone-empty";
    }
    return (
      <div
        data-position={this.props.position}
        data-parent-uuid={this.props.parent_uuid}
        data-variable-id={this.props.variable_id}
        className={cn} />
    );
  }
});
