var Root = React.createClass({
  getInitialState: function() {
    return {dragging: false, hover_uuid: null};
  },

  // helpers

  isAbsolute: function(uuid) {
    return this.getComponent(uuid).styles.position == "absolute";
  },
  isDragging: function() {
    return this.state.dragging && (!this.cuuid || !this.isAbsolute(this.cuuid));
  },
  getComponent: function(uuid) {
    return this.props.components[uuid];
  },
  getModel: function(mid) {
    return SHOGUN_UNDERSCORE.find(this.props.models, function(m) {
      return m.id == mid;
    });
  },
  getReferences: function() {
    return this.props.components[this.props.root_uuid].values[0].references;
  },
  calculateDistance: function(a, b) {
    return Math.sqrt( (a[0]-=b[0])*a[0] + (a[1]-=b[1])*a[1] );
  },
  checkVisible: function(c) {
    var ctop = c.offset().top,
    cbottom = c.offset().top + c.outerHeight(),
    wtop = SHOGUN_JQUERY(window).scrollTop(),
    wbottom = SHOGUN_JQUERY(window).scrollTop() + SHOGUN_JQUERY(window).outerHeight();
    if (ctop > wbottom || cbottom < wtop) {
      return false;
    }
    return true;
  },
  deactivateDropzone: function() {
    SHOGUN_JQUERY(".shogun-dropzone-active").removeClass("shogun-dropzone-active");
  },
  activateDropZone: function($dz) {
    if ($dz.hasClass("shogun-dropzone-active")) {
      // noop
    } else {
      this.deactivateDropzone();
      $dz.addClass('shogun-dropzone-active');
      var $c = $dz.closest(".shogun-component");
      var uuid = $c.attr("id");
      if (uuid) {
        this.drawActiveOutline(uuid);
      } else {
        this.hideActiveOutline();
      }
    }
  },
  buildDropzoneCache: function() {
    var $dropzones = SHOGUN_JQUERY('.shogun-dropzone').not(SHOGUN_JQUERY('.shogun-component-dragging .shogun-dropzone'));
    return SHOGUN_UNDERSCORE.map($dropzones, function(v) {
      var $dz = SHOGUN_JQUERY(v);
      var offset = $dz.offset();
      var width = $dz.outerWidth();
      var height = $dz.outerHeight();

      var coords = {
        x0: offset.left,
        y0: offset.top,
        x1: offset.left + width,
        y1: offset.top + height
      };

      return {
        $dz: $dz,
        coords: coords
      };
    });
  },

  // drawing

  redrawOutlines: function() {
    var hud = this.props.highlighted_uuid || this.hover_uuid;
    if (hud) {
      if (hud != this.props.selected_uuid) {
        this.drawHighlightOutline(hud);
      } else {
        this.hideHighlightOutline();
      }
    } else {
      this.hideHighlightOutline();
    }
    if (this.props.selected_uuid) {
      this.drawSelectedOutline(this.props.selected_uuid);
    } else {
      this.hideSelectedOutline();
    }
  },
  drawOutline: function(uuid, name) {
    var $c = SHOGUN_JQUERY("#" + uuid);
    var cls = ".shogun-outline-" + name;
    var $outline = SHOGUN_JQUERY(cls);
    if ($c.length > 0 && $c.is(":visible")) {
      var comp = this.getComponent(uuid);
      $outline.css({
        display: 'block',
        top: $c.offset().top,
        left: $c.offset().left,
        width: $c.outerWidth(),
        height: $c.outerHeight()
      });
      var $label = SHOGUN_JQUERY(cls + ' .shogun-outline-label');
      $label.html(comp.template_name);
      if ($outline.length > 0 && $outline.offset().top < 20) {
        if ($outline.outerHeight() > (SHOGUN_JQUERY('#shogun-root').outerHeight()) - 24) {
          $label.addClass('inside');
        } else {
          $label.addClass('bottom').removeClass('inside');
        }
      } else {
        $label.removeClass('bottom').removeClass('inside');
      }
    } else {
      $outline.hide();
    }
  },
  drawHighlightOutline: function(uuid) {
    this.drawOutline(uuid, "highlighted");
  },
  hideHighlightOutline: function() {
    SHOGUN_JQUERY('.shogun-outline-highlighted').hide();
  },
  drawSelectedOutline: function(uuid) {
    this.drawOutline(uuid, "selected");
  },
  hideSelectedOutline: function() {
    SHOGUN_JQUERY('.shogun-outline-selected').hide();
  },
  drawActiveOutline: function(uuid) {
    this.drawOutline(uuid, "active");
  },
  hideActiveOutline: function() {
    SHOGUN_JQUERY('.shogun-outline-active').hide();
  },
  redrawActiveDropzone: function(x, y) {
    if (this.isDragging()) {
      if (!this.dzcache) {
        this.dzcache = this.buildDropzoneCache();
      }

      var arr = SHOGUN_UNDERSCORE.map(this.dzcache, function(h) {
        var coords = h.coords;
        var cp = {};

        if (x > coords.x1) {
          cp.x = coords.x1;
        } else if (x < coords.x0) {
          cp.x = coords.x0;
        } else {
          cp.x = x;
        }

        if (y > coords.y1) {
          cp.y = coords.y1;
        } else if (y < coords.y0) {
          cp.y = coords.y0;
        } else {
          cp.y = y;
        }

        return {
          $dz: h.$dz,
          distance: this.calculateDistance([cp.x, cp.y], [x, y])
        };
      }.bind(this));

      var inside = SHOGUN_UNDERSCORE.find(arr, function(h) {
        return h.distance == 0;
      });

      if (inside) {
        // if mouse is inside a dropzone use that dropzone
        this.activateDropZone(inside.$dz);
      } else {
        // find the closest
        var closest = arr.sort(function(a, b) {
          return a.distance - b.distance;
        })[0];

        if (closest) {
          this.activateDropZone(closest.$dz);
        } else {
          this.deactivateDropzone();
        }
      }
    }
  },

  // react lifecycle

  componentDidUpdate: function(prevProps, prevState) {
    this.redrawOutlines();
    if (prevProps.dragging && !this.props.dragging) {
      if (this.interval) {
        clearInterval(this.interval);
      }
      if (this.state.dragging) {
        this.setState({dragging: false});
      }
      this.deactivateDropzone();
      this.hideActiveOutline();
      SHOGUN_JQUERY('.shogun-outline-dragging').removeClass("shogun-outline-dragging");
      this.dzcache = null;
      this.cuuid = null;
    } else if (!prevProps.dragging && this.props.dragging) {
      if (!this.state.dragging) {
        this.setState({dragging: true});
      }
      SHOGUN_JQUERY('.shogun-outline').not(SHOGUN_JQUERY(".shogun-outline-active")).addClass("shogun-outline-dragging");
    } else if (prevProps.selected_uuid != this.props.selected_uuid && this.props.selected_uuid) {
      var $c = SHOGUN_JQUERY("#" + this.props.selected_uuid);
      if ($c && $c.length && $c.offset()) {
        if (!this.checkVisible($c)) {
          SHOGUN_JQUERY('body').animate({
            scrollTop: $c.offset().top
          }, 500);
        }
      }
    }
  },
  componentDidMount: function() {
    SHOGUN_JQUERY('<div class="shogun-outline shogun-outline-highlighted"><div class="shogun-outline-label"></div></div>').appendTo("body");
    SHOGUN_JQUERY('<div class="shogun-outline shogun-outline-selected"><div class="shogun-outline-label"></div></div>').appendTo("body");
    SHOGUN_JQUERY('<div class="shogun-outline shogun-outline-active"><div class="shogun-outline-label"></div></div>').appendTo("body");
    SHOGUN_JQUERY(window).on("resize", this.redrawOutlines);
  },
  componentWillUnmount: function() {
    SHOGUN_JQUERY(".shogun-outline").remove();
    SHOGUN_JQUERY(window).off("resize", this.redrawOutlines);
  },

  // event handlers

  handleEditHTML: function(uuid, variable_id) {
    var $component = SHOGUN_JQUERY('#' + uuid);
    SHOGUN_JQUERY("body").animate({
      scrollTop: SHOGUN_JQUERY('#' + uuid).offset().top
    }, 300);

    this.props.onEditHTML(uuid, variable_id);
  },
  handleEditMarkdown: function(uuid, variable_id) {
    var $component = SHOGUN_JQUERY('#' + uuid);
    SHOGUN_JQUERY("body").animate({
      scrollTop: SHOGUN_JQUERY('#' + uuid).offset().top
    }, 300);

    this.props.onEditMarkdown(uuid, variable_id);
  },
  handleConfigure: function(uuid) {
    this.props.onConfigure(uuid);
  },
  handleMouseOver: function(e) {
    var uuid = SHOGUN_JQUERY(e.target).closest(".shogun-component").attr("id");
    this.hover_uuid = uuid;
    this.redrawOutlines();
  },
  handleMouseLeave: function(e) {
    this.hover_uuid = null;
    this.redrawOutlines();
  },
  handleSelect: function(e) {
    var uuid = SHOGUN_JQUERY(e.target).closest(".shogun-component").attr("id");
    if (uuid && uuid != this.props.selected_uuid) {
      // eager draw for faster feeling UI
      this.drawSelectedOutline(uuid);
      this.hideHighlightOutline();
      this.props.onConfigure(uuid);
    }
  },
  handleDragOver: function(e) {
    e.preventDefault();
    if (this.cuuid) {
      var c = this.getComponent(this.cuuid);
      if (c.styles.position == "absolute") {
        if (!this.cbegin) {
          var $c = SHOGUN_JQUERY("#"+this.cuuid);
          $c.hide();
          var flag = "position_" + this.props.currentScreenSize;
          var ln = "left";
          var tn = "top";
          if (c.styles[flag]) {
            ln += "_" + this.props.currentScreenSize;
            tn += "_" + this.props.currentScreenSize;
          }
          this.cbegin = {x: e.clientX - (parseInt(c.styles[ln]) || 0), y: e.clientY - (parseInt(c.styles[tn]) || 0)};
        }
        this.cend = {x: e.clientX, y: e.clientY};
      }
    }
    this.redrawActiveDropzone(e.pageX, e.pageY);
  },
  handleDrop: function(e) {
    e.preventDefault();
    clearInterval(this.interval);

    var $dz = SHOGUN_JQUERY('.shogun-dropzone-active').first();
    var parent_uuid = $dz.attr('data-parent-uuid');
    var position = parseInt($dz.attr('data-position'));
    var variable_id = $dz.attr('data-variable-id');
    var tid = e.dataTransfer.getData("template_id");
    var sid = e.dataTransfer.getData("snippet_id");

    if (tid) {
      this.props.onBuild(parent_uuid, variable_id, position, {template_id: tid});
    }
    if (sid) {
      this.props.onBuild(parent_uuid, variable_id, position, {snippet_id: sid});
    }
  },
  handleDragStart: function(e) {
    this.cuuid = e.dataTransfer.getData("component_uuid");
    e.dataTransfer.effectAllowed = "move";
    // chrome shits the bed when you modify the dom in this handler
    setTimeout(function() {
      var st, $c, ctop, after, diff;
      if (this.cuuid) {
        st = SHOGUN_JQUERY(window).scrollTop();
        if (st !== 0) {
          $c = SHOGUN_JQUERY("#" + this.cuuid);
          ctop = $c.offset().top;
        }
        this.setState({dragging: true}, function() {
          if (this.cuuid && st !== 0) {
            after = $c.offset().top;
            diff = after - ctop;
            SHOGUN_JQUERY(window).scrollTop(st + diff);
          }
          this.props.onDragStart();
        }.bind(this));
      }
    }.bind(this), 0);
  },
  handleDragEnd: function(e) {
    clearInterval(this.interval);

    var $dz = SHOGUN_JQUERY('.shogun-dropzone-active').first();
    var parent_uuid = $dz.attr('data-parent-uuid');
    var position = parseInt($dz.attr('data-position'));
    var variable_id = $dz.attr('data-variable-id');

    // TODO - one final edge case - dropping right at the bottom of the screen
    if (this.cuuid) {
      var $c = SHOGUN_JQUERY("#"+this.cuuid);
      if (!this.isAbsolute(this.cuuid)) {
        if ($dz.prev()[0] == $c[0]) {
          // use dropzone above
          $dz = $dz.prev().prev();
        }
        var dt = $dz.offset().top;
        $c.hide();
        var dt2 = $dz.offset().top;
        $c.show();
        if (dt2 != dt) {
          // dropzone is being pushed down by component
          dt += (dt - dt2);
        }
        var st = SHOGUN_JQUERY(window).scrollTop();
        this.setState({dragging: false}, function() {
          var st2 = SHOGUN_JQUERY(window).scrollTop();
          // if sdiff > 0 we were near the bottom of the screen and need to adjust
          var sdiff = st - st2;
          // dd = the distance from the top of the screen to the top of the dropzone
          var dd = dt - st;
          // after = the offset now
          var after = $dz.offset().top;
          // z = the distance from the top of the screen to the top of the dropzone now
          var z = after - st;
          var diff = dd - z;
          SHOGUN_JQUERY(window).scrollTop(SHOGUN_JQUERY(window).scrollTop() - diff + sdiff);
          this.props.onMove(parent_uuid, variable_id, position, this.cuuid);
        }.bind(this));
      } else {
        var c = this.getComponent(this.cuuid);
        var t = this.cend.y - this.cbegin.y;
        var l = this.cend.x - this.cbegin.x;
        $c.css({transform: "translate("+l+"px,"+t+"px)"});
        var flag = "position_" + this.props.currentScreenSize;
        var tn = "top";
        var ln = "left";
        if (c.styles[flag]) {
          tn += "_" + this.props.currentScreenSize;
          ln += "_" + this.props.currentScreenSize;
        }
        var up = {};
        up[tn] = t;
        up[ln] = l;
        this.props.onUpdateStyles(this.cuuid, up);
        this.props.onDragEnd();
        this.cbegin = null;
        this.cend = null;
        $c.show();
      }
    } else {
      this.props.onDragEnd();
    }
  },
  dragscroll: function(e) {
    clearInterval(this.interval);

    if (e != 0) {
      var frequency = 100/e;
      var direction = e > 0 ? 1 : -1;
      var x = SHOGUN_JQUERY(window).outerWidth()/2;

      var interval = setInterval(function() {
        SHOGUN_JQUERY('html,body').animate(
          {scrollTop: direction*5 + SHOGUN_JQUERY(window).scrollTop()},
          0,
          'linear'
        );

        var y = e > 0 ? SHOGUN_JQUERY(window).scrollTop() + SHOGUN_JQUERY(window).outerHeight() : SHOGUN_JQUERY(window).scrollTop();

        if (SHOGUN_JQUERY(window).scrollTop() != 0 && (SHOGUN_JQUERY(window).scrollTop() + SHOGUN_JQUERY(window).outerHeight() != SHOGUN_JQUERY('body')[0].scrollHeight)) {
          this.redrawActiveDropzone(x, y);
        }
      }.bind(this), frequency);

      this.interval = interval;
    }
  },

  // rendering

  renderStyleTags: function() {
    var components = SHOGUN_UNDERSCORE.values(this.props.components);

    var valid = SHOGUN_UNDERSCORE.filter(components, function(c) {
      return c.template_version_id && c.css;
    });

    var versions = {};
    var unique = [];
    SHOGUN_UNDERSCORE.each(valid, function(c) {
      if (!versions[c.template_version_id]) {
        versions[c.template_version_id] = true;
        unique.push({css: c.css, template_version_id: c.template_version_id});
      }
    });

    return SHOGUN_UNDERSCORE.map(unique, function(o) {
      return (
        <style key={o.template_version_id} type="text/css" dangerouslySetInnerHTML={{__html: o.css}}></style>
      );
    });
  },
  renderDragzones: function() {
    return (
      <div className="shogun-dragzones">
        <div className="scroll-top-fast" onDragEnter={this.dragscroll.bind(this, -40)}></div>
        <div className="scroll-top-normal" onDragEnter={this.dragscroll.bind(this, -10)}></div>
        <div className="scroll-stop" onDragEnter={this.dragscroll.bind(this, 0)}></div>
        <div className="scroll-bottom-normal" onDragEnter={this.dragscroll.bind(this, 10)}></div>
        <div className="scroll-bottom-fast" onDragEnter={this.dragscroll.bind(this, 40)}></div>
      </div>
    );
  },

  render: function() {
    if (!this.props.components) {
      return <h1>Loading...</h1>;
    }
    var cn = "shogun-root",
    dragzones;
    if (this.props.dropzones) {
      cn += " shogun-root-dropzones";
    }
    if (this.isDragging()) {
      cn += " shogun-root-dragging";
      dragzones = this.renderDragzones();
    }

    var standardProps = {
      getComponent: this.getComponent,
      getModel: this.getModel,
      onEditHTML: this.handleEditHTML,
      onEditMarkdown: this.handleEditMarkdown,
      onRedrawOutlines: this.redrawOutlines,
      onUpdate: this.props.onUpdate,
      onUploadImage: this.props.onUploadImage,
      selected_uuid: this.props.selected_uuid,
      site: this.props.site,
      currentScreenSize: this.props.currentScreenSize
    };

    return (
      <div
        key="root"
        className={cn}
        onDragStart={this.handleDragStart}
        onDrag={this.handleDrag}
        onDragEnter={this.handleDragEnter}
        onDragLeave={this.handleDragLeave}
        onDragEnd={this.handleDragEnd}
        onDragOver={this.handleDragOver}
        onDrop={this.handleDrop}
        onLoad={this.redrawOutlines}
        onClick={this.handleSelect}
        onInput={this.redrawOutlines}
        onMouseOver={this.handleMouseOver}
        onMouseLeave={this.handleMouseLeave}
        ref="root">
        {this.renderStyleTags()}
        {dragzones}
        <MultiDropzone
          // standard properties
          {...standardProps}
          // component specific
          parent_uuid={this.props.root_uuid}
          references={this.getReferences()}
          variable_id={null} />
      </div>
    );
  }
});
