var MultiDropzone = React.createClass({
  getComponentClass: function(comp) {
    var cfg = SHOGUN_UNDERSCORE.find(STANDARD_TEMPLATES, function(c) {
      return c.id == comp.template_id;
    });
    if (cfg && cfg.component) {
      return window[cfg.component];
    } else {
      return CustomComponent;
    }
  },
  renderReference: function(reference) {
    var comp = this.props.getComponent(reference.uuid);
    var CurrentComponent = this.getComponentClass(comp);
    return <CurrentComponent {...this.props} {...comp} key={comp.id || comp.uuid} />;
  },
  renderDropzone: function(position, before_uuid, after_uuid) {
    return (
      <Dropzone
        key={"dz-" + position}
        position={position}
        parent_uuid={this.props.parent_uuid}
        variable_id={this.props.variable_id}
        before_uuid={before_uuid}
        after_uuid={after_uuid}
        empty={this.props.references.length == 0} />
    );
  },
  render: function() {
    var components = [];
    var index = 0;

    var before_uuid = null;
    var after_uuid = null;

    this.props.references.forEach(function(reference) {
      after_uuid = reference.uuid;
      components.push(this.renderDropzone(index, before_uuid, after_uuid));
      components.push(this.renderReference(reference));
      before_uuid = reference.uuid;
      index += 1;
    }.bind(this));

    components.push(this.renderDropzone(index, after_uuid, null));

    return <div className="dropzone-wrapper">{components}</div>;
  }
});
