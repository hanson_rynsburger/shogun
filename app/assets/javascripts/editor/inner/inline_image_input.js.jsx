var InlineImageInput = React.createClass({
  render: function() {
    var url, width, height;
    var imgc = "img-responsive shogun-image";
    if (this.props.value && this.props.value.url) {
      url = this.props.value.url;
      width = this.props.value.width;
      height = this.props.value.height;
    } else {
      url = window.PLACEHOLDER_URL;
      imgc += " shogun-img-placeholder";
    }
    if (this.props.mutable) {
      imgc += " shogun-img-input";
      return (
        <div key="mutable-image" className="shogun-input-wrapper">
          <img src={url} style={{width: width, height: height}} className={imgc} />
          <span onClick={this.props.onUploadImage} className="shogun-overlay shogun-overlay-image"><i className="fa fa-image"></i></span>
        </div>
      );
    } else {
      return <img key="immutable-image" src={url} style={{width: width, height: height}} className={imgc} />
    }
  }
});
