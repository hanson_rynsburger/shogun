var CustomComponent = React.createClass({
  mixins: [ComponentMixin],

  // injections

  injectDropzone: function(value, domNode) {
    var dz = (
      <MultiDropzone
        {...this.props}
        parent_uuid={this.props.uuid}
        references={value.references}
        variable_id={value.variable_id} />
    );

    dz = React.render(dz, domNode);

    this.inlineVariableUpdaters[value.variable_id] = function(value) {
      dz.setProps({references: value.references});
    }.bind(this);

    this.dropzones.push(dz);
  },
  injectContentEditable: function(value, domNode) {
    var c = this;
    var f = SHOGUN_UNDERSCORE.debounce(function(nv) {
      c.handleUpdate(value.variable_id, nv);
    }, 500);
    var ce = React.render(
      <ContentEditable mutable={this.isSelected()} value={value.value[0] || "Text"} onChange={f} />,
      domNode
    );
    this.inlineVariableUpdaters[value.variable_id] = function(value) {
      ce.setProps({value: (value.value[0] || "Text")});
    };
    this.mutabilityUpdaters.push(function(m) {
      ce.setProps({mutable: m});
    });
  },
  injectRichText: function(value, domNode) {
    var c = this;
    var f = SHOGUN_UNDERSCORE.debounce(function(nv) {
      c.handleUpdate(value.variable_id, nv);
    }, 500);
    var rt = React.render(
      <InlineRichText fonts={this.props.site.fonts} colors={this.props.site.colors} mutable={this.isSelected()} value={value.value[0] || "Text"} onChange={f} />,
      domNode
    );
    this.inlineVariableUpdaters[value.variable_id] = function(value) {
      rt.setProps({value: (value.value[0] || "Text")});
    };
    this.mutabilityUpdaters.push(function(m) {
      rt.setProps({mutable: m});
    });
  },
  injectImage: function(value, domNode) {
    var i = <InlineImageInput mutable={this.isSelected()} value={value.value[0]} onUploadImage={this.handleUploadImage.bind(this, value.variable_id)} />;
    i = React.render(i, domNode);
    this.inlineVariableUpdaters[value.variable_id] = function(value) {
      i.setProps({value: value.value[0]});
    };
    this.mutabilityUpdaters.push(function(m) {
      i.setProps({mutable: m});
    });
  },
  injectHTML: function(value, domNode) {
    var html = <InlineHTML mutable={this.isSelected()} value={value.value[0]} onEditHTML={this.handleEditHTML.bind(this, value.variable_id)}/>;
    html = React.render(html, domNode);
    this.inlineVariableUpdaters[value.variable_id] = function(value) {
      html.setProps({value: value.value[0]});
    };
    this.mutabilityUpdaters.push(function(h) {
      html.setProps({mutable: h});
    });
  },
  injectMarkdown: function(value, domNode) {
    var md = <InlineMarkdown mutable={this.isSelected()} value={value.value[0]} onEditMarkdown={this.handleEditMarkdown.bind(this, value.variable_id)}/>;
    md = React.render(md, domNode);
    this.inlineVariableUpdaters[value.variable_id] = function(value) {
      md.setProps({value: value.value[0]});
    };
    this.mutabilityUpdaters.push(function(m) {
      md.setProps({mutable: m});
    });
  },
  inject: function(vnames, injections) {
    SHOGUN_UNDERSCORE(vnames).each(function(vname) {
      var value = this.findValue(vname);
      var id = injections[vname];
      this.disposals.push(id);
      var domNode = document.getElementById(id);
      if(value.type == "text") {
        this.injectContentEditable(value, domNode);
      } else if (value.type == "dropzone") {
        this.injectDropzone(value, domNode);
      } else if (value.type == "richtext") {
        this.injectRichText(value, domNode);
      } else if (value.type == "image") {
        this.injectImage(value, domNode);
      } else if (value.type == "markdown") {
        this.injectMarkdown(value, domNode);
      } else if (value.type == "html") {
        this.injectHTML(value, domNode);
      } else {
        console.log("NOT IMPLEMENTED");
      }
    }.bind(this));
  },

  // rendering

  renderValue: function(type, value, input) {
    if (Array.isArray(value)) {
      if (input == "multi") {
        return SHOGUN_UNDERSCORE.map(value, function(v) {
          return this.renderValue(type, v, "multi");
        }.bind(this));
      } else {
        value = value[0];
      }
    }

    if (!value) {
      return "";
    }

    if (type == "markdown") {
      return marked(value);
    } else if (type == "image") {
      return '<img src="'+value.url+'" width="'+value.width+'" height="'+value.height+'" class="shogun-image img-responsive" />';
    } else if (type == "integer") {
      return parseInt(value);
    } else {
      return value;
    }
  },
  renderEntry: function(model, e) {
    if (model.external) {
      return e;
    } else {
      var keys = SHOGUN_UNDERSCORE.keys(model.properties);
      var r = {};
      SHOGUN_UNDERSCORE.each(keys, function(k) {
        r[k] = this.renderValue(model.properties[k], e[k]);
      }.bind(this));
      return r;
    }
  },
  renderLiquid: function() {
    var tmpl = Liquid.parse(this.props.liquid);
    // on this.props.values
    // type
    // name
    // value
    // input
    // data_source
    // model_id
    var injections = {};
    var args = {};
    this.props.values.forEach(function(v) {
      if (v.data_source == "model") {
        var model = this.props.getModel(v.model_id);
        if (v.input == "hidden") {
          var entryId = v.value[0];
          if (entryId) {
            var entry = SHOGUN_UNDERSCORE.find(model.entries, function(e) {
              return e.id == entryId;
            });
            if (entry) {
              args[v.name] = this.renderEntry(model, entry);
            }
          }
        } else {
          var rendered = SHOGUN_UNDERSCORE.map(model.entries, function(e) {
            return this.renderEntry(model, e);
          }.bind(this));
          args[v.name] = rendered;
        }
      } else if (v.type == "complex") {
        var map = {};
        SHOGUN_UNDERSCORE.each(v.properties, function(prop) {
          map[prop.name] = prop.type;
        });
        var rendered = SHOGUN_UNDERSCORE.map(v.value, function(e) {
          var keys = SHOGUN_UNDERSCORE.keys(map);
          var r = {};
          SHOGUN_UNDERSCORE.each(keys, function(k) {
            r[k] = this.renderValue(map[k], e[k]);
          }.bind(this));
          return r;
        }.bind(this));
        args[v.name] = rendered;
      } else {
        if (v.input == "inline") {
          var u = uuid.v4();
          injections[v.name] = u;
          if (v.type == "text") {
            args[v.name] = '<span id="'+u+'"></span>';
          } else {
            args[v.name] = '<div id="'+u+'"></div>';
          }
        } else {
          args[v.name] = this.renderValue(v.type, v.value, v.input);
        }
      }
    }.bind(this));
    return {html: tmpl.render(args), injections: injections};
  },

  // react lifecycle helpers

  componentDidMount: function() {
    var id = this.props.template_id;
    var site_id = this.props.site.id;
    var data = this.renderLiquid();
    SHOGUN_JQUERY(React.findDOMNode(this.refs.component)).html(data.html);
    var vnames = SHOGUN_UNDERSCORE.keys(data.injections);
    if (vnames.length > 0) {
      this.disposals = [];
      this.dropzones = [];
      this.inlineVariableUpdaters = {};
      this.mutabilityUpdaters = [];
      this.inject(vnames, data.injections);
    }
  },
  componentWillUnmount: function() {
    this.inlineVariableUpdaters = null;
    this.mutabilityUpdaters = null;
    if (this.disposals) {
      SHOGUN_UNDERSCORE(this.disposals).each(function(id) {
        React.unmountComponentAtNode(document.getElementById(id));
      });
      this.disposals = null;
    }
    this.dropzones = null;
  },
  componentWillReceiveProps: function(nextProps) {
    if (this.inlineVariableUpdaters && SHOGUN_UNDERSCORE.keys(this.inlineVariableUpdaters).length > 0) {
      nextProps.values.forEach(function(value) {
        var f = this.inlineVariableUpdaters[value.variable_id];
        if (f) {
          f(value);
        }
      }.bind(this));
    }

    if (this.mutabilityUpdaters && this.mutabilityUpdaters.length > 0) {
      var mutable = (this.isSelected() && !nextProps.selected_uuid) || nextProps.selected_uuid == this.props.uuid;
      SHOGUN_UNDERSCORE.each(this.mutabilityUpdaters, function(f) {
        f(mutable);
      });
    }

    // trickle down
    if (this.dropzones && this.dropzones.length > 0) {
      var sc = nextProps.selected_uuid != this.props.selected_uuid || nextProps.currentScreenSize != this.props.currentScreenSize;
      if (sc) {
        SHOGUN_UNDERSCORE.each(this.dropzones, function(dz) {
          dz.setProps({selected_uuid: nextProps.selected_uuid, currentScreenSize: nextProps.currentScreenSize});
        });
      }
    }
  },

  // rendering

  render: function() {
    return <div {...this.getElementProperties()} />;
  }
});
