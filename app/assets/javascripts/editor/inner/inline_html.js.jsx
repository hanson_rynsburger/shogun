var InlineHTML = React.createClass({
  handleClick: function(e) {
    e.preventDefault();
    this.props.onEditHTML();
  },
  render: function() {
    var value = this.props.value;
    if (this.props.mutable) {
      return (
        <div key="mutable-html" className="shogun-input-wrapper shogun-html" onClick={this.handleClick}>
          <div dangerouslySetInnerHTML={{__html: value}} />
          <div className="shogun-overlay"><img src={window.HTML_IMAGE_URL} /></div>
        </div>
      );
    } else {
      return <div key="immutable-html" className="shogun-html" dangerouslySetInnerHTML={{__html: value}} />;
    }
  }
});
