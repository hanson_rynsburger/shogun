var ContentEditable = React.createClass({
  getInitialState: function() {
    return {focused: false};
  },
  shouldComponentUpdate: function(nextProps, nextState) {
    return (nextProps.value !== React.findDOMNode(this.refs.ce).innerHTML && !this.state.focused) || (nextState.focused != this.state.focused) || (nextProps.mutable != this.props.mutable);
  },
  handleBlur: function() {
    this.setState({focused: false});
    this.handleChange();
  },
  handleFocus: function() {
    this.setState({focused: true});
  },
  handleChange: function() {
    var html = React.findDOMNode(this.refs.ce).innerHTML;
    if (html !== this.lastHtml) {
      this.props.onChange(html);
    }
    this.lastHtml = html;
  },
  componentDidMount: function() {
    var $ce = SHOGUN_JQUERY(React.findDOMNode(this.refs.ce));
    var $parent = $ce.parent().parent();
    var tagName = $parent.prop("tagName");

    if (tagName == "A" || tagName == "BUTTON") {
      $parent.on("click", function(e) {
        e.preventDefault();
        $ce.focus();
      });
    }
  },
  componentWillUnmount: function() {
    this.lastHtml = null;
  },
  handleClick: function(e) {
    e.preventDefault();
  },
  render: function() {
    if (this.props.mutable) {
      var cn = "shogun-input-wrapper shogun-input-wrapper-inline";
      if (this.state.focused) {
        cn += " shogun-ce-focused";
      }
      return (
        <span className={cn}>
          <span
            className="shogun-ce"
            ref="ce"
            onClick={this.handleClick}
            onInput={this.handleChange}
            onBlur={this.handleBlur}
            onFocus={this.handleFocus}
            contentEditable={true}
            dangerouslySetInnerHTML={{__html: this.props.value}}></span>
          <span className="shogun-overlay shogun-overlay-inherit"><img src={window.TEXT_IMAGE_URL} /></span>
        </span>
      );
    } else {
      return <span ref="ce" dangerouslySetInnerHTML={{__html: this.props.value}}></span>;
    }
  }
});
