// Abandon all hope, ye who enter here.

var InlineRichText = React.createClass({
  getInitialState: function() {
    return {focused: false, ready: false};
  },
  shouldComponentUpdate: function(nextProps, nextState) {
    if (window['CKEDITOR']) {
      CKEDITOR.disableAutoInline = true;
    }
    return (nextProps.value !== React.findDOMNode(this.refs.editor).innerHTML) || (nextState.focused != this.state.focused) || (nextProps.mutable != this.props.mutable) || (nextState.ready != this.state.ready);
  },
  handleChange: function() {
    if (!this.skip) {
      var e = CKEDITOR.instances[this.editorName];
      this.props.onChange(e.getData());
    }
  },
  handleBlur: function() {
    this.setState({focused: false});
  },
  handleFocus: function() {
    if (this.state.ready) {
      this.setState({focused: true});
    }
  },
  handleClick: function() {
    if (this.state.ready) {
      React.findDOMNode(this.refs.editor).focus();
    }
  },
  setupEditor: function() {
    try {
      var node = React.findDOMNode(this.refs.editor);
      var fonts = this.props.fonts.join(";");
      var ckfonts = CKEDITOR.config.font_names;
      var font_names = fonts ? fonts + ";" + ckfonts : ckfonts;
      var colors = this.props.colors;
      var s = "";
      if (colors.length > 0) {
        s = colors.join(",");
        s = s + ",";
      }
      SHOGUN_JQUERY(node).html(this.props.value);
      var e = CKEDITOR.inline(
        node,
        {
          removePlugins: 'liststyle,tabletools,contextmenu',
          font_names: font_names,
          colorButton_colors: (s + CKEDITOR.config.colorButton_colors),
          toolbar: [
            { name: 'toolbar',
              groups: [ 'tools' ],
              items: [
                'Bold',
                'Italic',
                'Underline',
                'Strike',
                'Link',
                'Format',
                'Font',
                'FontSize',
                'TextColor',
                'BGColor',
                'JustifyLeft',
                'JustifyCenter',
                'JustifyRight',
                'JustifyBlock',
                'NumberedList',
                'BulletedList',
                'Outdent',
                'Indent',
                'RemoveFormat'
              ]
            }
          ]
        }
      );
      this.editorName = e.name;
      e.on("change", this.handleChange);
      e.on("instanceReady", function() {
        this.setState({ready: true}, function() {
          this.skip = true;
          e.setData(this.props.value);
          this.skip = false;
        }.bind(this));
      }.bind(this));
    } catch(err) {
      console.log(err);
    }
  },
  teardownEditor: function() {
    var e = CKEDITOR.instances[this.editorName];
    if (e && e.destroy) {
      try {
        e.destroy(true);
        delete CKEDITOR.instances[this.editorName];
      } catch (err) {
        console.log(err);
      }
    }
    this.editorName = null;
    this.setState({ready: false});
  },
  componentDidUpdate: function(prevProps, prevState) {
    if (this.props.mutable && !prevProps.mutable) {
      this.setupEditor();
    } else if(!this.props.mutable && prevProps.mutable) {
      this.teardownEditor();
    }
    if (prevProps.value != this.props.value && this.editorName && !this.state.focused) {
      var e = CKEDITOR.instances[this.editorName];
      if (e.getData() != this.props.value) {
        this.skip = true;
        e.setData(this.props.value);
        this.skip = false;
      }
    }
  },
  componentDidMount: function() {
    if (this.props.mutable) {
      this.setupEditor();
    }
  },
  componentWillUnmount: function() {
    this.teardownEditor();
  },
  render: function() {
    if (this.props.mutable) {
      var cn = "shogun-input-wrapper";
      if (this.state.focused) {
        cn += " shogun-rt-focused";
      }
      return (
        <div key="mutable-rt" className={cn}>
          <div
            onBlur={this.handleBlur}
            onFocus={this.handleFocus}
            className="shogun-rt"
            contentEditable={true}
            ref="editor" />
          <span onClick={this.handleClick} className="shogun-overlay shogun-overlay-rich-text"><img src={window.TEXT_IMAGE_URL} /></span>
        </div>
      );
    } else {
      return <div key="immutable-rt" ref="editor" dangerouslySetInnerHTML={{__html: this.props.value}}></div>;
    }
  }
});
