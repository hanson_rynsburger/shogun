var InlineMarkdown = React.createClass({
  handleClick: function(e) {
    e.preventDefault();
    this.props.onEditMarkdown();
  },
  render: function() {
    var value = this.props.value;
    if (this.props.mutable) {
      return (
        <div key="mutable-md" className="shogun-input-wrapper shogun-markdown" onClick={this.handleClick}>
          <div dangerouslySetInnerHTML={{__html: marked(value)}} />
          <div className="shogun-overlay"><img src={window.MARKDOWN_IMAGE_URL} /></div>
        </div>
      );
    } else {
      return <div key="immutable-md" className="shogun-markdown" dangerouslySetInnerHTML={{__html: marked(value)}} />;
    }
  }
});
