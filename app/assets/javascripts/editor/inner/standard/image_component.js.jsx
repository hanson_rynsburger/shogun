var ImageComponent = React.createClass({
  mixins: [ComponentMixin],

  render: function() {
    return (
      <div {...this.getElementProperties()}>
        <InlineImageInput
          value={this.props.values[0].value[0]}
          mutable={this.isSelected()}
          onUploadImage={this.handleUploadImage.bind(this, "image_variable_id")} />
      </div>
    );
  }
});
