var AccordionComponent = React.createClass({
  mixins: [ComponentMixin],

  getInitialState: function() {
    return {active: 1};
  },

  handlePanelChange: function(k){
    this.setState({active: k});
  },

  render: function() {
    var vbv = SHOGUN_UNDERSCORE.indexBy(this.props.values, function(v) {
      return v.variable_id;
    });
    var titles = vbv["accordion_titles_variable_id"].value;
    var count = titles.length > 0 ? titles.length : 1;
    var active = count < this.state.active ? count : this.state.active;
    
    var panels = [];

    for (var i = 1; i <= count; i++) {
      var key = "accordion_"+i+"_variable_id";
      var panel = vbv[key];

      var dz = (
        <MultiDropzone
          {...this.props}
          parent_uuid={this.props.uuid}
          references={panel.references}
          variable_id={panel.variable_id} />
      );

      var cname = (i == active) ? "in" : "";
      panels.push(
        <div className="shogun-panel" key={key}>
          <div className="shogun-panel-heading">
            <h4 className="shogun-panel-title">
              <a href="#" onClick={this.handlePanelChange.bind(this, i)}>
                { titles[i-1] }
              </a>
            </h4>
          </div>
          <div className={"shogun-panel-body " + cname}>
            {dz}
          </div>
        </div>
      );
    }

    return (
      <div {...this.getElementProperties()}>
        <div className="shogun-panel-group">
          { panels }
        </div>
      </div>
    );
  }
});
