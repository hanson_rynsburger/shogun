var TabsComponent = React.createClass({
  mixins: [ComponentMixin],

  getInitialState: function() {
    return {active: 1};
  },

  handleTabCountChange: function(k){
    this.setState({active: k});
  },

  render: function() {
    var vbv = SHOGUN_UNDERSCORE.indexBy(this.props.values, function(v) {
      return v.variable_id;
    });
    var tc = parseInt(vbv["tabs_count_variable_id"].value[0]);
    tc = tc > 0 ? tc : 1;
    var active = tc < this.state.active ? tc : this.state.active;
    var titles = vbv["tabs_title_variable_id"].value;

    var panes = [];
    var navs = [];
    for (var i = 1; i <= tc; i++) {
      var key = "tab_"+i+"_variable_id";
      var tab = vbv[key];

      var dz = (
        <MultiDropzone
          {...this.props}
          parent_uuid={this.props.uuid}
          references={tab.references}
          variable_id={tab.variable_id} />
      );

      var cname = (i == active) ? "active" : "";
      panes.push(
        <div className={"shogun-tab-pane " + cname} key={key}>
          {dz}
        </div>
      );

      navs.push(
        <li className={cname} key={key}>
          <a href={"#" + key} onClick={this.handleTabCountChange.bind(this, i)}>
            {titles[i - 1] || 'New tab'}
          </a>
        </li>
      );
    }

    return (
      <div {...this.getElementProperties()}>
        <ul className="shogun-nav-tabs">
          {navs}
        </ul>
        <div className="shogun-tab-content">
          {panes}
        </div>
      </div>
    );
  }
});
