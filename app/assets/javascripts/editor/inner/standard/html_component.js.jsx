var HTMLComponent = React.createClass({
  mixins: [ComponentMixin],

  render: function() {
    return (
      <div {...this.getElementProperties()}>
        <InlineHTML
          mutable={this.isSelected()}
          value={this.props.values[0].value[0]}
          onEditHTML={this.handleEditHTML.bind(this, "html_variable_id")} />
      </div>
    );
  }
});
