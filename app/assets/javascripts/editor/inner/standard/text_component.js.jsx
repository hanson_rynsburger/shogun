var TextComponent = React.createClass({
  mixins: [ComponentMixin],

  render: function() {
    return (
      <div {...this.getElementProperties()}>
        <InlineRichText
          mutable={this.isSelected()}
          fonts={this.props.site.fonts}
          colors={this.props.site.colors}
          value={this.props.values[0].value[0] || "Text"}
          onChange={this.handleUpdate.bind(this, "text_variable_id")} />
      </div>
    );
  }
});
