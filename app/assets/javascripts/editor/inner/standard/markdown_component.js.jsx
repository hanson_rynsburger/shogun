var MarkdownComponent = React.createClass({
  mixins: [ComponentMixin],

  render: function() {
    return (
      <div {...this.getElementProperties()}>
        <InlineMarkdown
          mutable={this.isSelected()}
          value={this.props.values[0].value[0]}
          onEditMarkdown={this.handleEditMarkdown.bind(this, "markdown_variable_id")} />
      </div>
    );
  }
});
