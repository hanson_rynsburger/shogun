var ColumnsComponent = React.createClass({
  mixins: [ComponentMixin],

  buildClassName: function(value) {
    var base = "shg-c-";
    var cname = "";
    var p = value.value[0];

    SHOGUN_UNDERSCORE.each(SCREEN_SIZES, function(s) {
      cname += base + s + "-" + (p[s] || 12);
      if (s != "xs") {
        cname += " ";
      }
    });
    return cname;
  },

  renderGutterStyle: function() {
    var vbv = SHOGUN_UNDERSCORE.indexBy(this.props.values, function(v) {
      return v.variable_id;
    });
    var value = vbv["col_gutter_variable_id"]

    if (!value) {
      return null;
    }

    value = value.value[0];

    var actualGap = parseInt( value.substring( 0, value.length - 2 ) ) / 2;
    var gapUnit = value.substring( value.length - 2 );

    var uuid = this.props.uuid;
    var newcss = '[id="'+uuid+'"] > .shg-row { margin-left: -' + actualGap + gapUnit + '; margin-right: -' + actualGap + gapUnit + '; }\n'
             + '[id="'+uuid+'"] > .shg-row > div {\n'
             + 'padding-right: ' + actualGap + gapUnit + '; padding-left: ' + actualGap + gapUnit + ';'
             + '}';

    return (
      <style type="text/css" dangerouslySetInnerHTML={{__html: newcss}}></style>
    );
  },

  render: function() {
    var vbv = SHOGUN_UNDERSCORE.indexBy(this.props.values, function(v) {
      return v.variable_id;
    });
    var cc = parseInt(vbv["col_count_variable_id"].value[0]);

    cc = cc > 0 ? cc : 2;

    var cols = [];

    for (var i = 1; i <= cc; i++) {
      var k = "col_"+i+"_variable_id";
      var value = vbv[k];
      var dz = (
        <MultiDropzone
          {...this.props}
          parent_uuid={this.props.uuid}
          references={value.references}
          variable_id={value.variable_id} />
      );
      var cname = this.buildClassName(value);
      cols.push(
        <div className={cname} key={k}>
          {dz}
        </div>
      );
    }

    return (
      <div {...this.getElementProperties()}>
        {this.renderGutterStyle()}
        <div className="shg-row">
          {cols}
        </div>
      </div>
    );
  }
});
