var ButtonComponent = React.createClass({
  mixins: [ComponentMixin, Hoverable, Clickable],
  render: function() {
    var ep = this.getElementProperties();
    var style = ep["style"];
    delete ep["style"];

    var vbv = SHOGUN_UNDERSCORE.indexBy(this.props.values, function(v) {
      return v.variable_id;
    });

    var href = vbv["button_href_variable_id"].value[0];
    var text = vbv["button_text_variable_id"].value[0];
    var classes = vbv["button_classes_variable_id"].value[0];

    if (!style.display) {
      style.display = "block";
    }

    return (
      <div {...ep}>
        <a href={href} className={classes} style={style} {...this.getHoverEvents()} {...this.getClickEvents()}>
          {text || "TEXT"}
        </a>
      </div>
    );
  }
});
