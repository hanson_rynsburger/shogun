var VideoComponent = React.createClass({
  mixins: [ComponentMixin],

  render: function() {
    var values = this.props.values;
    var url = SHOGUN_UNDERSCORE.find(values, function(v) {
      return v.variable_id == "url_variable_id";
    });
    var aspectRatio = SHOGUN_UNDERSCORE.find(values, function(v) {
      return v.variable_id == "aspect_ratio_variable_id";
    });

    var cn = "shogun-video shogun-video-" + aspectRatio.value[0];
    var content;
    if (!url.value[0]) {
      cn += " shogun-video-blank";
      content = <Icon name="play" />;
    } else {
      content = <iframe className="shogun-video-embed" src={url.value[0]} />;
    }
    return (
      <div key="video" {...this.getElementProperties()}>
        <div className={cn}>
          {content}
        </div>
      </div>
    );
  }
});
