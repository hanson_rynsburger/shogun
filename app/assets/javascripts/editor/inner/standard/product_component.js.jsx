var ProductComponent = React.createClass({
  mixins: [ComponentMixin],

  render: function() {
    var values = this.props.values;
    var vbv = SHOGUN_UNDERSCORE.indexBy(values, function(v) {
      return v.variable_id;
    });

    var pidvar = vbv["product_id_variable_id"];
    var vidvar = vbv["variant_id_variable_id"];
    var outputvar = vbv["output_variable_id"];
    var pid = pidvar.value[0];
    var vid = vidvar.value[0];
    var output = outputvar.value[0];
    var model = this.props.getModel(pidvar.model_id);

    var content;

    if (pid && vid) {
      var entry = SHOGUN_UNDERSCORE.find(model.entries, function(e) {
        return e.id == pid;
      });
      if (entry) {
        var variant = SHOGUN_UNDERSCORE.find(entry.variants, function(v) {
          return v.id == vid;
        });
        if (variant) {
          if (!output || output == "image") {
            var image = SHOGUN_UNDERSCORE.find(entry.images, function(i) {
              return i.id == variant.image_id || !variant.image_id;
            });
            if (!!image) {
              var link = vbv["link_variable_id"];
              var i = <img src={image.src} className="shogun-image" />;
              if (link.value[0]) {
                content = <a href={"/products/"+entry.handle}>{i}</a>;
              } else {
                content = i;
              }
            }
          } else if (output == "body_html") {
            content = <div dangerouslySetInnerHTML={{__html: entry.body_html}} />;
          } else if (output == "variant_title") {
            content = <h1>{variant.title}</h1>;
          } else if (output == "title") {
            content = <h1>{entry.title}</h1>;
          }
        }
      }
    }

    return (
      <div key="product" {...this.getElementProperties()}>
        {content}
      </div>
    );
  }
});
