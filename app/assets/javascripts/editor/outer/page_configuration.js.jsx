var PageConfiguration = React.createClass({
  handleNameChange: function(name) {
    this.props.onChange({name: name});
  },
  handlePathChange: function(path) {
    this.props.onChange({path: path});
  },
  handleAuthorChange: function(author) {
    this.props.onChange({author: author});
  },
  renderPathField: function() {
    if (this.props.site.shopify) {
      var handle = this.props.path;
      if (handle && handle.toString().indexOf("/") == 0) {
        handle = handle.substring(1);
      }
      return (
        <div className="input-group input-group-sm">
          <span className="input-group-addon">pages/</span>
          <TextInput small value={handle} onChange={this.handlePathChange} placeholder="handle" />
        </div>
      );
    } else {
      return <TextInput small value={this.props.path} onChange={this.handlePathChange} placeholder="Path" />;
    }
  },
  renderAuthorField: function() {
    if (this.props.site.shopify && this.props.isBlog) {
      return (
        <div className="input-group input-group-sm" style={{marginLeft: "10px"}}>
          <TextInput small value={this.props.author} onChange={this.handleAuthorChange} placeholder="Author" />
        </div>
      )
    }
  },
  renderForm: function() {
    var metaButton,
    icon = this.props.saving ? <Icon spin name="refresh" /> : <Icon name="save" />;
    if (this.props.site.shopify || this.props.site.features.meta_tags) {
      metaButton = <Button small onClick={this.props.toggleAdvanced} icon="cogs" title="Advanced Configuration" tooltip="bottom" />
    }
    return (
      <div className="form-inline">
        <TextInput small value={this.props.name} onChange={this.handleNameChange} placeholder="Name" />
        {this.renderPathField()}
        {this.renderAuthorField()}
        <div className="btn-group">
          <Button small onClick={this.props.onSave} disabled={this.props.saving || !this.props.changed} title="Save" tooltip="bottom">{icon}</Button>
          {metaButton}
        </div>
      </div>
    );
  },
  renderPreviewButton: function() {
    var preview_url;

    var site = this.props.site;
    if (site.shopify) {
      preview_url = site.url + "/a/shogun/previews/" + this.props.id;
    } else if (site.preview_url) {
      preview_url = site.preview_url + "/shogun/previews/" + this.props.id;
    } else {
      preview_url = site.url + "/shogun/previews/" + this.props.id;
    }

    return <Button small key="preview" tooltip="bottom" title="Preview" href={preview_url} target="_blank" icon="eye" />;
  },
  renderLiveButton: function() {
    if (this.props.published) {
      var url = this.props.site.url;

      if (this.props.site.shopify) {
        if (this.props.blog) {
          var kvp = _(this.props.blogs).indexBy(function(obj) { return obj[1] });
          url += "/blogs/" + kvp[this.props.blog][0] + this.props.path;
        } else {
          url += "/pages" + this.props.path;
        }
      } else {
        url += this.props.path;
      }
      return <Button small key="live" tooltip="bottom" title="View Live Page" href={url} target="_blank" icon="external-link-square" />;
    }
  },
  renderPublishButton: function() {
    var icon = this.props.publishing ? <Icon spin name="refresh" /> : <Icon name="cloud-upload" />;
    if (this.props.site.permissions.publish && !this.props.updating && (this.props.outdated || this.props.draft)) {
      if (this.props.external_errors.length) {
        return <Button warning small key="publish-errors" onClick={this.props.onPublish} tooltip="bottom" title={"Shopify errors: " + this.props.external_errors.join(". ") + ". Click to retry!"} icon="warning" />;
      } else {
        return <Button small key="publish" onClick={this.props.onPublish} disabled={this.props.publishing || this.props.unpublishing} title="Publish" tooltip="bottom">{icon}</Button>;
      }
    }
  },
  renderUnpublishButton: function() {
    var icon = this.props.unpublishing ? <Icon spin name="refresh" /> : <Icon name="cloud-download" />;
    if (this.props.site.permissions.publish && !this.props.updating && this.props.published) {
      return <Button small key="unpublish" title="Unpublish" onClick={this.props.onUnpublish} disabled={this.props.unpublishing || this.props.publishing} tooltip="bottom">{icon}</Button>;
    }
  },
  renderWorkingButton: function() {
    if (this.props.updating) {
      return <Button small key="working" disabled title="Working..." tooltip="bottom"><Icon spin name="refresh" /></Button>;
    }
  },
  renderDuplicateButton: function() {
    return <Button small key="duplicate" onClick={this.props.onDuplicate} title="Duplicate" tooltip="bottom" icon="copy" />;
  },
  renderVersionsButton: function() {
    return <Button small key="versions" onClick={this.props.onVersions} title="Versions" tooltip="bottom" icon="history" />;
  },
  renderUndoButton: function() {
    return <Button small key="undo" onClick={this.props.onUndo} disabled={this.props.undos.length == 0} title="Undo" tooltip="bottom" icon="undo" />;
  },
  renderRedoButton: function() {
    return <Button small key="redo" onClick={this.props.onRedo} disabled={this.props.redos.length == 0} title="Redo" tooltip="bottom" icon="repeat" />;
  },
  renderChangedActions: function() {
    return (
      <div className="page-actions">
        <div className="btn-group">
          {this.renderUndoButton()}
          {this.renderRedoButton()}
        </div>
      </div>
    );
  },
  renderUnchangedActions: function() {
    var preview = this.renderPreviewButton();
    var live = this.renderLiveButton();
    var publish = this.renderPublishButton();
    var unpublish = this.renderUnpublishButton();
    var working = this.renderWorkingButton();
    var duplicate = this.renderDuplicateButton();
    var versions = this.renderVersionsButton()

    return (
      <div className="page-actions">
        <div className="btn-group">
          {preview}
          {publish}
          {unpublish}
          {live}
          {working}
        </div>
        <div className="btn-group">
          {versions}
          {duplicate}
        </div>
      </div>
    );
  },
  renderActions: function() {
    if (this.props.changed) {
      return this.renderChangedActions();
    } else if (!!this.props.id) {
      return this.renderUnchangedActions();
    }
  },
  render: function() {
    return (
      <div className="shogun-page-configuration">
        {this.renderForm()}
        {this.renderActions()}
      </div>
    );
  }
});
