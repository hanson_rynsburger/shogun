var Sidebar = React.createClass({
  render: function() {
    var cn = "shogun-sidebar";
    if (this.props.right) {
      cn += " shogun-sidebar-right";
    } else if (this.props.left) {
      cn += " shogun-sidebar-left";
    }
    return (
      <div className={cn}>
        {this.props.children}
      </div>
    );
  }
});
