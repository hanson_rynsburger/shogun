var AccordionConfiguration = React.createClass({

  getAccordionCount: function(component) {
    var values = component.values;
    var vbv = _.indexBy(values, function(v) {
      return v.variable_id;
    });
    return vbv["accordion_titles_variable_id"].value.length;
  },

  getAccordionTitles: function(component) {
    var values = component.values;
    var vbv = _.indexBy(values, function(v) {
      return v.variable_id;
    });
    return vbv["accordion_titles_variable_id"];
  },

  moveContents: function(vbv, from, to){
    var i, minPos, maxPos;
    var temp = vbv["accordion_"+from+"_variable_id"];
    if (from < to) {
      minPos = from;
      maxPos = to;
      for (i = from; i <to; i++) {
        vbv["accordion_"+i+"_variable_id"] = vbv["accordion_"+(i+1)+"_variable_id"]
      }
    } else {
      minPos = to;
      maxPos = from;
      for (i = from; i > to; i--) {
        vbv["accordion_"+i+"_variable_id"] = vbv["accordion_"+(i-1)+"_variable_id"]
      }
    }
    vbv["accordion_"+to+"_variable_id"] = temp;
    
    for (i = minPos; i <= maxPos; i++) {
      vbv["accordion_"+i+"_variable_id"].variable_id = "accordion_"+i+"_variable_id";
      vbv["accordion_"+i+"_variable_id"].name = "accordion_"+i;
    }
  },

  removeContent: function(vbv, pos){
    vbv["accordion_"+pos+"_variable_id"].references = [];
  },

  changeTitles: function(value, action, pos) {
    if (value.length < 1 || value.length > 10) return;

    var clone = JSON.parse(JSON.stringify(this.props.component));
    var oldCount = this.getAccordionCount(clone);
    var vbv = _.indexBy(clone.values, function(v) {
      return v.variable_id;
    });
    var titles = this.getAccordionTitles(clone);
    titles.value = value;

    if (action === 'move') {
      this.moveContents(vbv, pos[0] + 1, pos[1] + 1);
    } else if (action === 'remove') {
      this.removeContent(vbv, pos+1);
      this.moveContents(vbv, pos+1, oldCount);
    } else if (action === 'add') {
      value[pos - 1] = "New Accordion";
    }

    this.props.onUpdate(clone);
  },
  render: function() {
    var clone = JSON.parse(JSON.stringify(this.props.component));
    var titles = this.getAccordionTitles(clone);
    return (
      <MultiTextInput
        small
        key={titles.name}
        label="Titles"
        value={titles}
        onChange={this.changeTitles} />
    );
  }
});
