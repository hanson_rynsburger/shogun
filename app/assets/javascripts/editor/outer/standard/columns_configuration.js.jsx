var ColumnsConfiguration = React.createClass({
  getColumnCount: function(component) {
    var values = component.values;
    var vbv = _.indexBy(values, function(v) {
      return v.variable_id;
    });
    var cc = vbv["col_count_variable_id"];
    return cc.value[0];
  },
  getColumnGutter: function(component) {
    var values = component.values;
    var vbv = _.indexBy(values, function(v) {
      return v.variable_id;
    });

    return vbv["col_gutter_variable_id"];
  },
  getColumnValues: function(override) {
    var count = this.getColumnCount(this.props.component);
    var sizes = this.getColumnSizes(count);
    var values = [];
    var vbv = _.indexBy(this.props.component.values, function(v) {
      return v.variable_id;
    });
    for (var i = 0; i < sizes.length - 1; i++) {
      var col = vbv["col_"+(i + 1)+"_variable_id"];
      var v;
      if (!col.value[0] || override) {
        v = sizes[i].lg;
      } else {
        v = col.value[0].lg;
      }
      if (i > 0) {
        v += values[i - 1];
      }
      values.push(v);
    }
    return values;
  },
  handleSlide: function(event, ui) {
    if (ui.value == 0 || ui.value == 12) {
      return false;
    }
    var count = 0;
    _.each(ui.values, function(v) {
      if (v == ui.value) {
        count += 1;
      }
    });
    if (count > 1) {
      return false;
    }
    var ordered = _.every(ui.values, function(v, i) {
      return i == ui.values.length - 1 || ui.values[i + 1] >= v;
    });

    if (ordered) {
      this.handleChange(ui.values);
    } else {
      return false;
    }
  },
  handleChange: function(val) {
    var count = this.getColumnCount(this.props.component);
    var clone = JSON.parse(JSON.stringify(this.props.component));

    var vbv = _.indexBy(clone.values, function(v) {
      return v.variable_id;
    });

    for (var i = 1; i <= count; i++) {
      var col = vbv["col_"+i+"_variable_id"];
      var ev = col.value[0];
      var c;
      if (i == count) {
        c = 12 - val[i - 2];
      } else {
        c = val[i - 1];
        if (i != 1) {
          c -= val[i - 2];
        }
      }
      col.value = [
        {xs: ev.xs, sm: c, md: c, lg: c}
      ];
    }

    this.props.onUpdate(clone);
  },
  getSliderOptions: function() {
    return {
      step: 1,
      min: 0,
      max: 12,
      values: this.getColumnValues(),
      slide: this.handleSlide
    };
  },
  setupSlider: function() {
    var node = React.findDOMNode(this.refs.slider);
    $(node).slider(this.getSliderOptions());
  },
  teardownSlider: function() {
    var node = React.findDOMNode(this.refs.slider);
    $(node).slider("destroy");
  },
  componentWillUnmount: function() {
    this.teardownSlider();
  },
  componentDidMount: function() {
    this.setupSlider();
  },
  componentDidUpdate: function(prevProps, prevState) {
    var oldCount = this.getColumnCount(prevProps.component);
    var count = this.getColumnCount(this.props.component);

    if (oldCount != count) {
      this.teardownSlider();
      this.setupSlider(true);
    }
  },
  getColumnSizes: function(count) {
    var cs;
    switch(count) {
      case 2:
        cs = {xs: 12, sm: 6, md: 6, lg: 6};
        return [cs, cs];
      case 3:
        cs = {xs: 12, sm: 4, md: 4, lg: 4}
        return [cs, cs, cs];
      case 4:
        cs = {xs: 6, sm: 3, md: 3, lg: 3};
        return [cs, cs, cs, cs];
      case 5:
        cs = {xs: 6, sm: 2, md: 2, lg: 2};
        return [cs, cs, {xs: 12, sm: 4, md: 4, lg: 4}, cs, cs];
      case 6:
        cs = {xs: 6, sm: 2, md: 2, lg: 2};
        return [cs, cs, cs, cs, cs, cs];
    }
  },
  handleColumnCountChange: function(count) {
    var clone = JSON.parse(JSON.stringify(this.props.component));

    var vbv = _.indexBy(clone.values, function(v) {
      return v.variable_id;
    });
    var cc = vbv["col_count_variable_id"];
    var oldCount = cc.value[0];
    if (oldCount != count) {
      cc.value[0] = count;
      var val = this.getColumnSizes(count);
      for (var i = 1; i <= count; i++) {
        var col = vbv["col_"+i+"_variable_id"];
        col.value = [val[i - 1]];
      }
      if (oldCount > count) {
        var recipient = vbv["col_"+count+"_variable_id"];
        for (var i = count + 1; i <= oldCount; i++) {
          var col = vbv["col_"+i+"_variable_id"];
          _.each(col.references, function(ref) {
            recipient.references.push({
              uuid: ref.uuid,
              position: recipient.references.length
            });
          });
          col.references = [];
        }
      }
      this.props.onUpdate(clone);
    }
  },
  handleColumnGutterChange: function(value) {
    var clone = JSON.parse(JSON.stringify(this.props.component));
    var vbv = _.indexBy(clone.values, function(v) {
      return v.variable_id;
    });
    var cc = vbv["col_gutter_variable_id"];
    var oldCount = cc.value[0];

    if (oldCount == value) return;

    _.extend(_.findWhere(clone.values, { variable_id: "col_gutter_variable_id" }), {value: [value]});
    this.props.onUpdate(clone);
  },
  renderSlider: function() {
    return (
      <div ref="slider"></div>
    );
  },
  render: function() {
    var count = this.getColumnCount(this.props.component);
    var gutter = this.getColumnGutter(this.props.component);
    var gutterInput = gutter ? <UnitTextInput label="Column gutter" value={gutter.value[0]} onChange={this.handleColumnGutterChange} /> : null;
    var minusDisabled = count == 2;
    var plusDisabled = count == 6;
    return (
      <div className="shogun-columns-configuration">
        <h4>Configuration</h4>
        <label className="control-label">Number of columns</label>
        <div className="input-group input-group-sm">
          <span className="input-group-btn">
            <Button small icon="minus-circle" disabled={minusDisabled} onClick={this.handleColumnCountChange.bind(this, count - 1)} />
          </span>
          <input type="text" className="form-control" disabled value={count} />
          <span className="input-group-btn">
            <Button small icon="plus-circle" disabled={plusDisabled} onClick={this.handleColumnCountChange.bind(this, count + 1)} />
          </span>
        </div>
        <label className="control-label">Column widths</label>
        {this.renderSlider()}
        {gutterInput}
      </div>
    );
  }
});
