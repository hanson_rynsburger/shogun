var ProductConfiguration = React.createClass({
  handleChange: function(attr, value) {
    this.props.onUpdateValue(attr, value);
    if (attr == "product_id_variable_id") {
      this.props.onUpdateValue("variant_id_variable_id", null);
    }
  },
  render: function() {
    var values = this.props.component.values;
    var vbv = _.indexBy(values, function(v) {
      return v.variable_id;
    });
    var model = this.props.getModel(values[0].model_id);
    var entries = model.entries;

    var options = _.map(entries, function(e) {
      return [e.title, e.id];
    });
    options.unshift(["", null]);

    var pidvar = vbv["product_id_variable_id"];
    var pid = pidvar.value[0];

    var selectedProduct, variantSelector, variantImage, outputSelector, link;
    if (pid) {
      selectedProduct = _.find(model.entries, function(m) {
        return m.id == pid;
      });
      if (selectedProduct) {
        var vidvar = vbv["variant_id_variable_id"];
        var vid = vidvar.value[0];

        var variantOptions = _.map(selectedProduct.variants, function(v) {
          return [v.title, v.id];
        });
        variantOptions.unshift(["", null]);

        variantSelector = (
          <SelectInput options={variantOptions} label="Variant" onChange={this.handleChange.bind(this, "variant_id_variable_id")} value={vid} />
        );

        if (vid) {
          var variant = _.find(selectedProduct.variants, function(v) {
            return v.id == vid;
          });
          if (variant) {
            var outputs = [["Image", "image"], ["Description", "body_html"], ["Product Title", "title"], ["Variant Title", "variant_title"]];
            var outputvar = vbv["output_variable_id"];
            var output = outputvar.value[0];
            outputSelector = (
              <SelectInput options={outputs} label="Output" onChange={this.handleChange.bind(this, "output_variable_id")} value={output} />
            );
            if (!output || output == "image" || output == "title" || output == "variant_title") {
              var l = vbv["link_variable_id"].value[0];
              link = <Checkbox checked={l} onChange={this.handleChange.bind(this, "link_variable_id")} label="Link to product" />;
            }
            var imageId = variant.image_id;
            var img = _.find(selectedProduct.images, function(i) {
              return i.id == imageId || !imageId;
            });
            if (!!img) {
              variantImage = (
                <div>
                  <p>Image</p>
                  <img className="img-responsive" src={img.src} />
                </div>
              );
            } else {
              variantImage = <div className="alert alert-danger">Missing image!</div>;
            }
          }
        }
      }
    }

    return (
      <div className="shogun-product-configuration">
        <h4>Configuration</h4>
        <SelectInput options={options} label="Product" onChange={this.handleChange.bind(this, "product_id_variable_id")} value={pid} />
        {variantSelector}
        {outputSelector}
        {link}
        {variantImage}
      </div>
    );
  }
});
