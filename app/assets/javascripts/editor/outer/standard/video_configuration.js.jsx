var VideoConfiguration = React.createClass({
  isEmbedUrl: function(u) {
    return u.hostname.indexOf("player.vimeo.com") >= 0 || (u.hostname.indexOf("youtube.com") >= 0 && u.pathname.indexOf("/embed") == 0);
  },
  parseURL: function(url) {
    try {
      var u = new URL(url);
      if (this.isEmbedUrl(u)) {
        return url;
      }

      if (u.hostname.indexOf("youtube.com") >= 0 && u.pathname.indexOf("/watch") == 0) {
        var search = u.search.substring(1);
        var split = search.split("&");
        var v = _.find(split, function(s) {
          return s.indexOf("v=") == 0;
        });
        if (v) {
          var ytid = v.substring(2);
          return "https://youtube.com/embed/" + ytid;
        }
      } else if (u.hostname.indexOf("youtu.be") >= 0) {
        var re = /^\/[\w]+/;
        if (re.test(u.pathname)) {
          var ytid = u.pathname.split("/")[1];
          return "https://youtube.com/embed/" + ytid;
        }
      } else if (u.hostname.indexOf("vimeo") >= 0) {
        var split = u.pathname.split("/");
        var last = split[split.length - 1];
        var re = /^[\d]+$/;
        if (re.test(last)) {
          return "https://player.vimeo.com/video/" + last;
        }
      }
    } catch(e) {
    }
    return url;
  },
  handleChange: function(attr, value) {
    if (attr == "url_variable_id") {
      value = this.parseURL(value);
    }
    this.props.onUpdateValue(attr, value);
  },
  render: function() {
    var values = this.props.component.values;

    var vbv = _.indexBy(values, function(v) {
      return v.variable_id;
    });

    var url = vbv["url_variable_id"];
    var loop = vbv["loop_variable_id"];
    var autoplay = vbv["autoplay_variable_id"];
    var aspectRatio = vbv["aspect_ratio_variable_id"];
    var options = [{value: "16x9", label: "16 x 9"}, {value: "4x3", label: "4 x 3"}];

    return (
      <div className="shogun-video-configuration">
        <h4>Configuration</h4>
        <label className="control-label">URL</label>
        <TextInput
          small
          value={url.value[0]}
          onChange={this.handleChange.bind(this, "url_variable_id")} />
        <label className="control-label">Aspect Ratio</label>
        <RadioButtonGroup
          small
          justified
          options={options}
          onSelect={this.handleChange.bind(this, "aspect_ratio_variable_id")}
          value={aspectRatio.value[0]} />
        <Columns>
          <Checkbox key="loop" label="loop" checked={loop.value[0]} onChange={this.handleChange.bind(this, "loop_variable_id")} />
          <Checkbox key="autoplay" label="autoplay" checked={autoplay.value[0]} onChange={this.handleChange.bind(this, "autoplay_variable_id")} />
        </Columns>
      </div>
    );
  }
});
