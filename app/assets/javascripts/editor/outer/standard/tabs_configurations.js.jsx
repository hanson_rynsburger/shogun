var TabsConfiguration = React.createClass({

  getTabCount: function(component) {
    var values = component.values;
    var vbv = _.indexBy(values, function(v) {
      return v.variable_id;
    });
    var tc = vbv["tabs_count_variable_id"];
    return tc.value[0];
  },

  getTitles: function(component) {
    var values = component.values;
    var vbv = _.indexBy(values, function(v) {
      return v.variable_id;
    });
    return vbv["tabs_title_variable_id"];
  },

  moveContents: function(vbv, from, to){
    var i, minPos, maxPos;
    var temp = vbv["tab_"+from+"_variable_id"];
    if (from < to) {
      minPos = from;
      maxPos = to;
      for (i = from; i <to; i++) {
        vbv["tab_"+i+"_variable_id"] = vbv["tab_"+(i+1)+"_variable_id"]
      }
    } else {
      minPos = to;
      maxPos = from;
      for (i = from; i > to; i--) {
        vbv["tab_"+i+"_variable_id"] = vbv["tab_"+(i-1)+"_variable_id"]
      }
    }
    vbv["tab_"+to+"_variable_id"] = temp;
    
    for (i = minPos; i <= maxPos; i++) {
      vbv["tab_"+i+"_variable_id"].variable_id = "tab_"+i+"_variable_id";
      vbv["tab_"+i+"_variable_id"].name = "tab_"+i;
    }
  },

  removeContent: function(vbv, pos){
    vbv["tab_"+pos+"_variable_id"].references = [];
  },

  changeTitles: function(value, action, pos) {
    if (value.length < 1 || value.length > 10) return;

    var clone = JSON.parse(JSON.stringify(this.props.component));
    var count = this.getTabCount(clone);
    var vbv = _.indexBy(clone.values, function(v) {
      return v.variable_id;
    });
    var titles = vbv["tabs_title_variable_id"];
    var cc = vbv["tabs_count_variable_id"];
    if (value.length != count) {
      cc.value[0] = value.length;
    }

    if (action === 'move') {
      this.moveContents(vbv, pos[0] + 1, pos[1] + 1);
    } else if (action === 'remove') {
      this.removeContent(vbv, pos+1);
      this.moveContents(vbv, pos+1, count);
    } else if (action === 'add') {
      value[count] = "New Tab";
    }

    titles.value = value;
    this.props.onUpdate(clone);
  },
  render: function() {
    var clone = JSON.parse(JSON.stringify(this.props.component));
    var count = this.getTabCount(clone);
    var titles = this.getTitles(clone);
    return (
      <MultiTextInput
        small
        key={titles.name}
        label="Tab Titles"
        value={titles}
        onChange={this.changeTitles} />
    );
  }
});