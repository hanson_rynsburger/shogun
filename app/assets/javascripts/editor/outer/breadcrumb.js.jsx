var Breadcrumb = React.createClass({
  handleMouseOver: function(uuid) {
    this.props.onHighlight(uuid);
  },
  handleMouseOut: function() {
    this.props.onHighlight(null);
  },
  handleSelect: function(uuid) {
    this.props.onSelect(uuid);
  },
  renderBreadcrumbs: function() {
    return this.props.hierarchy.map(function(component, i) {
      if (i == this.props.hierarchy.length - 1) {
        return (
          <li className="active" key={component.uuid}>{component.template_name}</li>
        );
      } else {
        return (
          <li key={component.uuid}>
            <a onMouseOver={this.handleMouseOver.bind(this, component.uuid)} onMouseOut={this.handleMouseOut} onClick={this.handleSelect.bind(this, component.uuid)}>{component.template_name}</a>
          </li>
        );
      }
    }.bind(this));
  },
  render: function() {
    return (
      <div className="shogun-breadcrumb">
        <ol className="breadcrumb">
          {this.renderBreadcrumbs()}
        </ol>
      </div>
    );
  }
});
