var Editor = React.createClass({
  mixins: [EmbedManager],

  getInitialState: function() {
    var s = this.buildState(this.props.page);
    s.view = "desktop";
    s.dropzones = false;
    return s;
  },

  // serialization

  getValuesAttributes: function(values) {
    return values.map(function(val) {
      var arg_value = [];
      val.value.forEach(function(v) {
        arg_value.push(v);
      });
      return {
        value: arg_value,
        variable_id: val.variable_id,
        references_attributes: this.getReferencesAttributes(val.references)
      }
    }.bind(this));
  },
  getComponentAttributes: function(component) {
    var comp = {
      uuid: component.uuid,
      template_id: component.template_id,
      template_version_id: component.template_version_id,
      styles: component.styles,
      data_attributes: component.data_attributes,
      css_classes: component.css_classes
    };
    if (comp.template_id) {
      comp.template_name = component.template_name;
    }
    comp.values_attributes = this.getValuesAttributes(component.values);
    return comp;
  },
  getReferencesAttributes: function(references) {
    return references.map(function(ref) {
      var component = this.state.components[ref.uuid];
      return {
        id: ref.id,
        component_attributes: this.getComponentAttributes(component),
        position: ref.position
      }
    }.bind(this));
  },
  getArgs: function() {
    var s = this.state;
    var path = s.path;
    if (!!path.toString() && path.indexOf("/") != 0) {
      path = "/" + path;
    }
    var args = {site_id: this.props.site.id, id: s.id, path: path, name: s.name, meta: s.meta, shopify_template: s.shopify_template};
    var root = this.state.components[this.state.root_uuid];
    var value = root.values[0];
    args.current_version_attributes = {
      root_attributes: {
        values_attributes: [{
          references_attributes: this.getReferencesAttributes(root.values[0].references)
        }]
      }
    };

    if (this.props.params.blog_id) {
      args.external_blog_id = this.props.params.blog_id;
    }

    if ( this.state.author ) {
      args.author = this.state.author;
    }

    return args;
  },

  // state management

  save: function() {
    this.props.onSave(this.getArgs());
  },
  handleChange: function(chg) {
    chg.changed = true;
    this.setState(chg);
  },
  flattenComponentTree: function(map, component) {
    var components = [component];
    while(components.length > 0) {
      var c = components.shift();

      if (!c.uuid) {
        c.uuid = uuid.v4();
      }

      var values = _.map(c.values, function(v) {
        var r = _(v.references);
        r.each(function(ref) {
          var c1 = ref.component;
          c1.parent_uuid = c.uuid;
          components.push(c1);
        });
        var references = r.sortBy(function(ref) {
          return parseInt(ref.position);
        }).map(function(ref) {
          return {id: ref.id, uuid: ref.component.uuid, position: ref.position};
        });
        var variable = v.variable;
        if (variable) {
          return {
            id: v.id,
            variable_id: variable.id,
            value: v.value,
            references: references,
            // convenience
            type: variable.type,
            input: variable.input,
            name: variable.name,
            data_source: variable.data_source,
            model_id: variable.model_id,
            properties: variable.properties
          };
        } else {
          return {
            id: v.id,
            variable_id: null,
            value: [],
            references: references,
            // convenience
            type: "dropzone",
            input: "inline",
            name: "root",
            properties: []
          };
        }
      });

      map.components[c.uuid] = {
        id: c.id,
        uuid: c.uuid,
        liquid: c.liquid,
        data_attributes: (c.data_attributes || {}),
        parent_uuid: c.parent_uuid,
        template_id: c.template_id,
        css_classes: c.css_classes,
        template_version_id: c.template_version_id,
        template_name: c.template_name,
        css: c.css,
        values: values,
        styles: (c.styles || {})
      };
    }
  },
  cloneState: function() {
    var c = {components: this.state.components, root_uuid: this.state.root_uuid, changed: this.state.changed};
    return JSON.parse(JSON.stringify(c));
  },
  buildState: function(page) {
    var s = {};

    s.id = page.id;
    s.current_version_id = page.current_version_id;
    s.path = page.path;
    s.name = page.name;
    s.author = page.author;
    s.meta = page.meta_tags;
    s.outdated = page.outdated;
    s.draft = page.draft;
    s.published = page.published;
    s.updating = page.updating;
    s.external_errors = page.external_errors;
    s.undos = [];
    s.redos = [];
    s.selected_uuid = null;
    s.highlighted_uuid = null;
    s.tab = "palette";

    s.blog = page.external_blog_id;
    s.blogs = this.props.blogs.blogs;

    s.shopify_template = page.shopify_template;

    s.components = {};
    s.changed = false;
    if (!page.root_component.uuid) {
      page.root_component.uuid = uuid.v4();
    }
    s.root_uuid = page.root_component.uuid;
    this.flattenComponentTree(s, page.root_component);
    return s;
  },

  // helpers

  getModel: function(mid) {
    return _.find(this.props.models, function(m) {
      return m.id == mid;
    });
  },
  getCurrentScreenSize: function() {
    return {
      preview: "lg",
      desktop: "md",
      tablet: "sm",
      mobile: "xs"
    }[this.state.view];
  },
  getDimensions: function(cuuid) {
    this.promises = this.promises || {};
    var rid = uuid.v4();
    var p = $.Deferred();
    this.promises[rid] = p;
    this.postMessage({getDimensions: {uuid: cuuid, rid: rid}});
    return p;
  },
  changePage: function(pageId, blogId) {
    this.props.onChangePage(pageId, blogId);
  },
  findTemplate: function(template_id) {
    var st = _.find(STANDARD_TEMPLATES, function(s) {
      return s.id == template_id;
    });
    if (st) {
      return st;
    }
    return _(this.props.templates).find(function(t) {
      return t.id == template_id;
    });
  },
  findValue: function(component, variable_id) {
    return _(component.values).find(function(v) {
      return v.variable_id == variable_id;
    });
  },
  toggleDropzones: function() {
    this.setState({dropzones: !this.state.dropzones});
  },
  toggleAdvanced: function() {
    this.setState({advanced: !this.state.advanced})
  },
  openMarkdownEditor: function(md) {
    this.setState({markdown: md});
  },
  closeMarkdownEditor: function() {
    this.setState({markdown: null});
  },
  openHTMLEditor: function(html) {
    this.setState({html: html});
  },
  closeHTMLEditor: function() {
    this.setState({html: null});
  },
  openImageUpload: function(ui) {
    _uploadImage(function(url) {
      this.updateComponentValue(ui.uuid, ui.variable_id, [{url: url}]);
    }.bind(this));
  },

  // react lifecycle

  componentWillReceiveProps: function(nextProps) {
    var p = nextProps.page;
    var s = this.state;
    if (p.id != s.id || p.current_version_id != s.current_version_id) {
      this.setState(this.buildState(p));
    } else if (p.updating != s.updating || p.draft != s.draft || p.outdated != s.outdated || p.published != s.published) {
      this.setState({updating: p.updating, draft: p.draft, published: p.published, outdated: p.outdated});
    }
  },
  componentDidUpdate: function(prevProps, prevState) {
    if (!prevState.embedReady && this.state.embedReady) {
      this.props.onShogun(); // toranaga
    }
    if (this.state.changed) {
      this.props.onChanged();
    } else {
      this.props.onUnchanged();
    }
  },

  // application code

  changeUUID: function(component) {
    var old_uuid = component.uuid;
    component.uuid = uuid.v4();
    component.id = null;
    var parent = this.state.components[component.parent_uuid];

    // update parent_uuid for all child components
    for (var key in this.state.components) {
      var comp = this.state.components[key];
      if (comp.parent_uuid == old_uuid) {
        comp.parent_uuid = component.uuid;
      }
    }

    delete this.state.components[old_uuid];
    this.state.components[component.uuid] = component;
    parent.values.forEach(function(value) {
      value.references.forEach(function(reference) {
        if (reference.uuid == old_uuid) {
          reference.uuid = component.uuid;
        }
      });
    });
  },
  resetPositions: function(references) {
    references.forEach(function(ref, i) {
      ref.position = i;
    });
  },
  addReference: function(value, reference) {
    var temp = [];
    var position = reference.position;
    value.references.forEach(function(ref) {
      if (ref.position < position) {
        temp.push(ref);
      } else if (ref.position >= position) {
        if (ref.position == position) {
          temp.push(reference);
        }
        ref.position += 1;
        temp.push(ref);
      }
    });

    if (position == temp.length) {
      temp.push(reference);
    }

    this.resetPositions(temp);

    value.references = temp;
  },
  removeReference: function(value, reference) {
    var temp = [];

    value.references.forEach(function(ref) {
      if (ref.position < reference.position) {
        temp.push(ref);
      } else if (ref.position != reference.position) {
        ref.position -= 1;
        temp.push(ref);
      }
    });

    this.resetPositions(temp);

    value.references = temp;
  },
  findAndRemoveReference: function(component, uuid) {
    _(component.values).each(function(v) {
      var ref = this.findReference(v, uuid);
      if (ref) {
        this.removeReference(v, ref);
      }
    }.bind(this));
  },
  findReference: function(value, uuid) {
    return _(value.references).find(function(r) {
      return r.uuid == uuid;
    });
  },
  setReferencePosition: function(value, reference, position) {
    var references = value.references;

    if (position == reference.position || position == reference.position + 1) {
      return false;
    }

    var rp = reference.position;

    this.removeReference(value, reference);
    if (position >= rp) {
      position -= 1;
    }
    this.addReference(value, {uuid: reference.uuid, position: position});
    return true;
  },
  moveComponent: function(parent_uuid, variable_id, position, uuid) {
    var component = this.state.components[uuid];
    var parent = this.state.components[component.parent_uuid];

    var change = {changed: true, selected_uuid: uuid, markdown: null, html: null, tab: "configure", dragging: false, advanced: false};
    var clone = this.cloneState();

    if (parent_uuid == parent.uuid) {
      var value = this.findValue(parent, variable_id);
      var reference = this.findReference(value, uuid);
      // even with a matching parent, the variable can be different
      if (reference) {
        // same variable
        if (this.setReferencePosition(value, reference, position)) {
          // actually moved
          this.recordChange(clone);
          this.setState(change);
        } else {
          // just select the component
          this.selectComponent(uuid);
        }
      } else {
        // different variable
        this.findAndRemoveReference(parent, uuid);
        this.addReference(value, {uuid: uuid, position: position});
        this.recordChange(clone);
        this.setState(change);
      }
    } else {
      var new_parent = this.state.components[parent_uuid];
      component.parent_uuid = new_parent.uuid;
      var value = this.findValue(new_parent, variable_id);
      this.addReference(value, {uuid: uuid, position: position});
      this.findAndRemoveReference(parent, uuid);
      this.recordChange(clone);
      this.setState(change);
    }
  },
  prepareSnippetComponent: function(comp) {
    comp.uuid = uuid.v4();
    comp.id = null;
    _.each(comp.values, function(v) {
      _.each(v.references, function(r) {
        this.prepareSnippetComponent(r.component);
      }.bind(this));
    }.bind(this));
  },
  insertSnippet: function(parent_uuid, variable_id, position, snippet_id) {
    var snip = _.find(this.props.snippets, function(s) {
      return s.id == snippet_id;
    });
    var root = snip.root_component;

    this.prepareSnippetComponent(root);

    this.flattenComponentTree(this.state, root);
    var parent = this.state.components[parent_uuid];
    var value = this.findValue(parent, variable_id);
    this.state.components[root.uuid].parent_uuid = parent_uuid;
    this.addReference(value, {uuid: root.uuid, position: position});
    this.setState({changed: true, selected_uuid: root.uuid, markdown: null, html: null, tab: "configure", advanced: false});
  },
  debouncedUndo: _.debounce(function(clone) {
    this.recordChange(clone);
    this.forceUpdate();
  }, 500, true),
  updateComponent: function(comp) {
    var clone = this.cloneState();
    this.state.components[comp.uuid] = comp;
    this.recordChange(clone);
    this.setState({changed: true});
  },
  updateComponentValue: function(uuid, variable_id, value, hacks) {
    if (!(value instanceof Array)) {
      value = [value];
    }
    var clone = this.cloneState();
    var comp = this.state.components[uuid];
    var val = this.findValue(comp, variable_id);
    val.value = value;

    if (hacks) {
      this.changeUUID(comp);
    }

    this.setState({changed: true, selected_uuid: comp.uuid}, function() {
      this.debouncedUndo(clone);
    }.bind(this));
  },
  changeToAbsolutePosition: function(comp) {
    this.getDimensions(comp.uuid).then(function(dimensions) {
      var clone = this.cloneState();
      var s = _.extend({position: "absolute"}, dimensions);
      s.width = s.width + "px";
      s.left = null;
      s.top = null;
      s.height = null;
      this.applyStyles(comp, s);
      this.recordChange(clone);
      this.setState({changed: true});
    }.bind(this));
  },
  changeToDefaultPosition: function(comp) {
    var clone = this.cloneState();
    var s = {position: null, top: null, left: null};
    this.applyStyles(comp, s);
    this.recordChange(clone);
    this.setState({changed: true});
  },
  applyStyles: function(comp, styles) {
    var keys = _.keys(styles);
    _.each(keys, function(k) {
      comp.styles[k] = styles[k];
    });
  },
  updateComponentStyles: function(uuid, styles) {
    var comp = this.state.components[uuid];
    if (styles.position == "absolute" && !styles.width && !styles.height && !styles.top && !styles.left) {
      this.changeToAbsolutePosition(comp);
    } else if (_.has(styles, "position") && !styles.position) {
      this.changeToDefaultPosition(comp);
    } else {
      var clone = this.cloneState();
      this.applyStyles(comp, styles);
      this.setState({changed: true, selected_uuid: uuid}, function() {
        this.debouncedUndo(clone);
      }.bind(this));
    }
  },
  updateComponentDataAttributes: function(uuid, data_attributes) {
    var comp = this.state.components[uuid];
    comp.data_attributes = data_attributes;
    this.setState({changed: true});
  },
  getDefaultValue: function(variable) {
    if (variable.type == "complex") {
      return [];
    } else if (variable.default_value) {
      return [variable.default_value];
    } else if (variable.type == "boolean") {
      return [false];
    } else {
      return [""];
    }
  },
  buildComponent: function(parent_uuid, variable_id, position, template_id) {
    var template = this.findTemplate(template_id);

    var values = template.variables.map(function(variable) {
      var dv = this.getDefaultValue(variable);
      var mid = variable.model_id;
      // hack to inject model_id into product template
      if (template_id == "product") {
        var model = _.find(this.props.models, function(m) {
          return m.external;
        });
        mid = model.id;
      }
      return {
        variable_id: variable.id,
        value: dv,
        references: [],
        // convenience, not sent to server
        type: variable.type,
        input: variable.input,
        name: variable.name,
        data_source: variable.data_source,
        model_id: mid,
        properties: variable.properties
      }
    }.bind(this));

    var component = {
      id: null,
      uuid: uuid.v4(),
      liquid: template.liquid,
      parent_uuid: parent_uuid,
      css_classes: "",
      template_id: template.id,
      template_version_id: template.current_version_id,
      template_name: template.name,
      css: template.css,
      values: values,
      styles: JSON.parse(JSON.stringify(template.styles)),
      data_attributes: {}
    };

    var clone = this.cloneState();

    // todo use immutable
    this.state.components[component.uuid] = component;
    var parent = this.state.components[parent_uuid];
    var value = this.findValue(parent, variable_id);
    this.addReference(value, {uuid: component.uuid, position: position});

    this.recordChange(clone);
    this.setState({changed: true, selected_uuid: component.uuid, markdown: null, html: null, tab: "configure", dragging: false, advanced: false});
  },
  selectComponent: function(suuid) {
    // todo this is janky, should refactor when we have shogun components
    var component = this.state.components[suuid];
    var attributes = this.getComponentAttributes(component);
    var state = {selected_uuid: suuid, highlighted_uuid: null, tab: "configure", dragging: false, advanced: false};
    if (attributes.template_name == "HTML") {
      state["markdown"] = null;
    } else if (attributes.template_name == "Markdown") {
      state["html"] = null;
    } else {
      state["markdown"] = null;
      state["html"] = null;
    }
    state.rand = uuid.v4();
    this.setState(state);
  },
  highlightComponent: function(uuid) {
    this.setState({highlighted_uuid: uuid});
  },
  changeTab: function(t) {
    this.setState({tab: t});
  },
  changeView: function(v) {
    this.setState({view: v});
  },
  destroyComponent: function() {
    var clone = this.cloneState();
    var uuid = this.state.selected_uuid;
    var component = this.state.components[uuid];
    var parent = this.state.components[component.parent_uuid];
    delete this.state.components[uuid];
    this.findAndRemoveReference(parent, uuid);
    this.recordChange(clone);
    this.setState({selected_uuid: null, markdown: null, html: null, changed: true, tab: "palette", advanced: false});
  },
  saveSnippet: function() {
    var uuid = this.state.selected_uuid;
    var component = this.state.components[uuid];
    var args = {root_attributes: this.getComponentAttributes(component), site_id: this.props.site.id};
    this.props.onSaveSnippet(args);
  },
  recordChange: function(state) {
    this.state.undos.push(state);
    // keep only 20 undos
    var sliced = this.state.undos.slice(Math.max(0, this.state.undos.length - 50));
    this.state.undos = sliced;
    this.state.redos = [];
  },
  replayState: function(s) {
    if (this.state.selected_uuid && !s.components[this.state.selected_uuid]) {
      s.selected_uuid = null;
      if (s.tab == "configure" || s.tab == "data") {
        s.tab = "palette";
      }
    }
    if (this.state.markdown && !s.components[this.state.markdown.uuid]) {
      s.markdown = null;
    }
    s.rand = uuid.v4();
    this.setState(s);
  },
  redo: function() {
    if (this.state.redos.length <= 0) return;
    var r = this.state.redos.pop();
    this.state.undos.push(this.cloneState());
    this.replayState(r);
  },
  undo: function() {
    if (this.state.undos.length <= 0) return;
    var u = this.state.undos.pop();
    this.state.redos.push(this.cloneState());
    this.replayState(u);
  },
  highlight: function(selector, animation) {
    var jqbtn = $(".shogun-page-configuration .page-actions a[data-original-title="+selector+"]:not(.disabled)");

    jqbtn.removeClass(animation + " animated").addClass(animation + " animated").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function(){
      jqbtn.removeClass(animation + " animated");
    });
  },

  // embed

  getSyncArgs: function() {
    var models = this.sycned ? null : this.props.models;
    this.sycned = this.props.ready;

    return {
      root_uuid: this.state.root_uuid,
      components: this.state.components,
      site: this.props.site,
      selected_uuid: this.state.selected_uuid,
      highlighted_uuid: this.state.highlighted_uuid,
      dropzones: this.state.dropzones,
      dragging: this.state.dragging,
      models: models,
      ready: this.props.ready,
      currentScreenSize: this.getCurrentScreenSize()
    };
  },
  getEmbedOrigin: function() {
    return window.location.protocol + "//" + window.location.host;
  },
  getEmbedSource: function() {
    return "inner";
  },
  getEmbedPath: function() {
    if (QueryString.d) {
      return "/shogun/editor/inner?d=" + QueryString.d;
    } else if (this.props.site.shopify) {
      if (this.props.params.blog_id) {
        return "/shopify/editor/inner?blog_id=" + this.props.params.blog_id;
      } else {
        return "/shopify/editor/inner";
      }
    } else {
      return "/editor/inner";
    }
  },
  handleMessageData: function(data) {
    if (data.moveComponent) {
      var mc = data.moveComponent;
      this.moveComponent(mc.parent_uuid, mc.variable_id, mc.position, mc.uuid);
    } else if (data.buildComponent) {
      var bc = data.buildComponent;
      if (bc.args.template_id) {
        this.buildComponent(bc.parent_uuid, bc.variable_id, bc.position, bc.args.template_id);
      } else if (bc.args.snippet_id) {
        this.insertSnippet(bc.parent_uuid, bc.variable_id, bc.position, bc.args.snippet_id);
      } else {
        console.log("NOT IMPLEMENTED");
      }
    } else if (data.updateComponentValue) {
      var ucv = data.updateComponentValue;
      this.updateComponentValue(ucv.uuid, ucv.variable_id, ucv.value);
    } else if (data.selectComponent) {
      this.selectComponent(data.selectComponent.uuid);
    } else if (data.dragStart) {
      this.handleDragStart();
    } else if (data.dragEnd) {
      this.handleDragEnd();
    } else if (data.editMarkdown) {
      this.openMarkdownEditor(data.editMarkdown);
    } else if (data.editHTML) {
      this.openHTMLEditor(data.editHTML);
    } else if (data.uploadImage) {
      this.openImageUpload(data.uploadImage);
    } else if (data.updateStyles) {
      var us = data.updateStyles;
      this.updateComponentStyles(us.uuid, us.styles);
    } else if (data.dimensions) {
      var d = data.dimensions;
      if (this.promises[d.rid]) {
        this.promises[d.rid].resolve(d.dimensions);
        delete this.promises[d.rid];
      }
    } else if (data.undo) {
      this.highlight("Undo", "bounce");
      this.undo();
    } else if (data.redo) {
      this.highlight("Redo", "bounce");
      this.redo();
    } else {
      console.log("NOT IMPLEMENTED");
    }
  },

  // event handlers

  handleDragStart: function(e) {
    if (!e || $(e.target).find(".shogun-palette-tool").length > 0) {
      this.setState({dragging: true, selected_uuid: null, markdown: null, html: null, advanced: false});
    }
  },
  handleDragEnd: function(e) {
    this.setState({dragging: false});
  },

  // rendering

  renderNavigator: function() {
    return (
      <Navigator
        tabName="navigator"
        tabIcon="list"
        key="navigator"
        components={this.state.components}
        root_uuid={this.state.root_uuid}
        onSelect={this.selectComponent}
        onHighlight={this.highlightComponent}
        selected_uuid={this.state.selected_uuid} />
    );
  },
  renderComponentConfiguration: function() {
    if (this.state.selected_uuid) {
      var comp = this.state.components[this.state.selected_uuid];
      var template = this.findTemplate(comp.template_id);
      return (
        <div tabIcon="gear" key="configure">
          <ComponentConfiguration
            getModel={this.getModel}
            currentScreenSize={this.getCurrentScreenSize()}
            site={this.props.site}
            key={this.state.rand}
            activeAddOns={this.props.activeAddOns}
            template={template}
            component={comp}
            onUpdate={this.updateComponent.bind(this)}
            onUpdateValue={this.updateComponentValue}
            onUpdateStyles={this.updateComponentStyles}
            onDestroy={this.destroyComponent}
            onSave={this.saveSnippet} />
        </div>
      );
    }
  },
  renderDataAttributes: function() {
    if (this.state.selected_uuid && this.props.activeAddOns.length > 0) {
      var comp = this.state.components[this.state.selected_uuid];
      return (
        <div tabIcon="plug" key="data">
          <DataAttributes
            data_attributes={comp.data_attributes}
            activeAddOns={this.props.activeAddOns}
            onUpdate={this.updateComponentDataAttributes.bind(this, comp.uuid)} />
        </div>
      );
    }
  },
  renderBreadcrumb: function() {
    var hierarchy = [];
    if (this.state.selected_uuid) {
      var tracker = this.state.selected_uuid;
      while (!!tracker) {
        var c = this.state.components[tracker];
        if (c.parent_uuid) {
          hierarchy.unshift(c);
        }
        tracker = c.parent_uuid;
      }
    }
    if (hierarchy.length > 0) {
      return (
        <Breadcrumb
          onHighlight={this.highlightComponent}
          onSelect={this.selectComponent}
          hierarchy={hierarchy} />
      );
    }
  },
  renderRightSidebar: function() {
    var cc = this.renderComponentConfiguration();
    var data = this.renderDataAttributes();
    var pal = this.renderPalette();
    var nav = this.renderNavigator();

    var tabs = [cc, data, pal, nav];
    return (
      <Sidebar right>
        <Tabs active={this.state.tab} onChangeTab={this.changeTab}>
          {tabs}
        </Tabs>
      </Sidebar>
    );
  },

  renderDropzonesButton: function() {
    return (
      <Button small active={this.state.dropzones} onClick={this.toggleDropzones} title="Show dropzones" tooltip="right"><Icon name="square-o" /></Button>
    );
  },
  renderLeftSidebar: function() {
    return (
      <Sidebar left>
        <div className="shogun-dd-controls btn-group-vertical btn-group-sm">
          {this.renderDropzonesButton()}
        </div>
      </Sidebar>
    );
  },
  renderPalette: function() {
    return (
      <Palette
        key="palette"
        tabIcon="plus-circle"
        tabName="palette"
        templates={this.props.templates}
        snippets={this.props.snippets}
        site={this.props.site}
        onDestroySnippet={this.props.onDestroySnippet} />
    );
  },
  renderLogo: function() {
    return (
      <a className="shogun-logo" onClick={this.props.onExit}></a>
    );
  },
  renderPagesDropdown: function() {
    var pages = [];
    if (this.props.params.blog_id) {
      pages = _(this.props.pages).filter(function(obj) {
        return obj.external_blog_id != null;
      });
    } else {
      pages = _(this.props.pages).filter(function(obj) {
        return obj.external_blog_id == null;
      });
    }

    var sorted = _.sortBy(pages, function(p) {
      return p.name.toLowerCase();
    });
    var links = _.map(sorted, function(p) {
      return (
        <li key={p.id}>
          <a onClick={this.changePage.bind(this, p.id, p.external_blog_id)}>{p.name}</a>
        </li>
      );
    }.bind(this));

    return (
      <div className="shogun-pages-dropdown dropdown">
        <a className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
          { this.props.params.blog_id ? "Articles" : "Pages" }
          <span className="caret"></span>
        </a>
        <ul className="dropdown-menu shogun-pages-menu" role="menu">
          <li><a onClick={this.props.onCreatePage}>+ New { this.props.params.blog_id ? "Article" : "Page" }</a></li>
          <li className="divider"></li>
          {links}
        </ul>
      </div>
    );
  },
  renderPageConfiguration: function() {
    return (
      <PageConfiguration
        {...this.state}
        pages={this.props.pages}
        saving={this.props.saving}
        publishing={this.props.publishing}
        unpublishing={this.props.unpublishing}
        onSave={this.save}
        onUndo={this.undo}
        onRedo={this.redo}
        onVersions={this.props.onVersions}
        onPublish={this.props.onPublish}
        onUnpublish={this.props.onUnpublish}
        onDuplicate={this.props.onDuplicate}
        site={this.props.site}
        toggleAdvanced={this.toggleAdvanced}
        isBlog={this.props.params.blog_id}
        onChange={this.handleChange} />
    );
  },
  renderViewConfiguration: function() {
    return <ViewConfiguration view={this.state.view} onChange={this.changeView} />;
  },
  renderTopBar: function() {
    var advancedConfiguration;

    if (this.state.advanced) {
      advancedConfiguration = (
        <AdvancedConfiguration
          {...this.state}
          shopifyTemplates={this.props.shopifyTemplates}
          site={this.props.site}
          onClose={this.toggleAdvanced}
          onChange={this.handleChange} />
      );
    }

    return (
      <Toolbar top>
        {this.renderLogo()}
        {this.renderPagesDropdown()}
        {this.renderPageConfiguration()}
        {this.renderViewConfiguration()}
        {advancedConfiguration}
      </Toolbar>
    );
  },
  renderBottomBar: function() {
    return (
      <Toolbar bottom>
        {this.renderBreadcrumb()}
      </Toolbar>
    );
  },
  renderMarkdownEditor: function() {
    if (this.state.markdown) {
      var md = this.state.markdown;
      var comp = this.state.components[md.uuid];
      var value = this.findValue(comp, md.variable_id);
      return (
        <MarkdownEditor
          onClose={this.closeMarkdownEditor}
          onChange={this.updateComponentValue.bind(this, md.uuid, md.variable_id)}
          value={value.value[0]} />
      );
    }
  },
  renderHTMLEditor: function() {
    if (this.state.html) {
      var html = this.state.html;
      var comp = this.state.components[html.uuid];
      var value = this.findValue(comp, html.variable_id);
      return (
        <HTMLEditor
          onClose={this.closeHTMLEditor}
          onChange={this.updateComponentValue.bind(this, html.uuid, html.variable_id)}
          value={value.value[0]} />
      );
    }
  },

  render: function() {
    var cn = "shogun-editor";
    cn += " shogun-editor-" + this.state.view;
    if (this.state.markdown) {
      cn += " shogun-editor-with-markdown-editor";
    }
    if (this.state.html) {
      cn += " shogun-editor-with-html-editor";
    }

    return (
      <div className={cn} onDragStart={this.handleDragStart} onDragEnd={this.handleDragEnd}>
        {this.renderTopBar()}
        {this.renderLeftSidebar()}
        <div className="embed-underlay"></div>
        {this.getEmbed(true)}
        {this.renderHTMLEditor()}
        {this.renderMarkdownEditor()}
        {this.renderRightSidebar()}
        {this.renderBottomBar()}
      </div>
    );
  }
});
