var Padding = React.createClass({
  mixins: [Clearable],
  clearStyles: function() {
    this.props.onUpdate({padding_top: null, padding_bottom: null, padding_left: null, padding_right: null});
  },
  render: function() {
    var mapping = {top: "padding_top", bottom: "padding_bottom", left: "padding_left", right: "padding_right"};
    return (
      <div>
        {this.renderClear()}
        <DimensionDiamond percentage styles={this.props.styles} onUpdate={this.props.onUpdate} mapping={mapping} />
      </div>
    );
  }
});
