var Visibility = React.createClass({
  handleChange: function(attr, value) {
    var up = {};
    if (value) {
      up[attr] = null;
    } else {
      up[attr] = "none";
    }
    this.props.onUpdate(up);
  },

  render: function() {
    var s = this.props.styles;
    var boxes = _.map(SCREEN_SIZES, function(size) {
      var k = "display_" + size;
      return <Checkbox key={size} icon={SCREEN_ICONS[size]} checked={!s[k]} onChange={this.handleChange.bind(this, k)} />;
    }.bind(this));
    return (
      <div className="shogun-visibility">
        {boxes}
      </div>
    );
  }
});
