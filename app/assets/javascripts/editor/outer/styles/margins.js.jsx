var Margins = React.createClass({
  mixins: [Clearable],
  clearStyles: function() {
    this.props.onUpdate({margin_top: null, margin_bottom: null, margin_left: null, margin_right: null});
  },
  render: function() {
    var mapping = {top: "margin_top", bottom: "margin_bottom", left: "margin_left", right: "margin_right"};
    return (
      <div>
        {this.renderClear()}
        <DimensionDiamond auto percentage negative styles={this.props.styles} onUpdate={this.props.onUpdate} mapping={mapping} />
      </div>
    );
  }
});
