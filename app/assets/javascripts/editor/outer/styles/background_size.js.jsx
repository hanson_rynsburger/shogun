var BackgroundSize = React.createClass({
  handleBackgroundSizeChange: function(value) {
    if (value == "cover" || value == "contain") {
      this.props.onChange(value);
    } else {
      this.props.onChange("");
    }
  },
  handleBackgroundWidthChange: function(value) {
    var height = this.backgroundHeight() || '0';
    this.props.onChange(value + " " + height);
  },
  handleBackgroundHeightChange: function(value) {
    var width = this.backgroundWidth() || '0';
    this.props.onChange(width + " " + value);
  },

  isCustom: function() {
    return ["contain", "cover"].indexOf(this.props.value) < 0;
  },
  backgroundWidth: function() {
    if (this.props.value) {
      return this.props.value.split(" ")[0];
    }
  },
  backgroundHeight: function() {
    if (this.props.value) {
      return this.props.value.split(" ")[1];
    }
  },
  renderCustomSizeFields: function() {
    if (this.isCustom()) {
      return (
        <div className="background-size-fields">
          <Columns>
            <UnitTextInput percentage key="bwidth" label="width" value={this.backgroundWidth()} onChange={this.handleBackgroundWidthChange} />
            <UnitTextInput percentage key="bheight" label="height" value={this.backgroundHeight()} onChange={this.handleBackgroundHeightChange} />
          </Columns>
        </div>
      );
    }
  },
  render: function() {
    var bgSizeOptions = [{label: "Cover", value: "cover"}, {label: "Contain", value: "contain"}, {label: "Custom", value: "custom"}];
    return (
      <div className="form-group">
        <label className="control-label">background size</label>
        <RadioButtonGroup small justified options={bgSizeOptions} onSelect={this.handleBackgroundSizeChange} value={this.isCustom() ? "custom" : this.props.value} />
        {this.renderCustomSizeFields()}
      </div>
    );
  }
});
