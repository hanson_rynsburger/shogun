var Basic = React.createClass({
  mixins: [Clearable],

  handleChange: function(attr, value) {
    var u = {};
    u[attr] = value;
    this.props.onUpdate(u);
  },
  clearStyles: function() {
    this.props.onUpdate({text_align: null, width: null, height: null, opacity: null});
  },
  renderAlignment: function() {
    var s = this.props.styles;
    var alignments = [{icon: "ellipsis-h", value: null, tooltip: "Default"}, {icon: "align-left", value: "left", tooltip: "Left"}, {icon: "align-center", value: "center", tooltip: "Center"}, {icon: "align-right", value: "right", tooltip: "Right"}];

    return (
      <div className="form-group">
        <label className="control-label">alignment</label>
        <RadioButtonGroup justified small options={alignments} onSelect={this.handleChange.bind(this, "text_align")} value={s.text_align} />
      </div>
    );
  },
  renderDimensions: function(){
    var s = this.props.styles;
    return (
      <div className="dimensions">
        <Columns>
          <UnitTextInput percentage key="width" label="width" value={s.width} onChange={this.handleChange.bind(this, "width")} />
          <UnitTextInput percentage key="height" label="height" value={s.height} onChange={this.handleChange.bind(this, "height")} />
        </Columns>
      </div>
    );
  },
  renderOpacity: function() {
    var s = this.props.styles;
    return (
      <SliderInput key="opacity" label="opacity" value={s.opacity} onChange={this.handleChange.bind(this, "opacity")} />
    );
  },
  render: function() {
    return (
      <div>
        {this.renderClear()}
        {this.renderDimensions()}
        {this.renderAlignment()}
        {this.renderOpacity()}
      </div>
    );
  }
});
