var Borders = React.createClass({
  mixins: [Clearable],
  handleChange: function(attr, value) {
    var u = {};
    u[attr] = value;
    this.props.onUpdate(u);
  },
  clearStyles: function() {
    this.props.onUpdate({border_top_width: null, border_bottom_width: null, border_left_width: null, border_right_width: null, border_radius: null, border_color: null});
  },
  render: function() {
    var s = this.props.styles;
    var mapping = {top: "border_top_width", bottom: "border_bottom_width", left: "border_left_width", right: "border_right_width"};
    return (
      <div>
        {this.renderClear()}
        <Columns>
          <ColorInput colors={this.props.site.colors} key="color" value={s.border_color} onChange={this.handleChange.bind(this, "border_color")} />
          <UnitTextInput key="radius" label="radius" value={s.border_radius} onChange={this.handleChange.bind(this, "border_radius")} />
        </Columns>
        <DimensionDiamond styles={this.props.styles} onUpdate={this.props.onUpdate} mapping={mapping} />
      </div>
    );
  }
});
