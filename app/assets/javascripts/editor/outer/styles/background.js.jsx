var Background = React.createClass({
  mixins: [Clearable],
  handleChange: function(attr, value) {
    var u = {};
    if (attr == "background_image") {
      if (value) {
        u["background_size"] = "cover";
        u["background_position"] = "center center";
      } else {
        u["background_size"] = null;
        u["background_position"] = null;
      }
    }
    u[attr] = value;
    this.props.onUpdate(u);
  },
  clearStyles: function() {
    this.props.onUpdate({background_image: null, background_size: null, background_position: null, background_color: null});
  },

  // helpers

  hasBackgroundImage: function() {
    return !!this.props.styles.background_image;
  },

  // rendering

  renderBackgroundSize: function() {
    if (this.hasBackgroundImage()) {
      return <BackgroundSize value={this.props.styles.background_size} onChange={this.handleChange.bind(this, "background_size")} />;
    }
  },
  renderBackgroundPosition: function() {
    if (this.hasBackgroundImage()) {
      return <BackgroundPosition value={this.props.styles.background_position} onChange={this.handleChange.bind(this, "background_position")} />;
    }
  },
  renderBackgroundImage: function() {
    var remove = this.hasBackgroundImage() ? <span>(<a onClick={this.handleChange.bind(this, "background_image", null)}>remove</a>)</span> : null;
    return (
      <div className="form-group">
        <label className="control-label">background image {remove}</label>
        <SimpleImageInput value={this.props.styles.background_image} onChange={this.handleChange.bind(this, "background_image")} />
      </div>
    );
  },
  renderBackgroundColor: function() {
    return <ColorInput colors={this.props.site.colors} key="color" value={this.props.styles.background_color} onChange={this.handleChange.bind(this, "background_color")} />;
  },
  render: function() {
    return (
      <div>
        {this.renderClear()}
        {this.renderBackgroundColor()}
        {this.renderBackgroundImage()}
        {this.renderBackgroundSize()}
        {this.renderBackgroundPosition()}
      </div>
    );
  }
});
