var Font = React.createClass({
  mixins: [Clearable],
  handleChange: function(attr, value) {
    var u = {};
    u[attr] = value;
    this.props.onUpdate(u);
  },
  clearStyles: function() {
    this.props.onUpdate({font_size: null, font_weight: null, color: null});
  },
  render: function() {
    var s = this.props.styles;
    return (
      <div>
        {this.renderClear()}
        <ColorInput colors={this.props.site.colors} key="color" value={s.color} onChange={this.handleChange.bind(this, "color")} />
        <UnitTextInput key="size" label="size" value={s.font_size} onChange={this.handleChange.bind(this, "font_size")} />
        <label className="control-label">weight</label>
        <RadioButtonGroup small onSelect={this.handleChange.bind(this, "font_weight")} justified options={[{value: null, label: "normal"}, {value: "bold", label: "bold"}]} value={s.font_weight} />
      </div>
    );
  }
});
