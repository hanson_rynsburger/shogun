var Position = React.createClass({
  handleChange: function(attr, value) {
    var up = {};
    up[attr] = value;
    if (attr != "z_index") {
      _.each(SCREEN_SIZES, function(s) {
        var flag = "position_" + s;
        if (attr != "position") {
          // keep in sync
          if (!this.props.styles[flag]) {
            up[attr + "_" + s] = value;
          }
        }
        if (attr == flag && !value) {
          up["top_"+s] = this.props.styles.top;
          up["left_"+s] = this.props.styles.left;
        }
      }.bind(this));
    }
    this.props.onUpdate(up);
  },
  renderInputs: function(postfix) {
    var s = this.props.styles;
    var t = "top" + postfix;
    var l = "left" + postfix;

    return (
      <div>
        <Columns>
          <UnitTextInput negative key={t} label="top" value={s[t]} onChange={this.handleChange.bind(this, t)} />
          <UnitTextInput negative key={l} label="left" value={s[l]} onChange={this.handleChange.bind(this, l)} />
        </Columns>
      </div>
    );
  },
  renderSizeOffsets: function() {
    return _.map(SCREEN_SIZES, function(size) {
      var k = "position_" + size;
      var enabled = this.props.styles[k];
      var inputs = enabled ? this.renderInputs("_" + size) : null;
      return (
        <div>
          <Checkbox icon={SCREEN_ICONS[size]} onChange={this.handleChange.bind(this, k)} checked={enabled} />
          {inputs}
        </div>
      );
    }.bind(this));
  },
  renderOffsets: function(){
    var s = this.props.styles;
    if (s.position == "absolute") {
      return (
        <div className="offsets">
          {this.renderInputs("")}
          <label className="control-label">z-index</label>
          <TextInput key="z-index" value={s.z_index} onChange={this.handleChange.bind(this, "z_index")} />
          <br />
          <label className="control-label">Overrides</label>
          {this.renderSizeOffsets()}
        </div>
      );
    }
  },
  render: function() {
    var s = this.props.styles;
    var options = [{label: "Default", value: null}, {label: "Absolute", value: "absolute"}];
    return (
      <div className="shogun-position">
        <label className="control-label">position</label>
        <RadioButtonGroup
          small
          justified
          options={options}
          value={s.position}
          options={options}
          onSelect={this.handleChange.bind(this, "position")} />
        {this.renderOffsets()}
      </div>
    );
  }
});
