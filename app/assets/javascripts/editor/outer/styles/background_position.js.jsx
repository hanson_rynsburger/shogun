var BackgroundPosition = React.createClass({
  handleHorizontalChange: function(value) {
    this.props.onChange(value + " " + this.verticalPosition());
  },
  handleVerticalChange: function(value) {
    this.props.onChange(this.horizontalPosition() + " " + value);
  },
  horizontalPosition: function() {
    if (this.props.value) {
      return this.props.value.split(" ")[0];
    }
  },
  verticalPosition: function() {
    if (this.props.value) {
      return this.props.value.split(" ")[1];
    }
  },
  render: function() {
    var verticalPositions = [{tooltip: "Top", icon: "arrow-circle-up", value: "top"}, {tooltip: "Center", icon: "dot-circle-o", value: "center"}, {tooltip: "Bottom", icon: "arrow-circle-down", value: "bottom"}, {tooltip: "Custom", icon: "gear", value: "custom"}];
    var horizontalPositions = [{tooltip: "Left", icon: "arrow-circle-left", value: "left"}, {tooltip: "Center", icon: "dot-circle-o", value: "center"}, {tooltip: "Right", icon: "arrow-circle-right", value: "right"}, {tooltip: "Custom", icon: "gear", value: "custom"}];
    return (
      <div>
        <ScalarBackgroundPosition key="horizontal" name="horizontal" positions={horizontalPositions} onChange={this.handleHorizontalChange} value={this.horizontalPosition()} />
        <ScalarBackgroundPosition key="vertical" name="vertical" positions={verticalPositions} onChange={this.handleVerticalChange} value={this.verticalPosition()} />
      </div>
    );
  }
});

var ScalarBackgroundPosition = React.createClass({
  handleChange: function(value) {
    this.props.onChange(value);
  },
  isCustom: function() {
    return ["top", "center", "bottom", "left", "right"].indexOf(this.props.value) < 0;
  },
  renderCustomSizeFields: function() {
    var value = this.props.value;
    if (this.isCustom()) {
      return (
        <div className="background-position-fields">
          <UnitTextInput negative percentage value={this.props.value} onChange={this.handleChange} />
        </div>
      );
    }
  },
  render: function() {
    return (
      <div className="form-group">
        <label className="control-label">{this.props.name} position</label>
        <RadioButtonGroup small justified options={this.props.positions} onSelect={this.handleChange} value={this.isCustom() ? "custom" : this.props.value} />
        {this.renderCustomSizeFields()}
      </div>
    );
  }
});
