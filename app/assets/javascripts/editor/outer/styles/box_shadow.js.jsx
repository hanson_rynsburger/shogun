var BoxShadow = React.createClass({
  mixins: [Clearable],
  handleChange: function(attr, value) {
    var u = {};
    u[attr] = value;
    if (attr != "border_radius" && !this.props.styles.box_shadow_color && !!value) {
      u.box_shadow_color = "#000000";
    }
    this.props.onUpdate(u);
  },
  clearStyles: function() {
    this.props.onUpdate({box_shadow_color: null, box_shadow_horizontal: null, box_shadow_vertical: null, box_shadow_blur: null, box_shadow_spread: null});
  },
  render: function() {
    var s = this.props.styles;
    return (
      <div>
        {this.renderClear()}
        <ColorInput colors={this.props.site.colors} key="color" value={s.box_shadow_color} onChange={this.handleChange.bind(this, "box_shadow_color")} />
        <Columns>
          <UnitTextInput negative key="horizontal" label="horizontal" value={s.box_shadow_horizontal} onChange={this.handleChange.bind(this, "box_shadow_horizontal")} />
          <UnitTextInput negative key="vertical" label="vertical" value={s.box_shadow_vertical} onChange={this.handleChange.bind(this, "box_shadow_vertical")} />
        </Columns>
        <Columns>
          <UnitTextInput key="blur" label="blur" value={s.box_shadow_blur} onChange={this.handleChange.bind(this, "box_shadow_blur")} />
          <UnitTextInput key="spread" label="spread" value={s.box_shadow_spread} onChange={this.handleChange.bind(this, "box_shadow_spread")} />
        </Columns>
      </div>
    );
  }
});
