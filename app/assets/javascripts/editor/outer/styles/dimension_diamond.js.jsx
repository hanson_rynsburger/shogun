var DimensionDiamond = React.createClass({
  getInitialState: function() {
    return {locked: this.getLocked(this.props)};
  },
  toggleLock: function() {
    this.setState({locked: !this.state.locked});
  },
  getLocked: function(props) {
    var s = props.styles;
    var m = props.mapping;
    if (s[m.top] == s[m.bottom] && s[m.right] == s[m.left]) {
      return true;
    }
    return false;
  },
  handleChange: function(key, value) {
    var u = {};
    var m = this.props.mapping;
    u[m[key]] = value;
    if (this.state.locked) {
      if (key == "top") {
        u[m["bottom"]] = value;
      } else if (key == "bottom") {
        u[m["top"]] = value;
      } else if (key == "left") {
        u[m["right"]] = value;
      } else if (key == "right") {
        u[m["left"]] = value;
      }
    }
    this.props.onUpdate(u);
  },
  renderLock: function() {
    var locked = this.state.locked;
    var title = locked ? "Unlock" : "Lock";
    var icon = locked ? "lock" : "unlock-alt";

    return <Button className="shogun-lock-button" active={locked} xs onClick={this.toggleLock} title={title} tooltip="bottom" icon={icon} key={icon} />;
  },
  render: function() {
    var s = this.props.styles;
    var m = this.props.mapping;
    return (
      <div className="shogun-dimension-diamond">
        {this.renderLock()}
        <div className="row">
          <div className="col-xs-6 col-xs-offset-3">
            <UnitTextInput auto={this.props.auto} negative={this.props.negative} percentage={this.props.percentage} key="top" label="top" value={s[m.top]} onChange={this.handleChange.bind(this, "top")} />
          </div>
        </div>
        <Columns>
          <UnitTextInput auto={this.props.auto} negative={this.props.negative} percentage={this.props.percentage} key="left" label="left" value={s[m.left]} onChange={this.handleChange.bind(this, "left")} />
          <UnitTextInput auto={this.props.auto} negative={this.props.negative} percentage={this.props.percentage} key="right" label="right" value={s[m.right]} onChange={this.handleChange.bind(this, "right")} />
        </Columns>
        <div className="row">
          <div className="col-xs-6 col-xs-offset-3">
            <UnitTextInput auto={this.props.auto} negative={this.props.negative} percentage={this.props.percentage} key="bottom" label="bottom" value={s[m.bottom]} onChange={this.handleChange.bind(this, "bottom")} />
          </div>
        </div>
      </div>
    );
  }
});
