var Styles = React.createClass({
  renderFont: function() {
    return (
      <Font
        site={this.props.site}
        key="font"
        tabName="Font"
        styles={this.props.styles}
        onUpdate={this.props.onUpdate} />
    );
  },
  renderPosition: function() {
    return (
      <Position
        currentScreenSize={this.props.currentScreenSize}
        site={this.props.site}
        key="position"
        tabName="Position"
        styles={this.props.styles}
        onUpdate={this.props.onUpdate} />
    );
  },
  renderVisibility: function() {
    return (
      <Visibility
        site={this.props.site}
        key="display"
        tabName="Display"
        styles={this.props.styles}
        onUpdate={this.props.onUpdate} />
    );
  },
  renderMargins: function() {
    return (
      <Margins
        currentScreenSize={this.props.currentScreenSize}
        site={this.props.site}
        key="margins"
        tabName="Margins"
        styles={this.props.styles}
        onUpdate={this.props.onUpdate} />
    );
  },
  renderPadding: function() {
    return (
      <Padding
        currentScreenSize={this.props.currentScreenSize}
        site={this.props.site}
        key="padding"
        tabName="Padding"
        styles={this.props.styles}
        onUpdate={this.props.onUpdate} />
    );
  },
  renderBorders: function() {
    return (
      <Borders
        currentScreenSize={this.props.currentScreenSize}
        site={this.props.site}
        key="borders"
        tabName="Borders"
        styles={this.props.styles}
        onUpdate={this.props.onUpdate} />
    );
  },
  renderBoxShadow: function() {
    return (
      <BoxShadow
        currentScreenSize={this.props.currentScreenSize}
        site={this.props.site}
        key="box-shadow"
        tabName="Box
        Shadow"
        styles={this.props.styles}
        onUpdate={this.props.onUpdate} />
    );
  },
  renderBasic: function() {
    return (
      <Basic
        currentScreenSize={this.props.currentScreenSize}
        site={this.props.site}
        key="basic"
        tabName="Basic"
        styles={this.props.styles}
        onUpdate={this.props.onUpdate} />
    );
  },
  renderBackground: function() {
    return (
      <Background
        currentScreenSize={this.props.currentScreenSize}
        site={this.props.site}
        key="background"
        tabName="Background"
        styles={this.props.styles}
        onUpdate={this.props.onUpdate} />
    );
  },
  render: function() {
    var children = [];
    if (this.props.font) {
      children.push(this.renderFont());
    }
    children.push(this.renderBasic());
    children.push(this.renderBackground());
    children.push(this.renderVisibility());
    children.push(this.renderPosition());
    children.push(this.renderMargins());
    children.push(this.renderPadding());
    children.push(this.renderBorders());
    children.push(this.renderBoxShadow());
    return (
      <div className="shogun-component-styles">
        <Accordion active={this.props.active} onChangeTab={this.props.onChangeTab}>
          {children}
        </Accordion>
      </div>
    );
  }
});
