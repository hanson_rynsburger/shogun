var ComponentValues = React.createClass({
  getConfigurableValues: function() {
    return _.filter(this.props.values, function(v) {
      return (v.data_source != "model" && (v.input != "inline" || v.type == "image")) || (v.data_source == "model" && v.input == "hidden");
    });
  },
  handleVariableChange: function(variable_id, update) {
    this.props.onUpdate(variable_id, update);
  },
  renderSingleValue: function(value) {
    var types = {
      text: TextInput,
      html: TextareaInput,
      image: ImageInput,
      markdown: Markdown,
      integer: TextInput,
      boolean: Checkbox
    };
    var SingleHandler = types[value.type];
    if (SingleHandler) {
      var v;
      if (value.value && value.value[0]) {
        v = value.value[0];
      }
      if (value.type === 'boolean') {
        return (
          <SingleHandler
            small
            checked={v}
            label={value.label}
            onChange={this.handleVariableChange.bind(this, value.variable_id)} />
        );
      } else {
        return (
          <SingleHandler
            small
            value={v}
            onChange={this.handleVariableChange.bind(this, value.variable_id)} />
        );
      }
    } else {
      console.log("NOT IMPLEMENTED");
    }
  },
  renderMultiValue: function(value) {
    var types = {
      text: MultiTextInput,
      image: MultiImageInput,
      richtext: MultiRichText,
      complex: MultiComplexValue
    };

    var MultiHandler = types[value.type];
    if (MultiHandler) {
      return (
        <MultiHandler
          small
          key={value.name}
          label={value.name}
          value={value}
          onChange={this.handleVariableChange.bind(this, value.variable_id)} />
      );
    } else {
      console.log("NOT IMPLEMENTED");
    }
  },
  renderModelSelector: function(value) {
    var model = this.props.getModel(value.model_id);
    var options = _.map(model.entries, function(e) {
      var label = e.name || e.title || e.id;
      return [label, e.id];
    });
    options.unshift(["", null]);
    var entryId = value.value[0];
    return (
      <SelectInput
        onChange={this.handleVariableChange.bind(this, value.variable_id)}
        options={options}
        label={"Choose a " + model.name}
        value={entryId} />
    );
  },
  render: function() {
    var label,
    inputs = _.map(this.getConfigurableValues(), function(value) {
      var input = value.input;
      var content;
      if (input == "hidden") {
        if (value.name == "new_window") {
          value.label = "Open in new window";
        }
        if (value.data_source == "model") {
          content = this.renderModelSelector(value);
        } else {
          content = this.renderSingleValue(value);
        }
        return (
          <Panel key={value.name} title={value.name}>
            {content}
          </Panel>
        );
      } else if (input == "multi") {
        return this.renderMultiValue(value);
      } else if (input == "inline" && value.type == "image") {
        if (value.value && value.value[0] && value.value[0].url) {
          return (
            <div key={value.name} className="panel panel-default">
              <div className="panel-heading">
                <h3 className="panel-title">{value.name}</h3>
              </div>
              <div className="panel-body">
                <ImageAttributes
                value={value.value[0]}
                onChange={this.handleVariableChange.bind(this, value.variable_id)} />
              </div>
            </div>
          );
        }
      }
    }.bind(this));

    if (inputs.length > 0) {
      label = <h4>Variables</h4>;
    }
    return (
      <div className="shogun-component-values">
        {label}
        {inputs}
      </div>
    );
  }
});
