var ViewConfiguration = React.createClass({
  handleChange: function(v) {
    this.props.onChange(v);
  },
  render: function() {
    var map = {preview: "desktop", desktop: "laptop"};
    var titles = {preview: "Widescreen", desktop: "Desktop", tablet: "Tablet", mobile: "Mobile"};
    var views = _.map(["preview", "desktop", "tablet", "mobile"], function(v) {
      var icon = map[v] ? "fa fa-" + map[v] : "fa fa-" + v;
      return (
        <Button title={titles[v]} tooltip="bottom" key={v} active={this.props.view == v} onClick={this.handleChange.bind(this, v)}><Icon name={map[v] || v} /></Button>
      );
    }.bind(this));

    return (
      <div className="shogun-view-configuration btn-group btn-group-sm">
        {views}
      </div>
    );
  }
});
