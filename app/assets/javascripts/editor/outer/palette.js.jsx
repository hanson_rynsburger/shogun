var Palette = React.createClass({
  handleDestroy: function(sid) {
    this.props.onDestroySnippet(sid);
  },
  renderStandardComponents: function() {
    return (
      <div>
        <Columns>
          <StandardComponent id="video" key="video" />
          <StandardComponent id="image" key="image" />
        </Columns>
        <Columns>
          <StandardComponent id="text" key="text" />
          <StandardComponent id="columns" key="columns" />
        </Columns>
        <Columns>
          <StandardComponent id="html" key="html" />
          <StandardComponent id="markdown" key="markdown" />
        </Columns>
        <Columns>
          <StandardComponent id="button" key="button" />
          <StandardComponent id="tabs" key="tabs" />
        </Columns>
        <Columns>
          <StandardComponent id="accordion" key="button" />
          <br key="yo" />
        </Columns>
        <Columns>
          <StandardComponent id="container" key="container" />
          <StandardComponent id="box" key="box" />
        </Columns>
      </div>
    );
  },
  renderCustomTemplates: function() {
    var sorted = _.sortBy(this.props.templates, function(t) {
      return t.name.toLowerCase();
    });
    return sorted.map(function(t) {
      return <CustomTemplate key={t.id} name={t.name} id={t.id} type={t.type} />;
    });
  },
  renderSnippets: function() {
    var sorted = _.sortBy(this.props.snippets, function(s) {
      return s.name.toLowerCase();
    });
    return sorted.map(function(s) {
      return <Snippet key={s.id} name={s.name} id={s.id} onDestroy={this.handleDestroy} />;
    }.bind(this));
  },
  renderShopifyComponents: function() {
    if (this.props.site.shopify) {
      return (
        <div>
          <p>Shopify Components</p>
          <div>
            <Columns>
              <StandardComponent id="product" key="product" />
              <br />
            </Columns>
          </div>
        </div>
      );
    }
  },
  render: function() {
    return (
      <div className="shogun-palette">
        <h4>Drag onto page</h4>
        <div className="shogun-templates">
          <p>Standard Components</p>
          {this.renderStandardComponents()}
          {this.renderShopifyComponents()}
          <p>Custom Templates</p>
          {this.renderCustomTemplates()}
          <p>Snippets</p>
          {this.renderSnippets()}
        </div>
      </div>
    );
  }
});

var StandardComponent = React.createClass({
  handleDragStart: function(e) {
    e.dataTransfer.setData("template_id", this.props.id);
  },
  componentDidMount: function() {
    $(React.findDOMNode(this.refs.component)).tooltip({
      animation: false,
      trigger: 'hover',
      container: 'body',
      placement: 'bottom'
    });
  },
  componentWillUnmount: function() {
    $(React.findDOMNode(this.refs.component)).tooltip('destroy');
  },
  render: function() {
    var cu = window.COMPONENT_IMAGE_URLS;
    return (
      <div title={this.props.id} ref="component" draggable onDragStart={this.handleDragStart}>
        <div className="shogun-palette-tool shogun-standard-component">
          <img src={cu[this.props.id]} />
        </div>
      </div>
    );
  }
});

var CustomTemplate = React.createClass({
  handleDragStart: function(e) {
    e.dataTransfer.setData("template_id", this.props.id);
  },
  render: function() {
    return (
      <div draggable onDragStart={this.handleDragStart}>
        <div className="shogun-palette-tool">
          <span>{this.props.name}</span>
        </div>
      </div>
    );
  }
});

var Snippet = React.createClass({
  handleDragStart: function(e) {
    e.dataTransfer.setData("snippet_id", this.props.id);
  },
  handleDestroy: function() {
    this.props.onDestroy(this.props.id);
  },
  render: function() {
    return (
      <div draggable onDragStart={this.handleDragStart}>
        <div className="shogun-palette-tool">
          <span>{this.props.name}</span>
          <Button xs icon="trash-o" onClick={this.handleDestroy} />
        </div>
      </div>
    );
  }
});
