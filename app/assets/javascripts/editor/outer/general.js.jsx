var General = React.createClass({
  handleChange: function(attr, value) {
    var clone = JSON.parse(JSON.stringify(this.props.component));
    clone[attr] = value;
    this.props.onUpdate(clone);
  },
  render: function() {
    return (
      <div className="shogun-component-general">
        <TextInput
          value={this.props.component.css_classes}
          onChange={this.handleChange.bind(this, "css_classes")}
          placeholder="CSS class" />
      </div>
    );
  }
});
