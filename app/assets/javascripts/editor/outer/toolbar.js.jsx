var Toolbar = React.createClass({
  render: function() {
    var cn = "shogun-toolbar";
    if (this.props.top) {
      cn += " shogun-toolbar-top";
    } else if (this.props.bottom) {
      cn += " shogun-toolbar-bottom";
    }
    return (
      <div className={cn}>
        {this.props.children}
      </div>
    );
  }
});
