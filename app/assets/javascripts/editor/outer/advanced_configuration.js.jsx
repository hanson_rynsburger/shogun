var AdvancedConfiguration = React.createClass({
  // shopify templates

  handleShopifyTemplateChange: function(stemp) {
    this.props.onChange({shopify_template: stemp.target.value});
  },


  // meta helpers
  handleMetaChange: function(name, value) {
    var meta = this.toDict(this.props.meta) || {};
    meta[name] = {
      'property' : name,
      'value' : value
    };
    this.props.onChange({meta: this.toArr(meta)});
  },
  toDict: function(metaArr) {
    meta = {};
    _.each(metaArr, function(m) {
      meta[m.property] = {
        'property' : m.property,
        'value' : m.value
      }
    });
    return meta;
  },
  toArr: function(metaDict) {
    meta = [];
    _.each(Object.keys(metaDict), function(k){
      meta.push(metaDict[k]);
    });
    return meta;
  },
  renderShopifyTemplates: function() {
    if (!this.props.site.shopify) {
      return null;
    }

    var key = this.props.isBlog ? "article" : "page";
    var temp = this.props.shopifyTemplates[key];

    var options = temp.map(function(o) {
      return <option key={o} value={o}>{o}</option>;
    });

    return (
      <div className="input-group input-group-sm">
        <span className="input-group-addon">Layout</span>
        <select className="form-control input-sm" defaultValue={this.props.shopify_template} onChange={this.handleShopifyTemplateChange}>
          {options}
        </select>
      </div>
    );
  },
  render: function() {
    var title, description;
    if (this.props.meta) {
      var meta = this.toDict(this.props.meta);
      title = meta.title && meta.title.value ? meta.title.value : null;
      description = meta.description && meta.description.value ? meta.description.value : null;
    }
    return (
      <div className="shogun-subnav-menu shogun-meta-tags-menu">
        <a className="close-btn" onClick={this.props.onClose}><Icon name="close" /></a>
        <div className="form-inline">
          <TextInput small value={title} onChange={this.handleMetaChange.bind(this, 'title')} placeholder="Meta Title" />
          <TextInput small value={description} onChange={this.handleMetaChange.bind(this, 'description')} placeholder="Meta Description" />
          {this.renderShopifyTemplates()}
        </div>
      </div>
    );
  }
});
