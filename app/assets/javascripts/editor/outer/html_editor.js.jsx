var HTMLEditor = React.createClass({
  handleClick: function() {
    this.props.onClose();
  },
  handleChange: function(e) {
    this.props.onChange(e.target.value);
  },
  componentDidMount: function() {
    React.findDOMNode(this.refs.h).focus();
  },
  render: function() {
    return (
      <div className="shogun-html-editor">
        <a className="shogun-html-editor-close" onClick={this.handleClick}><Icon name="close" /></a>
        <textarea
          ref="h"
          placeholder="Type HTML..."
          onChange={this.handleChange}
          value={this.props.value} />
      </div>
    );
  }
});
