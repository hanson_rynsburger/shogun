var MarkdownEditor = React.createClass({
  handleClick: function() {
    this.props.onClose();
  },
  handleChange: function(e) {
    this.props.onChange(e.target.value);
  },
  componentDidMount: function() {
    React.findDOMNode(this.refs.md).focus();
  },
  render: function() {
    return (
      <div className="shogun-markdown-editor">
        <a className="shogun-markdown-editor-close" onClick={this.handleClick}><Icon name="close" /></a>
        <textarea
          ref="md"
          placeholder="Type markdown..."
          onChange={this.handleChange}
          value={this.props.value} />
      </div>
    );
  }
});
