var DataAttributes = React.createClass({
  handleChange: function(name, attr, value) {
    var clone = JSON.parse(JSON.stringify(this.props.data_attributes));
    clone[name] = clone[name] || {};
    clone[name][attr] = value;
    this.props.onUpdate(clone);
  },
  getAddOnConfiguration: function(name) {
    return _.find(window.ADD_ONS, function(cfg) {
      return cfg.name == name;
    });
  },
  getConfigurableAddOns: function() {
    return _.filter(this.props.activeAddOns, function(addOn) {
      var cfg = this.getAddOnConfiguration(addOn.name);
      return !!cfg.editor_fields;
    }.bind(this));
  },
  renderAddOns: function() {
    return _.map(this.getConfigurableAddOns(), function(addOn) {
      var name = addOn.name;
      var cfg = this.getAddOnConfiguration(name);
      var fields = _.map(cfg.editor_fields, function(field) {
        var value = '';
        if (typeof this.props.data_attributes[name] !== 'undefined') {
          value = this.props.data_attributes[name][field];
        }
        return (
          <div className="form-group" key={field}>
            <label className="control-label">{field}</label>
            <TextInput small value={value} onChange={this.handleChange.bind(this, name, field)} />
          </div>
        );
      }.bind(this));
      return (
        <div className={name + '-fields'} key={name}>
          <h4>{cfg.human_name} Event</h4>
          {fields}
        </div>
      );
    }.bind(this));
  },
  render: function() {
    return (
      <div className="shogun-data-attributes">
        <h3>Data</h3>
        {this.renderAddOns()}
      </div>
    );
  }
});
