var Navigator = React.createClass({
  handleSelect: function(uuid) {
    this.props.onSelect(uuid);
  },
  handleMouseOver: function(uuid) {
    this.props.onHighlight(uuid);
  },
  handleMouseOut: function() {
    this.props.onHighlight(null);
  },
  renderComponents: function(key, references) {
    var rendered = _.map(references, function(ref) {
      var comp = this.props.components[ref.uuid];
      var cn = comp.uuid == this.props.selected_uuid ? "active" : null;
      var link = <a onMouseOver={this.handleMouseOver.bind(this, ref.uuid)} onMouseOut={this.handleMouseOut} className={cn} onClick={this.handleSelect.bind(this, ref.uuid)}>{comp.template_name}</a>;

      var children = _.map(comp.values, function(val, i) {
        if (val.references.length > 0) {
          var k = comp.uuid + "-" + i;
          return this.renderComponents(k, val.references);
        }
      }.bind(this));

      return (
        <li key={comp.uuid}>
          {link}
          {children}
        </li>
      );
    }.bind(this));

    return (
      <ul key={key} className="list-unstyled">
        {rendered}
      </ul>
    )
  },
  render: function() {
    var root = this.props.components[this.props.root_uuid];
    var refs = root.values[0].references;

    return (
      <div className="shogun-navigator">
        {this.renderComponents("root", refs)}
      </div>
    );
  }
});
