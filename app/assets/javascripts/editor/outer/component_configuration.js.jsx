var ComponentConfiguration = React.createClass({
  getInitialState: function() {
    var active = this.props.component.template_id == "button" ? "font" : "basic";
    return {active: active};
  },

  handleStyleChange: function(up) {
    this.props.onUpdateStyles(this.props.component.uuid, up);
  },
  handleHoverStyleChange: function(up) {
    this.props.onUpdateStyles(this.props.component.uuid, {hover: _.extend({}, this.props.component.styles.hover, up)});
  },
  handleActiveStyleChange: function(up) {
    this.props.onUpdateStyles(this.props.component.uuid, {active: _.extend({}, this.props.component.styles.active, up)});
  },
  handleValueChange: function(variable_id, value) {
    this.props.onUpdateValue(this.props.component.uuid, variable_id, value, true);
  },
  handleComponentChange: function(component) {
    this.props.onUpdate(component);
  },

  renderActions: function() {
    return (
      <div className="btn-group">
        <Button sm title="Delete" tooltip="bottom" onClick={this.props.onDestroy} icon="trash-o" />
        <Button sm title="Save" tooltip="bottom" onClick={this.props.onSave} icon="save" />
      </div>
    );
  },
  renderConfiguration: function() {
    var template_id = this.props.component.template_id;
    var cfg = _.find(STANDARD_TEMPLATES, function(c) {
      return c.id == template_id;
    });
    if (cfg && cfg.config) {
      var Configuration = window[cfg.config];
      return (
        <Configuration
          getModel={this.props.getModel}
          component={this.props.component}
          onUpdateValue={this.handleValueChange}
          onUpdate={this.handleComponentChange} />
      );
    } else {
      return this.renderValues();
    }
  },
  handleTabChange: function(tab) {
    this.setState({active: tab});
  },
  renderGeneral: function() {
    if (this.props.component.template_id != "button") {
      return (
        <General
          component={this.props.component}
          onUpdate={this.handleComponentChange} />
      );
    }
  },
  renderStyles: function() {
    var sp = {currentScreenSize: this.props.currentScreenSize, site: this.props.site};
    if (this.props.component.template_id == "button") {
      var styles = this.props.component.styles;
      return (
        <Tabs defaultActive="default">
          <Styles
            font
            active={this.state.active}
            onChangeTab={this.handleTabChange}
            key="default"
            tabName="Default"
            {...sp}
            styles={styles}
            onUpdate={this.handleStyleChange} />
          <Styles
            font
            active={this.state.active}
            onChangeTab={this.handleTabChange}
            key="hover"
            tabName="Hover"
            {...sp}
            styles={_.extend({}, styles, styles.hover)}
            onUpdate={this.handleHoverStyleChange} />
          <Styles
            font
            active={this.state.active}
            onChangeTab={this.handleTabChange}
            key="active"
            tabName="Active"
            {...sp}
            styles={_.extend({}, styles, styles.hover, styles.active)}
            onUpdate={this.handleActiveStyleChange} />
        </Tabs>
      );
    } else {
      return (
        <Styles
          active={this.state.active}
          onChangeTab={this.handleTabChange}
          {...sp}
          styles={this.props.component.styles}
          onUpdate={this.handleStyleChange} />
      );
    }
  },
  renderValues: function() {
    return (
      <ComponentValues
        getModel={this.props.getModel}
        values={this.props.component.values}
        onUpdate={this.handleValueChange} />
    );
  },
  render: function() {
    return (
      <div className="shogun-component-configuration">
        <h3>{this.props.component.template_name}</h3>
        {this.renderActions()}
        {this.renderConfiguration()}
        <h4>Styles</h4>
        {this.renderStyles()}
        <h4>General</h4>
        {this.renderGeneral()}
      </div>
    );
  }
});
