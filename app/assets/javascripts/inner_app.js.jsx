var rootDomNode = document.getElementById("shogun-root");

function pm(args) {
  args.source = "inner";
  window.parent.postMessage(args, window.location.protocol + "//" + window.location.host);
}

function selectComponent(uuid) {
  pm({selectComponent: {uuid: uuid}});
}

function buildComponent(parent_uuid, variable_id, position, args) {
  pm({buildComponent: {parent_uuid: parent_uuid, variable_id: variable_id, position: position, args: args}});
}

function moveComponent(parent_uuid, variable_id, position, uuid) {
  pm({moveComponent: {parent_uuid: parent_uuid, variable_id: variable_id, position: position, uuid: uuid}});
}

function updateComponentValue(uuid, variable_id, value) {
  pm({updateComponentValue: {uuid: uuid, variable_id: variable_id, value: value}});
}

function editHTML(uuid, variable_id) {
  pm({editHTML: {uuid: uuid, variable_id: variable_id}});
}

function editMarkdown(uuid, variable_id) {
  pm({editMarkdown: {uuid: uuid, variable_id: variable_id}});
}

function uploadImage(uuid, variable_id) {
  pm({uploadImage: {uuid: uuid, variable_id: variable_id}});
}

function dragStart() {
  pm({dragStart: true});
}

function dragEnd() {
  pm({dragEnd: true});
}

function updateStyles(u, s) {
  pm({updateStyles: {uuid: u, styles: s}});
}

var rootComponent;

var cache = {};

function receiver(e) {
  if (e.data.sync) {
    if (e.data.sync.ready && !cache.models) {
      cache.models = e.data.sync.models;
    } else if (!e.data.sync.ready) {
      cache = {};
    }
  }
  var sync = e.data.sync && SHOGUN_UNDERSCORE.extend({}, e.data.sync, cache);
  if (!rootComponent && e.data.sync) {
    var root = (
      <Root
        {...sync}
        onConfigure={selectComponent}
        onBuild={buildComponent}
        onUpdate={updateComponentValue}
        onMove={moveComponent}
        onEditHTML={editHTML}
        onEditMarkdown={editMarkdown}
        onUploadImage={uploadImage}
        onDragStart={dragStart}
        onDragEnd={dragEnd}
        onUpdateStyles={updateStyles} />
    );
    rootComponent = React.render(root, rootDomNode);
  } else if (rootComponent && e.data.sync) {
    rootComponent.setProps(sync);
  } else if (rootComponent && e.data.getDimensions) {
    var uuid = e.data.getDimensions.uuid;
    var rid = e.data.getDimensions.rid;
    var $c = SHOGUN_JQUERY("#" + uuid);
    var offset = $c.offset();
    var d = {
      top: offset.top,
      left: offset.left,
      width: $c.outerWidth(),
      height: $c.outerHeight()
    };
    pm({dimensions: {dimensions: d, rid: rid}});
  }
};

SHOGUN_JQUERY(document).on("click", function(e) {
  if (e.target.nodeName == "A" || e.target.nodeName == "BUTTON" || e.currentTarget.activeElement.nodeName == "A" || e.currentTarget.activeElement.nodeName == "BUTTON") {
    return false;
  }
});

var ctrlKeys = [17, 91, 92]; // Ctrl for Win/Mac, L Command, R Command for Mac
var ctrlDown = false; // jquery has ctrlDown but for only Windows
var shiftDown = false; // 16: Shift

SHOGUN_JQUERY(document).on("keydown", function(e) {
  ctrlDown = ( ctrlKeys.indexOf( e.keyCode ) > -1 ) ? true : ctrlDown;
  shiftDown = ( e.keyCode == 16 ) ? true : shiftDown;
});
SHOGUN_JQUERY(document).on("keyup", function(e) {
  ctrlDown = ( ctrlKeys.indexOf( e.keyCode ) > -1 ) ? false : ctrlDown;
  shiftDown = ( e.keyCode == 16 ) ? false : shiftDown;
  if ( ctrlDown && e.keyCode == 90 ) {
    if ( SHOGUN_JQUERY(e.target).hasClass('cke_editable') ) return;
    if ( shiftDown ) { pm({redo: true}); }
    else { pm({undo: true}); }
  }
});

window.addEventListener("message", receiver);
pm({ready: true});
