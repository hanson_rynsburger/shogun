var Router = ReactRouter;
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

var App = React.createClass({
  mixins: [Router.State],

  renderNavbar: function() {
    var routeName = this.getRoutes()[this.getRoutes().length - 1].name;
    if (routeName == "edit_page" || routeName == "duplicate_page" || routeName == "new_page") {
      return null;
    }
    return <Navbar {...this.props} />;
  },
  render: function() {
    return (
      <div>
        {this.renderNavbar()}
        <RouteHandler {...this.props} />
      </div>
    )
  }
});

var routes = (
  <Route name="root" handler={UserWrapper} path="/">
    <Route handler={App}>
      <Route name="support" path="support" handler={Support} />
      <Route name="new_site" handler={NewSite} path="/sites/new" />
      <Route name="site" handler={SiteWrapper} path="/sites/:site_id">
        <Route handler={Site}>
          <Route name="pages" path="pages" handler={Pages} />
          <Route name="templates" path="templates" handler={Templates} />
          <Route name="models" path="collections" handler={Models} />

          <Route path="settings" handler={SiteSettingsWrapper}>
            <Route name="edit_site" handler={EditSite} path="edit" />
            <Route name="billing" handler={Billing} path="billing" />
            <Route name="users" handler={Users} path="users" />
            <Route name="integrate" handler={Integrate} path="integrate" />
            <Route name="add_ons" handler={AddOns} path="add_ons" />
          </Route>
        </Route>

        <Route name="new_template" handler={NewTemplate} path="templates/new" />
        <Route name="template" handler={TemplateWrapper} path="templates/:template_id">
          <Route name="edit_template" handler={Template} path="edit" />
        </Route>

        <Route name="new_model" handler={NewModel} path="collections/new" />
        <Route name="model" handler={ModelWrapper} path="collections/:model_id">
          <Route name="edit_model" handler={Model} path="edit" />

          <Route name="new_entry" handler={NewEntry} path="entries/new" />
          <Route name="entry" handler={EntryWrapper} path="entries/:entry_id">
            <Route name="edit_entry" handler={Entry} path="edit" />
          </Route>
        </Route>

        <Route name="layouts" handler={Layouts} path="pages/layouts" />
        <Route handler={NewPage}>
          <Route name="new_page" handler={Page} path="pages/new" />
        </Route>
        <Route handler={DuplicatePage} path="pages/:page_id">
          <Route name="duplicate_page" handler={Page} path="duplicate" />
        </Route>
        <Route name="page" handler={PageWrapper} path="pages/:page_id">
          <Route name="edit_page" handler={Page} path="edit" />
        </Route>
      </Route>

      <Route name="edit_user" handler={EditUser} path="/settings" />

      <DefaultRoute handler={Home} />
    </Route>
  </Route>
);

var div = document.getElementById("app");
if (div) {
  Router.run(routes, Router.HistoryLocation, function(Handler, state) {
    analytics.page();
    React.render(<Handler params={state.params} />, div);
  });
}
