var Router = ReactRouter;
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

var App = React.createClass({
  mixins: [Router.State],

  renderNavbar: function() {
    var routeName = this.getRoutes()[this.getRoutes().length - 1].name;
    var skip = ["edit_page", "duplicate_page", "new_page", "edit_article", "duplicate_article", "new_article"];

    if (skip.indexOf(routeName) >= 0) {
      return null;
    }

    return <ShopifyNavbar {...this.props} />;
  },
  render: function() {
    return (
      <div>
        {this.renderNavbar()}
        <RouteHandler {...this.props} />
      </div>
    )
  }
});

var routes = (
  <Route handler={UserWrapper}>
    <Route handler={App}>
      <Route handler={ShopifySiteWrapper}>
        <Route name="support" path="support" handler={Support} />
        <Route handler={ShopifySite}>
          <Route name="templates" path="templates" handler={Templates} />
          <Route name="blogs" path="blogs" handler={Blogs} />
          <Route name="models" path="collections" handler={Models} />

          <Route path="settings" handler={ShopifySiteSettingsWrapper}>
            <Route name="edit_site" handler={EditSite} path="edit" />
            <Route name="add_ons" handler={AddOns} path="add_ons" />
            <Route name="billing" handler={ShopifyBilling} path="billing" />
            <Route name="fonts" handler={Fonts} path="fonts" />
          </Route>
          <Route handler={Pages} name="pages" path="/" />
        </Route>

        <Route name="new_template" handler={NewTemplate} path="templates/new" />
        <Route name="template" handler={TemplateWrapper} path="templates/:template_id">
          <Route name="edit_template" handler={Template} path="edit" />
        </Route>

        <Route name="new_model" handler={NewModel} path="collections/new" />
        <Route name="model" handler={ModelWrapper} path="collections/:model_id">
          <Route name="edit_model" handler={Model} path="edit" />

          <Route name="new_entry" handler={NewEntry} path="entries/new" />
          <Route name="entry" handler={EntryWrapper} path="entries/:entry_id">
            <Route name="edit_entry" handler={Entry} path="edit" />
          </Route>
        </Route>

        <Route name="layouts" handler={Layouts} path="pages/layouts" />
        <Route name="blogs_layouts" path="blogs/:blog_id/layouts" handler={Layouts} />

        <Route handler={NewPage}>
          <Route name="new_page" handler={Page} path="pages/new" />
          <Route name="new_article" handler={Page} path="blogs/:blog_id/new" />
        </Route>

        <Route handler={DuplicatePage} path="pages/:page_id">
          <Route name="duplicate_page" handler={Page} path="duplicate" />
        </Route>
        <Route handler={DuplicatePage} path="blogs/:blog_id/article/:page_id">
          <Route name="duplicate_article" handler={Page} path="duplicate" />
        </Route>

        <Route name="page" handler={PageWrapper} path="pages/:page_id">
          <Route name="edit_page" handler={Page} path="edit" />
        </Route>
        <Route name="article" handler={PageWrapper} path="blogs/:blog_id/article/:page_id">
          <Route name="edit_article" handler={Page} path="edit" />
        </Route>
      </Route>
    </Route>
  </Route>
);

var div = document.getElementById("shopify_app");
if (div) {
  Router.run(routes, Router.HistoryLocation, function(Handler, state) {
    React.render(<Handler params={state.params} />, div);
  });
}
