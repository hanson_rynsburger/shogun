class TemplateParser
  def initialize(liq)
    @temp = Liquid::Template.parse liq, error_mode: :strict
    @root = LiquidProbeHash.new("root")
    @values = []
  end

  def parse
    @rerun = false

    @temp.render @root
    @root.accesses.each do |name|
      unless @root.has_key?(name)
        @root[name] = LiquidProbeHash.new(name)
      end
    end
    @temp.render @root
    @root.accesses.each do |name|
      p = @root[name]
      if !p.nil?
        next unless p.kind_of?(LiquidProbe)
        if p.array?
          if p.is_a?(LiquidProbeArray)
            if p.empty?
              p << LiquidProbeHash.new("#{name}.0")
              @rerun = true
            else
              first = p.first
              first.accesses.each do |a|
                if first[a].nil?
                  first[a] = LiquidProbeHash.new("#{name}.0.#{a}")
                  @rerun = true
                end
              end
            end
          else
            @root[name] = LiquidProbeArray.new(name)
            @rerun = true
          end
        end
      else
        @root[name] = LiquidProbeHash.new(name)
        @rerun = true
      end
    end

    @rerun ? parse : variables
  end

  def variables
    v = []
    @root.accesses.each do |name|
      p = @root[name]
      if p.is_a?(LiquidProbe)
        if p.array?
          if p.first.object?
            properties = p.first.accesses.map { |a| {name: a, type: "text"} }
            v << {name: name, type: "complex", input: "multi", properties: properties}
          else
            v << {name: name, type: "text", input: "multi"}
          end
        elsif !p.object?
          v << {name: name, type: "text", input: "inline"}
        else
          properties = p.accesses.map { |a| {name: a, type: "text"} }
          v << {name: name, type: "complex", properties: properties}
        end
      end
    end
    v
  end
end
