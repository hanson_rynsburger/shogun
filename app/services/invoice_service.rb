class InvoiceService
  def self.pay!(site: nil, invoice:, stripe_token:)
    return true if invoice.paid?
    return false unless stripe_token.present? || (site.present? && site.default_card.present?)

    card = site.try(:default_card)
    if stripe_token.present?
      if site.present?
        card = site.cards.build stripe_token: stripe_token
      else
        card = Card.new stripe_token: stripe_token
      end
      card.create_stripe_customer!
      ActiveRecord::Base.transaction do
        card.save!
        site.update! default_card_id: card.id if site.present?
      end
    end
    customer = card.stripe_customer
    ch = Stripe::Charge.create(
      amount: invoice._amount,
      customer: customer.id,
      currency: "usd",
      description: invoice.description
    )
    invoice.update! stripe_charge_id: ch.id, paid_at: Time.now, card_id: card.id
  end
end
