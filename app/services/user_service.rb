class UserService
  def self.find_or_create(user_params)
    provider = user_params[:provider]
    uid = user_params[:uid]

    if provider && uid
      user = User.find_for_provider_and_uid(provider, uid)
      if user.blank? && user_params[:email].present?
        user = User.where(email: user_params[:email].to_s.downcase).take
      end
      if user.present?
        Analytics.track(user_id: user.id, event: "Signed in", properties: {method: user.provider})
        return user
      end
      user_params[:password] = SecureRandom.uuid
      user_params[:password_confirmation] = user_params[:password]
    end

    create user_params
  end

  def self.create(user_params)
    User.create user_params
  end
end
