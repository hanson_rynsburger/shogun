class ApiService
  S3_DEFAULTS = {
    cache_control: "public, no-cache",
    acl: "public-read"
  }.freeze

  def self.reset!(site)
    versions = site.published_versions
    upload_json! site, versions
    purge_fastly! site
  end

  def self.purge_fastly!(site)
    site.purge if ENV["FASTLY_SERVICE_ID"].present? && ENV["FASTLY_API_KEY"].present?
  end

  def self.upload_json!(site, versions)
    json_obj = Aws::S3::Object.new bucket_name: ENV["CACHE_BUCKET_NAME"], key: "#{site.id}-#{site.secret_token}.json"
    json = versions.map { |v| {path: v.page.path, uuid: v.uuid, languages: [], meta_tags: v.page.meta_tags} }.to_json
    json_obj.put S3_DEFAULTS.merge({
      content_type: "application/json",
      body: json,
      metadata: {"surrogate-key" => site.record_key}
    })
  end
end
