class TemplateService
  def self.template_directory
    Rails.root.join("app", "templates")
  end

  def self.fetch_css(template_id)
    fn = template_directory.join("#{template_id}.css")
    if File.exists?(fn)
      File.read(fn)
    end
  end

  def self.fetch_js(template_id)
    fn = template_directory.join("#{template_id}.js")
    if File.exists?(fn)
      File.read(fn)
    end
  end

  def self.fetch_liquid(template_id)
    fn = template_directory.join("#{template_id}.liquid")
    if File.exists?(fn)
      File.read(fn)
    end
  end

  def self.standard_templates(site=nil)
    Settings.templates.map do |t|
      tc = t.clone
      tid = tc["id"]
      tc["current_version_id"] = tid
      tc["css"] = fetch_css tid
      tc["js"] = fetch_js tid
      tc["liquid"] = fetch_liquid tid
      tc["custom"] = false
      tc["styles"] = t["styles"] || {}
      if site.present? && site.shopify? && tid == "product"
        model = site.models.where(external: true).first
        model_id = model._id.to_s
        tc["variables"].each do |var|
          var["model_id"] = model_id
        end
      end
      Hashie::Mash.new(tc)
    end
  end

  def self.create(site, template_params)
    template = site.templates.build template_params
    template.current_version.template_id = template._id
    template.save && (template.current_version.persisted? || template.current_version.save!)
    template
  end

  def self.update(template, template_params, current_user)
    template.current_version_id = nil
    template.attributes = template_params
    template.current_version.template_id = template._id
    template.current_version.creator_id = current_user.id
    template.current_version.save && template.save!
  end

  def self.version_changed(template, old_version_id, current_version_id)
    old_version = MongoTemplateVersion.find old_version_id
    current_version = MongoTemplateVersion.find current_version_id

    mpv = MongoPageVersion.in(template_versions: old_version._id)
    page_ids = Set.new
    version_ids = Set.new
    mpv.each do |version|
      page_ids.add(version.page_id)
      version_ids.add(version._id)
    end

    pages = MongoPage.where(deleted_at: nil).in(_id: page_ids.to_a, current_version_id: version_ids.to_a)

    pages.each do |page|
      existing_page_version = page.versions.desc(:created_at).in(template_versions: current_version._id).first
      # if the page has a page_version which includes the current_version_id of the template, switch to that page_version
      if existing_page_version.present?
        page.update current_version_id: existing_page_version._id
      else
        # create a version
        version = create_page_version page, old_version, current_version
        # switch to it
        page.update current_version_id: version._id
      end
    end
  end

  def self.create_page_version(page, old_template_version, current_template_version)
    page_version = page.current_version
    clone = page.versions.build
    clone.uuid = SecureRandom.uuid
    clone.creator_id = current_template_version.creator_id
    root_clone = ComponentService.deep_clone page_version.root
    clone.root = root_clone
    ComponentService.each_in_tree(root_clone) do |component|
      # don't expand these components when saving them
      component.skip_expansion = true
      if component.template_version_id == old_template_version._id
        replace_template_version component, current_template_version
      end
    end
    clone.save!
    clone
  end

  def self.replace_template_version(component, template_version)
    values = []

    # update matching values
    variables_by_name = template_version.variables.index_by(&:name)
    component.values.each do |value|
      if variable = variables_by_name[value.variable_name]
        old_variable_type = value.variable_type
        value.variable_type = variable.type
        value.variable_input = variable.input
        value.variable_properties = variable.properties
        value.variable_id = variable._id
        value.variable_data_source = variable.data_source
        value.variable_model_id = variable.model_id
        if old_variable_type != value.variable_type
          case value.variable_type
          when "dropzone", "complex", "integer"
            # cannot carry value from any other variable type into dropzone, complex or integer
            value.value = []
          else
            # can carry value but not references
            value.references = []
          end
        end
        values << value
      end
    end

    # add missing values
    values_by_name = values.index_by(&:variable_name)
    template_version.variables.each do |variable|
      unless values_by_name[variable.name].present?
        values << MongoValue.new(
          variable_id: variable._id,
          variable_type: variable.type,
          variable_input: variable.input,
          variable_name: variable.name,
          variable_data_source: variable.data_source,
          variable_model_id: variable.model_id,
          variable_properties: variable.properties
        )
      end
    end

    component.values = values
    component.liquid = template_version.liquid
    component.css = template_version.css
    component.js = template_version.js
    component.template_version_id = template_version._id
  end

  def self.parse_variables(liquid)
    TemplateParser.new(liquid).parse
  end

  def self.copy(template)
    t = MongoTemplate.new
    t.name = template.name
    t.type = template.type
    t
  end
end
