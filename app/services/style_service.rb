class StyleService
  def self.render(styles)
    return "" unless styles.present?

    s = styles.clone
    arr = []

    s.except! *MongoComponent::ABSOLUTE_OVERRIDES
    s.except! *MongoComponent::DISPLAY_OVERRIDES
    s.except! "top", "left"

    render_box_shadow s, arr
    render_z_index s, arr
    render_background_image s, arr
    render_background_size s, arr
    render_sizes s, arr
    render_other s, arr
    render_border s, arr

    arr.join
  end

  def self.render_z_index(s, arr)
    if s["z_index"]
      arr << "z-index:#{s["z_index"]};"
    end
    s.except! "z_index"
  end

  def self.render_other(s, arr)
    s.each do |name, value|
      next if name == "hover" || name == "active"
      arr << "#{name.dasherize}:#{value};" unless value.blank?
    end
  end

  def self.render_sizes(s, arr)
    MongoComponent::SIZES.each do |size|
      value = s[size]
      if value.present?
        if size.start_with?('margin_') && value == 'auto'
          arr << "#{size.dasherize}:#{value};"
        else
          arr << "#{size.dasherize}:#{with_unit(value)};"
        end
      end
    end
    s.except! *MongoComponent::SIZES
  end

  def self.render_border(s, arr)
    if ["top_width", "left_width", "right_width", "bottom_width", "color"].any? { |name| s["border_#{name}"].present? }
      arr << "border-style:solid;"
    end
  end

  def self.render_background_size(s, arr)
    if s["background_size"].present?
      value = s["background_size"]
      if value == "contain" || value == "cover"
        arr << "background-size:#{value};"
      else
        bs = value.split.map { |v| with_unit(v) }.join(" ")
        arr << "background-size:#{bs};"
      end
      s.except! "background_size"
    end
  end

  def self.render_background_image(s, arr)
    if s["background_image"].present?
      arr << "background-image:url(#{s["background_image"]});background-repeat:no-repeat;"
      s.except! "background_image"
    end
  end

  def self.with_unit(num)
    n = num.to_s.strip
    if n.to_i.to_s == n
      "#{n}px"
    else
      n
    end
  end

  def self.render_box_shadow(s, arr)
    if MongoComponent::SHADOWS.any? { |prop| s[prop].present? }
      arr << "box-shadow:#{with_unit(s["box_shadow_horizontal"])} #{with_unit(s["box_shadow_vertical"])} #{with_unit(s["box_shadow_blur"])} #{with_unit(s["box_shadow_spread"])} #{s["box_shadow_color"]};"
      s.except! *MongoComponent::SHADOWS
    end
  end
end
