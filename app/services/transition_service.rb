class TransitionService
  def self.remove_translations!
    MongoPageVersion.each do |mpv|
      ComponentService.each_in_tree(mpv.root) do |comp|
        comp.values.each do |value|
          if MongoVariable::LOCALIZED_TYPES.include?(value.variable_type)
            value.value = value.value.to_a.map do |s|
              s[MongoValue::DEFAULT_LANGUAGE_KEY]
            end
          end
        end
      end
      mpv.save!
    end
  end

  def self.move_entries!
    MongoModel.each do |m|
      m.entries.index_by { |e| e._id.to_s }.values.each do |e|
        me = m.model_entries.build e.as_json
        me.upsert
      end
    end
  end

  def self.check_entries!
    MongoModel.each do |m|
      puts m._id unless m.entries.map(&:id).uniq.count == m.model_entries.count
    end
  end

  def self.remove_entries!
    MongoModel.update_all(entries: nil)
  end

  def self.fix_models!
    move_entries!
    check_entries!
    # remove_entries!
  end
end
