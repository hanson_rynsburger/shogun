class ComponentService
  def self.deep_clone(component)
    clone = component.clone
    clone.values = clone.values.map(&:clone)
    clone.values.each do |value|
      value.references = value.references.map(&:clone)
      value.references.each do |reference|
        reference.component = deep_clone reference.component
      end
    end
    clone
  end

  def self.each_in_tree(component, &blk)
    blk.call component
    component.values.each do |value|
      value.references.each do |reference|
        each_in_tree reference.component, &blk
      end
    end
  end

  def self.render_css(component)
    versions_css = {}

    each_in_tree(component) do |comp|
      if comp.template_version_id.present?
        versions_css[comp.template_version_id] = comp.css unless comp.css.blank?
      end
    end

    versions_css.values.map { |css| "<style type=\"text/css\">#{css}</style>" }.join
  end

  def self.render_js(component)
    versions_js = {}

    each_in_tree(component) do |comp|
      if comp.template_version_id.present?
        versions_js[comp.template_version_id] = comp.js unless comp.js.blank?
      end
    end

    versions_js.values.map { |js| "<script type=\"text/javascript\">#{js}</script>" }.join
  end

  def self.build(params)
    mc = MongoComponent.new
    # TODO validate these belong to the site
    mc.template_id = params[:template_id]
    mc.template_version_id = params[:template_version_id]
    mc.template_name = params[:template_name]
    mc.uuid = params[:uuid] || SecureRandom.uuid
    mc.css_classes = params[:css_classes]
    if params[:styles]
      mc.styles = params[:styles].slice(*MongoComponent::STYLES)
    else
      mc.styles = {}
    end
    if params[:data_attributes]
      mc.data_attributes = params[:data_attributes]
    else
      mc.data_attributes = {}
    end
    params[:values_attributes].to_a.each do |va|
      mv = mc.values.build
      mv.variable_id = va[:variable_id]
      if mv.variable_id.nil?
        mv.variable_type = "dropzone"
        mv.variable_input = "inline"
        mv.variable_name = "root"
      end
      mv.value = va[:value].to_a
      va[:references_attributes].to_a.each do |ra|
        mr = mv.references.build
        mr.position = ra[:position].to_i
        mr.component = build(ra[:component_attributes])
      end
    end
    mc
  end
end
