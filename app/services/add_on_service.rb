class AddOnService
  def self.render_js(mpv)
    site = mpv.page.site
    add_ons = site.add_ons.where(active: true).index_by(&:name)
    mixpanel = add_ons["mixpanel"]
    optimizely = add_ons["optimizely"]
    segment = add_ons["segment"]
    olark = add_ons["olark"]
    supportkit = add_ons["supportkit"]
    typekit = add_ons["typekit"]
    google_analytics = add_ons["google_analytics"]
    picreel = add_ons["picreel"]
    crazyegg = add_ons["crazyegg"]
    heap = add_ons["heap"]

    js = <<-JS
      <script type="text/javascript">
        window.LOAD_ANALYTICS = [];
        window.ANALYTICS_FUNCTIONS = [];
        $(function() {
          $('.shogun-component').on('click', function(e) {
            var $parent = $(e.target).parent();
            $.each(window.ANALYTICS_FUNCTIONS, function(k, v) {
              v(e, $parent);
            });
          });
        });
      </script>
    JS

    if heap.present?
      js << <<-JS
        <script type="text/javascript">
          window.heap=window.heap||[],heap.load=function(t,e){window.heap.appid=t,window.heap.config=e;var a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=("https:"===document.location.protocol?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+t+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(t){return function(){heap.push([t].concat(Array.prototype.slice.call(arguments,0)))}},p=["clearEventProperties","identify","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
          heap.load("#{heap.properties['app_id']}");
        </script>
      JS
    end

    if crazyegg.present?
      an = crazyegg.properties['account_number']
      path = an.insert(an.length/2, '/')
      js << <<-JS
        <script type="text/javascript">
        setTimeout(function(){var a=document.createElement("script");
        var b=document.getElementsByTagName("script")[0];
        a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/#{path}.js?"+Math.floor(new Date().getTime()/3600000);
        a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
        </script>
      JS
    end

    if picreel.present?
      js << <<-JS
        <script>
          $("body").append('<script src="//assets.pcrl.co/js/jstracker.min.js">');
        </script>
      JS
    end

    if google_analytics.present?
      js << <<-JS
        <script>
          if (!window['GoogleAnalyticsObject']) {
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            ga('create', '#{google_analytics.properties['property_id']}', 'auto');
            ga('send', 'pageview');
          }

          window.ANALYTICS_FUNCTIONS.push(function(ev, parent) {
            if(parent.hasClass('shogun-component') && !!parent.data('google_analytics-event-label')) {

              var en = parent.data('google_analytics-event-label');
              ga('send', 'event', 'button', 'click', en);
            }
          });
        </script>
      JS
    end

    if typekit.present?
      js << <<-JS
        <script>
          (function(d) {
            var config = {
              kitId: "#{typekit.properties['kit_id']}",
              scriptTimeout: 3000
            },
            h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='//use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
          })(document);
        </script>
      JS
    end

    if olark.present?
      js << <<-JS
        <script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
        f[z]=function(){
        (a.s=a.s||[]).push(arguments)};var a=f[z]._={
        },q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
        f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
        0:+new Date};a.P=function(u){
        a.p[u]=new Date-a.p[0]};function s(){
        a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
        hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
        return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
        b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
        b.contentWindow[g].open()}catch(w){
        c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
        var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
        b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
        loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
        /* custom configuration goes here (www.olark.com/documentation) */
        olark.identify("#{olark.properties['site_id']}");/*]]>*/</script>
      JS
    end

    if supportkit.present?
      js << <<-JS
        <script src="https://cdn.supportkit.io/supportkit.min.js"></script>
        <script>
          SupportKit.init({appToken: "#{supportkit.properties['app_token']}"});
        </script>
      JS
    end

    if mixpanel.present?
      js << <<-JS
        <script type="text/javascript">
          window.LOAD_ANALYTICS.push(function(){
            if(typeof mixpanel == 'undefined' || !$("head > script[src*='mxpnl']").length > 0){(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
              for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
              mixpanel.init("#{mixpanel.properties['token']}");}
          });
          window.ANALYTICS_FUNCTIONS.push(function(ev, parent) {
            if(parent.hasClass('shogun-component') && !!parent.data('mixpanel-event-name')) {
              var en = parent.data('mixpanel-event-name');
              var raw = parent.data('mixpanel-properties') || '{}';
              var prop;
              try {
                prop = JSON.parse(raw.replace(/'/g, '"'));
              } catch(foo) {}
              mixpanel.track(en, (prop || {}));
            }
          });
        </script>
      JS
    end

    if optimizely.present?
      js << <<-JS
        <script type="text/javascript" src="//cdn.optimizely.com/js/#{optimizely.properties['project_id']}.js"></script>
      JS
    end

    if segment.present?
      js << <<-JS
        <script type="text/javascript">
          window.loadAnalytics = function() {
            $.each(window.LOAD_ANALYTICS, function(k, v) {
              v();
            });
          }
          !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","group","track","ready","alias","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t){var e=document.createElement("script");e.type="text/javascript";e.onload=function(){loadAnalytics()};e.async=!0;e.src=("https:"===document.location.protocol?"https://":"http://")+"cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(e,n)};analytics.SNIPPET_VERSION="3.0.1";
          analytics.load("#{segment.properties['write_token']}");
          analytics.page()
          }}();
          window.ANALYTICS_FUNCTIONS.push(function(e, parent) {
            if(parent.hasClass('shogun-component') && !!parent.data('segment-event-name')) {
              var en = parent.data('segment-event-name');
              var raw = parent.data('segment-properties') || '{}';
              var prop;
              try {
                prop = JSON.parse(raw.replace(/'/g, '"'));
              } catch(foo) {}
              analytics.track(en, (prop || {}));
            }
          })
        </script>
      JS
    else
      js << <<-JS
        <script type="text/javascript">
          $.each(window.LOAD_ANALYTICS, function(k, v) {
            v();
          });
        </script>
      JS
    end

    js
  end

end
