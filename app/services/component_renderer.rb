class ComponentRenderer
  attr_reader :component

  def initialize(component)
    @component = component
  end

  def render
    args = {}
    if component.template_id.nil?
      liquid = '<div class="shogun-root">{{components}}</div>'
      value = component.values.first
      args["components"] = render_references value.references
      Liquid::Template.parse(liquid).render(args)
    elsif component.template_id == "product"
      render_product(component)
    else
      liquid = component.liquid
      gs = ""
      component.values.each do |value|
        variable = value.variable
        if variable.type == "dropzone"
          args[variable.name] = render_references value.references
        elsif variable.type == "complex_dropzone"
          if component.template_id == "columns"
            args[variable.name] = render_columns value
          elsif component.template_id == "tabs"
            args[variable.name] = render_tabs value, variable.name
          elsif component.template_id == "accordion"
            args[variable.name] = render_accordion value, variable.name
          end
        else
          args[variable.name] = render_variable variable, value
        end

        if variable.name == "col_gutter"
          gs = value.value[0]
        end
      end

      styles = StyleService.render(component.styles)

      if component.template_id == "button"
        args["__styles"] = styles
        args["__uuid"] = component.uuid
        args["__data"] = render_data
      else
        liquid = "<div id=\"c#{component.uuid}\" style=\"#{styles}\" #{render_data} class=\"shogun-component #{component.css_classes}\">#{liquid}</div>"
      end

      if component.template_id == 'video'
        if /^https:\/\/youtube\.com/ =~ args['url']
          args['videoid'] = /^https:\/\/youtube\.com\/embed\/(.+)$/.match(args['url'])[1]
        end
      end

      r = Liquid::Template.parse(liquid).render(args)
      s = render_style_tags(component.uuid, component.styles)

      if gs.present?
        gs = render_gutter_style(component.uuid, gs)
      end
      "#{r}#{s}#{gs}"
    end
  end

  def media_query(css, size)
    if size == "xs"
      "@media (max-width: 767px){#{css}}"
    elsif size == "sm"
      "@media (min-width: 768px) and (max-width: 991px){#{css}}"
    elsif size == "md"
      "@media (min-width: 992px) and (max-width: 1199px){#{css}}"
    elsif size == "lg"
      "@media (min-width: 1200px){#{css}}"
    end
  end

  def render_product(component)
    vbv = component.values.index_by(&:variable_id)
    product_id = vbv["product_id_variable_id"].value[0]
    variant_id = vbv["variant_id_variable_id"].value[0] rescue nil
    link = vbv["link_variable_id"].value[0]
    output = vbv["output_variable_id"].value[0]
    model = component.values.first.variable.model
    if product_id.present?
      product = model.entries.find product_id.to_i rescue nil
      if product.present? && variant_id.present?
        variant = product.variants.detect { |v| v["id"].to_s == variant_id.to_s }
        if variant.present?
          liquid = component.liquid
          image = product.images.detect { |i| i["id"].to_s == variant["image_id"].to_s || !variant["image_id"] }
          output = "image" if output.blank?
          return Liquid::Template.parse(liquid).render({"link" => link, "output" => output, "variant" => variant.as_json, "product" => product.as_json, "image" => image.as_json})
        end
      end
    end
    ""
  end

  def render_style_tags(uuid, s)
    ao = ""
    if s["position"] == "absolute"
      ao << "#c#{uuid}{transform:translate(#{s["left"]}px,#{s["top"]}px);}"
    end
    ao << MongoComponent::SCREEN_SIZES.reverse.map do |size|
      css = ""
      if s["position_#{size}"]
        x = s["left_#{size}"]
        y = s["top_#{size}"]
        t = "#c#{uuid}{transform:translate(#{x}px,#{y}px);}"
        css << media_query(t, size)
      end
      if s["display_#{size}"]
        css << media_query("#c#{uuid}{display:none;}", size)
      end
      css
    end.compact.join
    if s["hover"].present?
      hover = StyleService.render(s["hover"])
      hover.gsub!(";", " !important;")
      ao << "#c#{uuid} > a:hover {#{hover}}"
    end
    if s["active"].present?
      active = StyleService.render(s["active"])
      active.gsub!(";", " !important;")
      ao << "#c#{uuid} > a:active {#{active}}"
    end
    unless ao.blank?
      "<style type=\"text/css\">#{ao}</style>"
    end
  end

  def render_variable(variable, value)
    rendered = self.render_values variable, value
    if variable.input == "multi"
      rendered
    else
      rendered[0]
    end
  end

  def render_data
    return "" unless component.data_attributes.present?
    component.data_attributes.map do |add_on_name, config|
      config.map do |key, value|
        "data-#{add_on_name}-#{key}=\"#{value}\""
      end
    end.flatten.join(' ')
  end

  def render_references(references)
    references.sort_by(&:position).map do |reference|
      ComponentRenderer.new(reference.component).render
    end
  end

  def render_value(type, value)
    return nil unless value.present?

    if type == "image"
      "<img src=\"#{value["url"]}\" width=\"#{value["width"]}\" height=\"#{value["height"]}\" alt=\"#{value["alt_text"]}\" class=\"shogun-image img-responsive\" />"
    elsif type == "markdown"
      markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: true, fenced_code_blocks: true)
      markdown.render(value)
    else
      value
    end
  end

  def render_columns(value)
    refs = render_references value.references
    base = "shg-c-"
    cn = ""
    cfg = value.value[0]
    MongoComponent::SCREEN_SIZES.each do |size|
      cn += "#{base}#{size}-#{cfg[size]}"
      cn += " " unless size == "xs"
    end
    "<div class=\"#{cn}\">#{refs.join}</div>"
  end

  def render_tabs(value, key)
    refs = render_references value.references
    cname = key == "tab_1" ? "active" : ""
    "<div class=\"shogun-tab-pane #{cname}\" role=\"tabpanel\">#{refs.join}</div>"
  end

  def render_accordion(value, key)
    refs = render_references value.references
    cn = key === "accordion_1" ? " in" : ""
    cn = "shogun-panel-body" + cn
    "<div class=\"#{cn}\" id=\"#{key}\">#{refs.join}</div>"
  end

  def render_entry(model, e)
    keys = model.properties.keys
    if model.external?
      e.as_json.merge("id" => e._id.to_s)
    else
      r = {}
      keys.each do |k|
        r[k] = render_value model.properties[k], e[k]
      end
      r
    end
  end

  def render_values(variable, value)
    if variable.data_source == "model"
      model = variable.model
      entries = model.entries.order(position: :asc)
      if variable.input == "hidden"
        entry_id = value.value[0]
        if entry_id.present?
          entry = entries.find entry_id rescue nil
          if entry.nil? && entry_id.to_i > 0
            entry = entries.find entry_id.to_i rescue nil
          end
          if entry.present?
            [render_entry(model, entry)]
          else
            []
          end
        else
          []
        end
      else
        entries.map do |e|
          render_entry model, e
        end
      end
    elsif variable.type == "complex"
      map = {}
      variable.properties.each do |prop|
        map[prop["name"]] = prop["type"]
      end
      keys = map.keys
      value.value.to_a.map do |v|
        r = {}
        keys.each do |k|
          r[k] = render_value map[k], v[k]
        end
        r
      end
    elsif variable.type == "integer"
      value.value.to_a.map(&:to_i)
    else
      value.value.to_a.map { |v| render_value(variable.type, v) }
    end
  end

  def render_gutter_style(uuid, value)
    agap = value[0, value.length-2].to_f / 2
    return nil if agap == 15
    unit = value[-2, 2]
    "<style type=\"text/css\">[id=\"c#{uuid}\"] > .shg-row { margin-left: -#{agap}#{unit}; margin-right: -#{agap}#{unit}; } [id=\"c#{uuid}\"] > .shg-row > div { padding-right: #{agap}#{unit}; padding-left: #{agap}#{unit}; }</style>"
  end
end
