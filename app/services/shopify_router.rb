class ShopifyRouter
  def self.route(shop:, model:, topic:, payload:)
    Rails.logger.tagged shop.id do
      Rails.logger.info topic
      Rails.logger.tagged topic do
        case topic
        when *ShopifyShop::ORDER_HOOKS
          product_ids = payload["line_items"].to_a.map { |li| li["product_id"] }.compact.uniq
          if shop.id == "b02106bd-6536-4c57-8917-9b874440b559"
            Shopify::UpdateProductsWorker.perform_async model._id.to_s, product_ids if product_ids.any?
          end
        when "products/create", "products/update"
          Rails.logger.info "upsert"
          entry = model.entries.build(payload)
          entry.upsert
        when "products/delete"
          Rails.logger.tagged payload["id"] do
            Rails.logger.info "delete"
            model.entries.where(id: payload["id"].to_i).delete_all
          end
        when *ShopifyShop::APP_HOOKS
          Rails.logger.info "uninstall"
          shop.update! active: false, uninstalled_at: Time.zone.now
          Analytics.track(user_id: shop.id, event: "Uninstalled Shopify App")
          Shopify::UninstalledWorker.perform_async shop.id
        when *ShopifyShop::SHOP_HOOKS
          Rails.logger.info "sync"

          begin
            shop.sync_shopify_data!
            shop.save!
          rescue
          end
        end
      end
    end
  end
end
