require "render_anywhere"
require "yajl"

class ShopifyService < RenderAnywhere::RenderingController
  include RenderAnywhere

  class RenderingController < RenderAnywhere::RenderingController
    attr_accessor :current_user
    helper_method :current_user
  end

  def rendering_controller
    @rendering_controller ||= begin
      rc = self.class.const_get("RenderingController").new
      rc.current_user = @current_user
      rc
    end
  end

  class ShopifyError < StandardError
    attr_accessor :errors
    def initialize(errors)
      @errors = errors
    end
  end

  class ShopifyNotFoundError < StandardError
  end

  class ShopifyUnavailableShopError < StandardError
  end

  ARTICLE_TEMPLATE_KEY = "templates/article.liquid".freeze
  SHOGUN_ARTICLE_TEMPLATE_KEY = "templates/article.shogun.liquid".freeze
  SHOGUN_ARTICLE_TEMPLATE_CONTENT = "{{page.content}}".freeze

  PAGE_TEMPLATE_KEY = "templates/page.liquid".freeze
  SHOGUN_PAGE_TEMPLATE_KEY = "templates/page.shogun.liquid".freeze
  SHOGUN_PAGE_TEMPLATE_CONTENT = "{{page.content}}".freeze

  def initialize(shop)
    @shop = shop
    site = shop.site
    if site.present?
      @current_user = site.users.take!
    end
    @url = "https://#{shop.name}.myshopify.com/admin"
  end

  def json(params)
    MultiJson.dump(params)
  end

  def parse(response)
    MultiJson.load(response.body_str)
  end

  def http(url)
    easy = Curl::Easy.new
    easy.url = url
    easy.headers["X-Shopify-Access-Token"] = @shop.token
    easy.timeout = 10
    easy
  end

  def json_http(url)
    easy = http(url)
    easy.headers["Content-Type"] = "application/json"
    easy
  end

  def get(path)
    easy = http("#{@url}#{path}")
    easy.http_get
    easy
  end

  def post(path, params)
    easy = json_http("#{@url}#{path}")
    easy.post_body = json(params)
    easy.http_post
    easy
  end

  def put(path, params)
    easy = json_http("#{@url}#{path}")
    easy.http_put json(params)
    easy
  end

  def delete(path)
    easy = http("#{@url}#{path}")
    easy.http_delete
    easy
  end

  def scrape(blog_id = nil)
    site = @shop.site

    easy = Curl::Easy.new
    easy.follow_location = true
    easy.url = "#{site.url}/a/shogun/probe?blog_id=#{blog_id}"
    easy.timeout = 10
    easy.http_get

    doc = Nokogiri::HTML(easy.body_str)
    doc.css("head")[0].add_child("<base href=\"#{site.url}\">")
    doc.css("script").each(&:remove) unless @shop.javascript?
    probe = doc.css("#shogun-probe")[0]
    if probe.present?
      inner = render template: "shopify/editor/inner", layout: nil
      probe.children = inner
      doc.to_html
    end
  end

  def get_templates
    output = { "page" => [ "page" ], "article" => [ "article" ] }

    main = get_main_theme
    assets = get_theme_assets main["id"]
    assets.each do |asset|
      temp = /^templates\/(page|article)\.([^.\/\s]+)\.liquid$/.match asset["key"]
      if temp.present? && temp.size == 3
        type = temp[1].to_s
        output[type].push "#{type}.#{temp[2].to_s}"
      end
    end

    output
  end

  def each_theme_assets
    themes = get_themes
    themes.each do |theme|
      unless ["unpublished", "demo"].include?(theme["role"])
        assets = get_theme_assets(theme["id"])
        yield theme, assets
      end
    end
  end

  def add_shogun_template!
    each_theme_assets do |theme, assets|
      unless assets.any? { |asset| asset["key"] == SHOGUN_PAGE_TEMPLATE_KEY }
        create_theme_asset theme["id"], {key: SHOGUN_PAGE_TEMPLATE_KEY, value: SHOGUN_PAGE_TEMPLATE_CONTENT}
      end
    end
  end

  def remove_shogun_template!
    each_theme_assets do |theme, assets|
      assets.each do |asset|
        if asset["key"] == SHOGUN_PAGE_TEMPLATE_KEY
          delete_theme_asset theme["id"], SHOGUN_PAGE_TEMPLATE_KEY
        end
      end
    end
  end

  def get_liquid(keys, default)
    themes = get_themes
    main = themes.detect { |theme| theme["role"] == "main" }
    if main.present?
      assets = get_theme_assets main["id"]
      assets_by_key = assets.index_by { |asset| asset["key"] }
      keys.each do |k|
        if template = assets_by_key[k]
          full = get_theme_asset main["id"], template["key"]
          return full["value"]
        end
      end
    end
    default
  end

  def get_article_template_liquid(key = nil)
    get_liquid [key, SHOGUN_ARTICLE_TEMPLATE_KEY, ARTICLE_TEMPLATE_KEY].compact, SHOGUN_ARTICLE_TEMPLATE_CONTENT
  end

  def get_page_template_liquid(key = nil)
    get_liquid [key, SHOGUN_PAGE_TEMPLATE_KEY, PAGE_TEMPLATE_KEY].compact, SHOGUN_PAGE_TEMPLATE_CONTENT
  end

  def get_main_theme
    get_themes.detect { |theme| theme["role"] == "main" }
  end

  def get_themes
    r = get "/themes.json"
    raise unless r.response_code == 200
    parse(r)["themes"]
  end

  def get_theme_asset(theme_id, key)
    r = get "/themes/#{theme_id}/assets.json?asset[key]=#{key}"
    raise unless r.response_code == 200
    parse(r)["asset"]
  end

  def get_theme_assets(theme_id)
    r = get "/themes/#{theme_id}/assets.json"
    raise unless r.response_code == 200
    parse(r)["assets"]
  end

  def create_theme_asset(theme_id, params)
    r = put "/themes/#{theme_id}/assets.json", {asset: params}
    raise unless r.response_code == 200
    parse(r)["asset"]
  end

  def delete_theme_asset(theme_id, key)
    r = delete "/themes/#{theme_id}/assets.json?asset[key]=#{key}"
    raise unless r.response_code == 200
  end

  def get_shop_data
    r = get "/shop.json"
    raise unless r.response_code == 200
    parse(r)["shop"]
  end

  def products(page=1)
    r = get "/products.json?page=#{page}&limit=250"
    raise unless r.response_code == 200
    parse(r)["products"]
  end

  def get_product(shopify_product_id)
    r = get "/products/#{shopify_product_id}.json"
    raise unless r.response_code == 200
    parse(r)["product"]
  end

  def delete_page(shopify_page_id)
    r = delete "/pages/#{shopify_page_id}.json"
    handle_errors(r)
  end

  def update_page(shopify_page_id, params)
    if params[:metafields].present?
      indexed = params[:metafields].index_by { |mf| mf[:key] }
      get_page_metafields(shopify_page_id).each do |mf|
        key = mf["key"]
        if indexed.has_key?(key)
          indexed[key]["id"] = mf["id"]
        end
      end
    end
    r = put "/pages/#{shopify_page_id}.json", {page: params}
    handle_errors(r)
    parse(r)["page"]
  end

  def create_page(params)
    r = post "/pages.json", {page: params}
    handle_errors(r)
    parse(r)["page"]
  end

  def blogs
    r = get "/blogs.json"
    raise unless r.response_code == 200
    parse(r)["blogs"]
  end

  def get_blog(external_id)
    r = get "/blogs/#{external_id}.json"
    raise unless r.response_code == 200
    parse(r)["blog"]
  end

  def create_blog(params)
    r = post "/blogs.json", {blog: params}
    handle_errors(r)
    parse(r)["blog"]
  end

  def articles(blog_id)
    r = get "/blogs/#{blog_id}/articles.json"
    raise unless r.response_code == 200
    parse(r)["articles"]
  end

  def get_article_metafields(blog_id, article_id)
    r = get "/blogs/#{blog_id}/articles/#{article_id}/metafields.json"
    handle_errors(r)
    parse(r)["metafields"]
  end

  def get_article(blog_id, article_id)
    r = get "/blogs/#{blog_id}/articles/#{article_id}.json"
    raise unless r.response_code == 200
    parse(r)["article"]
  end

  def create_article(blog_id, params)
    r = post "/blogs/#{blog_id}/articles.json", {article: params}
    handle_errors(r)
    parse(r)["article"]
  end

  def delete_article(blog_id, article_id)
    r = delete "/blogs/#{blog_id}/articles/#{article_id}.json"
    raise unless r.response_code == 200
  end

  def update_article(blog_id, article_id, params)
    if params[:metafields].present?
      indexed = params[:metafields].index_by { |mf| mf[:key] }
      get_article_metafields(blog_id, article_id).each do |mf|
        key = mf["key"]
        if indexed.has_key?(key)
          indexed[key]["id"] = mf["id"]
        end
      end
    end
    r = put "/blogs/#{blog_id}/articles/#{article_id}.json", {article: params}
    handle_errors(r)
    parse(r)["article"]
  end

  def pages
    r = get "/pages.json?limit=250"
    raise unless r.response_code == 200
    parse(r)["pages"].reject { |page| page["handle"]["shoguntemp"] }
  end

  def get_page_metafields(shopify_page_id)
    r = get "/pages/#{shopify_page_id}/metafields.json"
    handle_errors(r)
    parse(r)["metafields"]
  end

  def get_page(external_id)
    r = get "/pages/#{external_id}.json"
    raise unless r.response_code == 200
    parse(r)["page"]
  end

  def recurring_charges
    r = get "/recurring_application_charges.json"
    raise unless r.response_code == 200
    parse(r)["recurring_application_charges"]
  end

  def get_recurring_charge(id)
    r = get "/recurring_application_charges/#{id}.json"
    raise unless r.response_code == 200
    parse(r)["recurring_application_charge"]
  end

  def create_recurring_charge(params)
    r = post "/recurring_application_charges.json", {recurring_application_charge: params}
    raise unless r.response_code == 201
    parse(r)["recurring_application_charge"]
  end

  def cancel_recurring_charge(id)
    r = delete "/recurring_application_charges/#{id}.json"
    raise unless r.response_code == 200
  end

  def activate_recurring_charge(id)
    r = post "/recurring_application_charges/#{id}/activate.json", {}
    raise unless r.response_code == 200
    parse(r)["recurring_application_charge"]
  end

  def create_web_hook(params)
    r = post "/webhooks.json", {webhook: params}
    raise unless r.response_code == 201
    parse(r)["webhook"]
  end

  def handle_errors(r)
    if r.response_code >= 500
      raise
    elsif r.response_code >= 400
      errors = parse(r)["errors"]
      if errors == "Not Found"
        raise ShopifyNotFoundError.new
      elsif errors == "Unavailable Shop"
        raise ShopifyUnavailableShopError.new
      elsif errors.is_a?(String)
        raise ShopifyError.new(errors)
      else
        errors = errors.map do |(k, a)|
          errs = a.is_a?(Array) ? a.join(", ") : a
          "#{k}: #{errs}"
        end
        raise ShopifyError.new(errors)
      end
    end
  end
end
