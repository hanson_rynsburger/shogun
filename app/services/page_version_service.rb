class PageVersionService
  S3_DEFAULTS = {
    cache_control: "public, no-cache",
    acl: "public-read"
  }.freeze

  def self.upload!(page_version, force=false)
    if force || !page_version.uploaded? || page_version.stale?
      upload_html! page_version, page_version.raw_html, "#{page_version.uuid}.html"
      page_version.set uploaded_at: Time.now, stale: false
    end
  end

  def self.upload_html!(page_version, body, key)
    html_obj = Aws::S3::Object.new bucket_name: ENV["CACHE_BUCKET_NAME"], key: key
    html_obj.put S3_DEFAULTS.merge({
      content_type: "text/html",
      body: body,
      metadata: {"surrogate-key" => page_version.record_key}
    })
  end
end
