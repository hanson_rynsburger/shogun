class PageService
  def self.update(page, params, current_user)
    mpv = build_version page, params[:page], current_user
    return false unless page.valid? && mpv.valid? && mpv.save
    page.current_version_id = mpv._id
    page.save!
  end

  def self.switch_version(page, version_id)
    version = page.versions.find(version_id)
    page.current_version = version
    page.save!
  end

  def self.create(site, params, current_user)
    page = site.pages.build
    mpv = build_version page, params[:page], current_user
    return page unless page.valid? && mpv.valid? && mpv.save
    page.current_version = mpv
    page.save!
    page
  end

  def self.publish!(page)
    page.update! updating: true
    page.current_version.publish!
  end

  def self.unpublish!(page)
    page.update! published_version_id: nil
    UnpublishPageWorker.perform_async page._id.to_s
  end

  def self.copy(page)
    p = MongoPage.new
    p.path = page.path
    p.name = page.name
    p
  end

  private

  def self.build_version(page, params, current_user)
    page.path = params[:path]
    page.name = params[:name]
    page.meta_tags = params[:meta]
    page.external_blog_id = params[:external_blog_id]
    page.author = params[:author] if params[:author].present?
    page.shopify_template = params[:shopify_template]
    mpv = page.versions.build(creator_id: current_user.id)
    mpv.root = ComponentService.build params[:current_version_attributes][:root_attributes]
    mpv
  end
end
