class SiteService
  def self.collaborate_or_invite(current_user, site, email)
    u = User.where(email: email).take

    if u.present?
      unless site.collaborators.include?(u)
        ActiveRecord::Base.transaction do
          collaborator = site.collaborators.create user_id: u.id
          Analytics.track(user_id: current_user.id, event: "Invited User", properties: {existing: true, user: u.tracking_fields})
          CollaboratorMailer.delay.collaborator_email(current_user, u, site)
          # trigger update
          site.save
          collaborator
        end
      end
    else
      unless site.invites.pending.where(email: email).exists?
        invite = site.invites.create user_id: current_user.id, email: email
        Analytics.track(user_id: current_user.id, event: "Invited User", properties: {existing: false, email: email})
        InviteMailer.delay.invite_email(current_user, email, site)
        # trigger update
        site.save
        invite
      end
    end
  end

  def self.copy_templates_and_pages(old_site, new_site)
    mapped_template_versions = {}
    mapped_templates = {}
    old_site.templates.where(deleted_at: nil).each do |template|
      t = TemplateService.copy template
      t.site_id = new_site.id
      t.current_version = template.current_version.clone
      t.current_version.creator_id = nil
      t.current_version.save!
      mapped_template_versions[template.current_version_id] = t.current_version_id
      t.save!
      mapped_templates[template._id] = t._id
    end

    standard_template_ids = Set.new(TemplateService.standard_templates.map(&:id))

    old_site.pages.each do |page|
      p = PageService.copy page
      p.site_id = new_site.id
      cv = page.current_version
      pv = MongoPageVersion.new
      p.current_version = pv
      pv.root = ComponentService.deep_clone(cv.root)
      ComponentService.each_in_tree(pv.root) do |comp|
        comp.skip_expansion = true
        if comp.template_version_id.present?
          unless standard_template_ids.member?(comp.template_version_id)
            comp.template_version_id = mapped_template_versions[comp.template_version_id]
            comp.template_id = mapped_templates[comp.template_id]
            raise unless comp.template_id.present? && comp.template_version_id.present?
          end
        end
      end
      pv.save!
      p.save!
    end
  end

  def self.copy_site(old_site)
    new_site = Site.new
    new_site.url = old_site.url
    new_site.name = old_site.name
    new_site.editor_url = old_site.editor_url
    new_site.preview_url = old_site.preview_url
    new_site.digest = old_site.digest
    new_site.skip_templates = true
    new_site.save!
    new_site
  end

  def self.copy(old_site)
    new_site = copy_site old_site
    copy_templates_and_pages old_site, new_site
    new_site
  end
end
