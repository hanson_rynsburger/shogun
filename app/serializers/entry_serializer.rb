class EntrySerializer < ActiveModel::Serializer
  attributes :id, :position

  def id
    object._id.to_s
  end

  def attributes
    data = super
    if object.model.external?
      object.attributes.except("_id").each do |k, v|
        data[k] = v
      end
    else
      object.model.properties.keys.each do |k|
        data[k] = object[k]
      end
    end
    data
  end
end
