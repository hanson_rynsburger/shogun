class CollaboratorSerializer < ActiveModel::Serializer
  attributes :id, :admin, :site_id

  has_one :user
end
