class FullPageSerializer < PageSerializer
  attributes :root_component

  root "page"

  def root_component
    ComponentSerializer.new(object.current_version.root, root: false).as_json
  end
end
