class VariableSerializer < ActiveModel::Serializer
  attributes :id, :name, :type, :input, :properties, :data_source, :model_id

  def model_id
    object.model_id.present? ? object.model_id.to_s : nil
  end

  def id
    object._id.to_s
  end
end
