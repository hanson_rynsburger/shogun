class PageSerializer < ActiveModel::Serializer
  attributes :id, :path, :name, :site_id, :meta_tags, :published, :outdated, :draft, :version_number, :updating, :current_version_id, :external_errors, :article, :tags, :author, :external_blog_id, :shopify_template

  def article
    object.article?
  end

  def id
    object._id.to_s
  end

  def current_version_id
    object.current_version_id.to_s
  end

  def published
    object.published?
  end

  def outdated
    object.outdated?
  end

  def draft
    object.draft?
  end
end
