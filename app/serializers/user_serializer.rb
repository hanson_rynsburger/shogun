class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :first_name, :last_name, :name, :company_name
end
