class CurrentUserSerializer < UserSerializer
  attributes :pusher_channel, :has_password, :admin

  def has_password
    object.password_digest.present?
  end

  def admin
    object.admin?
  end
end
