class InvoiceSerializer < ActiveModel::Serializer
  attributes :id, :description, :_amount, :created_at, :paid_at

  has_one :card
end
