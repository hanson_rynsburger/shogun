class AddOnSerializer < ActiveModel::Serializer
  attributes :id, :site_id, :properties, :name, :active
end
