class TemplateSerializer < ActiveModel::Serializer
  attributes :id, :name, :type, :liquid, :css, :js, :site_id, :version_number, :deleted_at, :current_version_id, :styles

  has_many :variables

  def variables
    object.variables.map { |v| VariableSerializer.new(v, root: false) }
  end

  def id
    object._id.to_s
  end

  def current_version_id
    object.current_version_id.to_s
  end
end
