class ModelSerializer < ActiveModel::Serializer
  attributes :id, :name, :properties, :version_number, :site_id, :singularized_name, :pluralized_name, :mutable, :external

  has_many :entries, each_serializer: EntrySerializer

  def id
    object._id.to_s
  end

  def external
    object.external?
  end

  def mutable
    object.mutable?
  end

  def singularized_name
    object.name.singularize
  end

  def pluralized_name
    object.name.pluralize
  end

  def entries
    rel = object.entries.includes(:model)
    rel = rel.order(position: :asc) unless object.external?
    rel
  end
end
