class ComponentSerializer < ActiveModel::Serializer
  attributes :id, :template_id, :template_version_id, :styles, :uuid, :css, :data_attributes, :liquid, :template_name, :css_classes

  has_many :values

  def values
    object.values.map { |v| ValueSerializer.new(v, root: false) }
  end

  def id
    object._id.to_s
  end

  def template_name
    object.template_name || object.template.try(:name)
  end

  def template_id
    object.template_id.to_s
  end

  def template_version_id
    object.template_version_id.to_s
  end
end
