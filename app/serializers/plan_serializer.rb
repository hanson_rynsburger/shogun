class PlanSerializer < ActiveModel::Serializer
  attributes :id, :activated_at, :max_users, :max_pages, :price, :price_per_user, :price_per_page, :active, :name

  def active
    object.activated? && !object.deactivated?
  end
end
