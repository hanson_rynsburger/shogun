class ValueSerializer < ActiveModel::Serializer
  attributes :id, :value, :variable

  has_many :references

  def value
    object.value.to_a
  end

  def references
    object.references.map { |r| ReferenceSerializer.new(r, root: false) }
  end

  def id
    object._id.to_s
  end

  def variable
    v = {
      name: object.variable_name,
      type: object.variable_type,
      input: object.variable_input,
      properties: object.variable_properties,
      data_source: object.variable_data_source,
      model_id: object.variable_model_id.to_s
    }
    if object.variable_id.present?
      v[:id] = object.variable_id.to_s
    end
    v
  end
end
