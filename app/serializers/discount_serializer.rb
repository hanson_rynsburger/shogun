class DiscountSerializer < ActiveModel::Serializer
  attributes :usable, :amount, :plan_name

  def usable
    object.usable?
  end
end
