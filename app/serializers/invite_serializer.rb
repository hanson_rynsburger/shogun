class InviteSerializer < ActiveModel::Serializer
  attributes :id, :email, :site_id
end
