class SiteSerializer < ActiveModel::Serializer
  attributes :id, :name, :url, :users_count, :published_pages_count, :max_users, :max_pages, :version_number, :secret_token, :digest, :permissions, :shopify_shop, :shopify, :editor_url, :features, :preview_url, :fonts, :colors, :demo

  has_one :default_card
  has_one :shopify_shop
  has_one :plan
  has_one :discount

  def secret_token
    if object.demo?
      nil
    else
      object.secret_token
    end
  end

  def colors
    object.colors.to_a
  end

  def features
    object.features.to_h
  end

  def shopify
    object.shopify?
  end

  def preview_url
    object.preview_url
  end

  def editor_url
    object.editor_url
  end

  def default_card
    if object.default_card.present?
      CardSerializer.new(object.default_card, root: false)
    end
  end

  def permissions
    {
      edit: true,
      publish: can_publish?
    }
  end

  def can_publish?
    return false if object.demo?
    object.collaborators.any? { |c| c.user_id == scope.id && c.admin? }
  end
end
