class SnippetSerializer < ActiveModel::Serializer
  attributes :id, :root_component, :name, :site_id

  def id
    object._id.to_s
  end

  def root_component
    ComponentSerializer.new(object.root, root: false)
  end
end
