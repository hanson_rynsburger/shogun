class ShopifyShopSerializer < ActiveModel::Serializer
  attributes :domain, :name

  def name
    object.domain.split(".").first
  end
end
