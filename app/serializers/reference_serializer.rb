class ReferenceSerializer < ActiveModel::Serializer
  attributes :id, :position

  has_one :component

  def id
    object._id.to_s
  end

  def component
    ComponentSerializer.new(object.component, root: false)
  end
end
