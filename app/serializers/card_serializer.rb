class CardSerializer < ActiveModel::Serializer
  attributes :id, :last4, :brand, :exp_month, :exp_year
end
