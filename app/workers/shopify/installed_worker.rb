class Shopify::InstalledWorker
  include Sidekiq::Worker

  def perform(shop_id)
    shop = ShopifyShop.find shop_id
    ShopifyMailer.shopify_installed(shop).deliver_now
    FounderMailer.shopify_installed(shop).deliver
  end
end
