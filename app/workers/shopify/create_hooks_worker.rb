class Shopify::CreateHooksWorker
  include Sidekiq::Worker

  sidekiq_options :queue => :critical

  def perform(ss_id)
    shop = ShopifyShop.find ss_id
    service = shop.service
    address = Rails.application.routes.url_helpers.receive_shopify_hook_url

    ShopifyShop::HOOKS.each do |topic|
      service.create_web_hook topic: topic, address: address rescue nil
    end
  end
end
