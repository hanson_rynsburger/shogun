class Shopify::DeleteTemporaryPageWorker
  include Sidekiq::Worker

  sidekiq_options :queue => :critical

  def perform(shop_id, shopify_page_id)
    shop = ShopifyShop.find shop_id
    service = shop.service
    service.delete_page shopify_page_id
  rescue ShopifyService::ShopifyNotFoundError
  end
end
