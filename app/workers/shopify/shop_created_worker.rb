class Shopify::ShopCreatedWorker
  include Sidekiq::Worker

  def perform(shop_id)
    shop = ShopifyShop.find shop_id
    model = shop.site.models.find_or_create_by(name: "Shopify Products", mutable: false, external: true, properties: {"id" => "text", "title" => "text", "handle" => "text"})
    Shopify::FetchProductsWorker.perform_async shop.id, model._id.to_s
  end
end
