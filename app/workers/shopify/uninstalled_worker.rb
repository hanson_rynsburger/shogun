class Shopify::UninstalledWorker
  include Sidekiq::Worker

  def perform(shop_id)
    shop = ShopifyShop.find shop_id
    ShopifyMailer.shopify_uninstalled(shop).deliver_now
    FounderMailer.shopify_uninstalled(shop).deliver
  end
end
