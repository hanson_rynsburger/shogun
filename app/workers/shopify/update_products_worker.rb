class Shopify::UpdateProductsWorker
  include Sidekiq::Worker

  sidekiq_options :queue => :low

  def perform(model_id, product_ids)
    m = MongoModel.find model_id
    shop = m.site.shopify_shop
    service = shop.service

    product_ids.each do |pid|
      product = service.get_product(pid) rescue nil
      next unless product.present?
      if entry = m.entries.where(id: product["id"].to_i).first
        entry.attributes = product
      end
    end

    m.save!
  end
end
