class Shopify::DeleteTemporaryArticleWorker
  include Sidekiq::Worker

  sidekiq_options :queue => :critical

  def perform(shop_id, shopify_blog_id, shopify_article_id)
    shop = ShopifyShop.find shop_id
    service = shop.service
    service.delete_article shopify_blog_id, shopify_article_id
  rescue ShopifyService::ShopifyNotFoundError
  end
end
