class Shopify::ModifyThemesWorker
  include Sidekiq::Worker

  sidekiq_options :queue => :critical

  def perform(ss_id)
    shop = ShopifyShop.find ss_id
    service = shop.service
    service.add_shogun_template!
  end
end
