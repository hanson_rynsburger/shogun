class Shopify::ChargeAcceptedWorker
  include Sidekiq::Worker

  def perform(shop_id)
    s = ShopifyShop.find shop_id
    FounderMailer.shopify_charge_accepted(s).deliver
  end
end
