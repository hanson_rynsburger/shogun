class Shopify::FetchProductsWorker
  include Sidekiq::Worker

  def perform(shop_id, model_id, page=1)
    shop = ShopifyShop.find shop_id
    model = MongoModel.find model_id
    service = shop.service

    p = page
    loop do
      begin
        products = service.products p
        products.each do |product|
          entry = model.entries.build product
          entry.upsert
        end
        if products.count < 250
          Shopify::CreateHooksWorker.perform_async shop_id
          break
        end
        p += 1
      rescue
        Shopify::FetchProductsWorker.perform_in(60.seconds, shop_id, model_id, p)
        return
      end
    end
  end
end
