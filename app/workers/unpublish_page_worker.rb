class UnpublishPageWorker
  include Sidekiq::Worker

  sidekiq_options :queue => :critical

  def perform(page_id)
    page = MongoPage.find page_id
    page.site.unpublish_page! page
  end
end
