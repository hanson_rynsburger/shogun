class PageUpdatedWorker
  include Sidekiq::Worker

  def perform(page_id)
    page = MongoPage.where(deleted_at: nil).find page_id
    payload = PageSerializer.new(page, root: false).to_json

    page.site.users.each do |user|
      user.push! "page:updated", payload
    end
  end
end
