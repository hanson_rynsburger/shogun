class HerokuAddOnWorker
  include Sidekiq::Worker

  sidekiq_options :retry => false

  def perform(site_id, user_id)
    begin
      site = Site.find site_id
      user = User.find user_id
      response = Curl::Easy.get "https://#{ENV['HEROKU_USERNAME']}:#{ENV['HEROKU_PASSWORD']}@api.heroku.com/vendor/apps/#{site.id}"
      json = ActiveSupport::JSON.decode response.body_str
      site.update url: "https://#{json['domains'][0]}", name: json['name']
      user.update email: json['owner_email'], company_name: json['name']

      client = Mailchimp::API.new(ENV['MAILCHIMP_API_KEY'])
      id = client.lists.list({'list_name' => 'Heroku Users'})['data'][0]['id']
      client.lists.subscribe(id, {email: json['owner_email']}, {'FNAME' => user.name}, 'html', false)
    rescue
    end
  end
end
