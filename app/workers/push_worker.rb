class PushWorker
  include Sidekiq::Worker

  def perform(channel, event_name, payload)
    Pusher.trigger channel, event_name, payload
  end
end
