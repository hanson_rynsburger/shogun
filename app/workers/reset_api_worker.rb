class ResetApiWorker
  include Sidekiq::Worker

  def perform(site_id)
    site = Site.find site_id
    ApiService.reset! site unless Rails.env.development?
    # trigger updates
    site.save!
    site.updating_pages.each do |page|
      page.update! updating: false
    end
  end
end
