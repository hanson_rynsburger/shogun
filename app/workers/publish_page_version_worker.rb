class PublishPageVersionWorker
  include Sidekiq::Worker

  sidekiq_options :queue => :critical

  def perform(page_version_id)
    page_version = MongoPageVersion.find page_version_id
    page = page_version.page
    site = page.site
    site.publish_page_version! page, page_version
  end
end
