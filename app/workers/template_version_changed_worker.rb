class TemplateVersionChangedWorker
  include Sidekiq::Worker

  sidekiq_options :queue => :critical

  def perform(template_id, old_version_id, current_version_id)
    mt = MongoTemplate.find template_id
    TemplateService.version_changed mt, old_version_id, current_version_id
  end
end
