class MailchimpWorker
  include Sidekiq::Worker

  def perform(user_id)
    user = User.find(user_id)
    if Rails.env.production? && !user.heroku? && ENV['NAME'] != 'staging'
      client = Mailchimp::API.new(ENV['MAILCHIMP_API_KEY'])
      list_name = 'Shogun Users'
      id = client.lists.list({'list_name' => list_name})['data'][0]['id']
      client.lists.subscribe(id, {email: user.email}, {'FNAME' => user.name}, 'html', false)
    end
  end
end
