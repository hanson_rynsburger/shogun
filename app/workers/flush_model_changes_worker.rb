class FlushModelChangesWorker
  include Sidekiq::Worker

  def perform(model_id)
    model = MongoModel.find model_id
    site = model.site

    # find template versions that use the model
    template_versions = MongoTemplateVersion.where("variables.model_id" => model._id)

    # find page versions using those templates
    relevant_page_versions = MongoPageVersion.in(template_versions: template_versions.pluck(:_id))

    # only care about uploaded files
    unless site.shopify?
      relevant_page_versions = relevant_page_versions.ne(uploaded_at: nil)
    end

    published_version_ids = site.pages.pluck(:published_version_id)

    # filter to only currently published page versions
    published_versions = relevant_page_versions.in(_id: published_version_ids)

    # reupload for all relevant published page versions immediately
    published_versions.each do |page_version|
      RefreshPageVersionWorker.perform_async page_version._id.to_s
    end

    # mark stale file uploads
    unless site.shopify?
      stale_versions = relevant_page_versions.nin(_id: published_version_ids)
      stale_versions.update_all(stale: true)
    end
  end
end
