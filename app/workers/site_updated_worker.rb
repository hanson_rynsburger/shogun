class SiteUpdatedWorker
  include Sidekiq::Worker

  def perform(site_id)
    site = Site.find site_id

    site.users.each do |user|
      user.push! "site:updated", SiteSerializer.new(site, root: false, scope: user).to_json
    end
  end
end
