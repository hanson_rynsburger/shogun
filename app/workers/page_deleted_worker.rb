class PageDeletedWorker
  include Sidekiq::Worker

  sidekiq_options :queue => :critical

  def perform(page_id)
    page = MongoPage.find page_id

    if page.published?
      page.site.delete_page! page
    end
  end
end
