module LiquidProbe
  extend ActiveSupport::Concern

  def initialize(name)
    @name = name
    @array = false
    @object = false
    @value = false
    @accesses = []
  end

  included do
    public_instance_methods.each do |meth|
      next if [:__send__, :object_id].include?(meth)
      eval <<-RUBY
      def #{meth}(*args, &blk)
        probe :#{meth}, *args unless [:probe, :accesses, :array?, :object?, :value?].include?(:#{meth})
        super
      end
      RUBY
    end
  end

  def accesses
    @accesses.uniq
  end

  def array?
    @array
  end

  def object?
    @object
  end

  def value?
    @value
  end

  def probe(meth, *args)
    puts "#{@name}->#{meth}:#{args}" if Rails.env.development?
    if meth == :tainted?
      @value = true
    elsif meth == :respond_to? && args.first == :each
      @array = true
    elsif meth == :respond_to? && args.first == :has_key?
      @object = true
    elsif meth == :has_key? || meth == :[]
      @accesses << args.first
    end
  end
end
