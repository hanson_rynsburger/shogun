document.addEventListener('DOMContentLoaded', function() {
  jQuery('.shogun-panel-title > a').on('click', function(e) {
    e.preventDefault();
    var group = jQuery(this).closest('.shogun-panel-group');
    var panel = jQuery(this).closest('.shogun-panel');
    var currentPanel = panel.children('.shogun-panel-body');
    var isActive = currentPanel.hasClass('in');

    group.children('.shogun-panel').each(function(){
      jQuery(this).children('.shogun-panel-body.in').each(function() {
        var old = jQuery(this);
        old.slideToggle('fast', function() {
          old.removeClass('in');
        });
      });
    });

    if (!isActive) {
      currentPanel.slideToggle('fast', function() {
        currentPanel.addClass('in');
      });
    }
  });
}, false);
