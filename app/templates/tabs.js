document.addEventListener('DOMContentLoaded', function() {
  jQuery('.shogun-nav-tabs > li > a').on('click', function(e) {
    e.preventDefault();
    var active = 'active';
    if (jQuery(this).parent().hasClass(active)) return;
    var $li = jQuery(this).parent();
    var num = $li.index();
    $li.siblings().removeClass(active);
    $li.addClass(active);
    var $panes = $li.parent().parent().find('.shogun-tab-content > .shogun-tab-pane');
    $panes.removeClass(active);
    jQuery($panes[num]).addClass(active);
  });
}, false);
