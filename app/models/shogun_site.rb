# == Schema Information
#
# Table name: sites
#
#  id              :uuid             not null, primary key
#  name            :string
#  url             :string
#  created_at      :datetime
#  updated_at      :datetime
#  secret_token    :string
#  old_plan        :string
#  default_card_id :uuid
#  version_number  :integer          default(0)
#  deleted_at      :datetime
#  editor_url      :string
#  preview_url     :string
#  features        :text
#  colors          :text             default([]), is an Array
#  digest          :string
#  demo            :boolean
#  type            :string
#  fonts           :text             default([]), is an Array
#
# Indexes
#
#  index_sites_on_deleted_at  (deleted_at)
#  index_sites_on_type        (type)
#

class ShogunSite < Site
  def delete_page!(page)
    reset_api!
  end

  def publish_page_version!(page, page_version)
    PageVersionService.upload! page_version unless Rails.env.development?
    page.update! published_version_id: page_version._id
    reset_api!
  end

  def unpublish_page!(page)
    reset_api!
  end

  def refresh_page_version!(page, page_version)
    PageVersionService.upload! page_version, true unless Rails.env.development?
  end

  private

  def reset_api!
    ResetApiWorker.perform_async id
  end
end
