class MongoVariable
  include Persistable

  field :name, type: String
  field :type, type: String
  field :input, type: String
  # [{name: "...", type: "..."}, ...]
  field :properties, type: Array
  field :data_source, type: String, default: "user"
  belongs_to :model, class_name: "MongoModel"

  embedded_in :template_version, class_name: "MongoTemplateVersion"

  INPUTS = ["inline", "multi", "hidden"].freeze
  DATA_SOURCES = ["user", "model"].freeze
  INPUT_NAMES = {
    "inline" => "Inline",
    "multi" => "Multi",
    "hidden" => "Sidebar"
  }.freeze

  VISIBLE = ["inline", "multi"].freeze
  MULTI = ["multi"].freeze
  INLINE = ["inline"].freeze
  HIDDEN = ["hidden"].freeze

  SELECTABLE_TYPES = ["text", "dropzone", "richtext", "image", "markdown", "html", "integer"].freeze
  TYPES = (SELECTABLE_TYPES + ["complex"]).freeze

  TYPE_INPUTS = {
    "text" => INPUTS,
    "integer" => HIDDEN,
    "dropzone" => INLINE,
    "richtext" => VISIBLE,
    "image" => INPUTS,
    "complex" => (MULTI + HIDDEN).freeze,
    "markdown" => INPUTS,
    "html" => INPUTS
  }.freeze

  TYPE_NAMES = {
    "text" => "Plain Text",
    "integer" => "Integer",
    "dropzone" => "Dropzone",
    "richtext" => "Rich Text",
    "image" => "Image",
    "complex" => "Complex",
    "markdown" => "Markdown",
    "html" => "HTML"
  }.freeze

  # these exist for standard templates:
  # number, boolean, complex_dropzone

  LOCALIZED_TYPES = ["text", "richtext", "html"].freeze

  TYPES_FOR_SELECT = SELECTABLE_TYPES.map { |t| [TYPE_NAMES[t], t] }.freeze
  TYPES_FOR_SELECT_EXCLUDING_DROPZONE = SELECTABLE_TYPES.select { |t| t != "dropzone" }.map { |t| [TYPE_NAMES[t], t] }.freeze

  validates :input, inclusion: {in: INPUTS}
  validates :type, inclusion: {in: TYPES}
  validates :data_source, inclusion: {in: DATA_SOURCES}
  validate :input_suitable_for_type

  before_save :simplify_properties

  def input_suitable_for_type
    unless TYPE_INPUTS[type].include?(input)
      self.errors[:input] << ("#{input} invalid for type #{type}")
    end
  end

  def simplify_properties
    if self.properties.present?
      if self.properties.respond_to?(:values)
        self.properties = self.properties.values.to_a
      else
        self.properties = self.properties.to_a
      end
    end
  end
end
