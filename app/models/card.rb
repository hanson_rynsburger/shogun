# == Schema Information
#
# Table name: cards
#
#  id                 :uuid             not null, primary key
#  site_id            :uuid
#  stripe_customer_id :string
#  created_at         :datetime
#  updated_at         :datetime
#  brand              :string
#  last4              :string
#  exp_month          :integer
#  exp_year           :integer
#
# Indexes
#
#  index_cards_on_site_id  (site_id)
#

class Card < ActiveRecord::Base
  belongs_to :site

  attr_accessor :stripe_token

  def stripe_customer
    @stripe_customer ||= Stripe::Customer.retrieve stripe_customer_id
  end

  def create_stripe_customer!
    raise unless stripe_token.present?
    @stripe_customer = Stripe::Customer.create card: stripe_token
    card = @stripe_customer.sources.data.first
    self.last4 = card.last4
    self.brand = card.brand
    self.exp_month = card.exp_month
    self.exp_year = card.exp_year
    self.stripe_customer_id = @stripe_customer.id
  end
end
