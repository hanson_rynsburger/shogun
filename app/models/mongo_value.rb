class MongoValue
  include Persistable

  DEFAULT_LANGUAGE_KEY = "_".freeze

  field :value, type: Array
  field :variable_name, type: String
  field :variable_type, type: String
  field :variable_input, type: String
  # [{name: "...", type: "..."}, ...]
  field :variable_properties, type: Array
  field :variable_data_source, type: String, default: "user"
  field :variable_model_id, type: BSON::ObjectId

  belongs_to :variable, class_name: "MongoVariable"
  embedded_in :component, class_name: "MongoComponent"
  embeds_many :references, class_name: "MongoReference"

  class VariableShim < Struct.new(:name, :type, :input, :properties, :data_source, :model_id)
    def model
      MongoModel.find model_id
    end
  end

  def variable
    VariableShim.new(variable_name, variable_type, variable_input, variable_properties, variable_data_source, variable_model_id)
  end

  def variable_name
    if variable_id =~ /\Atab_\d{1,2}_variable_id\z/
      num = variable_id.split("_")[1]
      "tab_#{num}"
    else
      super
    end
  end

  # TODO validate value is acceptable
end
