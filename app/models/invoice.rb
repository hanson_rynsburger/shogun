# == Schema Information
#
# Table name: invoices
#
#  id               :uuid             not null, primary key
#  plan_id          :uuid
#  stripe_charge_id :string
#  _amount          :integer
#  created_at       :datetime
#  updated_at       :datetime
#  paid_at          :datetime
#  site_id          :uuid
#  card_id          :uuid
#  description      :text
#
# Indexes
#
#  index_invoices_on_card_id  (card_id)
#  index_invoices_on_plan_id  (plan_id)
#  index_invoices_on_site_id  (site_id)
#

class Invoice < ActiveRecord::Base
  belongs_to :plan
  belongs_to :site
  belongs_to :card

  validates :site_id, presence: true
  validates :_amount, numericality: {greater_than: 0}

  def paid?
    paid_at.present?
  end
end
