class MongoPage
  include Persistable
  include MongoVersioned

  scope :published, -> { ne(published_version_id: nil) }
  scope :article, -> { ne(external_blog_id: nil) }
  scope :not_article, -> { where(external_blog_id: nil) }

  field :site_id, type: String
  field :path, type: String
  field :name, type: String
  field :meta_tags, type: Array
  field :updating, type: Boolean, default: false
  field :deleted_at, type: DateTime
  field :external_errors, type: Array, default: []
  field :external_page_id, type: String

  field :shopify_template, type: String

  # article fields

  field :tags, type: Array, default: []
  field :external_blog_id, type: String
  field :author, type: String

  validates :path, :name, presence: true
  validates :name, :path, uniqueness: { scope: [:site_id, :deleted_at] }
  with_options unless: :article? do |o|
    o.validates :path, format: {with: /\A\/([\.a-zA-Z0-9_-]+\/?)*\z/, message: "must be in the form /foo"}
    o.validate :path_not_already_in_use_externally
  end

  before_validation :fix_path

  has_many :versions, class_name: "MongoPageVersion"
  belongs_to :published_version, class_name: "MongoPageVersion"
  belongs_to :current_version, class_name: "MongoPageVersion"

  after_update do
    PageUpdatedWorker.perform_async self._id.to_s unless deleted_at.present?
  end

  store_in collection: "pages"

  index({published_version_id: 1}, {background: true})
  index({site_id: 1}, {background: true})
  index({external_blog_id: 1}, {background: true})

  def self.published_pages_by_site_id
    m0 = {"$match" => {"published_version_id" => {"$ne" => nil}}}
    g0 = {"$group" => {"_id" => "$site_id", total: {"$sum" => 1}}}
    collection.aggregate([m0, g0]).each_with_object({}) do |row, h|
      h[row["_id"]] = row["total"]
    end
  end

  def shopify_template_key
    unless (ts = shopify_template_suffix).blank?
      type = article? ? "article" : "page"
      "templates/#{type}.#{ts}.liquid"
    end
  end

  def shopify_template_suffix
    template_suffix = shopify_template.to_s.split(".").last
    template_suffix = "shogun" if template_suffix.blank?
    template_suffix
  end

  def article?
    external_blog_id.present?
  end

  def tracking_fields
    {
      name: name,
      path: path,
      version_number: version_number
    }
  end

  def published?
    published_version_id.present?
  end

  def outdated?
    published? && (published_version_id != current_version_id)
  end

  def draft?
    !published?
  end

  def site
    Site.find site_id
  end

  def version_history
    cache = {}
    versions.order(created_at: :desc).map do |v|
      user = cache[v.creator_id] ||= v.creator
      {
        id: v.id.to_s,
        created_at: v.created_at,
        creator: UserSerializer.new(user, root: false),
        current_version: v._id == current_version_id
      }
    end
  end

  private

  def path_not_already_in_use_externally
    if site.shopify?
      ser = site.shopify_shop.service
      pages = ser.pages rescue []
      check = self.path[1..-1].downcase
      pages.each do |p|
        if p["handle"].downcase == check && self.external_page_id.to_s != p["id"].to_s
          self.errors[:base] << "You already have a page with that handle on Shopify."
          return false
        end
      end
    end
  end

  def fix_path
    return if article?

    unless self.path.strip.to_s[0] == "/"
      p = self.path.to_s.strip
      self.path = "/#{p}"
    end
  end
end
