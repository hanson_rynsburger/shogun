class MongoTemplate
  include Persistable
  include MongoVersioned

  BASIC = "basic".freeze
  ORIGINAL_BASIC = "original_basic".freeze
  USER = "user".freeze
  TYPES = [BASIC, ORIGINAL_BASIC, USER].freeze

  field :name, type: String
  field :site_id, type: String
  field :type, type: String, default: "user"
  field :deleted_at, type: DateTime

  validates :type, inclusion: {in: TYPES}
  validates :name, presence: true, uniqueness: {scope: [:site_id, :deleted_at] }

  has_many :versions, class_name: "MongoTemplateVersion"
  belongs_to :current_version, class_name: "MongoTemplateVersion"
  accepts_nested_attributes_for :current_version

  delegate :liquid, :css, :js, :variables, to: :current_version

  after_update :trigger_update, if: -> { current_version_id_changed? }

  store_in collection: "templates"

  index({site_id: 1}, {background: true})

  def tracking_fields
    {
      name: name,
      version_number: version_number,
      type: type
    }
  end

  def site
    Site.find self.site_id
  end

  def trigger_update
    TemplateVersionChangedWorker.perform_async self._id.to_s, current_version_id_was.to_s, current_version_id.to_s
  end

  def styles
    {}
  end
end
