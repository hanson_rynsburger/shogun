# == Schema Information
#
# Table name: add_ons
#
#  id         :integer          not null, primary key
#  site_id    :string
#  name       :string
#  properties :hstore
#  active     :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_add_ons_on_site_id  (site_id)
#

class AddOn < ActiveRecord::Base
  belongs_to :site

  validates :name, presence: true, uniqueness: {scope: :site_id}
end
