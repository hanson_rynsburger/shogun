class MongoReference
  include Persistable

  field :position, type: String

  embedded_in :value, class_name: "MongoValue"
  embeds_one :component, class_name: "MongoComponent", as: :parent

  def position
    super.to_i
  end
end
