# == Schema Information
#
# Table name: sites
#
#  id              :uuid             not null, primary key
#  name            :string
#  url             :string
#  created_at      :datetime
#  updated_at      :datetime
#  secret_token    :string
#  old_plan        :string
#  default_card_id :uuid
#  version_number  :integer          default(0)
#  deleted_at      :datetime
#  editor_url      :string
#  preview_url     :string
#  features        :text
#  colors          :text             default([]), is an Array
#  digest          :string
#  demo            :boolean
#  type            :string
#  fonts           :text             default([]), is an Array
#
# Indexes
#
#  index_sites_on_deleted_at  (deleted_at)
#  index_sites_on_type        (type)
#

class Site < ActiveRecord::Base
  include Versioned

  attr_accessor :skip_templates

  validates :url, :name, presence: true
  validate :url_is_a_url

  has_many :collaborators, dependent: :destroy
  has_many :invites, dependent: :destroy
  has_many :cards, dependent: :destroy
  has_many :invoices
  has_one :default_card, class_name: "Card"
  has_one :shopify_shop
  has_one :plan
  has_many :users, through: :collaborators
  has_many :add_ons, dependent: :destroy
  has_one :discount

  accepts_nested_attributes_for :default_card
  store :features, accessors: [:templates, :collections, :integrations, :meta_tags]

  scope :shopify, -> { where(type: "ShopifySite") }
  scope :shogun, -> { where(type: "ShogunSite") }
  scope :with_plan, -> { where(id: Plan.select(:site_id)) }
  scope :without_plan, -> { where.not(id: Plan.select(:site_id)) }

  before_create :set_secret_token
  after_commit(on: :update) do
    SiteUpdatedWorker.perform_async id
  end

  def scrapes
    MongoScrape.where(site_id: self.id)
  end

  def shopify?
    type == "ShopifySite"
  end

  def shogun?
    type == "ShogunSite"
  end

  def tracking_fields
    {
      name: name,
      url: url,
      version_number: version_number
    }
  end

  def published_versions
    pages.published.includes(:published_version).map(&:published_version)
  end

  def updating_pages
    pages.where(updating: true)
  end

  def published_pages_count
    pages.published.count
  end

  def users_count
    cc = collaborators.loaded? ? collaborators.length : collaborators.count
    ic = invites.loaded? ? invites.length : invites.count
    cc + ic
  end

  def max_users
    plan.try(:max_users) || 2
  end

  def max_pages
    plan.try(:max_pages) || 5
  end

  def within_user_limit?
    max_users > users_count
  end

  def within_published_page_limit?
    max_pages > published_pages_count
  end

  def templates
    MongoTemplate.where(site_id: self.id)
  end

  def pages
    MongoPage.where(site_id: self.id, deleted_at: nil)
  end

  def models
    MongoModel.where(site_id: self.id)
  end

  def snippets
    MongoSnippet.where(site_id: self.id)
  end

  def digest
    super || Digest::SHA256.hexdigest("#{id}-#{secret_token}")
  end

  private

  def url_is_a_url
    res = URI.parse(url) rescue nil
    unless res.present? && (res.kind_of?(URI::HTTP) || res.kind_of?(URI::HTTPS))
      self.errors.add(:url, "must be a url")
    end
  end

  def set_secret_token
    self.secret_token ||= SecureRandom.urlsafe_base64
  end
end
