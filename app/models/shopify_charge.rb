# == Schema Information
#
# Table name: shopify_charges
#
#  id                :uuid             not null, primary key
#  shopify_shop_id   :uuid             not null
#  activated_at      :datetime
#  cancelled_at      :datetime
#  trial_ends_at     :datetime
#  billing_at        :datetime
#  shopify_charge_id :integer          not null
#  name              :string
#  price             :integer
#  status            :string
#  created_at        :datetime
#  updated_at        :datetime
#
# Indexes
#
#  index_shopify_charges_on_shopify_charge_id  (shopify_charge_id) UNIQUE
#  index_shopify_charges_on_shopify_shop_id    (shopify_shop_id)
#

class ShopifyCharge < ActiveRecord::Base
  belongs_to :shopify_shop

  validates :shopify_charge_id, :shopify_shop_id, presence: true

  scope :active, -> { where(status: "active") }

  def trial_over?
    return true if trial_ends_at.nil?
    trial_ends_at < Time.now
  end

  def activated?
    activated_at.present?
  end

  def cancelled?
    cancelled_at.present?
  end

  def converted?
    activated? && trial_over?
  end
end
