class MongoSnippet
  include Persistable
  include ComponentOwner

  field :site_id, type: String
  field :name, type: String

  validates :name, presence: true

  store_in collection: "snippets"

  index({site_id: 1}, {background: true})

  def site
    Site.find site_id
  end
end
