# == Schema Information
#
# Table name: shopify_shops
#
#  id                :uuid             not null, primary key
#  domain            :string
#  token             :string
#  site_id           :uuid
#  created_at        :datetime
#  updated_at        :datetime
#  outside_billing   :boolean          default(FALSE)
#  email             :string
#  shop_owner        :string
#  name              :string
#  active            :boolean          default(FALSE), not null
#  trialled_at       :datetime
#  shopify_plan_name :string
#  dead              :boolean          default(FALSE), not null
#  uninstalled_at    :datetime
#  time_zone         :string
#  javascript        :boolean          default(FALSE)
#  scrape            :boolean          default(TRUE)
#  trial_days        :integer          default(7)
#
# Indexes
#
#  index_shopify_shops_on_domain   (domain) UNIQUE
#  index_shopify_shops_on_site_id  (site_id) UNIQUE
#

class ShopifyShop < ActiveRecord::Base
  ORDER_HOOKS = ["orders/create", "orders/delete", "orders/updated", "orders/paid", "orders/cancelled", "orders/fulfilled", "orders/partially_fulfilled"].freeze
  PRODUCT_HOOKS = ["products/create", "products/update", "products/delete"].freeze
  APP_HOOKS = ["app/uninstalled"].freeze
  SHOP_HOOKS = ["shop/update"].freeze
  HOOKS = (ORDER_HOOKS + PRODUCT_HOOKS + APP_HOOKS + SHOP_HOOKS).freeze

  belongs_to :site
  has_many :shopify_charges
  has_one :active_shopify_charge, -> { active }, class_name: 'ShopifyCharge'

  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false) }
  scope :dead, -> { where(dead: true) }
  scope :not_dead, -> { where(dead: false) }
  scope :with_plan, -> { where(site_id: Site.with_plan.select(:id)) }
  scope :without_plan, -> { where(site_id: Site.without_plan.select(:id)) }
  scope :in_trial, -> { joins(:active_shopify_charge).where('trial_ends_at > ?', Time.zone.now) }
  scope :out_of_trial, -> { joins(:active_shopify_charge).where('trial_ends_at < ?', Time.zone.now) }

  after_commit(on: :create) do
    Shopify::ShopCreatedWorker.perform_async self.id
    Shopify::InstalledWorker.perform_async self.id
  end

  def self.[](s)
    ShopifyShop.where(domain: "#{s}.myshopify.com").take
  end

  def name
    domain.split(".").first
  end

  def service
    ShopifyService.new(self)
  end

  def sync_shopify_data!
    data = service.get_shop_data
    self.email = data["email"]
    self.shop_owner = data["shop_owner"]
    self.name = data["name"]
    self.shopify_plan_name = data["plan_name"]
    self.time_zone = data["iana_timezone"]
    sync_shopify_charges!
  end

  def import_shopify_charge!(chg)
    sc = self.shopify_charges.find_or_initialize_by shopify_charge_id: chg["id"].to_i
    Time.use_zone self.time_zone do
      sc.activated_at = parse_shopify_date(chg["activated_on"])
      sc.cancelled_at = parse_shopify_date(chg["cancelled_on"])
      sc.billing_at = parse_shopify_date(chg["billing_on"])
      sc.trial_ends_at = parse_shopify_date(chg["trial_ends_on"])
    end
    sc.name = chg["name"]
    sc.price = chg["price"]
    sc.status = chg["status"]
    sc.save!
  end

  def sync_shopify_charges!
    charges = service.recurring_charges
    ActiveRecord::Base.transaction do
      charges.each do |chg|
        import_shopify_charge! chg
      end
    end
  end

  def check_dead!
    resp = Curl::Easy.get("https://#{domain}")
    if resp.response_code == 402 || resp.response_code == 404
      self.dead = true
    elsif dead?
      self.dead = false
      begin
        self.sync_shopify_data!
        self.active = true
      rescue
        self.active = false
      end
    end
    save if changed?
  end

  def check_active!
    begin
      service.get_shop_data
      self.active = true
    rescue
      self.active = false
    end
    save if changed?
  end

  def billable?
    !["trial", "affiliate"].include?(shopify_plan_name)
  end

  private

  def parse_shopify_date(date)
    if date.present?
      Time.zone.parse(date)
    end
  end
end
