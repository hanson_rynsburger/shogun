class MongoScrape
  include Persistable

  field :html, type: String
  field :site_id, type: String
  field :default, type: Boolean

  index({site_id: 1})
  index({created_at: 1}, {background: true})
  index({default: 1})

  def site
    Site.find self.site_id
  end
end
