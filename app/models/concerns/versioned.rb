module Versioned
  extend ActiveSupport::Concern

  included do
    before_save :increment_version_number
  end

  def increment_version_number
    self.version_number += 1
  end
end
