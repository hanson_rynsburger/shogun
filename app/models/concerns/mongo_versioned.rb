module MongoVersioned
  extend ActiveSupport::Concern

  included do
    field :version_number, type: Integer, default: 0
    before_save :increment_version_number
  end

  def increment_version_number
    self.version_number += 1
  end
end
