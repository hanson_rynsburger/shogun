module ComponentOwner
  extend ActiveSupport::Concern

  included do
    before_create :expand_components
    embeds_one :root, class_name: "MongoComponent", as: :parent
    # store_as:
  end

  def expand_components
    templates = site.templates.includes(:current_version)
    versions = templates.map { |t| t.current_version }
    cache = versions.compact.index_by(&:_id)
    TemplateService.standard_templates(site).each do |st|
      cache[st.id] = st
    end
    each_component do |comp|
      comp.expand_from_template_version cache
    end
  end

  def each_component(&blk)
    ComponentService.each_in_tree(root, &blk)
  end

  def raw_html
    "#{add_ons_js}\n#{render_css}\n#{render_html}\n#{render_js}"
  end

  def render_html
    ComponentRenderer.new(root).render
  end

  def render_css
    ComponentService.render_css root
  end

  def render_js
    ComponentService.render_js root
  end

  def first_component
    root.values.first.references.first.component
  end
end
