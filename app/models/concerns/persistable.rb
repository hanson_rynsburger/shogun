module Persistable
  extend ActiveSupport::Concern

  included do
    include Mongoid::Document
    include Mongoid::Timestamps
    # include Mongoid::Timestamps::Short
  end
end
