class MongoModel
  include Mongoid::Document
  include Mongoid::Timestamps
  include MongoVersioned

  field :name, type: String
  field :properties, type: Hash, default: {}
  field :site_id, type: String
  field :external, type: Boolean, default: false
  field :mutable, type: Boolean, default: true

  has_many :entries, class_name: "MongoModelEntry"
  accepts_nested_attributes_for :entries

  validates :name, presence: true

  index({site_id: 1}, {background: 1})

  store_in collection: "models"

  after_update :flush_changes_to_pages!

  def flush_changes_to_pages!
    FlushModelChangesWorker.perform_async self._id.to_s
  end

  def site
    Site.find site_id
  end
end
