# == Schema Information
#
# Table name: users
#
#  id              :uuid             not null, primary key
#  email           :string
#  password_digest :string
#  created_at      :datetime
#  updated_at      :datetime
#  token           :string
#  name            :string
#  provider        :string
#  uid             :string
#  pusher_channel  :string
#  first_name      :string
#  last_name       :string
#  company_name    :string
#  heroku          :boolean          default(FALSE)
#  admin           :boolean          default(FALSE)
#  shopify         :boolean
#
# Indexes
#
#  index_users_on_email             (email) UNIQUE
#  index_users_on_provider_and_uid  (provider,uid) UNIQUE
#

class User < ActiveRecord::Base
  has_many :collaborators, dependent: :destroy
  has_many :sites, through: :collaborators
  has_many :shopify_shops, through: :sites

  scope :shopify, -> { where(shopify: true) }

  validates :email, email: true, uniqueness: true, presence: true
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :company_name, presence: true

  has_secure_password

  before_create :set_token
  before_create :set_pusher_channel
  before_validation :downcase_email, if: -> { email_changed? }

  with_options unless: :shopify? do |o|
    o.after_create :track_sign_up
    o.after_create :setup_collaborators
    o.after_create :add_to_mailchimp
  end

  def self.find_for_provider_and_uid(provider, uid)
    where(provider: provider, uid: uid).take
  end

  def push!(event_name, payload)
    PushWorker.perform_async pusher_channel, event_name, payload
  end

  def tracking_fields
    {
      first_name: first_name,
      last_name: last_name,
      company_name: company_name,
      email: email
    }
  end

  def name
    "#{first_name} #{last_name}"
  end

  private

  def downcase_email
    self.email = self.email.to_s.downcase
  end

  def set_pusher_channel
    self.pusher_channel = SecureRandom.uuid
  end

  def set_token
    self.token = SecureRandom.urlsafe_base64(32)
  end

  def track_sign_up
    method = provider || "regular"
    Analytics.identify(user_id: id, traits: tracking_fields)
    Analytics.track(user_id: id, event: "Signed Up", properties: {method: method})
  end

  def setup_collaborators
    invites = Invite.where(email: self.email)
    invites.each do |i|
      ActiveRecord::Base.transaction do
        Analytics.track(user_id: id, event: "Accepted Invite")
        Collaborator.create site_id: i.site_id, user_id: self.id
        i.update accepted_at: Time.now
      end
    end
  end

  def add_to_mailchimp
    MailchimpWorker.perform_async id
  end
end
