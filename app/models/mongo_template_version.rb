class MongoTemplateVersion
  include Persistable

  field :liquid, type: String
  field :css, type: String
  field :js, type: String
  field :creator_id, type: String

  embeds_many :variables, class_name: "MongoVariable"
  accepts_nested_attributes_for :variables
  belongs_to :template, class_name: "MongoTemplate"

  store_in collection: "template_versions"

  index({"variables.model_id" => 1}, {background: true})
end
