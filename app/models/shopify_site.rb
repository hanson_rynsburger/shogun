# == Schema Information
#
# Table name: sites
#
#  id              :uuid             not null, primary key
#  name            :string
#  url             :string
#  created_at      :datetime
#  updated_at      :datetime
#  secret_token    :string
#  old_plan        :string
#  default_card_id :uuid
#  version_number  :integer          default(0)
#  deleted_at      :datetime
#  editor_url      :string
#  preview_url     :string
#  features        :text
#  colors          :text             default([]), is an Array
#  digest          :string
#  demo            :boolean
#  type            :string
#  fonts           :text             default([]), is an Array
#
# Indexes
#
#  index_sites_on_deleted_at  (deleted_at)
#  index_sites_on_type        (type)
#

class ShopifySite < Site
  def delete_page!(page)
    if epid = page.external_page_id
      service = shopify_shop.service
      if page.article?
        service.delete_article page.external_blog_id, epid
      else
        service.delete_page epid
      end
    end
  rescue ShopifyService::ShopifyNotFoundError
  end

  def publish_page_version!(page, page_version)
    service = shopify_shop.service

    params = {
      body_html: page_version.raw_html,
      metafields: to_metafields(page.meta_tags),
      title: page.name,
      published: true,
      template_suffix: page.shopify_template_suffix
    }

    if page.article?
      params.merge!(tags: page.tags, author: page.author, handle: page.path.split("/").last)
    else
      params.merge!(handle: page.path.split("/").last)
    end

    begin
      if page.external_page_id.present?
        if page.article?
          service.update_article page.external_blog_id, page.external_page_id, params
        else
          service.update_page page.external_page_id, params
        end
        page.update! published_version_id: page_version._id, updating: false, external_errors: []
      else
        if page.article?
          sp = service.create_article page.external_blog_id, params
        else
          sp = service.create_page params
        end
        page.update! published_version_id: page_version._id, updating: false, external_errors: [], external_page_id: sp["id"]
      end
    rescue ShopifyService::ShopifyError => e
      page.update! updating: false, external_errors: e.errors
    rescue ShopifyService::ShopifyNotFoundError => e
      page.update! external_page_id: nil
      publish_page_version! page, page_version
    end
  end

  def unpublish_page!(page)
    if epid = page.external_page_id
      if page.article?
        shopify_shop.service.update_article page.external_blog_id, epid, {published: false}
      else
        shopify_shop.service.update_page epid, {published: false}
      end
    end
    page.update! updating: false, external_errors: []
  end

  def refresh_page_version!(page, page_version)
    # only need to refresh if this is the currently published version
    if page_version._id == page.published_version_id
      service = shopify_shop.service
      if page.article?
        service.update_article page.external_blog_id, page.external_page_id, {body_html: page_version.raw_html}
      else
        service.update_page page.external_page_id, {body_html: page_version.raw_html}
      end
    end
  end

  private

  def to_metafields(meta_tags)
    meta_tags.to_a.map do |mt|
      if mt['property'] == 'title' || mt['property'] == 'description'
        key = mt['property'] + '_tag'
      else
        key = mt['property']
      end

      {
        key: key,
        value: mt['value'],
        value_type: 'string',
        namespace: 'global'
      }
    end
  end
end
