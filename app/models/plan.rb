# == Schema Information
#
# Table name: plans
#
#  id             :uuid             not null, primary key
#  site_id        :uuid
#  internal_notes :text
#  price          :integer
#  max_users      :integer
#  price_per_user :integer
#  max_pages      :integer
#  price_per_page :integer
#  activated_at   :datetime
#  deactivated_at :datetime
#  created_at     :datetime
#  updated_at     :datetime
#  name           :string
#
# Indexes
#
#  index_plans_on_site_id  (site_id)
#

class Plan < ActiveRecord::Base
  belongs_to :site
  has_many :invoices

  validates :price, :price_per_page, :price_per_user, :max_users, :max_pages, numericality: {greater_than: 0}, presence: true

  def activated?
    activated_at.present?
  end

  def deactivated?
    deactivated_at.present?
  end

  def activate!
    update! activated_at: Time.now
  end
end
