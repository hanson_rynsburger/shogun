class MongoPageVersion
  include Persistable
  include ComponentOwner

  field :uploaded_at, type: DateTime
  field :template_versions, type: Array
  field :uuid, type: String
  field :creator_id, type: String
  field :stale, type: Boolean, default: false

  belongs_to :page, class_name: "MongoPage", inverse_of: :versions

  store_in collection: "page_versions"

  before_create :store_template_versions
  before_create :set_uuid
  before_create :fix_if_broken!

  index({template_versions: 1}, {background: true})
  index({page_id: 1}, {background: true})
  index({uuid: 1}, {unique: true, background: true})
  index({created_at: -1}, {background: true})

  def fix!
    st = TemplateService.standard_templates.map { |t| t["id"] }

    each_component do |comp|
      if comp.template_id == "button" && comp.template_version_id != "button"
        if st.include?(comp.template_version_id)
          comp.template_id = comp.template_version_id
        else
          mtv = MongoTemplateVersion.find comp.template_version_id
          comp.template_id = mtv.template_id
        end
      end
    end
  end

  def broken?
    b = false
    each_component { |c| b = true if c.template_id == "button" && c.template_version_id != "button" }
    b
  end

  def site
    page.site
  end

  def creator
    if creator_id.present?
      User.find(creator_id)
    else
      page.site.users.first
    end
  end

  def add_ons_js
    AddOnService.render_js(self)
  end

  def uploaded?
    uploaded_at.present?
  end

  def store_template_versions
    tv = []
    each_component do |comp|
      tv << comp.template_version_id unless comp.template_version_id.blank?
    end
    self.template_versions = tv.uniq
  end

  def publish!
    PublishPageVersionWorker.perform_async self._id.to_s
  end

  def record_key
    "page_versions/#{uuid}"
  end

  private

  def fix_if_broken!
    if broken?
      Rails.logger.info "fixing broken page version #{_id.to_s}"
      fix!
    end
  end

  def set_uuid
    self.uuid ||= SecureRandom.uuid
  end
end
