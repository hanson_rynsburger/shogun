# == Schema Information
#
# Table name: invites
#
#  id          :uuid             not null, primary key
#  email       :string
#  user_id     :uuid
#  site_id     :uuid
#  accepted_at :datetime
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_invites_on_site_id  (site_id)
#

class Invite < ActiveRecord::Base
  belongs_to :user
  belongs_to :site

  scope :pending, -> { where(accepted_at: nil) }

  validates :email, email: true
end
