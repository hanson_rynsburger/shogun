class MongoModelEntry
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Attributes::Dynamic

  field :position, type: Integer

  belongs_to :model, class_name: "MongoModel"

  after_create :flush_changes_to_pages!
  after_update :flush_changes_to_pages!
  after_upsert :flush_changes_to_pages!

  index({model_id:1, position: 1}, {background: true})

  store_in collection: "entries"

  def flush_changes_to_pages!
    FlushModelChangesWorker.perform_async self.model_id.to_s
  end
end
