# == Schema Information
#
# Table name: discounts
#
#  id         :integer          not null, primary key
#  amount     :integer
#  site_id    :uuid             not null
#  plan_name  :string
#  used_at    :datetime
#  expires_at :datetime
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_discounts_on_site_id  (site_id)
#

class Discount < ActiveRecord::Base
  belongs_to :site

  validates :amount, numericality: { greater_than: 0 }
  validates :plan_name, inclusion: { in: Settings.shopify_plans.keys }

  def used?
    used_at.present?
  end

  def expired?
    expires_at.present? && expires_at.past?
  end

  def usable?
    !used? && !expired?
  end
end
