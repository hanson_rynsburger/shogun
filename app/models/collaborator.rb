# == Schema Information
#
# Table name: collaborators
#
#  id         :uuid             not null, primary key
#  site_id    :uuid
#  user_id    :uuid
#  admin      :boolean
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_collaborators_on_site_id_and_user_id  (site_id,user_id) UNIQUE
#

class Collaborator < ActiveRecord::Base
  belongs_to :site
  belongs_to :user
end
