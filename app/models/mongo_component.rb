class MongoComponent
  include Persistable

  attr_accessor :skip_expansion

  SCREEN_SIZES = ["lg", "md", "sm", "xs"].freeze
  ABSOLUTE_OVERRIDES = SCREEN_SIZES.flat_map { |s| ["position_#{s}", "top_#{s}", "left_#{s}"] }.freeze
  DISPLAY_OVERRIDES = SCREEN_SIZES.map { |s| "display_#{s}" }.freeze

  DIMENSIONS = ["top", "left", "bottom", "right"].freeze

  MARGINS = DIMENSIONS.map { |d| "margin_#{d}" }.freeze
  PADDINGS = DIMENSIONS.map { |d| "padding_#{d}" }.freeze
  BORDERS = DIMENSIONS.map { |d| "border_#{d}_width" }.freeze
  SHADOWS = ["box_shadow_horizontal", "box_shadow_vertical", "box_shadow_blur", "box_shadow_spread", "box_shadow_color"].freeze
  SIZES = (MARGINS + PADDINGS + BORDERS + ["border_radius", "width", "height"]).freeze
  OTHER = ["position", "z_index", "opacity", "background_image", "text_align", "background_size", "background_position", "border_color", "background_color"].freeze

  FONT = ["font_size", "font_family", "color", "text_transform", "text_decoration"].freeze
  STATES = ["hover", "active"].freeze

  STYLES = (DIMENSIONS + SHADOWS + SIZES + OTHER + ABSOLUTE_OVERRIDES + DISPLAY_OVERRIDES + FONT + STATES).freeze

  field :uuid, type: String
  field :liquid, type: String
  field :css, type: String
  field :js, type: String
  field :styles, type: Hash
  field :data_attributes, type: Hash
  field :tn, as: :template_name, type: String
  field :css_classes, type: String

  belongs_to :template, class_name: "MongoTemplate"
  belongs_to :template_version, class_name: "MongoTemplateVersion"

  embeds_many :values, class_name: "MongoValue"
  embedded_in :parent, polymorphic: true

  def standard_templates
    @standard_templates ||= TemplateService.standard_templates
  end

  def custom?
    template_id.nil? || standard_templates.none? { |t| t["id"] == template_id }
  end

  def liquid
    if custom?
      super
    else
      standard_templates.detect { |t| t["id"] == template_id }["liquid"]
    end
  end

  def css
    if custom?
      super
    else
      standard_templates.detect { |t| t["id"] == template_id }["css"]
    end
  end

  def js
    if custom?
      super
    else
      standard_templates.detect { |t| t["id"] == template_id }["js"]
    end
  end

  def expand_from_template_version(cache={})
    # when copying components, don't re-expand
    return if self.skip_expansion

    tv = nil

    if custom?
      tv = cache[template_version_id] || self.template_version
    else
      tv = cache[template_id]
    end

    return unless tv.present?
    self.liquid = tv.liquid
    self.css = tv.css
    self.js = tv.js
    values_with_variable = self.values.select { |v| v.variable_id.present? }
    return if values_with_variable.blank?
    values_by_variable_id = values_with_variable.index_by(&:variable_id)
    tv.variables.each do |variable|
      value = values_by_variable_id[variable.id]
      next unless value.present?
      value.variable_name = variable.name
      value.variable_type = variable.type
      value.variable_input = variable.input
      value.variable_properties = variable.properties
      value.variable_data_source = variable.data_source
      value.variable_model_id = variable.model_id
    end
  end
end
