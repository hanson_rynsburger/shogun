require "rails_helper"

describe ApiService do
  let(:service) { ApiService }
  let(:site) { double("Site", id: "id", secret_token: "token", record_key: "record_key") }

  describe "reset!" do
    it "uploads the json for the published versions and purges fastly" do
      expect(site).to receive(:published_versions).and_return([1,2,3])
      expect(service).to receive(:upload_json!).with(site, [1,2,3]).ordered
      expect(service).to receive(:purge_fastly!).with(site).ordered
      service.reset! site
    end
  end

  describe "upload_json!" do
    it "uploads a json route description to S3" do
      s3 = double("S3")
      expect(Aws::S3::Object).to receive(:new).with(bucket_name: nil, key: "id-token.json").and_return(s3)
      versions = [double(page: double(path: "/path", meta_tags: []), uuid: "uuid", languages: [])]
      json = "[{\"path\":\"/path\",\"uuid\":\"uuid\",\"languages\":[],\"meta_tags\":[]}]"
      expect(s3).to receive(:put).with(ApiService::S3_DEFAULTS.merge(content_type: "application/json", body: json, metadata: {"surrogate-key" => "record_key"}))
      service.upload_json! site, versions
    end
  end
end
