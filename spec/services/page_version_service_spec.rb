require "rails_helper"

describe PageVersionService do
  let(:service) { PageVersionService }
  let(:page) { double("Page") }
  let(:page_version) { double("PageVersion", page: page, _id: "pvid", uuid: "uuid", record_key: "record_key", raw_html: "raw_html", languages: []) }

  describe "upload!" do
    context "uploads the html, sets uploaded_at and updates the published_version_id of the page" do
      before do
        expect(service).to receive(:upload_html!).with(page_version, "raw_html", "#{page_version.uuid}.html")
        expect(page_version).to receive(:set).with(uploaded_at: an_instance_of(Time), stale: false)
      end

      after do
        service.upload! page_version
      end

      it "when not uploaded" do
        expect(page_version).to receive(:uploaded?).and_return(false)
      end

      it "when stale" do
        expect(page_version).to receive(:uploaded?).and_return(true)
        expect(page_version).to receive(:stale?).and_return(true)
      end
    end

    it "skips the upload if the version has previously been uploaded and it is not stale" do
      expect(page_version).to receive(:uploaded?).and_return(true)
      expect(page_version).to receive(:stale?).and_return(false)
      expect(page_version).not_to receive(:touch).with(:uploaded_at)
      expect(service).not_to receive(:upload_html!).with(page_version, "raw_html", "#{page_version.uuid}.html")
      service.upload! page_version
    end
  end

  describe "upload_html!" do
    it "uploads the html of the page_version" do
      s3 = double("S3")
      expect(Aws::S3::Object).to receive(:new).with(bucket_name: nil, key: "uuid.html").and_return(s3)
      expect(s3).to receive(:put).with(PageVersionService::S3_DEFAULTS.merge(content_type: "text/html", body: "raw_html", metadata: {"surrogate-key" => "record_key"}))
      service.upload_html! page_version, "raw_html", "uuid.html"
    end
  end
end
