require "rails_helper"

describe PageService do
  let(:service) { PageService }
  let(:page) { double("Page", site_id: "site_id", _id: "pid") }
  let(:page_version) { double("PageVersion", _id: "pvid") }
  let(:site) { double("Site") }
  let(:current_user) { double("User") }

  describe "update" do
    it "builds and saves a new current_version" do
      expect(service).to receive(:build_version).with(page, {foo: 1}, current_user).and_return(page_version)
      expect(page_version).to receive(:save).and_return(true)
      expect(page_version).to receive(:valid?).and_return(true)
      expect(page).to receive(:valid?).and_return(true)
      expect(page).to receive(:current_version_id=).with("pvid")
      expect(page).to receive(:save!)
      service.update page, {page: {foo: 1}}, current_user
    end
  end

  describe "create" do
    it "builds a new page and sets its current_version" do
      expect(site).to receive(:pages).and_return(double(build: page))
      expect(service).to receive(:build_version).with(page, {foo: 1}, current_user).and_return(page_version)
      expect(page_version).to receive(:save).and_return(true)
      expect(page_version).to receive(:valid?).and_return(true)
      expect(page).to receive(:valid?).and_return(true)
      expect(page).to receive(:current_version=).with(page_version)
      expect(page).to receive(:save!)
      expect(service.create(site, {page: {foo: 1}}, current_user)).to eq(page)
    end
  end

  describe "publish!" do
    it "sets the page to be updating and publishes the current_version" do
      expect(page).to receive(:update!).with(updating: true)
      expect(page).to receive(:current_version).and_return(page_version)
      expect(page_version).to receive(:publish!)
      service.publish! page
    end
  end

  describe "unpublish!" do
    it "sets the page to have no published_version_id and starts the UnpublishPageWorker" do
      expect(page).to receive(:update!).with(published_version_id: nil)
      expect(UnpublishPageWorker).to receive(:perform_async).with("pid")
      service.unpublish! page
    end
  end
end
