FactoryGirl.define do
  factory :user do
    password { SecureRandom.uuid }
    email { FFaker::Internet.email }
    name { FFaker::Name.name }
    first_name { FFaker::Name.name }
    last_name { FFaker::Name.name }
    company_name { FFaker::Name.name }
    token { SecureRandom.urlsafe_base64 }

    factory :user_with_site do
      sites { [create(:site)] }
    end
  end
end
