FactoryGirl.define do
  factory :invite do
    email { FFaker::Internet.email }
  end
end
