FactoryGirl.define do
  factory :plan do
    name { FFaker::Company.name }
    price 100
    max_users 2
    max_pages 5
    price_per_page 100
    price_per_user 100
  end
end
