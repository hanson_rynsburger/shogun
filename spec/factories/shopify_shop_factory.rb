FactoryGirl.define do
  factory :shopify_shop do
    domain { "#{FFaker::Internet.slug}.myshopify.com" }
    email { FFaker::Internet.email }
    shop_owner { FFaker::Name.name }
    name { FFaker::Company.name }
    token { SecureRandom.uuid }
  end
end
