FactoryGirl.define do
  factory :component, class: MongoComponent do
    values { [build(:value)] }
  end
end
