FactoryGirl.define do
  factory :page, class: MongoPage do
    name { FFaker::Company.name }
    path { FFaker::Internet.slug }
    site_id { create(:site_with_user).id }
    after(:build) do |page|
      version = page.versions.build attributes_for(:page_version)
      page.current_version = version
      version.save
    end
    factory :published_page do
      after(:build) do |page|
        page.published_version = page.current_version
      end
    end
  end
end
