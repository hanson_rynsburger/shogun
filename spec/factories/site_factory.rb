FactoryGirl.define do
  factory :site, class: ShogunSite do
    name { FFaker::Company.name }
    url { FFaker::Internet.http_url }
    factory :site_with_user do
      users { [create(:user)] }
    end

    factory :shopify_site, class: ShopifySite do
      shopify_shop
    end
    type "ShogunSite"
  end
end
