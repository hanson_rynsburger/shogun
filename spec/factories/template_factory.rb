FactoryGirl.define do
  factory :template, class: MongoTemplate do
    name { FFaker::BaconIpsum.word }
    after(:build) do |template|
      template.current_version = template.versions.build attributes_for(:template_version)
      template.current_version.save
    end
    site_id { create(:site_with_user).id }
  end
end
