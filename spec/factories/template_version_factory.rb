FactoryGirl.define do
  factory :template_version, class: MongoTemplateVersion do
    liquid "<h1>Hello, World</h1>"
    css "h1 { color: red; }"
    js "alert('yo');"
  end
end
