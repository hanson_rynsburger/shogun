FactoryGirl.define do
  factory :page_version, class: MongoPageVersion do
    root { build(:component) }
  end
end
