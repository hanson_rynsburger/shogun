require "rails_helper"

describe PublishPageVersionWorker do
  let(:worker) { PublishPageVersionWorker }
  let(:page) { double("Page", site: site) }
  let(:page_version) { double("PageVersion", _id: "pvid", page: page) }
  let(:site) { double("Site") }

  it "publishes the page version through the site" do
    expect(MongoPageVersion).to receive(:find).with("pvid").and_return(page_version)
    expect(site).to receive(:publish_page_version!).with(page, page_version)
    worker.new.perform "pvid"
  end
end
