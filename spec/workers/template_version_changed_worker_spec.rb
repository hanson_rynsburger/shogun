require "rails_helper"

describe TemplateVersionChangedWorker do
  let(:worker) { TemplateVersionChangedWorker }
  let(:template) { double("Template") }

  it "finds the template and calls the TemplateService" do
    expect(MongoTemplate).to receive(:find).with("tid").and_return(template)
    expect(TemplateService).to receive(:version_changed).with(template, "oid", "cid")
    worker.new.perform "tid", "oid", "cid"
  end
end
