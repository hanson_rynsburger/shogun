require "rails_helper"

describe ResetApiWorker do
  let(:worker) { ResetApiWorker }
  let(:site) { double("Site") }

  it "resets the api and clears the updating pages" do
    expect(Site).to receive(:find).with("sid").and_return(site)
    expect(ApiService).to receive(:reset!).with(site)
    expect(site).to receive(:save!)
    p1 = double("p1")
    p2 = double("p2")
    p3 = double("p3")
    expect(p1).to receive(:update!).with(updating: false)
    expect(p2).to receive(:update!).with(updating: false)
    expect(p3).to receive(:update!).with(updating: false)
    expect(site).to receive(:updating_pages).and_return([p1, p2, p3])
    worker.new.perform "sid"
  end
end
