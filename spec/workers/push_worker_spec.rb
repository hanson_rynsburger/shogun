require "rails_helper"

describe PushWorker do
  let(:worker) { PushWorker }

  it "triggers a pusher event on the channel with the payload" do
    expect(Pusher).to receive(:trigger).with("channel", "event_name", "payload")
    worker.new.perform "channel", "event_name", "payload"
  end
end
