require "rails_helper"

describe PageUpdatedWorker do
  let(:worker) { PageUpdatedWorker }
  let(:page) { double("Page") }

  it "finds and serializes the page, then pushes out the payload to each user of the site" do
    rel = double("ActiveRecord::Relation")
    expect(MongoPage).to receive(:where).with(deleted_at: nil).and_return(rel)
    expect(rel).to receive(:find).with("pid").and_return(page)
    expect(PageSerializer).to receive(:new).with(page, root: false).and_return(double(to_json: "json"))
    site = double("Site")

    u1 = double("u1")
    u2 = double("u2")
    u3 = double("u3")
    expect(u1).to receive(:push!).with("page:updated", "json")
    expect(u2).to receive(:push!).with("page:updated", "json")
    expect(u3).to receive(:push!).with("page:updated", "json")
    expect(site).to receive(:users).and_return([u1, u2, u3])
    expect(page).to receive(:site).and_return(site)
    worker.new.perform "pid"
  end
end
