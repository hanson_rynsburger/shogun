require "rails_helper"

describe SiteUpdatedWorker do
  let(:worker) { SiteUpdatedWorker }
  let(:site) { double("Site") }

  it "finds and serializes the site, then pushes the payload to each user" do
    expect(Site).to receive(:find).with("sid").and_return(site)
    u1 = double("u1")
    u2 = double("u2")
    u3 = double("u3")
    expect(SiteSerializer).to receive(:new).with(site, root: false, scope: u1).and_return(double(to_json: "json"))
    expect(SiteSerializer).to receive(:new).with(site, root: false, scope: u2).and_return(double(to_json: "json"))
    expect(SiteSerializer).to receive(:new).with(site, root: false, scope: u3).and_return(double(to_json: "json"))
    expect(u1).to receive(:push!).with("site:updated", "json")
    expect(u2).to receive(:push!).with("site:updated", "json")
    expect(u3).to receive(:push!).with("site:updated", "json")
    expect(site).to receive(:users).and_return([u1, u2, u3])
    worker.new.perform "sid"
  end
end
