module ShogunApiHelper
  def skip_auth!
    controller.class.skip_before_filter :auth!
  end

  def current_user
    @current_user ||= FactoryGirl.create :user_with_site
  end

  def sign_in!(user)
    @current_user = user
  end

  def auth!
    request.env["warden"] = double(user: current_user, authenticate!: true)
  end

  def authed_post(path, params={})
    auth!
    post path, params
  end

  def authed_put(path, params={})
    auth!
    put path, params
  end

  def authed_get(path, params={})
    auth!
    get path, params
  end

  def authed_delete(path, params={})
    auth!
    delete path, params
  end
end
