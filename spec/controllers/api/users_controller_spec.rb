require "rails_helper"

describe Api::UsersController do
  # describe "POST create" do
  #   before :each do
  #     @site = create :site, users: [current_user]
  #     @invite = create :invite, email: 'test@example.com', user_id: current_user.id, site_id: @site.id
  #     @user = create :user, email: 'test@example.com'
  #   end

  #   it "creates users" do
  #     post :create, user: {name: FFaker::Name.name, email: FFaker::Internet.email, password: "foo", password_confirmation: "foo"}
  #     expect(response).to be_success
  #     json = JSON.parse response.body
  #     expect(json["user"]).to be_present
  #   end

  #   it "creates Collaborators from Invites" do
  #     expect(Collaborator.find_by_user_id @user.id).not_to be_nil
  #   end

  #   it "marks Invites with accepted_at timestamp" do
  #     invite = Invite.find_by_email @invite.email
  #     expect(invite.accepted_at).not_to be_nil
  #   end
  # end

  describe "GET show" do
    it "shows the current user" do
      authed_get :show
      expect(response).to be_success
      json = JSON.parse response.body
      expect(json["user"]["id"]).to eq(current_user.id)
    end
  end

  describe "PUT update" do
    it "updates the current user" do
      name = FFaker::Name.name
      authed_put :update, user: {first_name: name}
      expect(response).to be_success
      current_user.reload
      expect(current_user.first_name).to eq(name)
    end
  end
end
