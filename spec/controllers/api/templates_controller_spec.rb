require "rails_helper"

describe Api::TemplatesController do
  describe "GET show" do
    it "shows templates" do
      template = create :template
      sign_in! template.site.users.first
      authed_get :show, site_id: template.site_id, id: template.id
      expect(response).to be_success
      json = JSON.parse response.body
      expect(json["template"]["id"]).to eq(template.id.to_s)
    end
  end

  describe "GET index" do
    it "shows all templates for the site" do
      p1 = create :template
      p2 = create :template, site_id: p1.site_id
      sign_in! p1.site.users.first
      authed_get :index, site_id: p1.site_id
      expect(response).to be_success
      json = JSON.parse response.body
      expect(json["templates"].count).to eq(2)
    end
  end

  describe "POST create" do
    it "creates templates" do
      site = current_user.sites.first
      name = FFaker::Company.name

      authed_post :create, site_id: site.id, template: { name: name, current_version_attributes: attributes_for(:template_version) }
      expect(response).to be_success
      expect(MongoTemplate.count).to eq(1)
      expect(MongoTemplate.first.name).to eq(name)
    end
  end

  describe "PUT update" do
    it "updates templates" do
      template = create :template
      sign_in! template.site.users.first
      name = FFaker::HipsterIpsum.word

      authed_put :update, site_id: template.site_id, id: template.id, template: { name: name, current_version_attributes: { liquid: "foo" } }
      expect(response).to be_success
      expect(MongoTemplate.first.name).to eq(name)
    end
  end

  describe "DELETE destroy" do
    it "destroys templates" do
      template = create :template
      sign_in! template.site.users.first
      authed_delete :destroy, site_id: template.site_id, id: template.id
      expect(response).to be_success
      expect(MongoTemplate.where(deleted_at: nil).count).to be_zero
    end
  end

  describe "POST parse" do
    it "parses the variables from the liquid" do
      expect(TemplateService).to receive(:parse_variables).with("liquid").and_return(["parsed"])
      template = create :template
      sign_in! template.site.users.first
      authed_post :parse, site_id: template.site_id, liquid: "liquid"
      expect(response).to be_success
      json = JSON.parse response.body
      expect(json["variables"].count).to eq(1)
      expect(json["variables"].first).to eq("parsed")
    end
  end
end
