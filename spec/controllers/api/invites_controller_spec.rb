require "rails_helper"

describe Api::InvitesController do
  describe "DELETE destroy" do
    it "destroys invites" do
      s = current_user.sites.first
      i = s.invites.create email: FFaker::Internet.email
      expect(s.invites.count).to eq(1)
      authed_delete :destroy, site_id: s.id, id: i.id
      expect(response).to be_success
      expect(s.invites.count).to be_zero
    end
  end

  describe "GET index" do
    it "shows all invites for the given site" do
      s = current_user.sites.first
      s.invites.create email: FFaker::Internet.email
      s.invites.create email: FFaker::Internet.email
      s.invites.create email: FFaker::Internet.email
      authed_get :index, site_id: s.id
      expect(response).to be_success
      json = JSON.parse response.body
      expect(json["invites"].count).to eq(3)
    end
  end
end
