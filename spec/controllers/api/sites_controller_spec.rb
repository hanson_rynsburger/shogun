require "rails_helper"

describe Api::SitesController do
  describe "POST create" do
    it "creates sites" do
      authed_post :create, site: {name: FFaker::Company.name, url: FFaker::Internet.http_url}
      expect(response).to be_success
      expect(current_user.sites.count).to eq(2)
    end
  end

  describe "POST users" do
    it "errors if the site exceeds the user limit" do
      site = create :site_with_user, plan: create(:plan, max_users: 1)
      sign_in! site.users.first
      authed_post :users, email: "valid@gmail.com", id: site.id
      expect(response).not_to be_success
      expect(response.body).to match(/Upgrade your plan/)
    end

    it "errors unless the email is valid" do
      site = create :site_with_user
      sign_in! site.users.first
      authed_post :users, email: "sdfdfkjdslkfjfds", id: site.id
      expect(response).not_to be_success
      expect(response.body).to match(/Invalid email/)
    end

    it "creates invites for users that don't exist yet" do
      site = create :site_with_user
      sign_in! site.users.first
      expect(Invite.count).to be_zero
      authed_post :users, email: "yoyo@gogo.com", id: site.id
      expect(response).to be_success
      expect(Invite.count).to eq(1)
    end

    it "creates collaborators for users that exist" do
      site = create :site_with_user
      sign_in! site.users.first
      user = create :user
      expect(site.users.count).to eq(1)
      authed_post :users, email: user.email, id: site.id
      expect(response).to be_success
      expect(site.users.count).to eq(2)
    end
  end

  describe "POST update" do
    it "updates sites" do
      site = create :site, users: [current_user]
      name = FFaker::Company.name
      authed_put :update, id: site.id, site: {name: name}
      expect(response).to be_success
      site.reload
      expect(site.name).to eq(name)
    end
  end

  describe "GET show" do
    it "shows sites" do
      site = create :site, users: [current_user]
      authed_get :show, id: site.id
      expect(response).to be_success
      json = JSON.parse response.body
      expect(json["site"]["id"]).to eq(site.id)
    end
  end

  describe "GET index" do
    it "shows all sites for the current user" do
      expect(current_user.sites.count).to eq(1)
      current_user.sites.create attributes_for :site
      current_user.sites.create attributes_for :site
      expect(current_user.sites.count).to eq(3)
      authed_get :index
      expect(response).to be_success
      json = JSON.parse response.body
      expect(json["sites"].count).to eq(3)
    end
  end
end
