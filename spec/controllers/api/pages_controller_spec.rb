require "rails_helper"

describe Api::PagesController do
  describe "GET show" do
    it "shows pages" do
      page = create :page
      sign_in! page.site.users.first
      authed_get :show, site_id: page.site_id, id: page.id
      expect(response).to be_success
      json = JSON.parse response.body
      expect(json["page"]["id"]).to eq(page.id.to_s)
    end
  end

  describe "GET index" do
    it "shows all pages for the site" do
      p1 = create :page
      p2 = create :page, site_id: p1.site_id
      sign_in! p1.site.users.first
      authed_get :index, site_id: p1.site_id
      expect(response).to be_success
      json = JSON.parse response.body
      expect(json["pages"].count).to eq(2)
    end
  end

  describe "POST create" do
    it "creates pages" do
      site = current_user.sites.first
      path = FFaker::Internet.slug
      name = FFaker::Company.name
      authed_post :create, site_id: site.id, page: { path: path, name: name, current_version_attributes: { root_attributes: {values_attributes: [{}]} } }
      expect(response).to be_success
      expect(MongoPage.count).to eq(1)
      expect(MongoPage.first.path).to eq("/#{path}")
    end
  end

  describe "PUT update" do
    it "updates pages" do
      page = create :page
      sign_in! page.site.users.first
      path = FFaker::Internet.slug
      authed_put :update, site_id: page.site_id, id: page.id, page: { path: path, name: "foo", current_version_attributes: {root_attributes: {values_attributes: [{}]} } }
      expect(response).to be_success
      expect(MongoPage.first.path).to eq("/#{path}")
    end
  end

  describe "DELETE destroy" do
    it "destroys pages" do
      page = create :page
      sign_in! page.site.users.first
      authed_delete :destroy, site_id: page.site_id, id: page.id
      expect(response).to be_success
      expect(MongoPage.where(deleted_at: nil).count).to be_zero
    end
  end

  describe "POST publish" do
    it "publishes the page" do
      page = create :page
      sign_in! page.site.users.first
      page.site.collaborators.update_all(admin: true)
      expect(PageService).to receive(:publish!).with(page)
      authed_post :publish, site_id: page.site_id, id: page.id
      expect(response).to be_success
    end
  end

  describe "POST unpublish" do
    it "unpublishes the page" do
      page = create :published_page
      expect(UnpublishPageWorker).to receive(:perform_async).with(page._id.to_s)
      sign_in! page.site.users.first
      authed_post :unpublish, site_id: page.site_id, id: page.id
      expect(response).to be_success
      page.reload
      expect(page.published_version).to be_nil
    end
  end
end
