require "rails_helper"

describe Api::CollaboratorsController do
  describe "POST update" do
    it "updates collaborators" do
      s = current_user.sites.first
      current_user.collaborators.update_all(admin: true)
      u = create :user
      c = s.collaborators.create user_id: u.id
      expect(c).not_to be_admin
      authed_put :update, site_id: s.id, id: c.id, collaborator: {admin: true}
      expect(response).to be_success
      c.reload
      expect(c).to be_admin
    end
  end

  describe "DELETE destroy" do
    it "destroys collaborators" do
      s = current_user.sites.first
      current_user.collaborators.update_all(admin: true)
      u = create :user
      c = s.collaborators.create user_id: u.id
      expect(s.users).to include(u)
      authed_delete :destroy, site_id: s.id, id: c.id
      expect(response).to be_success
      s.reload
      expect(s.users).not_to include(u)
    end
  end

  describe "GET index" do
    it "shows all collaborators for the given site" do
      s = current_user.sites.first
      c1 = create :collaborator, site: s
      c2 = create :collaborator, site: s
      authed_get :index, site_id: s.id
      expect(response).to be_success
      json = JSON.parse response.body
      expect(json["collaborators"].count).to eq(3)
    end
  end
end
