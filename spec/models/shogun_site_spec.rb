require "spec_helper"

describe ShogunSite do
  let(:page) { double("Page", _id: "pid") }
  let(:page_version) { double("PageVersion", _id: "pvid") }
  let(:site) { create :site }

  describe '#publish_page_version!' do
    it "uploads the version and resets the api" do
      expect(PageVersionService).to receive(:upload!).with(page_version)
      expect(page).to receive(:update!).with(published_version_id: "pvid")
      expect(ResetApiWorker).to receive(:perform_async).with(site.id)
      site.publish_page_version! page, page_version
    end
  end

  describe '#unpublish_page!' do
    it "resets the api" do
      expect(ResetApiWorker).to receive(:perform_async).with(site.id)
      site.unpublish_page! page
    end
  end

  describe '#refresh_page_version!' do
    it "force uploads the page version" do
      expect(PageVersionService).to receive(:upload!).with(page_version, true)
      site.refresh_page_version! page, page_version
    end
  end

  describe '#delete_page!' do
    it "resets the api" do
      expect(ResetApiWorker).to receive(:perform_async).with(site.id)
      site.delete_page! page
    end
  end
end
