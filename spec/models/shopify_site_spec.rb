require "spec_helper"

describe ShopifySite do
  let(:site) { create :shopify_site }
  let(:service) { double("ShopifyService") }
  let(:page) { double("Page", meta_tags: [], name: "foo", path: "/foo", article?: false) }
  let(:page_version) { double("PageVersion", raw_html: "html", _id: "pvid") }
  let(:shopify_page_params) {
    {body_html: page_version.raw_html, metafields: [], title: page.name, handle: page.path[1..-1], published: true, template_suffix: "shogun"}
  }

  before do
    allow(site.shopify_shop).to receive(:service).and_return(service)
  end

  describe '#delete_page!' do
    it "deletes the page using the shopify service" do
      expect(page).to receive(:external_page_id).and_return("epid")
      expect(service).to receive(:delete_page).with("epid")
      site.delete_page! page
    end
  end

  describe '#publish_page_version!' do
    before do
      allow(page_version).to receive(:raw_html)
    end

    it "updates existing pages" do
      allow(page).to receive(:external_page_id).and_return("epid")
      expect(service).to receive(:update_page).with("epid", shopify_page_params)
      expect(page).to receive(:update!).with(published_version_id: page_version._id, updating: false, external_errors: [])
      site.publish_page_version! page, page_version
    end

    it "creates new pages" do
      allow(page).to receive(:external_page_id)
      expect(service).to receive(:create_page).with(shopify_page_params).and_return({"id" => "spid"})
      expect(page).to receive(:update!).with(published_version_id: page_version._id, updating: false, external_page_id: "spid", external_errors: [])
      site.publish_page_version! page, page_version
    end
  end

  describe '#unpublish_page!' do
    it "updates the shopify page to published:false" do
      allow(page).to receive(:external_page_id).and_return("epid")
      expect(service).to receive(:update_page).with("epid", {published: false})
      expect(page).to receive(:update!).with(updating: false, external_errors: [])
      site.unpublish_page! page
    end
  end

  describe '#refresh_page_version!' do
    it "does nothing if the published_version_id is not the page version id" do
      allow(page).to receive(:published_version_id).and_return("other")
      site.refresh_page_version! page, page_version
    end

    it "updates the body_html of the page when it is published" do
      allow(page).to receive(:external_page_id).and_return("epid")
      expect(page).to receive(:published_version_id).and_return(page_version._id)
      expect(service).to receive(:update_page).with("epid", {body_html: "html"})
      site.refresh_page_version! page, page_version
    end
  end
end
