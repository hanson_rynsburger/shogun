require 'sidekiq/web'

class ProbablyAuthenticated
  def matches?(request)
    request.cookie_jar.signed[:token].present?
  end
end

class CertainlyAnonymous
  def matches?(request)
    !request.cookie_jar.signed[:token].present?
  end
end

class DefinitelyAdmin
  def matches?(request)
    token = request.cookie_jar.signed[:token]
    return false unless token.present?

    user = User.where(token: token).take
    user.present? && user.admin?
  end
end

Rails.application.routes.draw do
  constraints ProbablyAuthenticated.new do
    constraints DefinitelyAdmin.new do
      mount Sidekiq::Web => '/sidekiq'
      get '/admin' => redirect('admin/pages')
      namespace :admin do
        resources :shops, only: [:index, :show] do
          collection do
            get :export
          end
          member do
            post :become
            post :retrial
          end
        end
        resources :discounts, only: [:create, :update]
        resources :pages, only: [:index]
        resources :sites, only: [:show, :index] do
          member do
            post :duplicate
          end
          resources :plans, only: [:create]
          resources :invoices, except: [:index, :show, :destroy]
        end
        resources :users, only: [:index]
      end
    end
    resources :embeds, only: [:show] do
      member do
        get :css
        get :js
      end
    end
    namespace :api do
      resources :sites, only: [:show, :index, :create, :update] do
        member do
          post :users
        end

        resources :invoices, only: [:index] do
          member do
            post :pay
          end
        end

        resources :collaborators, only: [:index, :update, :destroy]
        resources :invites, only: [:index, :destroy]
        resources :models, only: [:index, :create, :update] do
          resources :entries, only: [:create, :destroy, :index, :update]
        end
        resources :templates, only: [:show, :index, :create, :update, :destroy] do
          collection do
            post :parse
          end
        end
        resources :snippets, only: [:create, :destroy, :index]
        resources :pages, only: [:show, :index, :create, :update, :destroy] do
          member do
            post :publish
            post :unpublish
            post :switch_version
            get :version_history
          end
        end
        resources :shopify_pages, only: [:index] do
          member do
            post :import
          end
        end

        resources :shopify_articles, only: [:index] do
          get :blogs, on: :collection
          member do
            post :import
          end
        end
        resources :shopify_templates, only: [:index]
        resources :add_ons, only: [:index, :create, :update]
      end

      resource :user, only: [:show, :update]
    end

    resource :session, only: [:destroy]

    get '/editor/outer' => 'editor#outer'
    get '/editor/inner' => 'editor#inner'

    get '(sites/:id)' => 'app#app', as: :app
    get '(sites/:id)/pages' => 'app#app'
    get '(sites/:id)/templates' => 'app#app'
    get '(sites/:id)/collections' => 'app#app'
    get '(sites/:id)/settings' => 'app#app'
    get '(sites/:id)/settings/billing' => 'app#app'
    get '(sites/:id)/settings/fonts' => 'app#app'
    get '(sites/:id)/settings/users' => 'app#app'
    get '(sites/:id)/settings/integrate' => 'app#app'
    get '(sites/:id)/settings/add_ons' => 'app#app'
    get '(sites/:id)/settings/edit' => 'app#app'
    get '(sites/:id)/templates/new' => 'app#app'
    get '(sites/:id)/pages/new' => 'app#app'
    get '(sites/:id)/pages/layouts' => 'app#app'
    get '(sites/:id)/templates/:id/edit' => 'app#app'
    get '(sites/:id)/collections/:id/edit' => 'app#app'
    get '(sites/:id)/collections/new' => 'app#app'
    get '(sites/:id)/collections/:id/entries/new' => 'app#app'
    get '(sites/:id)/collections/:id/entries/:id/edit' => 'app#app'
    get '(sites/:id)/pages/:id/edit' => 'app#app'
    get '(sites/:id)/pages/:id/duplicate' => 'app#app'

    get 'blogs' => 'app#app'
    get 'blogs/:bid/layouts' => 'app#app'
    get 'blogs/:bid/new' => 'app#app'
    get 'blogs/:bid/article/:pid/edit' => 'app#app'

    get 'sites/new' => 'app#app'
    get 'settings' => 'app#app'
    get 'support' => 'app#app'
  end

  constraints CertainlyAnonymous.new do
    resources :users, only: [:create]
    resource :session, only: [:create]
  end

  resources :invoices, only: [:show] do
    member do
      post :pay
    end
  end

  post "/api/hooks/shopify" => 'api/hooks/shopify#receive', as: :receive_shopify_hook

  match 'auth/failure' => 'sessions#failure', via: [:get]
  match 'auth/shopify/callback' => 'sessions#shopify', via: [:get, :post]
  match 'auth/:provider/callback' => 'sessions#omniauth', via: [:get, :post]

  get 'spendless' => 'cookies#spendless'
  get 'sloyalty20' => 'cookies#sloyalty20'
  get 'guru20' => 'cookies#guru20'

  resources :previews, only: [:show]

  get 'sign_up' => 'users#sign_up', as: :sign_up
  get 'sign_in' => 'users#sign_in', as: :sign_in

  get '/producthunt' => redirect("/")
  get '/unauthenticated' => 'users#sign_in', as: :unauthenticated

  # Heroku add on routes
  # SSO sign in
  get '/heroku/resources/:id' => 'heroku#sso'
  post '/sso/login' => 'heroku#sso'

  # Provision
  post '/heroku/resources' =>  'heroku#provision'

  # Deprovision
  delete '/heroku/resources/:id' => 'heroku#deprovision'

  # Change plan
  put '/heroku/resources/:id' => 'heroku#change_plan'

  get '/logout' => 'sessions#logout'

  get '/users/sign_in' => redirect('/sign_in')

  namespace :shopify do
    get '/proxy/previews/:id' => 'proxy#previews'
    get '/proxy/probe' => 'proxy#probe'

    resources :plans, only: [:index]

    get '/editor/outer' => 'editor#outer'
    get '/editor/inner' => 'editor#inner'

    resources :recurring_charges, only: [:create] do
      collection do
        get :activate
      end
    end
  end

  constraints subdomain: 'www' do
    get ':any', to: redirect(subdomain: nil, path: '/%{any}'), any: /.*/
  end
end
