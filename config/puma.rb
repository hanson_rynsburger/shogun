workers Integer(ENV["PUMA_WORKERS"] || 2)
threads Integer(ENV["MIN_THREADS"] || 2), Integer(ENV["MAX_THREADS"] || 2)

preload_app!

rackup DefaultRackup
port ENV["PORT"] || 3000
environment ENV["RACK_ENV"] || "development"

on_worker_boot do
  config = Rails.application.config.database_configuration[Rails.env]
  # seconds
  config["reaping_frequency"] = Integer(ENV["DB_REAP_FREQ"] || 10)
  config["pool"] = Integer(ENV["WEB_DB_POOL"] || 5)

  # worker specific setup
  ActiveSupport.on_load(:active_record) do
    ActiveRecord::Base.establish_connection(config)
  end
end
