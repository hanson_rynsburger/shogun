Rails.application.config.middleware.use OmniAuth::Builder do
  provider :shopify, ENV["SHOPIFY_API_KEY"], ENV["SHOPIFY_SECRET"], :scope => "read_content, write_content, read_products, write_products, read_themes, write_themes, read_script_tags, write_script_tags, read_orders", :provider_ignores_state => true
  provider :developer unless Rails.env.production?
  provider :google_oauth2, ENV["GOOGLE_CLIENT_ID"], ENV["GOOGLE_CLIENT_SECRET"]
end
