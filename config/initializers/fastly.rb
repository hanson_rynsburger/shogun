FastlyRails.configure do |c|
  c.api_key = ENV['FASTLY_API_KEY']
  c.max_age = 30.days.to_i
  c.service_id = ENV['FASTLY_SERVICE_ID']
end
