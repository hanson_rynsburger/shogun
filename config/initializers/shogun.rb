Shogun.url = ENV["SHOGUN_URL"]
Shogun.preview_url = ENV["SHOGUN_PREVIEW_URL"]
Shogun.editor_url = ENV["SHOGUN_EDITOR_URL"]
Shogun.layout = "shogun"

Shogun::ApplicationController
