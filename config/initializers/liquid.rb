module ConversionFilter
  def string(input)
    input.to_s
  end

  def int(input)
    input.to_i
  end

  def float(input)
    input.to_f
  end
end

Liquid::Template.register_filter(ConversionFilter)
