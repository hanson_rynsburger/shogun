if Rails.env.production?
  Analytics = Segment::Analytics.new({
    write_key: ENV['SEGMENT_WRITE_KEY'],
    on_error: Proc.new { |status, msg| print msg }
  })
else
  Analytics = Segment::Analytics.new({
    write_key: "YHW9pyMIxMPGPeIVvqWDU3OTstZa8XGp",
    on_error: Proc.new { |status, msg| print msg }
  })
end
