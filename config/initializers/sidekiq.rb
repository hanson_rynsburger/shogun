if Rails.env.development?
  require 'sidekiq/testing'
  Sidekiq::Testing.inline!
end

Sidekiq.configure_server do |config|
  if defined?(ActiveRecord::Base)
    config = Rails.application.config.database_configuration[Rails.env]
    config['reaping_frequency'] = Integer(ENV['DB_REAP_FREQ'] || 10) # seconds
    config['pool'] = Integer(ENV["WORKER_DB_POOL"] || 25)
    ActiveRecord::Base.establish_connection(config)
  end
end
