module Rollbar
  module Delay
    class Sidekiq
      sidekiq_options :retry => false
    end
  end
end
