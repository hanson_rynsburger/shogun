require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups(assets: %w(development test)))

class TokenStrategy < ::Warden::Strategies::Base
  def current_user_token
    cookies.signed["token"]
  end

  def cookies
    env["action_dispatch.cookies"]
  end

  def valid?
    current_user_token.present?
  end

  def authenticate!
    user = User.where(token: current_user_token).take
    if user.nil? || (user.shopify? && !user.sites.first.shopify_shop.active?)
      fail!
    else
      success! user
    end
  end
end

class HTTPBasicAuth < Warden::Strategies::BasicAuth
  def authenticate_user(username, password)
    username == ENV['HEROKU_USERNAME'] && password == ENV['HEROKU_PASSWORD']
  end
end

Warden::Strategies.add(:token, TokenStrategy)
Warden::Strategies.add(:basic_auth, HTTPBasicAuth)

module ShogunWeb
  class Application < Rails::Application
    config.assets.paths << Rails.root.join("app", "assets", "fonts")
    config.assets.precompile += %w(
      email.css
      shogun.css
      shogun.js
      outer.js
      outer.css
      inner.js
      inner.css
      basic.css
      shopify_application.js
      admin.css
      admin.js
      toolkit-entypo.eot
      toolkit-entypo.ttf
      toolkit-entypo.woff
      toolkit-entypo.woff2
    )
    config.active_record.raise_in_transactional_callbacks = true

    Warden::Manager.serialize_into_session do |user|
      user.id
    end

    Warden::Manager.serialize_from_session do |id|
      User.find_by_id(id)
    end

    Warden::Manager.before_failure do |env, opts|
      if env["action_dispatch.cookies"].present?
        env["action_dispatch.cookies"].delete :token
      end
    end

    config.middleware.insert_after ActionDispatch::Flash, Warden::Manager do |manager|
      manager.default_strategies :token
      manager.failure_app = ShogunWeb::Application.routes
    end

    Rails.application.routes.default_url_options[:host] = ENV["DEFAULT_HOST"]
    config.action_mailer.default_url_options = { host: ENV["DEFAULT_HOST"] }

    config.generators do |g|
      g.orm :active_record
    end
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
  end
end
